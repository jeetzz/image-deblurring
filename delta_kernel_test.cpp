#include "mex.h"
#include <matrix.h>
#include <math.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	  
    double *d1 = mxGetPr(prhs[0]);
	 
	int s = (int)d1[0];

	mxArray* out = plhs[0] = mxCreateDoubleMatrix(s,s,mxREAL);
	double *out_d = mxGetPr(out);

	if (s%2 == 0)
		s++;

	for (int i = 0; i < s ; i++){
		for (int j = 0; j < s ; j ++){
			out_d[j+3*i] = 0;
		}
	}

	int c = floor((float)(s/2));

	out_d[c+s*c]  = 1;
}