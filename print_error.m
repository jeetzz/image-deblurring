function print_error(ensemble,grad,T)
%     disp(T);
%   size(T,1)
try
    eval(T);

%% Checking ensemble Mat values

    e1 = mean(abs(ensemble_x1 - ensemble.x1));
	e2 = mean(abs(ensemble_x2 - ensemble.x2));
	e3 = mean(abs(ensemble_mx - ensemble.mx));
	e4 = mean(abs(ensemble_mx2 - ensemble.mx2));
	e5 = mean(abs(ensemble_log_lambda_x - ensemble.log_lambda_x));
	e6 = mean(abs(ensemble_pi_x_2 - ensemble.pi_x_2));
	e7 = mean(abs(ensemble_opt_pi_x_2  - ensemble.opt_pi_x_2)) ;
	e8 = mean(abs(ensemble_ba_x_2	 - ensemble.ba_x_2));	
    e9 = mean(abs(ensemble_opt_ba_x_2  - ensemble.opt_ba_x_2));
	e10 = mean(abs(ensemble_b_x_2     - ensemble.b_x_2)) ;
	e11 = mean(abs(ensemble_opt_b_x_2  - ensemble.opt_b_x_2));
	e12 = mean(abs(ensemble_D_x'     - ensemble.D_x));

%     
    
    
%% Checking ensemble double values   
    e_1 = ensemble.pi_x - pi_x;
    e_2 = ensemble.a_x - a_x;
    e_3 = ensemble.b_x - b_x;
    e_4 = ensemble.a_sigma - a_sigma;
    e_5 = ensemble.ba_sigma_2 - ba_sigma_2;
    e_6 = ensemble.opt_ba_sigma_2 -  opt_ba_sigma_2;
    e_7 = ensemble.b_sigma - b_sigma;
    e_8 = ensemble.b_sigma_2 - b_sigma_2;
    e_9 = ensemble.D_val - D_val;

%     
%%    Checking grad    
    e_x1 = grad_x1 - grad.x1;
    e_x2 = grad_x2 - grad.x2;
    e_pi_x_2 = grad_pi_x_2 - grad.pi_x_2;
    e_ba_x_2 = grad_ba_x_2 - grad.ba_x_2;
    e_b_x_2  = grad_b_x_2  - grad.b_x_2;
    
    a = mean(abs(e_x1));
    b = mean(abs(e_x2));
    c = mean(abs(e_pi_x_2));
    d = mean(abs(e_ba_x_2));
    e = mean(abs(e_b_x_2));

    
%%
    % ensemble array values
    if (e1 > 0.0001 || e2 > 0.0001 || e3 > 0.0001 || e4 > 0.0001 ||e5 > 0.0001 || e6 > 0.0001 ||e7 > 0.0001 || e8 > 0.0001 ||e9 > 0.0001 || e10 > 0.0001 ||e11 > 0.0001 || e12 > 0.0001 )
        fprintf('\nx1 %f\nx2 %f\nmx %f\nmx2 %f\nlog_lambda_x %f\npi_x_2 %f\nopt_pi_x_2 %f\nba_x_2 %f\nopt_ba_x_2 %f\nb_x_2 %f\nopt_b_x_2 %f\nD_x %f\n ',e1,e2,e3,e4,e5,e6,e7,e8,e9,e10,e11,e12);
        pause
    end
    
    % ensemble non-array values
    if (e_1 > 0.0001 || e_2 > 0.0001 || e_3 > 0.0001 || e_4 > 0.0001 || e_5 > 0.0001 || e_6 > 0.0001 || e_7 > 0.0001 || e_8 > 0.0001 || e_9 > 0.0001 )
        fprintf('pi_x %f\na_x %f\nb_x %f\na_sigma %f\nba_simga_2 %f\nopt_ba_sigma_2 %f\nb_sigma %f\nb_sigma_2 %f\nD_val %f\n ',e_1,e_2,e_3,e_4,e_5,e_6,e_7,e_8,e_9);
        pause
    end
    
    % grad values
    if (a > 0.0001 || b > 0.0001 ||c > 0.0001 ||d > 0.0001 ||e > 0.0001 )
        fprintf('\n%.10f \n%.10f \n%.10f \n%.10f \n%.10f\n',a,b,c,d,e);
        pause
    end
    
%     
catch e
    disp(e);
    disp('error in T');
end
end