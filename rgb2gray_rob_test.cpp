#include <mex.h> 
#include "MxArray.hpp"
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <matrix.h>
#include <math.h>

using namespace std;
using namespace cv;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	MxArray m(prhs[0]);
	Mat img = m.toMat(CV_8U,true);
	int dims[3];
	dims[0] = img.rows;
	dims[1] = img.cols;

// 	mexPrintf("double %f",img.at<Vec3b>(0,0)[0]);
// 	mexPrintf("unit %u",img.at<Vec3b>(0,0)[0]);
// 	mexPrintf("int %d",img.at<Vec3b>(0,0)[0]);

	const int SATURATION_LEVEL = 250;
	if(nrhs ==0)
		mexErrMsgTxt("Need input arguments.");

	Mat out_img(dims[0], dims[1], CV_8U);
	
	const double T[3] = {0.298936021293776,0.587043074451121,0.114020904255103};

	for(int i=0;i<dims[1];i++) //parallelizable for loops
	{
		for(int j=0;j<dims[0];j++)
		{
			if(img.at<Vec3b>(j,i)[0] > SATURATION_LEVEL || 
					img.at<Vec3b>(j,i)[1] > SATURATION_LEVEL ||
					img.at<Vec3b>(j,i)[2] > SATURATION_LEVEL){
						out_img.at<UINT8>(j,i) = 255;
					}
			else{
				out_img.at<UINT8>(j,i) = (UINT8) floor(T[0]*(double)img.at<Vec3b>(j,i)[0] + T[1]*(double)img.at<Vec3b>(j,i)[1] + T[2]*(double)img.at<Vec3b>(j,i)[2] + 0.5);
			}
		}
	}
	plhs[0] = MxArray(out_img);
}