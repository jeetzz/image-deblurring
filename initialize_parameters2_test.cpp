#include "deblur_include_file.h"
#include <cstringt.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
 
	// 0 obs, 1 blur, 2 im, 3 true_blur, 4 true_im, 5 pres,
	// 6 prior, 7 prior_num, 8 mode_im, 9 mode_blur, 10 obs_im, 
	// 11 big_blur, 12 spatial_mask, 13 priors, 14 FFT_MODE, 15 COLOR, 16 nLayers


    // obs is an M x N matrix. It contains two grad_x and grad_y concatenated to a single matrix ( two birds )
    mxArray *mx_obs = mxDuplicateArray(prhs[0]);  
	double* obs = mxGetPr(mx_obs);
	int obs_M = mxGetM(mx_obs);
	int obs_N = mxGetN(mx_obs);

	// Blur kernel. M x N matrix. Begins from 3 x 3 and then 5 x 5 and like that size increases...
	mxArray *mx_blur = mxDuplicateArray(prhs[1]);  
	double* blur = mxGetPr(mx_blur);
	int blur_M = mxGetM(mx_blur);
	int blur_N = mxGetN(mx_blur);

	// im is also an M x N matrix that contains two gradient matrices. grad_x and grad_y concatenated to a single matrix ( two birds ) 
	// find the true definitions <-------------
	mxArray *mx_im = mxDuplicateArray(prhs[2]);  
	double* im = mxGetPr(mx_im);
	int im_M = mxGetM(mx_im);
	int im_N = mxGetN(mx_im);

	// For the moment it's an empty matrix
	mxArray *mx_true_blur = mxDuplicateArray(prhs[3]);  
	double* true_blur = mxGetPr(mx_true_blur);
	int true_blur_M = mxGetM(mx_true_blur);
	int true_blur_N = mxGetN(mx_true_blur);

	// For the moment it's an empty matrix
	mxArray *mx_true_im = mxDuplicateArray(prhs[4]);  
	double* true_im = mxGetPr(mx_true_im);
	int true_im_M = mxGetM(mx_true_im);
	int true_im_N = mxGetN(mx_true_im);

	//  
	mxArray *mx_pres = mxDuplicateArray(prhs[5]);  
	double* pres = mxGetPr(mx_pres);
	int pres_M = mxGetM(mx_pres);
	int pres_N = mxGetN(mx_pres);

	//   
	mxArray *mx_prior = mxDuplicateArray(prhs[6]);  
	double* prior = mxGetPr(mx_prior);
	int prior_M = mxGetM(mx_prior);
	int prior_N = mxGetN(mx_prior);

	//  
	mxArray *mx_prior_num = mxDuplicateArray(prhs[7]);  
	double* prior_num = mxGetPr(mx_prior_num);
	int prior_num_M = mxGetM(mx_prior_num);
	int prior_num_N = mxGetN(mx_prior_num);

	// mode_im is a string 
	mxArray *mx_mode_im = mxDuplicateArray(prhs[8]);  
	size_t buflen = 30; 
	char *mode_im =(char *)mxMalloc(buflen);	
	buflen = mxGetN(mx_mode_im)*sizeof(mxChar)+1;	 
	mxGetString(mx_mode_im, mode_im, (mwSize)buflen);    // mode_blur char* is assigned with the input string

	//  mode_blur is a string 
	mxArray *mx_mode_blur = mxDuplicateArray(prhs[9]);  
	char *mode_blur =(char *)mxMalloc(buflen);	
	buflen = mxGetN(mx_mode_blur)*sizeof(mxChar)+1;	 
	mxGetString(mx_mode_blur, mode_blur, (mwSize)buflen);    // mode_blur char* is assigned with the input string

	//  
	mxArray *mx_obs_im = mxDuplicateArray(prhs[10]);  
	double* obs_im = mxGetPr(mx_obs_im);
	int obs_im_M = mxGetM(mx_obs_im);
	int obs_im_N = mxGetN(mx_obs_im);

	//  
	mxArray *mx_big_blur = mxDuplicateArray(prhs[11]);  
	double* big_blur = mxGetPr(mx_big_blur);
	int big_blur_M = mxGetM(mx_big_blur);
	int big_blur_N = mxGetN(mx_big_blur);

	// 
	mxArray *mx_spatial_mask = mxDuplicateArray(prhs[12]);  
	double* spatial_mask = mxGetPr(mx_spatial_mask);
	int spatial_mask_M = mxGetM(mx_spatial_mask);
	int spatial_mask_N = mxGetN(mx_spatial_mask);

	// 
	mxArray *mx_priors = mxDuplicateArray(prhs[13]);  
	double* priors = mxGetPr(mx_priors);
	int priors_M = mxGetM(mx_priors);
	int priors_N = mxGetN(mx_priors);

	//  
	mxArray *mx_FFT_MODE = mxDuplicateArray(prhs[14]);  
	double* FFT_MODE_ = mxGetPr(mx_FFT_MODE);
	int FFT_MODE  = (int)FFT_MODE_[0];
	
	// 
	mxArray *mx_COLOR = mxDuplicateArray(prhs[15]);  
	double* COLOR_ = mxGetPr(mx_COLOR);
	int COLOR = (int)(*COLOR_);
	 

	// 
	mxArray *mx_nLayers = mxDuplicateArray(prhs[16]);  
	double* nLayers = mxGetPr(mx_nLayers);
	int nLayers_M = mxGetM(mx_nLayers);
	int nLayers_N = mxGetN(mx_nLayers);
	
	// Begins the task
	
	
	 
	float ** me2;

	if (strcmp(mode_blur,"direct") == 0 )
		mexPrintf("1");
		//me2 = blur;
	else if ( strcmp(mode_blur,"true") == 0 )
		mexPrintf("2");
		//me2 = true_blur;
	else if (strcmp(mode_blur,"updown") == 0 )
		mexPrintf("3");
		//[K,L] = size(true_blur);
	    //me2 = imresize(imresize(true_blur,0.5,'bilinear'),[K L],'bilinear');
	else if (strcmp(mode_blur,"delta") == 0)
		mexPrintf("4");
		//[K,L] = size(true_blur);
	    //me2 = delta_kernel(K);
	else if (strcmp(mode_blur,"hbar") == 0)
	{
		
		int K = true_blur_M;
		int L = true_blur_N;

		int hK = floor(((float)K)/2);   
		int hL = floor(((float)L)/2); 

		me2 = new float*[K];    // L are cols , K are rows . This is actually a 'array of pointers to arrays'

		// Initialize it to zeros
		for (int i = 0;i<K;i++)		
			me2[i] = new float[L]();   // () initializes the arrays to zero
		
		// Set the horizontal elements to 1
		me2[hK][hL] = 1.0;
		me2[hK][hL-1] = 1.0;   // Alternate command : *(me2[hK]+hL-1) = 1;
		me2[hK][hL+1] = 1.0;

		// Print
		//for (int i = 0;i<K;i++)			 
		//	mexPrintf("%f %f %f \n",me2[i][0],me2[i][1],me2[i][2]);
		

		// Alternative solution for 2D arrays
		// int *ary = new int[sizeX * sizeY];
		// ary[y*sizeX + x]  // <- accessing elements like this
		 
	}
	else if (strcmp(mode_blur,"vbar") == 0)
	{

		
		int K = true_blur_M;
		int L = true_blur_N;

		int hK = floor(((float)K)/2);   
		int hL = floor(((float)L)/2); 

		me2 = new float*[K];    // L are cols , K are rows . This is actually a 'array of pointers to arrays'

		// Initialize it to zeros
		for (int i = 0;i<K;i++)		
			me2[i] = new float[L]();   // () initializes the arrays to zero

		// Set the horizontal elements to 1
		me2[hK-1][hL] = 1.0;
		me2[hK][hL] = 1.0;
		me2[hK+1][hL] = 1.0;

		// Print
		//for (int i = 0;i<K;i++)			 
		//	mexPrintf("%d %d %d \n",me2[i][0],me2[i][1],me2[i][2]);
		
	}
	else if (strcmp(mode_blur,"star") == 0)
		mexPrintf("7");
		//[K,L] = size(true_blur);
		//hK = floor(K/2)+1;
		//hL = floor(L/2)+1;
		//me2 = zeros(K,L);
		//me2(hK-1,hL+1) = 1;
		//me2(hK-1,hL-1) = 1;
		//me2(hK+1,hL-1) = 1;
		//me2(hK+1,hL+1) = 1;


	else if (strcmp(mode_blur,"random") == 0)
		mexPrintf("8");
		//[M,N] = size(true_blur);
	    //me2 = rand(M,N);
	else if (strcmp(mode_blur,"variational") == 0)
		mexPrintf("9");
		//me2 = blur;
	 
	else
	    mexPrintf("error");
	
	int C = 0;
	if (COLOR == 1)
		C = 3;
	else
		C = 1;   // s = 1 , 2 - 9 <-----------------
    
	
	// spatial_mask = spatial_mask(:);   //  Do not need it in here because when passed to mex files, it automatically 
	                                     //  get converted to the column vector with elements in a column being the adjacent ones. 
	                                     //  anyway, once we have the full implmentation this MUST BE IMPLEMENTED *********************
	
	int mx2;
	int dimensions[3][6] = {{1,1,1,0,0,1},{1,-1,4,1,0,0},{-1,-1,4,0,1,1}};
	                     //{{1,1,1,0,0,1},{1,K*L,4,1,0,0},{C,M*N,4,0,1,1}};
	 
	if (strcmp(mode_im,"direct") == 0)  // s = 2-9 <-----------------
		mexPrintf("----1");
	else if (strcmp(mode_im,"true")== 0)
		mexPrintf("2");
	//  mx2 = true_im;
	else if (strcmp(mode_im,"slight_blur_obs")== 0)
		mexPrintf("3");
	//	[M,N] = size(obs);
	//  f = [1 2 1; 2 4 2; 1 2 1]/16;

	//  for c=1:C
	//	  mx2(:,:,c) = real(ifft2(fft2(f,M,N).*fft2(obs(:,:,c),M,N)));
	//  end

	else if (strcmp(mode_im,"lucy")== 0)
		mexPrintf("4");
	//	obs2 = edgetaper(obs_im,big_blur);
	//  im_tmp = deconvlucy(obs2,big_blur);

	//  for c=1:C
	//	  im_x(:,:,c) = conv2(im_tmp(:,:,c),[1 -1],'valid');
	//  im_y(:,:,c) = conv2(im_tmp(:,:,c),[1 -1]','valid');
	//	  end

	//	[M,N] = size(obs);

	//  im_xs = imresize(im_x,[M N/2]+2,'bilinear');
	//  im_ys = imresize(im_y,[M N/2]+2,'bilinear');

	//  mx2 = zeros(M,N,C);
	//  mx2(1:M,1:N/2,:) = im_xs(2:end-1,2:end-1,:);
	//  mx2(1:M,N/2+1:N,:) = im_ys(2:end-1,2:end-1,:);

	else if (strcmp(mode_im,"reg")== 0)
		mexPrintf("5");
	//	obs2 = edgetaper(obs,blur);
	//  mx2 = deconvreg(obs2,blur);
	else if (strcmp(mode_im,"lucy_true")== 0)
		mexPrintf("6");
	//	obs2 = edgetaper(obs,true_blur);
	//  mx2 = deconvlucy(obs2,true_blur,20);
	else if (strcmp(mode_im,"reg_true")== 0)
		mexPrintf("7");
	//	obs2 = edgetaper(obs,true_blur);
	//  mx2 = deconvreg(obs2,true_blur);
	else if (strcmp(mode_im,"updown")== 0)
		mexPrintf("8");
	//	[M,N] = size(true_im);
	//  mx2 = imresize(imresize(true_im,0.5,'bilinear'),[M N],'bilinear');
	else if (strcmp(mode_im,"random")== 0)
	{
		mexPrintf("9");
	//	%%% generate image using laplacian distribution of 
	//	SCALE_PARAMETER = [7 6 4];
	//  [M,N] = size(im);

	//  tmp1 = rand(M,N,C);
	//  tmp2 = rand(M,N,C);

	//  mx2 = SCALE_PARAMETER(3) * (-log(tmp1) + log(tmp2));
	}
	else if (strcmp(mode_im,"variational")== 0)  // < s = 1----------------
	{		
	//	% ************ BEGIN OF VARIATIONAL ********************
	    int	MAX_ITERATIONS = 5000;
		int M = im_M; 		int N = im_N;
		int K = blur_M;     int L = blur_N;  // Using K,L for me2 size seems to work. But need more inspection !!!

		dimensions[1][1] = K*L; 
		dimensions[2][0] = C;
		dimensions[2][1] = M*N;

		float sum = 0;
		for (int i = 0 ; i<K ; i++)			 
			for (int j = 0; j<L ; j++)
				sum += me2[i][j];

		// Initialize the array
		float ** norm_blur = new float*[K];    
		for (int i = 0;i<K;i++)		
			norm_blur[i] = new float[L];   
		
		// Assign the values to the norm_blur array
		for (int i = 0 ; i<K ; i++){		 
			for (int j = 0; j<L ; j++){
				norm_blur[i][j] = (float)me2[i][j]/sum;
				//mexPrintf("%f ", norm_blur[i][j]);
			}
		}
	 

		//  n.b. for SIGGRAPH only 2 blur components were used.
		//	set dimensions(2,6)=1 to get back to SSG & IMA runs

		
		//Print the 'dimensions' matrix
		//	for (int j = 0; j < 3 ; j++)
		//		for (int i = 0 ; i < 6 ; i++)
		//			mexPrintf("%d ",dimensions[j][i]);

		float ** Dpf;
		float ** Df;

		if (FFT_MODE == 1 )  // s = 1<---------------
		{
			int	I = M*2; int J = N*2;  
			
			// Initialize the array  'Dpf'
			Dpf = new float*[I];    
			for (int i = 0;i<I;i++)		
				Dpf[i] = new float[J];   

			// Assign the values to 'Dpf'
			// mexPrintf("Dpf \n");
			for (int i = 0 ; i<I ; i++){
				for (int j = 0; j<J; j++){
					if ((i>= K-1 && i <= M-1 && j >= L-1 && j <= N/2 -1 ) || (i >= K-1 && i <= M-1 && j >= (L+N/2)-1 && j <= N-1 ))
					   Dpf[i][j] = 1;
					else 
                       Dpf[i][j] = 0;
					//mexPrintf("%f,", Dpf[i][j]);
				}
				//mexPrintf("\n ");
			}
 
			Df = new float*[2*M];    
			for (int i = 0;i<2*M;i++)		
				Df[i] = new float[2*N]();   

			//mexPrintf("size : %d  %d",obs_M,obs_N);
			for (int i = 0; i < obs_M ; i++){
				for (int j = 0 ; j < obs_N ; j ++){
					Df[i][j] = (float)obs[obs_M*j + i];   // columns in the matlab matrix will be stored adjacently in the double array in c++
				}
			}
			//mexPrintf("M %d, N %d, obs_M %d, obs_N %d\n",M,N,obs_M,obs_N);
			//for (int i = 0; i < 2*M; i++){
			//    for (int j = 0 ; j < 2*N ; j ++){
			//        mexPrintf("%f,",Df[i][j]);				
			//    }
			//    mexPrintf(";\n");
			//}

			
		}
		else
		{
		//	I = M; J = N;
		//  hK = floor(K/2); hL = floor(L/2);

		//  Dpf = zeros(I,J,C);
		//  Dpf(hK+1:M-hK,hL+1:N/2-hL,:) = 1;
		//  Dpf(hK+1:M-hK,N/2+hL+1:N-hL,:) = 1;

		//  shift_kernel = zeros(K,L);
		//  shift_kernel(1,1)=1;

		//  for c=1:C
		//	  Df(:,:,c) = conv2(obs(:,:,c),shift_kernel,'same');
		//  end
		}

	
		float *pres_vector   = new float[blur_M*blur_N + im_M*im_N + 1];
		
		// mexPrintf("press : %f , size %d",pres[0],blur_M*blur_N + im_M*im_N + 1);
		for (int i = 0; i < blur_M*blur_N + im_M*im_N + 1; i++ )
			pres_vector[i] = (float)pres[0];

		//  Two below lines not required for this case, because q is an empty vector for this configuration. Thus nothing happens to pres_vector
	    //  q=find(spatial_mask(:));
	    //  pres_vector(q+1+length(norm_blur(:))) = spatial_mask(q)';

		mexPrintf("print %d  %d",dimensions[1][2],blur_M*blur_N);
		float **dummy_blur_mask = new float*[dimensions[1][2]];    
		for (int i = 0;i<dimensions[1][2];i++)		
			dummy_blur_mask[i] = new float[blur_M*blur_N]();   
	    
	//  %%% Make vectors
		float *xx1 = new float[blur_M*blur_N + im_M*im_N + 1];

	//	xx1 = [0 norm_blur(:)' im(:)'] .* pres_vector;  
	//  xx2 = pres_vector;


	//  [ensemble,D_log,gamma_log]=train_ensemble_main6(dimensions,xx1,xx2,'','',[1e-4 0 1 0 0 MAX_ITERATIONS 0],Df,Dpf,I,J,K,L,M,N,priors,FFT_MODE,dummy_blur_mask,[1-(spatial_mask>0)]);    

	//  mx2 = reshape(train_ensemble_get(3,dimensions,ensemble.mx),M,N,C);

	//  % ************ END OF VARIATIONAL ********************
	}
	//	elseif strcmp(mode_im,'greenspan')  

	//	%%% Use default parameters
	//	s = create_greenspan_settings;
	//s.factor = 0;

	//[en,lo] = greenspan(obs,s);
	//mx2 = en;
	//%%% get size of estimated image at current scale
	//	%[M,N] = size(im);

	//%%% downsample obs_im (must work in intensity space)
	//	%obs_small = imresize(obs_im,([M N/2]/2)+1,'bilinear');

	//%%% use greenspan
	//	%[en,lo] = greenspan(obs_small,s);

	//%%% Now go back to gradient space
	//	%x_plane = conv2(en,[1 -1],'valid');
	//%y_plane = conv2(en,[1 -1]','valid');

	//	%mx2 = [x_plane(1:M,1:N/2), y_plane(1:M,1:N/2)];

	//else
	//	error foo
	//	end


		
    mexPrintf("\n");
	//mxArray* out = plhs[0] = mxCreateDoubleMatrix(s,s,mxREAL);
	//double *out_d = mxGetPr(out);
  
}