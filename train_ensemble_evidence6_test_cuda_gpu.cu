#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>

#include "train_ensemble_evidence6_test_kernels.cu"

#define DEBUG_LEVEL 1

void train_ensemble_evidence6_test_cuda_gpu(Ensemble* t_ensemble, Ensemble ensemble,Direction* grad,double step_len,Mat dimensions,char* opt_func,Direction direction,int state,Mat P1,Mat P2,int P3,int P4,int P5,int P6,int P7,int P8,Priors P9,int P10,Mat P11,Mat P12)
{
     
	 
	//tic();
/************************************************************************/
/* GPU KERNEL CALL DEMO                                                 */
/************************************************************************/   
// Here we call 'call_kernel' in demo_kernel.cu file - it will run on gpu
//  call_kernel();
/************************************************************************/
	t_ensemble->a_sigma         = ensemble.a_sigma;
	t_ensemble->a_x             = ensemble.a_x;
	t_ensemble->b_sigma         = ensemble.b_sigma;
	t_ensemble->b_sigma_2       = ensemble.b_sigma_2;
	t_ensemble->b_x             = ensemble.b_x;
	t_ensemble->b_x_2           = ensemble.b_x_2.clone();
	t_ensemble->ba_sigma_2      = ensemble.ba_sigma_2;
	t_ensemble->ba_x_2          = ensemble.ba_x_2.clone();
	t_ensemble->D_val           = ensemble.D_val;
	t_ensemble->D_x             = ensemble.D_x.clone();
	t_ensemble->log_lambda_x    = ensemble.log_lambda_x.clone();
	t_ensemble->mx              = ensemble.mx.clone();
	t_ensemble->mx2             = ensemble.mx2.clone();
	t_ensemble->opt_b_x_2       = ensemble.opt_b_x_2.clone();
	t_ensemble->opt_ba_sigma_2  = ensemble.opt_ba_sigma_2;
	t_ensemble->opt_ba_x_2      = ensemble.opt_ba_x_2.clone();
	t_ensemble->opt_pi_x_2      = ensemble.opt_pi_x_2.clone();
	t_ensemble->pi_x            = ensemble.pi_x;
	t_ensemble->pi_x_2          = ensemble.pi_x_2.clone();
	t_ensemble->x1              = ensemble.x1.clone();
	t_ensemble->x2              = ensemble.x2.clone();

	
	//%Step to the test point
	t_ensemble->x1 = t_ensemble->x1   + step_len*direction.x1;
	t_ensemble->x2  =abs(t_ensemble->x2    +step_len*direction.x2);
	t_ensemble->b_x_2 =abs(t_ensemble->b_x_2 +step_len*direction.b_x_2);
	t_ensemble->ba_x_2=abs(t_ensemble->ba_x_2 +step_len*direction.ba_x_2);
	t_ensemble->pi_x_2=abs(t_ensemble->pi_x_2 +step_len*direction.pi_x_2);

	//%Make a copy of the directions
	grad->b_x_2= direction.b_x_2;
	grad->ba_x_2= direction.ba_x_2;
	grad->pi_x_2= direction.pi_x_2;
	grad->x1= direction.x1;
	grad->x2 = direction.x2;

	//%Check for valid t_ensemble
	//Geethan :parallelise the following
	



	
	
	//	%Find evidence by summing over the components
	int ptr = 0;
	
	int total_length =  dimensions.at<int>(0,1) + dimensions.at<int>(1,1) + dimensions.at<int>(2,1);
	int comp_length =  dimensions.at<int>(0,2) + dimensions.at<int>(1,2) + dimensions.at<int>(2,2);


#if DEBUG_LEVEL > 4
	mexPrintf("grad->x1 at start");
	for(int i=0; i< total_length; i++){
		mexPrintf("%f ",grad->x1.at<double>(0,i));
		
	}
	mexPrintf("\ngrad->x2");
	for(int i=0; i< total_length; i++){
		mexPrintf("%f ",grad->x2.at<double>(0,i));
		
	}

	mexPrintf("\ngrad->b_x_2");
	for(int i=0; i< comp_length; i++){
		mexPrintf("%f ",grad->b_x_2.at<double>(0,i));
		
	}

	mexPrintf("\ngrad->ba_x_2");
	for(int i=0; i< comp_length; i++){
		mexPrintf("%f ",grad->ba_x_2.at<double>(0,i));
		
	}	
	
	mexPrintf("\ngrad->pi_x_2");
	for(int i=0; i< comp_length; i++){
		mexPrintf("%f ",grad->pi_x_2.at<double>(0,i));
		
	}
	mexPrintf("\n");
#endif	
	
	// float *h_t_ensemble_x1 = new float[total_length];
	// float *h_t_ensemble_x2 = new float[total_length];
	
	float *h_c_pi_x_2 = new float[comp_length];
	float *h_c_b_x_2 = new float[comp_length];
	float *h_c_ba_x_2 = new float[comp_length];
	
	double  * d_cmx_dbl; 
	double  * d_cmx2_dbl; 
	
	cudaMalloc((void**)&d_cmx_dbl  , sizeof(double)*total_length); 
	cudaMalloc((void**)&d_cmx2_dbl , sizeof(double)*total_length); 	
	cudaMemcpy(d_cmx_dbl, (double*)(t_ensemble->x1.data),sizeof(double)*total_length, cudaMemcpyHostToDevice); 
	cudaMemcpy(d_cmx2_dbl, (double*)(t_ensemble->x2.data), sizeof(double)*total_length, cudaMemcpyHostToDevice); 

	for(int i=0; i < comp_length; i++){
		h_c_pi_x_2[i] = (float)(t_ensemble->pi_x_2).at<double>(0,i);
		h_c_b_x_2[i]  = (float)(t_ensemble->b_x_2).at<double>(0,i);	
		h_c_ba_x_2[i] = (float)(t_ensemble->ba_x_2).at<double>(0,i);
	}
	float  * d_cx1; 
	float  * d_cx2; 
	
	float *d_c_pi_x_2 ;
	float *d_c_b_x_2  ;
	float *d_c_ba_x_2 ;
	
	
#if DEBUG_LEVEL > 4	

	mexPrintf("\n h_c_pi_x_2 start\n");
	for (int r = 0;r < comp_length; r++)
	{
		mexPrintf("%f ",(float)h_c_pi_x_2[r]);
	}
	mexPrintf("\n");
#endif		
	
	
	cudaMalloc((void**)&d_cx1 , sizeof(float)*total_length); //cudaMemcpy(d_cx1, h_t_ensemble_x1, sizeof(float)*total_length, cudaMemcpyHostToDevice); 
	cudaMalloc((void**)&d_cx2 , sizeof(float)*total_length); //cudaMemcpy(d_cx2, h_t_ensemble_x2, sizeof(float)*total_length, cudaMemcpyHostToDevice); 	
 
	cudaMalloc((void**)&d_c_pi_x_2 , sizeof(float)*comp_length); cudaMemcpy(d_c_pi_x_2, h_c_pi_x_2, sizeof(float)*comp_length, cudaMemcpyHostToDevice); 
	cudaMalloc((void**)&d_c_b_x_2  , sizeof(float)*comp_length); cudaMemcpy(d_c_b_x_2, h_c_b_x_2, sizeof(float)*comp_length, cudaMemcpyHostToDevice); 
	cudaMalloc((void**)&d_c_ba_x_2 , sizeof(float)*comp_length); cudaMemcpy(d_c_ba_x_2, h_c_ba_x_2, sizeof(float)*comp_length, cudaMemcpyHostToDevice); 
	
	
	float  * d_cHx_r; 
	float  * d_cHx_c; 
	float  * d_cmx; 
	
	float  * d_cmx2; 
	
	float  * d_c_log_lambda_x;
	// float  * d_max_c_log_lambda_x;    
	// float  * d_log_sum_lambda_x; 
	
	
	int width;
	if (dimensions.at<int>(1,2) > dimensions.at<int>(2,2))
		width = dimensions.at<int>(1,2);
	else 
		width = dimensions.at<int>(2,2);	

	
	cudaMalloc((void**)&d_cHx_r  , sizeof(float2)*total_length); 
	cudaMalloc((void**)&d_cHx_c  , sizeof(float2)*total_length); 
	cudaMalloc((void**)&d_cmx  , sizeof(float)*total_length); 
	cudaMalloc((void**)&d_cmx2 , sizeof(float)*total_length); 
	cudaMalloc((void**)&d_c_log_lambda_x   , sizeof(float)*total_length*width);  
	// cudaMalloc((void**)&d_max_c_log_lambda_x , sizeof(float)*total_length);  
	// cudaMalloc((void**)&d_log_sum_lambda_x 	, sizeof(float)*total_length);   
	
	
	// Estimating the parameters
	dim3 blockDim(512); 	dim3 gridDim((total_length + blockDim.x - 1) / blockDim.x);	
	 
 
	// Find a way to pass the array. this is stupid !!! *********************
	int b1,b2,b3,d1,d2,d3,c1,c2,c3,f1,f2,f3;
	b1  = dimensions.at<int>(0,1); 		b2  =  dimensions.at<int>(1,1);		b3  = dimensions.at<int>(2,1);
	c1 = dimensions.at<int>(0,2); 		c2 =  dimensions.at<int>(1,2);		c3 = dimensions.at<int>(2,2);
	d1 = dimensions.at<int>(0,3);	d2 = dimensions.at<int>(1,3); 	d3 = dimensions.at<int>(2,3); 
	f1 = dimensions.at<int>(0,5);	f2 = dimensions.at<int>(1,5); 	f3 = dimensions.at<int>(2,5); 
	 
	
	int total_comp_length = b1*c1 + b2*c2 + b3*c3;

#if DEBUG_LEVEL > 4	

	thrust::device_ptr<float> t_d_c_pi_x_2(d_c_pi_x_2);
	mexPrintf("\n ensemble->d_c_pi_x_2 in device \n");
	for (int r = 0;r < comp_length; r++)
	{
		mexPrintf("%f ",(float)t_d_c_pi_x_2[r]);
	}
	mexPrintf("\n");
#endif		


#if DEBUG_LEVEL > 4	

	thrust::device_ptr<float> t_d_cx2(d_cx2);
	mexPrintf("\n ensemble->d_cx2 in device \n");
	for (int r = 0;r < 10; r++)
	{
		mexPrintf("%f ",(float)t_d_cx2[r]);
		if(t_d_cx2[r]>1000.0){
			getchar();
		}
	}
	mexPrintf("\n");
#endif	

	
#if DEBUG_LEVEL > 4	
	double * check;
	cudaMalloc((void**)&check  , sizeof(double)*total_length); 
#endif

	float * d_c_log_lambda_x_exp;

	cudaMalloc((void**)&d_c_log_lambda_x_exp 	, sizeof(float)*total_length*width);	
	float * d_c_log_lambda_keys;
	cudaMalloc((void**)&d_c_log_lambda_keys 	, sizeof(float)*total_length*width);
	


	
	bool neg_found = false;
	// find_evidence<<<gridDim,blockDim>>>(&neg_found,d_c_log_lambda_keys,d_cmx_dbl,d_cmx2_dbl,d_cHx_r,d_cHx_c, d_cmx, d_cmx2, d_cx1, d_cx2, d_c_pi_x_2, d_c_b_x_2, d_c_ba_x_2,d_c_log_lambda_x,d_c_log_lambda_x_exp, b1,b2,b3,c1,c2,c3,d1,d2,d3,check );
	find_evidence<<<gridDim,blockDim>>>(&neg_found,d_c_log_lambda_keys,d_cmx_dbl,d_cmx2_dbl,d_cHx_r,d_cHx_c, d_cmx, d_cmx2, d_cx1, d_cx2, d_c_pi_x_2, d_c_b_x_2, d_c_ba_x_2,d_c_log_lambda_x,d_c_log_lambda_x_exp, b1,b2,b3,c1,c2,c3,d1,d2,d3 );

//=====================no need for following peiece of code if input is guaranteed to be valid also if neg_found is true algorithm will not run any further
	
	// if (neg_found == false)
	// {
		for (int i=0;i<t_ensemble->b_x_2.rows;i++)
		{
			for (int j=0;j<t_ensemble->b_x_2.cols;j++)
			{
				if ((t_ensemble->b_x_2.at<double>(i,j)<0 )||(t_ensemble->ba_x_2.at<double>(i,j)<0 ) || (t_ensemble->pi_x_2.at<double>(i,j)<0 ) )
				{
					neg_found = true;
					break;
				}
			}
		}
	// }
	if(neg_found){
		 
		t_ensemble->D_val = numeric_limits<double>::infinity();	
		grad->x1		= Mat::ones(grad->x1.rows,grad->x1.cols,CV_64F)*numeric_limits<double>::quiet_NaN() ;	
		grad->x2		= Mat::ones(grad->x2.rows,grad->x2.cols,CV_64F)*numeric_limits<double>::quiet_NaN() ;
		grad->b_x_2		= Mat::ones(grad->b_x_2.rows,grad->b_x_2.cols,CV_64F)*numeric_limits<double>::quiet_NaN() ;
		grad->ba_x_2 = Mat::ones(grad->ba_x_2.rows,grad->ba_x_2.cols,CV_64F)*numeric_limits<double>::quiet_NaN() ;
		grad->pi_x_2 = Mat::ones(grad->pi_x_2.rows,grad->pi_x_2.cols,CV_64F)*numeric_limits<double>::quiet_NaN() ;
		return;
	}
 
#if DEBUG_LEVEL > 4	

	mexPrintf("\n ensemble->d_cx2 in device after\n");
	for (int r = 0;r < 10; r++)
	{
		mexPrintf("%f ",(float)t_d_cx2[r]);
		if(t_d_cx2[r]>1000.0){
			getchar();
		}
	}
	mexPrintf("\n");
#endif	
#if DEBUG_LEVEL > 4	
	thrust::device_ptr<double> t_check(check);
	mexPrintf("\n t_check in device \n");
	for (int r = 1;r < 49*6+1; r++)
	{
		if ((r-1)%49 == 0)
			mexPrintf("\n");
		mexPrintf("%5.15g ",(double)t_check[r]);
		
	}
	mexPrintf("\n");
#endif

#if DEBUG_LEVEL > 4	

	thrust::device_ptr<float> t_d_cmx2(d_cmx2);
	mexPrintf("\n ensemble->d_cmx2 in device \n");
	for (int r = 0;r < 10; r++)
	{
		mexPrintf("%f ",(float)t_d_cmx2[r]);
		if(t_d_cmx2[r]>1000.0){
		}
	}
	
	mexPrintf("\n");
#endif	

#if DEBUG_LEVEL > 4

	thrust::device_ptr<float> t_c_log_lambda_x(d_c_log_lambda_x);
	mexPrintf("\n ensemble->log_lambda_x in device \n");
	for (int r = 0;r < total_comp_length; r++)
	{
		mexPrintf("%f ",(float)t_c_log_lambda_x[r]);
	}
	mexPrintf("\n");
#endif		
	
	

	
	dim3 blockDim2(512); 	dim3 gridDim2((total_length*width + blockDim.x - 1) / blockDim.x);		
	// get_exp<<<gridDim2,blockDim2>>>(d_c_log_lambda_x_exp,d_c_log_lambda_x,total_length*width);
	 
	
	// get_keys<<<gridDim2,blockDim2>>>(d_c_log_lambda_keys,total_length*width,b1,b2,b3,c1,c2,c3);
	
	thrust::device_ptr<float> t_c_log_lambda_x_exp(d_c_log_lambda_x_exp); // input
	thrust::device_ptr<float> keys(d_c_log_lambda_keys);       // keys
	
	
	thrust::device_vector<float> out_keys(total_length*width);                         // output keys
	thrust::device_vector<float> out_values(total_length*width);                         // output values

#if DEBUG_LEVEL > 4

	mexPrintf("\n in values\n");
	for (int r = 0;r < total_comp_length; r++)
	{
		mexPrintf("%f ",(float)t_c_log_lambda_x_exp[r]);
	}
	mexPrintf("\n");
#endif	    	

	thrust::reduce_by_key(keys, keys + total_length*width, t_c_log_lambda_x_exp , out_keys.begin(), out_values.begin());

#if DEBUG_LEVEL > 4

	mexPrintf("\n keys\n");
	for (int r = 0;r < total_comp_length; r++)
	{
		mexPrintf("%f ",(float)keys[r]);
	}
	mexPrintf("\n");
#endif	    

#if DEBUG_LEVEL > 2

	mexPrintf("\n out_values size = %d \n",out_values.size());
	for (int r = 0;r < total_comp_length; r++)
	{
		mexPrintf("%f ",(float)out_values[r]);
	}
	mexPrintf("\n");
#endif

	
	thrust::device_vector<float> t_opt_c_b_x_2(comp_length);       // output
	thrust::device_vector<float> t_opt_c_pi_x_2(comp_length);       // output
	thrust::device_vector<float> t_opt_c_ba_x_2(comp_length);
	
	int cmp_offset;
	for(int i=0; i<dimensions.at<int>(2,2);i++) {//dimensions.at<int>(2,2) assuming has the most components
		for(int c=0;c<3;c++){
			if(i<dimensions.at<int>(c,2)){
				switch (c)
				{
					case 0: cmp_offset = 0;
						break;
					case 1: cmp_offset = i + c1;
						break;
					case 2: cmp_offset = i + c1 + c2;
						break;
				}	
				t_opt_c_pi_x_2[cmp_offset] = (float)out_values[i*3 + c] + t_ensemble->pi_x;
				if(dimensions.at<int>(c,3) == 1){
					t_opt_c_b_x_2[cmp_offset] = t_opt_c_pi_x_2[i];
				}
				else if(dimensions.at<int>(c,3) == 0 || dimensions.at<int>(c,3) == 2){
					t_opt_c_b_x_2[cmp_offset] = (float)out_values[i*3 + c]/2 + t_ensemble->pi_x;
				}
				
			}
		}
	}
#if DEBUG_LEVEL > 4

	mexPrintf("\n t_opt_c_pi_x_2 new \n");
	for (int r = 0;r < comp_length; r++)
	{
		mexPrintf("%f ",(float)t_opt_c_pi_x_2[r]);
	}
	mexPrintf("\n");
#endif


	double *d_opt_c_b_x_2_dbl, *d_opt_c_pi_x_2_dbl, *d_opt_c_ba_x_2_dbl;
	cudaMalloc((void**)&d_opt_c_b_x_2_dbl , sizeof(double)*comp_length); 
	cudaMalloc((void**)&d_opt_c_pi_x_2_dbl , sizeof(double)*comp_length); 
	cudaMalloc((void**)&d_opt_c_ba_x_2_dbl , sizeof(double)*comp_length); 

	int count1 = 0,count2 = 0,count3 = 0,c;
	 
	
#if DEBUG_LEVEL > 4

	mexPrintf("\n t_opt_c_pi_x_2 old \n");
	for (int r = 0;r < comp_length; r++)
	{
		mexPrintf("%f ",(float)t_opt_c_pi_x_2[r]);
	}
	mexPrintf("\n");
#endif
	
	get_exp_mul_cmx_<<<gridDim2,blockDim2>>>(d_c_log_lambda_x_exp,d_cmx, d_cmx2 ,total_length,width,b1,b2,b3);	
	// thrust::device_ptr<float> t_c_log_lambda_x_exp_mul_cmx_(d_c_log_lambda_x_exp); // input	
	 
	thrust::reduce_by_key(keys, keys + total_length*width, t_c_log_lambda_x_exp , out_keys.begin(), out_values.begin());
    
	
	count1 = 0,count2 = 0,count3 = 0,c;
	float temp2;
	for (int i = 0; i < out_values.size(); i++)
	{
		if (i%3 == 0)
		{	count1++;
			if(count1 <= c1)		
			{
				c = 0;
				
				if (dimensions.at<int>(c,3) == 0 || dimensions.at<int>(c,3) == 2)
				{
					temp2 = t_ensemble->a_x + out_values[i]/2;
					t_opt_c_ba_x_2[count1-1] = t_opt_c_b_x_2[count1-1]/temp2;
					
				}
				else if (dimensions.at<int>(c,3) == 1)
				{
					
					// temp2 = t_ensemble->a_x + out_values[i];
					// t_opt_c_ba_x_2[count1-1] = t_opt_c_b_x_2[count1-1]/temp2;
				}
			}
		} else if (i%3 == 1)
		{
			count2++;
			if(count2 <= c2)
			{
				c = 1;
				if (dimensions.at<int>(c,3) == 0 || dimensions.at<int>(c,3) == 2)		
				{
					// mexPrintf("02");
					// temp2 = t_ensemble->a_x + out_values[i]/2;				
					// t_opt_c_ba_x_2[c1 + count2 - 1] = t_opt_c_b_x_2[c1 + count2  - 1]/temp2;
				}
				else if (dimensions.at<int>(c,3) == 1)
				{
					
					temp2 = t_ensemble->a_x + out_values[i];
					t_opt_c_ba_x_2[c1 + count2 - 1] = t_opt_c_b_x_2[c1   + count2 - 1]/temp2;
					// mexPrintf("temp : %f \n t_opt_c_b_x_2 : %f \n\n",t_ensemble->a_x,(float)out_values[i]);

				}
			}				
		}  else if (i%3 == 2)
		{
			count3++;
			if(count2 <= c3)
			{	
				c = 2;
			 
				if (dimensions.at<int>(c,3) == 0 || dimensions.at<int>(c,3) == 2)		
				{
					temp2 = t_ensemble->a_x + out_values[i]/2;
					t_opt_c_ba_x_2[c1 + c2 + count3 - 1] = t_opt_c_b_x_2[c1 + c2 + count3 - 1]/temp2;
				}
				else if (dimensions.at<int>(c,3) == 1)
				{
					// temp2 = t_ensemble->a_x + out_values[i];
					// t_opt_c_ba_x_2[c1 + c2 + count3 - 1] = t_opt_c_b_x_2[c1 + c2 + count3 - 1]/temp2;
				}
			}
		}
	}
	
	 
	
	for (int c = 0; c < 3; c ++)
	{
		if (dimensions.at<int>(c,4) >0)
		{
			//		if (c==3) %% Image prior
			if (c == 2)
			{
				//			opt_c_pi_x_2 = P9.pi(1:dimensions(c,1),:) * 1e3;

				Mat opt_c_pi_x_2_temp = Mat(P9.pi,Rect(0,0,dimensions.at<int>(c,2),dimensions.at<int>(c,0)))*1000;
				//opt_c_ba_x_2 = P9.gamma(1:dimensions(c,1),:);
				Mat opt_c_ba_x_2_temp = Mat(P9.gamma,Rect(0,0,dimensions.at<int>(c,2),dimensions.at<int>(c,0)));

				//opt_c_b_x_2 = ones(dimensions(c,1),dimensions(c,3)) * 1e-3; 
				Mat opt_c_b_x_2_temp = Mat::ones(dimensions.at<int>(c,2),dimensions.at<int>(c,0),CV_64F)/1000;
						
				for (int i = 0; i < dimensions.at<int>(c,2); i++)
				{
					t_opt_c_pi_x_2[c1+c2+i] = opt_c_pi_x_2_temp.at<double>(0,i);
					t_opt_c_b_x_2[c1+c2+i]  = opt_c_b_x_2_temp.at<double>(i,0);
					t_opt_c_ba_x_2[c1+c2+i] = opt_c_ba_x_2_temp.at<double>(0,i);
					
				 
				}

				
			}

			//elseif (c==2) %% Blur prior
			else if (c == 1)
			{
				// mexPrintf("in2");
				t_opt_c_ba_x_2[c1+0] = 5114.3;
				t_opt_c_ba_x_2[c1+1] = 5006.4;
				t_opt_c_ba_x_2[c1+2] = 173.8885;
				t_opt_c_ba_x_2[c1+3] = 50.6538;
				//opt_c_b_x_2 = [787.8988 201.7349 236.1948 143.1756];
				t_opt_c_b_x_2[c1+0] = 787.8988;
				t_opt_c_b_x_2[c1+1] = 201.7349;
				t_opt_c_b_x_2[c1+2] = 236.1948;
				t_opt_c_b_x_2[c1+3] = 143.1756;
				
				
			}

			//		else
			//			error foo
			//			end
			else {
				mexPrintf("error\n");
			}
		}
	
	}
	
	
	float * d_opt_cx1;
	double * d_opt_cx1_dbl;
	float * d_opt_cx2;
	double * d_opt_cx2_dbl;
	cudaMalloc((void**)&d_opt_cx1 	, sizeof(float)*total_length);   
	cudaMalloc((void**)&d_opt_cx1_dbl 	, sizeof(double)*total_length);   
	cudaMalloc((void**)&d_opt_cx2 	, sizeof(float)*total_length);   
	cudaMalloc((void**)&d_opt_cx2_dbl 	, sizeof(double)*total_length); 

	
	get_opt_cx<<<gridDim,blockDim>>>(d_opt_cx1,d_opt_cx2,d_c_ba_x_2,d_c_log_lambda_x,total_length,b1,b2,b3,c1,c2,c3,d1,d2,d3);

#if DEBUG_LEVEL > 4	

	thrust::device_ptr<float> t_opt_cx1(d_opt_cx1);
	mexPrintf("\n grad->x1 in device \n");
	for (int r = 0;r < total_length; r++)
	{
		mexPrintf("%f ",(float)t_opt_cx1[r]);
	}
	mexPrintf("\n");
	
#endif	


	thrust::device_ptr<float> t_cHx_r(d_cHx_r); // input
	thrust::device_vector<float> sum1_keys(3);   
	thrust::device_vector<float> t_sum1(3);  // Sum 1	 
	thrust::reduce_by_key(keys, keys + total_length, t_cHx_r , sum1_keys.begin(), t_sum1.begin());

#if DEBUG_LEVEL > 4	

	mexPrintf("\n t_cHx_r in device after\n");
	for (int r = 0;r < 50; r++)
	{
		mexPrintf("%f ",(float)t_cHx_r[r]);
	}
	mexPrintf("\n");
#endif	
		
	float * d_opt_c_b_x_2 = thrust::raw_pointer_cast(t_opt_c_b_x_2.data());
	float * d_opt_c_ba_x_2 = thrust::raw_pointer_cast(t_opt_c_ba_x_2.data());	
	float * sum2;
	int * comp_keys; 
	cudaMalloc((void**)&sum2 	, sizeof(float)*comp_length);	
	cudaMalloc((void**)&comp_keys 	, sizeof(int)*comp_length);	
	calc_sum2<<<1,comp_length>>>(sum2,d_c_b_x_2, d_c_ba_x_2, d_opt_c_b_x_2, d_opt_c_ba_x_2,t_ensemble->b_x,t_ensemble->a_x,comp_length,comp_keys,c1,c2,c3);
	// get_comp_keys<<<1,comp_length>>>(comp_keys,c1,c2,c3);	
 	thrust::device_ptr<float> t_sum2_input(sum2); // input
	thrust::device_ptr<int> t_comp_keys(comp_keys);
	thrust::device_vector<float> sum2_keys(3);   
	thrust::device_vector<float> t_sum2(3);  // Sum 2
	thrust::reduce_by_key(t_comp_keys, t_comp_keys + comp_length, t_sum2_input , sum2_keys.begin(), t_sum2.begin());
		
		
	float * sum3;
	cudaMalloc((void**)&sum3 	, sizeof(float)*comp_length);		
	float * d_opt_c_pi_x_2 = thrust::raw_pointer_cast(t_opt_c_pi_x_2.data());	
	calc_sum3<<<1,comp_length>>>(sum3,d_c_pi_x_2,d_opt_c_pi_x_2,t_ensemble->pi_x,comp_length); 
	thrust::device_ptr<float> t_sum3_input(sum3); // input
	thrust::device_vector<float> sum3_keys(3);   
	thrust::device_vector<float> t_sum3(3);  // Sum 2
	thrust::reduce_by_key(t_comp_keys, t_comp_keys + comp_length, t_sum3_input , sum3_keys.begin(), t_sum3.begin());
   
 
	
	 
	float * sum4;
	cudaMalloc((void**)&sum4 	, sizeof(float)*3);		
	thrust::device_ptr<float> t_c_pi_x_2(d_c_pi_x_2); 
	thrust::device_vector<float> sum_c_pi_x_2_values(3); 
	thrust::reduce_by_key(t_comp_keys, t_comp_keys + comp_length, t_c_pi_x_2 , sum3_keys.begin(), sum_c_pi_x_2_values.begin());
    float * d_temp2 = thrust::raw_pointer_cast(sum_c_pi_x_2_values.data());
	thrust::device_vector<float> V3(comp_length); 
	thrust::transform(t_c_pi_x_2, t_c_pi_x_2 + comp_length, t_opt_c_pi_x_2.begin(), V3.begin(), thrust::minus<float>());
	thrust::device_vector<float> t_temp5(3); 
	thrust::reduce_by_key(t_comp_keys, t_comp_keys + comp_length, V3.begin() , sum3_keys.begin(), t_temp5.begin());
    float * d_temp5 = thrust::raw_pointer_cast(t_temp5.data());
	calc_sum4<<<1,3>>>(sum4,d_c_pi_x_2,d_opt_c_pi_x_2, d_temp2,d_temp5,  t_ensemble->pi_x,c1,c2,c3);
	thrust::device_ptr<float> t_sum4(sum4);
	 

	
	 
	float * temp5;
	cudaMalloc((void**)&temp5 	, sizeof(float)*total_length);
	calc_sum5<<<gridDim,blockDim>>>(temp5,d_c_log_lambda_x,b1,b2,b3,c1,c2,c3);
	thrust::device_ptr<float> t_temp6(temp5);
	thrust::device_vector<float> t_sum5(3);
	thrust::device_vector<float> sum5_keys(3);   
	thrust::reduce_by_key(keys, keys + total_length, t_temp6 , sum5_keys.begin(), t_sum5.begin());
    
	
	
	thrust::device_vector<float> t_total_sum(3);
	for (int i = 0; i < 3;i++)
		t_total_sum[i] = t_sum1[i] + t_sum2[i] + t_sum3[i] + t_sum4[i] + t_sum5[i];

#if DEBUG_LEVEL > 4	

	mexPrintf("\n\nsums in device \n");
	mexPrintf("%f ",(float)t_sum1[1]);
	mexPrintf("%f ",(float)t_sum2[1]);
	mexPrintf("%f ",(float)t_sum3[1]);
	mexPrintf("%f ",(float)t_sum4[1]);
	mexPrintf("%f ",(float)t_sum5[1]);
	mexPrintf("\n");
#endif			
	
	
	for (int c =0; c<3; c++)
	{
		int start_idx =0;
		int end_idx = 0;
		for (int i =0; i<=c;i++)
		{
			if (i<=c-1)
			{
				start_idx += dimensions.at<int>(i,0);
			}
			end_idx += dimensions.at<int>(i,0);
		}
	 
		for (int i = 0; start_idx+i < end_idx; i++)
		{
			t_ensemble->D_x.at<double>(start_idx+i,0) = t_total_sum[start_idx+i];
			  
		 
		}
	
	}
	
	 
	float *d_c_log_lambda_x_2;double *d_c_log_lambda_x_2_dbl;
	cudaMalloc((void**)&d_c_log_lambda_x_2  , sizeof(float2)*(total_comp_length)); 
	cudaMalloc((void**)&d_c_log_lambda_x_2_dbl  , sizeof(double)*(total_comp_length)); 
	
	
	
	

	

	
	//test_kernel<<<gridDim2,blockDim2>>>(d_c_log_lambda_x,total_length,b1,b2,b3);
	
	swap_d_c_log_lambda_x<<<gridDim2,blockDim2>>>(d_c_log_lambda_x_2,d_c_log_lambda_x,b1,b2,b3,width);  // Converting d_c_log_lambda_x in to the correct order 
	
	
	
	// t_ensemble->d_log_lambda_x = d_c_log_lambda_x_2;
	// t_ensemble->d_mx			 = d_cmx;
	// t_ensemble->d_mx2			 = d_cmx2;
 
	 
	// t_ensemble->d_opt_ba_x_2 = thrust::raw_pointer_cast(t_opt_c_ba_x_2.data());
	// t_ensemble->d_opt_b_x_2 = thrust::raw_pointer_cast(t_opt_c_b_x_2.data());
	// t_ensemble->d_opt_pi_x_2 = thrust::raw_pointer_cast(t_opt_c_pi_x_2.data());
		
	// grad->d_x1 = d_opt_cx1;
	// grad->d_x2 = d_opt_cx2;
	
	// float * a = new float[4];
	// float * d_a;
	// a[0] = 10.0f;
	// a[1] = 20.0f;
	// a[2] = 30.0f;
	// a[3] = 40.0f;
	
	// cudaMalloc((void**)&d_a , sizeof(float)*4); cudaMemcpy(d_a, a, sizeof(float)*4, cudaMemcpyHostToDevice); 
	
	// thrust::device_ptr<float> t_a(d_a);
	// thrust::device_vector<float> t_a2(d_a, d_a+4); 
	
	// test_kernel<<<1,4>>>(d_a);
 
	
	// mexPrintf("\n\n\na \n");
	// for(int i = 0; i < 4; i++)
        // mexPrintf("%f ,",(float)t_a[i]);
	// mexPrintf("\nta \n");
	// for(int i = 0; i < 4; i++)
        // mexPrintf("%f ,",(float)t_a2[i]); 
	
	
	
	
	//----------------------not necessary
	// thrust::device_ptr<float> temp_t_c_pi_x_2(d_c_pi_x_2);
	// thrust::device_ptr<float> temp_t_c_b_x_2(d_c_b_x_2);
	// thrust::device_ptr<float> temp_t_c_ba_x_2(d_c_ba_x_2);
	// thrust::device_vector<float> temp_temp_t_c_pi_x_2(temp_t_c_pi_x_2, temp_t_c_pi_x_2 + comp_length	); // Clone's the array
	// thrust::device_vector<float> temp_temp_t_c_b_x_2(temp_t_c_b_x_2, temp_t_c_b_x_2 + comp_length	);
	// thrust::device_vector<float> temp_temp_t_c_ba_x_2(temp_t_c_ba_x_2,temp_t_c_ba_x_2 + comp_length 	);
   

	
	
	//-------------------------integrated to later kernel
	// if(state >= 2){
		 
		// thrust::device_vector<float> temp_t_opt_c_pi_x_2 = t_opt_c_pi_x_2; // Clone's the array
		// thrust::device_vector<float> temp_t_opt_c_b_x_2 = t_opt_c_b_x_2;
		// thrust::device_vector<float> temp_t_opt_c_ba_x_2 = t_opt_c_ba_x_2;
		// grad->d_pi_x_2 = thrust::raw_pointer_cast(temp_t_opt_c_pi_x_2.data());
		// grad->d_b_x_2 =  thrust::raw_pointer_cast(temp_t_opt_c_b_x_2.data());
		// grad->d_ba_x_2 = thrust::raw_pointer_cast(temp_t_opt_c_ba_x_2.data());
	// }
	// else{
	
		
		// grad->d_pi_x_2 = thrust::raw_pointer_cast(temp_temp_t_c_pi_x_2.data());
		// grad->d_b_x_2 = thrust::raw_pointer_cast(temp_temp_t_c_b_x_2.data());
		// grad->d_ba_x_2 = thrust::raw_pointer_cast(temp_temp_t_c_ba_x_2.data());
	// }

	int data_points;
	double rerror;
	float * d_dx1;
	float * d_dx2;

	cudaMalloc((void**)&d_dx1 , sizeof(float)*total_length); 
	cudaMalloc((void**)&d_dx2 , sizeof(float)*total_length); 
	
	if (P10 == 1)
		train_blind_deconv_test_cuda_gpu_2(d_dx1,d_dx2,&rerror,&data_points,dimensions,d_cmx,d_cmx2,P1,P2,P3,P4,P5,P6,P7,P8);
		
	t_ensemble->b_sigma_2 = t_ensemble->b_sigma + data_points/2;	 
	t_ensemble->opt_ba_sigma_2=t_ensemble->b_sigma_2/(t_ensemble->a_sigma+rerror/2);	
	
	
	if (state == 3)
	{
		t_ensemble->ba_sigma_2 = t_ensemble->opt_ba_sigma_2;
	}
	

// -------------------------integrated to copy_device_floats_to_double kernel	
	// update_grad_x1_x2<<<gridDim,blockDim>>>(d_opt_cx1,d_opt_cx2,d_dx1,d_dx2, t_ensemble->ba_sigma_2,total_length);
	
	
	int sum_row0 = 0;
	for (int i=0; i< dimensions.rows;i++)  // just 3 iterations 
	{
		sum_row0 += (int) dimensions.at<int>(i,0);
	}
	
	
	 
	
	 
	t_ensemble->D_x.at<double>(sum_row0,0) =  lgamma(t_ensemble->b_sigma) - lgamma(t_ensemble->b_sigma_2) - t_ensemble->b_sigma*log(t_ensemble->a_sigma)+ t_ensemble->b_sigma_2*log(t_ensemble->b_sigma_2/t_ensemble->ba_sigma_2) + (t_ensemble->b_sigma_2/t_ensemble->opt_ba_sigma_2 - t_ensemble->b_sigma_2/t_ensemble->ba_sigma_2)*t_ensemble->ba_sigma_2 + data_points*log(2*PI)/2;
	
	 
	//%Normalise from log_e to bits per data point
	//	t_ensemble.D_x=t_ensemble.D_x/data_points*log2(exp(1));
	t_ensemble->D_x = (t_ensemble->D_x/data_points)*(log(exp(1.0))/log(2.0));	// Geethan: log base2 written as log(x)/log(2)
	
	
	if((t_ensemble->D_val != t_ensemble->D_val)){		// Geethan: could not find isnan equivalent used f!=f
		
		t_ensemble->D_val = numeric_limits<double>::infinity();
	}

	 
	//	%Evaluate the KL divergence
	//	t_ensemble.D_val=sum(t_ensemble.D_x);
	t_ensemble->D_val = sum(t_ensemble->D_x)[0]; 
	 
	//mexPrintf("D_val : %f\n",t_ensemble->D_val);
	 
	update_grad_x1_x2_new<<<gridDim,blockDim>>>(d_opt_cx1,d_opt_cx2,d_cx1,d_cx2,t_ensemble->ba_sigma_2,d_dx1,d_dx2,b1, b2, b3, f1, f2, f3);
	
	
#if DEBUG_LEVEL > 4	


	// thrust::device_ptr<float> t_opt_cx1(d_opt_cx1);
	mexPrintf("\ngrad->x1 in device \n");
	for (int r = 0;r < total_length; r++)
	{
		mexPrintf("%f ",(float)t_opt_cx1[r]);
	}
	mexPrintf("\n");
#endif		
	//--------------------integrated to copy_device_floats_to_double kernel
	// update_grad_all<<<1,comp_length>>>(grad->d_b_x_2,grad->d_ba_x_2,grad->d_pi_x_2,thrust::raw_pointer_cast(temp_temp_t_c_b_x_2.data()),thrust::raw_pointer_cast(temp_temp_t_c_ba_x_2.data()),thrust::raw_pointer_cast(temp_temp_t_c_pi_x_2.data()),comp_length,true);
	
	
	//-------------------- integrated to update_grad_x1_x2_new kernel
	// reset_grad_x1_x2_by_key<<<gridDim,blockDim>>>(d_opt_cx1,d_opt_cx2, b1, b2, b3, f1, f2, f3);



	double* grad_d_pi_x_2_dbl, *grad_d_b_x_2_dbl, *grad_d_ba_x_2_dbl;
	cudaMalloc((void**)&grad_d_pi_x_2_dbl 	, sizeof(double)*comp_length);	
	cudaMalloc((void**)&grad_d_b_x_2_dbl 	, sizeof(double)*comp_length);	
	cudaMalloc((void**)&grad_d_ba_x_2_dbl 	, sizeof(double)*comp_length);	

	
	copy_device_floats_to_double<<<gridDim2,blockDim2>>>(d_c_log_lambda_x_2,d_c_log_lambda_x_2_dbl,d_cmx,d_cmx_dbl, d_cmx2, d_cmx2_dbl,
	d_opt_c_b_x_2, d_opt_c_b_x_2_dbl,d_opt_c_pi_x_2,d_opt_c_pi_x_2_dbl, d_opt_c_ba_x_2, d_opt_c_ba_x_2_dbl,
	d_opt_cx1, d_opt_cx1_dbl, d_opt_cx2, d_opt_cx2_dbl, 
	grad_d_pi_x_2_dbl, grad_d_b_x_2_dbl, grad_d_ba_x_2_dbl, total_length,comp_length,(total_comp_length),state);

	cudaMemcpy((double*)(t_ensemble->log_lambda_x.data), d_c_log_lambda_x_2_dbl, sizeof(double)*total_comp_length, cudaMemcpyDeviceToHost); 
	cudaMemcpy((double*)(t_ensemble->mx.data), d_cmx_dbl, sizeof(double)*total_length, cudaMemcpyDeviceToHost); 
	cudaMemcpy((double*)(t_ensemble->mx2.data), d_cmx2_dbl, sizeof(double)*total_length, cudaMemcpyDeviceToHost); 
	cudaMemcpy((double*)(t_ensemble->opt_b_x_2.data),  d_opt_c_b_x_2_dbl,  sizeof(double)*comp_length, cudaMemcpyDeviceToHost); 
	cudaMemcpy((double*)(t_ensemble->opt_ba_x_2.data), d_opt_c_ba_x_2_dbl, sizeof(double)*comp_length, cudaMemcpyDeviceToHost); 
	cudaMemcpy((double*)(t_ensemble->opt_pi_x_2.data), d_opt_c_pi_x_2_dbl, sizeof(double)*comp_length, cudaMemcpyDeviceToHost); 

	cudaMemcpy((double*)(grad->b_x_2.data),  grad_d_b_x_2_dbl,  sizeof(double)*comp_length, cudaMemcpyDeviceToHost); 
	cudaMemcpy((double*)(grad->ba_x_2.data), grad_d_ba_x_2_dbl, sizeof(double)*comp_length, cudaMemcpyDeviceToHost); 
	cudaMemcpy((double*)(grad->pi_x_2.data), grad_d_pi_x_2_dbl, sizeof(double)*comp_length, cudaMemcpyDeviceToHost); 
	
	cudaMemcpy((double*)(grad->x1.data), d_opt_cx1_dbl, sizeof(double)*total_length, cudaMemcpyDeviceToHost); 
	cudaMemcpy((double*)(grad->x2.data), d_opt_cx2_dbl, sizeof(double)*total_length, cudaMemcpyDeviceToHost); 

	
#if DEBUG_LEVEL > 4
	mexPrintf("grad->x1 at end");
	for(int i=0; i< total_length; i++){
		mexPrintf("%f ",grad->x1.at<double>(0,i));
		
	}
	mexPrintf("\ngrad->x2 ");
	for(int i=0; i< total_length; i++){
		mexPrintf("%f ",grad->x2.at<double>(0,i));
		
	}

	mexPrintf("\ngrad->b_x_2 ");
	for(int i=0; i< comp_length; i++){
		mexPrintf("%f ",grad->b_x_2.at<double>(0,i));
		
	}

	mexPrintf("\ngrad->ba_x_2 ");
	for(int i=0; i< comp_length; i++){
		mexPrintf("%f ",grad->ba_x_2.at<double>(0,i));
		
	}	
	
	mexPrintf("\ngrad->pi_x_2 ");
	for(int i=0; i< comp_length; i++){
		mexPrintf("%f ",grad->pi_x_2.at<double>(0,i));
		
	}
	mexPrintf("\n");
#endif		
		
	cudaFree(d_c_log_lambda_x_2_dbl);
	cudaFree(d_cmx_dbl);
	cudaFree(d_cmx2_dbl);
	cudaFree(d_opt_c_b_x_2_dbl);
	cudaFree(d_opt_c_ba_x_2_dbl);
	cudaFree(d_opt_c_pi_x_2_dbl);
	
	cudaFree(grad_d_b_x_2_dbl);
	cudaFree(grad_d_ba_x_2_dbl);
	cudaFree(grad_d_pi_x_2_dbl);
	cudaFree(d_opt_cx1_dbl);
	cudaFree(d_opt_cx2_dbl);
	
	
	
	
	cudaFree(d_cx1); cudaFree(d_cx2);

	cudaFree(d_c_pi_x_2);
	cudaFree(d_c_b_x_2);
	cudaFree(d_c_ba_x_2);

	cudaFree(d_cHx_r);
	cudaFree(d_cHx_c);

	cudaFree(d_cmx); cudaFree(d_cmx2); 

	cudaFree(d_c_log_lambda_x);
	// cudaFree(d_max_c_log_lambda_x); 
	// cudaFree(d_log_sum_lambda_x);
	cudaFree(d_c_log_lambda_x_exp); 
	cudaFree(d_c_log_lambda_keys);

	cudaFree(d_opt_cx1); 	cudaFree(d_opt_cx2); 
	cudaFree(sum2); 		cudaFree(comp_keys); 
	cudaFree(sum3);  
	cudaFree(sum4);  
	cudaFree(temp5);

	cudaFree(d_c_log_lambda_x_2); 

	cudaFree(d_dx1); cudaFree(d_dx2);
}


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	//tic();

 
	const char *field_names[] = {"x1"               ,
								 "x2"               ,
								 "mx"               ,
								 "mx2"              ,
								 "log_lambda_x"     ,
								 "pi_x"             ,
								 "pi_x_2"           ,
								 "opt_pi_x_2"       ,
								 "a_x"              ,
								 "ba_x_2"           ,
								 "opt_ba_x_2"       ,
								 "b_x"              ,
								 "b_x_2"            ,
								 "opt_b_x_2"        ,
								 "a_sigma"          ,
								 "ba_sigma_2"       ,
								 "opt_ba_sigma_2"   ,
								 "b_sigma"          ,
								 "b_sigma_2"        ,
								 "D_val"            ,
								 "D_x"              
	};
	
	const char *grad_field_name[] = {
								 "x1" ,
								 "x2" ,
								 "pi_x_2",
								 "ba_x_2" ,
								 "b_x_2" 
	};
	
	
	
	mwSize dims[2] = {1, 1 };
	plhs[0] = mxCreateStructArray(2, dims, 21, field_names);
	

	plhs[1] = mxCreateStructArray(2, dims, 5, grad_field_name);
	
	int grad_num_x1               = mxGetFieldNumber(plhs[1],"x1"               );
	int grad_num_x2               = mxGetFieldNumber(plhs[1],"x2"               );
	int grad_num_pi_x_2           = mxGetFieldNumber(plhs[1],"pi_x_2"           );
	int grad_num_ba_x_2           = mxGetFieldNumber(plhs[1],"ba_x_2"           );
	int grad_num_b_x_2            = mxGetFieldNumber(plhs[1],"b_x_2"            );
	
	int num_x1               = mxGetFieldNumber(plhs[0],"x1"               );
	int num_x2               = mxGetFieldNumber(plhs[0],"x2"               );
	int num_mx               = mxGetFieldNumber(plhs[0],"mx"               );
	int num_mx2              = mxGetFieldNumber(plhs[0],"mx2"              );
	int num_log_lambda_x     = mxGetFieldNumber(plhs[0],"log_lambda_x"     );
	int num_pi_x             = mxGetFieldNumber(plhs[0],"pi_x"             );
	int num_pi_x_2           = mxGetFieldNumber(plhs[0],"pi_x_2"           );
	int num_opt_pi_x_2       = mxGetFieldNumber(plhs[0],"opt_pi_x_2"       );
	int num_a_x              = mxGetFieldNumber(plhs[0],"a_x"              );
	int num_ba_x_2           = mxGetFieldNumber(plhs[0],"ba_x_2"           );
	int num_opt_ba_x_2       = mxGetFieldNumber(plhs[0],"opt_ba_x_2"       );
	int num_b_x              = mxGetFieldNumber(plhs[0],"b_x"              );
	int num_b_x_2            = mxGetFieldNumber(plhs[0],"b_x_2"            );
	int num_opt_b_x_2        = mxGetFieldNumber(plhs[0],"opt_b_x_2"        );
	int num_a_sigma          = mxGetFieldNumber(plhs[0],"a_sigma"          );
	int num_ba_sigma_2       = mxGetFieldNumber(plhs[0],"ba_sigma_2"       );
	int num_opt_ba_sigma_2   = mxGetFieldNumber(plhs[0],"opt_ba_sigma_2"   );
	int num_b_sigma          = mxGetFieldNumber(plhs[0],"b_sigma"          );
	int num_b_sigma_2        = mxGetFieldNumber(plhs[0],"b_sigma_2"        );
	int num_D_val            = mxGetFieldNumber(plhs[0],"D_val"            );
	int num_D_x              = mxGetFieldNumber(plhs[0],"D_x"              );
	
	
	// step_len
	mxArray *mx_step_len = mxDuplicateArray(prhs[0]);  
	double step_len = *mxGetPr(mx_step_len);

	//dimensions
	mxArray *mx_dimensions = mxDuplicateArray(prhs[1]);  
	double* dim_array = mxGetPr(mx_dimensions);
	int dim_M = mxGetM(mx_dimensions);
	int dim_N = mxGetN(mx_dimensions);
	int *r = new int[dim_M*dim_N];
	for (int i=0;i<dim_N*dim_M;i++)
	{
		*(r+i) = (int) *(dim_array+i);
	}

	Mat dimensions = Mat(dim_N,dim_M,CV_32S,r);
	dimensions = dimensions.t();



	mxArray *mx_opt_func = mxDuplicateArray(prhs[2]);  
	size_t buflen = 30;
	char *opt_func =(char *)mxMalloc(buflen);	
	buflen = mxGetN(mx_opt_func)*sizeof(mxChar)+1;	 
	mxGetString(mx_opt_func, opt_func, (mwSize)buflen);


	Ensemble ensemble;
	mxArray *mx_ensemble = mxDuplicateArray(prhs[3]);  
	mxArray *mx_en_x1 = mxGetFieldByNumber(mx_ensemble, 0, num_x1);
	double* en_x1 = mxGetPr(mx_en_x1); 
	dim_M = mxGetM(mx_en_x1);
	dim_N = mxGetN(mx_en_x1);
	ensemble.x1 = Mat(dim_M,dim_N,CV_64F,en_x1);

	mxArray *mx_en_x2 = mxGetFieldByNumber(mx_ensemble, 0, num_x2);
	double* en_x2 = mxGetPr(mx_en_x2); 
	dim_M = mxGetM(mx_en_x2);
	dim_N = mxGetN(mx_en_x2);
	ensemble.x2 = Mat(dim_M,dim_N,CV_64F,en_x2);

	
	
	mxArray *mx_en_mx = mxGetFieldByNumber(mx_ensemble, 0, num_mx); 
	double* en_mx= mxGetPr(mx_en_mx); 
	dim_M = mxGetM(mx_en_mx);
	dim_N = mxGetN(mx_en_mx);
	ensemble.mx = Mat(dim_M,dim_N,CV_64F,en_mx);

	mxArray *mx_en_mx2 = mxGetFieldByNumber(mx_ensemble, 0, num_mx2);
	double* en_mx2 = mxGetPr(mx_en_mx2); 
	dim_M = mxGetM(mx_en_mx2);
	dim_N = mxGetN(mx_en_mx2);
	ensemble.mx2 = Mat(dim_M,dim_N,CV_64F,en_mx2);

	mxArray *mx_en_log_lambda_x = mxGetFieldByNumber(mx_ensemble, 0, num_log_lambda_x);
	double* en_log_lambda = mxGetPr(mx_en_log_lambda_x); 
	dim_M = mxGetM(mx_en_log_lambda_x);
	dim_N = mxGetN(mx_en_log_lambda_x);
	ensemble.log_lambda_x = Mat(dim_M,dim_N,CV_64F,en_log_lambda);

	mxArray *mx_en_pi_x_2 = mxGetFieldByNumber(mx_ensemble, 0, num_pi_x_2);
	double* en_pi_x_2 = mxGetPr(mx_en_pi_x_2); 
	dim_M = mxGetM(mx_en_pi_x_2);
	dim_N = mxGetN(mx_en_pi_x_2);
	ensemble.pi_x_2 = Mat(dim_M,dim_N,CV_64F,en_pi_x_2);

	mxArray *mx_en_opt_pi_x_2 = mxGetFieldByNumber(mx_ensemble, 0, num_opt_pi_x_2);
	double* en_opt_pi_x_2 = mxGetPr(mx_en_opt_pi_x_2); 
	dim_M = mxGetM(mx_en_opt_pi_x_2);
	dim_N = mxGetN(mx_en_opt_pi_x_2);
	ensemble.opt_pi_x_2 = Mat(dim_M,dim_N,CV_64F,en_opt_pi_x_2);

	mxArray *mx_en_ba_x_2 = mxGetFieldByNumber(mx_ensemble, 0, num_ba_x_2);
	double* en_ba_x_2 = mxGetPr(mx_en_ba_x_2); 
	dim_M = mxGetM(mx_en_ba_x_2);
	dim_N = mxGetN(mx_en_ba_x_2);
	ensemble.ba_x_2 = Mat(dim_M,dim_N,CV_64F,en_ba_x_2);

	mxArray *mx_en_opt_ba_x_2 = mxGetFieldByNumber(mx_ensemble, 0, num_opt_ba_x_2);
	double* en_opt_ba_x_2 = mxGetPr(mx_en_opt_ba_x_2); 
	dim_M = mxGetM(mx_en_opt_ba_x_2);
	dim_N = mxGetN(mx_en_opt_ba_x_2);
	ensemble.opt_ba_x_2 = Mat(dim_M,dim_N,CV_64F,en_opt_ba_x_2);

	mxArray *mx_en_b_x_2 = mxGetFieldByNumber(mx_ensemble, 0, num_b_x_2);
	double* en_b_x_2 = mxGetPr(mx_en_b_x_2); 
	dim_M = mxGetM(mx_en_b_x_2);
	dim_N = mxGetN(mx_en_b_x_2);
	ensemble.b_x_2 = Mat(dim_M,dim_N,CV_64F,en_b_x_2);

	mxArray *mx_en_opt_b_x_2 = mxGetFieldByNumber(mx_ensemble, 0, num_opt_b_x_2);
	double* en_opt_b_x_2 = mxGetPr(mx_en_opt_b_x_2); 
	dim_M = mxGetM(mx_en_opt_b_x_2);
	dim_N = mxGetN(mx_en_opt_b_x_2);
	ensemble.opt_b_x_2 = Mat(dim_M,dim_N,CV_64F,en_opt_b_x_2);

	mxArray *mx_en_D_x = mxGetFieldByNumber(mx_ensemble, 0, num_D_x);
	double* en_D_x = mxGetPr(mx_en_D_x); 
	dim_M = mxGetM(mx_en_D_x);
	dim_N = mxGetN(mx_en_D_x);
	ensemble.D_x = Mat(dim_M,dim_N,CV_64F,en_D_x);

	mxArray *mx_en_pi_x = mxGetFieldByNumber(mx_ensemble, 0, num_pi_x);
	double* en_pi_x = mxGetPr(mx_en_pi_x); 
	ensemble.pi_x = *en_pi_x;   

	mxArray *mx_en_a_x = mxGetFieldByNumber(mx_ensemble, 0, num_a_x);
	double* en_a_x = mxGetPr(mx_en_a_x); 
	ensemble.a_x = *en_a_x;   

	mxArray *mx_en_b_x = mxGetFieldByNumber(mx_ensemble, 0, num_b_x);
	double* en_b_x = mxGetPr(mx_en_b_x); 
	ensemble.b_x = *en_b_x;   

	mxArray *mx_en_a_sigma = mxGetFieldByNumber(mx_ensemble, 0, num_a_sigma);
	double* en_a_sigma = mxGetPr(mx_en_a_sigma); 
	ensemble.a_sigma = *en_a_sigma;   

	mxArray *mx_en_ba_sigma_2 = mxGetFieldByNumber(mx_ensemble, 0, num_ba_sigma_2);
	double* en_ba_sigma_2 = mxGetPr(mx_en_ba_sigma_2); 
	ensemble.ba_sigma_2 = *en_ba_sigma_2;   

	mxArray *mx_en_opt_ba_sigma_2 = mxGetFieldByNumber(mx_ensemble, 0, num_opt_ba_sigma_2);
	double* en_opt_ba_sigma_2 = mxGetPr(mx_en_opt_ba_sigma_2); 
	ensemble.opt_ba_sigma_2 = *en_opt_ba_sigma_2;   

	mxArray *mx_en_b_sigma = mxGetFieldByNumber(mx_ensemble, 0, num_b_sigma);
	double* en_b_sigma = mxGetPr(mx_en_b_sigma); 
	ensemble.b_sigma = *en_b_sigma;   

	mxArray *mx_en_b_sigma_2 = mxGetFieldByNumber(mx_ensemble, 0, num_b_sigma_2);
	double* en_b_sigma_2 = mxGetPr(mx_en_b_sigma_2); 
	ensemble.b_sigma_2 = *en_b_sigma_2;   

	mxArray *mx_en_D_val = mxGetFieldByNumber(mx_ensemble, 0, num_D_val);
	double* en_D_val = mxGetPr(mx_en_D_val); 
	ensemble.D_val = *en_D_val;   

	Direction direction;
	mxArray *mx_direction = mxDuplicateArray(prhs[4]);

	mxArray *mx_di_x1 = mxGetField(mx_direction, 0, "x1");
	double* di_x1 = mxGetPr(mx_di_x1); 
	dim_M = mxGetM(mx_di_x1);
	dim_N = mxGetN(mx_di_x1);
	direction.x1 = Mat(dim_M,dim_N,CV_64F,di_x1);

	mxArray *mx_di_x2 = mxGetField(mx_direction, 0, "x2");
	double* di_x2 = mxGetPr(mx_di_x2); 
	dim_M = mxGetM(mx_di_x2);
	dim_N = mxGetN(mx_di_x2);
	direction.x2 = Mat(dim_M,dim_N,CV_64F,di_x2);

	mxArray *mx_di_pi_x_2 = mxGetField(mx_direction, 0, "pi_x_2");
	double* di_pi_x_2 = mxGetPr(mx_di_pi_x_2); 
	dim_M = mxGetM(mx_di_pi_x_2);
	dim_N = mxGetN(mx_di_pi_x_2);
	direction.pi_x_2 = Mat(dim_M,dim_N,CV_64F,di_pi_x_2);

	mxArray *mx_di_ba_x_2 = mxGetField(mx_direction, 0, "ba_x_2");
	double* di_ba_x_2 = mxGetPr(mx_di_ba_x_2); 
	dim_M = mxGetM(mx_di_ba_x_2);
	dim_N = mxGetN(mx_di_ba_x_2);
	direction.ba_x_2 = Mat(dim_M,dim_N,CV_64F,di_ba_x_2);

	mxArray *mx_di_b_x_2 = mxGetField(mx_direction, 0, "b_x_2");
	double* di_b_x_2 = mxGetPr(mx_di_b_x_2); 
	dim_M = mxGetM(mx_di_b_x_2);
	dim_N = mxGetN(mx_di_b_x_2);
	direction.b_x_2 = Mat(dim_M,dim_N,CV_64F,di_b_x_2);



	// state
	mxArray *mx_state = mxDuplicateArray(prhs[5]);  
	int state = (int)(*mxGetPr(mx_state));

	mxArray *mx_P1 = mxDuplicateArray(prhs[6]);  
	double* P1_array = mxGetPr(mx_P1);
	int P1_M = (int)mxGetM(mx_P1);
	int P1_N = (int)mxGetN(mx_P1);
	Mat P1 = Mat(P1_N,P1_M,CV_64F,P1_array).t();

	mxArray *mx_P2 = mxDuplicateArray(prhs[7]);  
	double* P2_array = mxGetPr(mx_P2);
	int P2_M = (int)mxGetM(mx_P2);
	int P2_N = (int)mxGetN(mx_P2);
	Mat P2 = Mat(P2_N,P2_M,CV_64F,P2_array).t();

	mxArray *mx_P11 = mxDuplicateArray(prhs[16]);  
	double* P11_array = mxGetPr(mx_P11);
	int P11_M = (int)mxGetM(mx_P11);
	int P11_N = (int)mxGetN(mx_P11);
	Mat P11 = Mat(P11_N,P11_M,CV_64F,P11_array).t();

	mxArray *mx_P12 = mxDuplicateArray(prhs[17]);  
	double* P12_array = mxGetPr(mx_P12);
	int P12_M = (int)mxGetM(mx_P12);
	int P12_N = (int)mxGetN(mx_P12);
	Mat P12 = Mat(P12_N,P12_M,CV_64F,P12_array).t();

	mxArray *mx_P3 = mxDuplicateArray(prhs[8]);  
	int P3 = (int)(*mxGetPr(mx_P3));

	mxArray *mx_P4 = mxDuplicateArray(prhs[9]);  
	int P4 = (int)(*mxGetPr(mx_P4));

	mxArray *mx_P5 = mxDuplicateArray(prhs[10]);  
	int P5 = (int)(*mxGetPr(mx_P5));

	mxArray *mx_P6 = mxDuplicateArray(prhs[11]);  
	int P6 = (int)(*mxGetPr(mx_P6));

	mxArray *mx_P7 = mxDuplicateArray(prhs[12]);  
	int P7 = (int)(*mxGetPr(mx_P7));

	mxArray *mx_P8 = mxDuplicateArray(prhs[13]);  
	int P8 = (int)(*mxGetPr(mx_P8));

	mxArray *mx_P10 = mxDuplicateArray(prhs[15]);  
	int P10 = (int)(*mxGetPr(mx_P10));

	Priors P9;
	mxArray *mx_priors = mxDuplicateArray(prhs[14]);

	mxArray *mx_p9_pi = mxGetField(mx_priors, 0, "pi");
	double* p9_pi = mxGetPr(mx_p9_pi); 
	dim_M = mxGetM(mx_p9_pi);
	dim_N = mxGetN(mx_p9_pi);
	P9.pi = Mat(dim_M,dim_N,CV_64F,p9_pi);

	mxArray *mx_p9_gam = mxGetField(mx_priors, 0, "gamma");
	double* p9_gam = mxGetPr(mx_p9_gam); 
	dim_M = mxGetM(mx_p9_gam);
	dim_N = mxGetN(mx_p9_gam);
	P9.gamma = Mat(dim_M,dim_N,CV_64F,p9_gam);

	Direction grad;
	 
// Checking 'ensemble' Mat variables ---- print the matlab syntax
// Mat x1,x2,mx,mx2, log_lambda_x,pi_x_2,opt_pi_x_2,ba_x_2,opt_ba_x_2,b_x_2,opt_b_x_2,D_x;
//	mexPrintf("ensemble_x1 = [");
//	for (int i = 0; i < ensemble.x1.cols ; i++)
//		mexPrintf("%f ", ensemble.x1.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("ensemble_x2 = [");
//	for (int i = 0; i < ensemble.x2.cols ; i++)
//		mexPrintf("%f ", ensemble.x2.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("ensemble_mx = [");
//	for (int i = 0; i < ensemble.mx.cols ; i++)
//		mexPrintf("%f ", ensemble.mx.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("ensemble_mx2 = [");
//	for (int i = 0; i < ensemble.mx2.cols ; i++)
//		mexPrintf("%f ", ensemble.mx2.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("ensemble_log_lambda_x = [");
//	for (int i = 0; i < ensemble.log_lambda_x.cols ; i++)
//		mexPrintf("%f ", ensemble.log_lambda_x.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("ensemble_pi_x_2 = [");
//	for (int i = 0; i < ensemble.pi_x_2.cols ; i++)
//		mexPrintf("%f ", ensemble.pi_x_2.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("ensemble_opt_pi_x_2 = [");
//	for (int i = 0; i < ensemble.opt_pi_x_2.cols ; i++)
//		mexPrintf("%f ", ensemble.opt_pi_x_2.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("ensemble_ba_x_2 = [");
//	for (int i = 0; i < ensemble.ba_x_2.cols ; i++)
//		mexPrintf("%f ", ensemble.ba_x_2.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("ensemble_opt_ba_x_2 = [");
//	for (int i = 0; i < ensemble.opt_ba_x_2.cols ; i++)
//		mexPrintf("%f ", ensemble.opt_ba_x_2.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("ensemble_b_x_2 = [");
//	for (int i = 0; i < ensemble.b_x_2.cols ; i++)
//		mexPrintf("%f ", ensemble.b_x_2.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("ensemble_opt_b_x_2 = [");
//	for (int i = 0; i < ensemble.opt_b_x_2.cols ; i++)
//		mexPrintf("%f ", ensemble.opt_b_x_2.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("ensemble_D_x = [");
//	for (int i = 0; i < ensemble.D_x.rows ; i++)
//		mexPrintf("%f ", ensemble.D_x.at<double>(0,i));
//	mexPrintf("];\n");
//
//// Checking 'ensemble' double values ---- print the matlab syntax
//	
//	
//	mexPrintf("pi_x = %f; a_x = %f; b_x = %f; a_sigma  = %f; ba_sigma_2  = %f; opt_ba_sigma_2  = %f; b_sigma = %f; b_sigma_2 = %f; D_val = %f;\n",ensemble.pi_x, ensemble.a_x, ensemble.b_x, ensemble.a_sigma , ensemble.ba_sigma_2 , ensemble.opt_ba_sigma_2 , ensemble.b_sigma, ensemble.b_sigma_2, ensemble.D_val); 	
//	
//	
////  Checking 'grad' values --- print the matlab Syntax
//	//mexPrintf("rows %d cols %d",grad.x1.rows, grad.x1.cols);
//	mexPrintf("grad_x1 = [");
//	for (int i = 0; i < grad.x1.cols ; i++)
//		mexPrintf("%f ", grad.x1.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("grad_x2 = [");
//	double a = 1234;
//	for (int i = 0; i < grad.x2.cols ; i++) 
//		mexPrintf("%f ", grad.x2.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("grad_pi_x_2 = [");
//	for (int i = 0; i < grad.pi_x_2.cols ; i++)		   
//		mexPrintf("%f ", grad.pi_x_2.at<double>(0,i));		
//	mexPrintf("];\n");
//
//	mexPrintf("grad_ba_x_2 = [");
//	for (int i = 0; i < grad.ba_x_2.cols ; i++)
//		mexPrintf("%f ", grad.ba_x_2.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("grad_b_x_2 = [");
//	for (int i = 0; i < grad.b_x_2.cols ; i++)
//		mexPrintf("%f ", grad.b_x_2.at<double>(0,i));
//	mexPrintf("];\n");


	//vector<Mat> Hx_mx_mx2 = train_ensemble_rectified5_test_u(x1,x2,type);

	Ensemble t_ensemble;

	train_ensemble_evidence6_test_cuda_gpu(&t_ensemble,ensemble,&grad,step_len,dimensions,opt_func,direction,state,P1,P2,P3,P4,P5,P6,P7,P8,P9,P10,P11,P12);

 
	delete [] r;
	mxArray* mx_t_a_sigma       ;
	mxArray* mx_t_a_x           ;
	mxArray* mx_t_b_sigma       ;
	mxArray* mx_t_b_sigma_2     ;
	mxArray* mx_t_b_x           ;
	mxArray* mx_t_b_x_2         ;
	mxArray* mx_t_ba_sigma_2    ;
	mxArray* mx_t_ba_x_2        ;
	mxArray* mx_t_D_val         ;
	mxArray* mx_t_D_x           ;
	mxArray* mx_t_log_lambda_x  ;
	mxArray* mx_t_mx            ;
	mxArray* mx_t_mx2           ;
	mxArray* mx_t_opt_b_x_2     ;
	mxArray* mx_t_opt_ba_sigma_2;
	mxArray* mx_t_opt_ba_x_2    ;
	mxArray* mx_t_opt_pi_x_2    ;
	mxArray* mx_t_pi_x          ;
	mxArray* mx_t_pi_x_2        ;
	mxArray* mx_t_x1            ;
	mxArray* mx_t_x2            ;
	
	
	mxArray* mx_grad_x1            ;
	mxArray* mx_grad_x2            ;
	mxArray* mx_grad_b_x_2         ;
	mxArray* mx_grad_pi_x_2        ;
	mxArray* mx_grad_ba_x_2        ;

	mx_grad_x1       = mxCreateDoubleMatrix(grad.x1.size().height       ,grad.x1.size().width       ,mxREAL);
	mx_grad_x2       = mxCreateDoubleMatrix(grad.x2.size().height       ,grad.x2.size().width       ,mxREAL);
	mx_grad_b_x_2    = mxCreateDoubleMatrix(grad.b_x_2.size().height    ,grad.b_x_2.size().width    ,mxREAL);
	mx_grad_pi_x_2   = mxCreateDoubleMatrix(grad.pi_x_2.size().height   ,grad.pi_x_2.size().width   ,mxREAL);
	mx_grad_ba_x_2   = mxCreateDoubleMatrix(grad.ba_x_2.size().height   ,grad.ba_x_2.size().width   ,mxREAL);
	
	
	mx_t_a_sigma       = mxCreateDoubleMatrix(1,1,mxREAL);
	mx_t_a_x           = mxCreateDoubleMatrix(1,1,mxREAL);
	mx_t_b_sigma       = mxCreateDoubleMatrix(1,1,mxREAL);
	mx_t_b_sigma_2     = mxCreateDoubleMatrix(1,1,mxREAL);
	mx_t_b_x           = mxCreateDoubleMatrix(1,1,mxREAL);
	mx_t_b_x_2         = mxCreateDoubleMatrix(ensemble.b_x_2.rows,ensemble.b_x_2.cols,mxREAL);
	mx_t_ba_sigma_2    = mxCreateDoubleMatrix(1,1,mxREAL);
	mx_t_ba_x_2        = mxCreateDoubleMatrix(ensemble.ba_x_2.rows,ensemble.ba_x_2.cols,mxREAL);
	mx_t_D_val         = mxCreateDoubleMatrix(1,1,mxREAL);
	mx_t_D_x           = mxCreateDoubleMatrix(ensemble.D_x.rows,ensemble.D_x.cols,mxREAL);
	mx_t_log_lambda_x  = mxCreateDoubleMatrix(ensemble.log_lambda_x.rows,ensemble.log_lambda_x.cols,mxREAL);
	mx_t_mx            = mxCreateDoubleMatrix(ensemble.mx.rows,ensemble.mx.cols,mxREAL);
	mx_t_mx2           = mxCreateDoubleMatrix(ensemble.mx2.rows,ensemble.mx2.cols,mxREAL);
	mx_t_opt_b_x_2     = mxCreateDoubleMatrix(ensemble.opt_b_x_2.rows,ensemble.opt_b_x_2.cols,mxREAL);
	mx_t_opt_ba_sigma_2= mxCreateDoubleMatrix(1,1,mxREAL);
	mx_t_opt_ba_x_2    = mxCreateDoubleMatrix(ensemble.opt_ba_x_2.rows,ensemble.opt_ba_x_2.cols,mxREAL);
	mx_t_opt_pi_x_2    = mxCreateDoubleMatrix(ensemble.opt_pi_x_2.rows,ensemble.opt_pi_x_2.cols,mxREAL);
	mx_t_pi_x          = mxCreateDoubleMatrix(1,1,mxREAL);
	mx_t_pi_x_2        = mxCreateDoubleMatrix(ensemble.pi_x_2.rows,ensemble.pi_x_2.cols,mxREAL);
	mx_t_x1            = mxCreateDoubleMatrix(ensemble.x1.rows,ensemble.x1.cols,mxREAL);
	mx_t_x2            = mxCreateDoubleMatrix(ensemble.x2.rows,ensemble.x2.cols,mxREAL);

	mxSetFieldByNumber(plhs[0],0,num_a_sigma        ,mx_t_a_sigma        );
	mxSetFieldByNumber(plhs[0],0,num_a_x            ,mx_t_a_x            );
	mxSetFieldByNumber(plhs[0],0,num_b_sigma        ,mx_t_b_sigma        );
	mxSetFieldByNumber(plhs[0],0,num_b_sigma_2      ,mx_t_b_sigma_2      );
	mxSetFieldByNumber(plhs[0],0,num_b_x            ,mx_t_b_x            );
	mxSetFieldByNumber(plhs[0],0,num_b_x_2          ,mx_t_b_x_2          );
	mxSetFieldByNumber(plhs[0],0,num_ba_sigma_2     ,mx_t_ba_sigma_2     );
	mxSetFieldByNumber(plhs[0],0,num_ba_x_2         ,mx_t_ba_x_2         );
	mxSetFieldByNumber(plhs[0],0,num_D_val          ,mx_t_D_val          );
	mxSetFieldByNumber(plhs[0],0,num_D_x            ,mx_t_D_x            );
	mxSetFieldByNumber(plhs[0],0,num_log_lambda_x   ,mx_t_log_lambda_x   );
	mxSetFieldByNumber(plhs[0],0,num_mx             ,mx_t_mx             );
	mxSetFieldByNumber(plhs[0],0,num_mx2            ,mx_t_mx2            );
	mxSetFieldByNumber(plhs[0],0,num_opt_b_x_2      ,mx_t_opt_b_x_2      );
	mxSetFieldByNumber(plhs[0],0,num_opt_ba_sigma_2 ,mx_t_opt_ba_sigma_2 );
	mxSetFieldByNumber(plhs[0],0,num_opt_ba_x_2     ,mx_t_opt_ba_x_2     );
	mxSetFieldByNumber(plhs[0],0,num_opt_pi_x_2     ,mx_t_opt_pi_x_2     );
	mxSetFieldByNumber(plhs[0],0,num_pi_x           ,mx_t_pi_x           );
	mxSetFieldByNumber(plhs[0],0,num_pi_x_2         ,mx_t_pi_x_2         );
	mxSetFieldByNumber(plhs[0],0,num_x1             ,mx_t_x1             );
	mxSetFieldByNumber(plhs[0],0,num_x2             ,mx_t_x2             );
	

	mxSetFieldByNumber(plhs[1],0,grad_num_x1          ,mx_grad_x1          );
	mxSetFieldByNumber(plhs[1],0,grad_num_x2          ,mx_grad_x2          );
	mxSetFieldByNumber(plhs[1],0,grad_num_b_x_2       ,mx_grad_b_x_2       );
	mxSetFieldByNumber(plhs[1],0,grad_num_pi_x_2      ,mx_grad_pi_x_2      );
	mxSetFieldByNumber(plhs[1],0,grad_num_ba_x_2      ,mx_grad_ba_x_2      );
	
	*mxGetPr(mx_t_a_sigma       )= t_ensemble.a_sigma       ;
	*mxGetPr(mx_t_a_x           )= t_ensemble.a_x           ;
	*mxGetPr(mx_t_b_sigma       )= t_ensemble.b_sigma       ;
	*mxGetPr(mx_t_b_sigma_2     )= t_ensemble.b_sigma_2     ;
	*mxGetPr(mx_t_b_x           )= t_ensemble.b_x           ;

	memcpy(mxGetPr(mx_t_b_x_2         ), (double*)(t_ensemble.b_x_2.data), sizeof(double) * t_ensemble.b_x_2.size().height*t_ensemble.b_x_2.size().width);
	memcpy(mxGetPr(mx_t_ba_x_2        ),(double*)t_ensemble.ba_x_2.data,  sizeof(double) * t_ensemble.ba_x_2.size().height*t_ensemble.ba_x_2.size().width);
	memcpy(mxGetPr(mx_t_D_x           ),(double*)t_ensemble.D_x.data ,  sizeof(double) * t_ensemble.D_x.size().height*t_ensemble.D_x.size().width);
	memcpy(mxGetPr(mx_t_log_lambda_x  ),(double*)t_ensemble.log_lambda_x.data,sizeof(double)* t_ensemble.log_lambda_x.size().height*t_ensemble.log_lambda_x.size().width);
	memcpy(mxGetPr(mx_t_mx            ),(double*)t_ensemble.mx.data          ,sizeof(double)* t_ensemble.mx.size().height*t_ensemble.mx.size().width);
	memcpy(mxGetPr(mx_t_mx2           ),(double*)t_ensemble.mx2.data         ,sizeof(double)* t_ensemble.mx2.size().height*t_ensemble.mx2.size().width);
	memcpy(mxGetPr(mx_t_opt_b_x_2     ),(double*)t_ensemble.opt_b_x_2.data   ,sizeof(double)* t_ensemble.opt_b_x_2.size().height*t_ensemble.opt_b_x_2.size().width);
	memcpy(mxGetPr(mx_t_opt_ba_x_2    ),(double*)t_ensemble.opt_ba_x_2.data  ,sizeof(double)* t_ensemble.opt_ba_x_2.size().height*t_ensemble.opt_ba_x_2.size().width);
	memcpy(mxGetPr(mx_t_opt_pi_x_2    ),(double*)t_ensemble.opt_pi_x_2.data  ,sizeof(double)* t_ensemble.opt_pi_x_2.size().height*t_ensemble.opt_pi_x_2.size().width);
	memcpy(mxGetPr(mx_t_pi_x_2        ),(double*)t_ensemble.pi_x_2.data      ,sizeof(double)* t_ensemble.pi_x_2.size().height*t_ensemble.pi_x_2.size().width);
	memcpy(mxGetPr(mx_t_x1            ),(double*)t_ensemble.x1.data          ,sizeof(double)* t_ensemble.x1.size().height*t_ensemble.x1.size().width);
	memcpy(mxGetPr(mx_t_x2            ),(double*)t_ensemble.x2.data          ,sizeof(double)* t_ensemble.x2.size().height*t_ensemble.x2.size().width);


	*mxGetPr(mx_t_ba_sigma_2    )= t_ensemble.ba_sigma_2    ;
	*mxGetPr(mx_t_D_val         )= t_ensemble.D_val         ;
	*mxGetPr(mx_t_opt_ba_sigma_2)= t_ensemble.opt_ba_sigma_2;
	*mxGetPr(mx_t_pi_x          )= t_ensemble.pi_x          ;
	
	

	memcpy(mxGetPr(mx_grad_x1              ),(double*)grad.x1.data          ,sizeof(double)* grad.x1.size().height*grad.x1.size().width);
	memcpy(mxGetPr(mx_grad_x2              ),(double*)grad.x2.data          ,sizeof(double)* grad.x2.size().height*grad.x2.size().width);
	memcpy(mxGetPr(mx_grad_b_x_2           ),(double*)grad.b_x_2.data          ,sizeof(double)* grad.b_x_2.size().height*grad.b_x_2.size().width);
	memcpy(mxGetPr(mx_grad_pi_x_2          ),(double*)grad.pi_x_2.data          ,sizeof(double)* grad.pi_x_2.size().height*grad.pi_x_2.size().width);
	memcpy(mxGetPr(mx_grad_ba_x_2          ),(double*)grad.ba_x_2.data          ,sizeof(double)* grad.ba_x_2.size().height*grad.ba_x_2.size().width);
	
	
	
	
	
	
	
	//
	//
	//
	//// Checking 't_ensemble' Mat variables ---- print the matlab syntax
	//// Mat x1,x2,mx,mx2, log_lambda_x,pi_x_2,opt_pi_x_2,ba_x_2,opt_ba_x_2,b_x_2,opt_b_x_2,D_x;
	//mexPrintf("ensemble_x1 = [");
	//for (int i = 0; i < t_ensemble.x1.cols ; i++)
	//	mexPrintf("%f ", t_ensemble.x1.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("ensemble_x2 = [");
	//for (int i = 0; i < t_ensemble.x2.cols ; i++)
	//	mexPrintf("%f ", t_ensemble.x2.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("ensemble_mx = [");
	//for (int i = 0; i < t_ensemble.mx.cols ; i++)
	//	mexPrintf("%f ", t_ensemble.mx.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("ensemble_mx2 = [");
	//for (int i = 0; i < t_ensemble.mx2.cols ; i++)
	//	mexPrintf("%f ", t_ensemble.mx2.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("ensemble_log_lambda_x = [");
	//for (int i = 0; i < t_ensemble.log_lambda_x.cols ; i++)
	//	mexPrintf("%f ", t_ensemble.log_lambda_x.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("ensemble_pi_x_2 = [");
	//for (int i = 0; i < t_ensemble.pi_x_2.cols ; i++)
	//	mexPrintf("%f ", t_ensemble.pi_x_2.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("ensemble_opt_pi_x_2 = [");
	//for (int i = 0; i < t_ensemble.opt_pi_x_2.cols ; i++)
	//	mexPrintf("%f ", t_ensemble.opt_pi_x_2.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("ensemble_ba_x_2 = [");
	//for (int i = 0; i < t_ensemble.ba_x_2.cols ; i++)
	//	mexPrintf("%f ", t_ensemble.ba_x_2.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("ensemble_opt_ba_x_2 = [");
	//for (int i = 0; i < t_ensemble.opt_ba_x_2.cols ; i++)
	//	mexPrintf("%f ", t_ensemble.opt_ba_x_2.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("ensemble_b_x_2 = [");
	//for (int i = 0; i < t_ensemble.b_x_2.cols ; i++)
	//	mexPrintf("%f ", t_ensemble.b_x_2.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("ensemble_opt_b_x_2 = [");
	//for (int i = 0; i < t_ensemble.opt_b_x_2.cols ; i++)
	//	mexPrintf("%f ", t_ensemble.opt_b_x_2.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("ensemble_D_x = [");
	//for (int i = 0; i < t_ensemble.D_x.rows ; i++)
	//	mexPrintf("%f ", t_ensemble.D_x.at<double>(0,i));
	//mexPrintf("];\n");

	//// Checking 't_ensemble' double values ---- print the matlab syntax
	//mexPrintf("pi_x = %f; a_x = %f; b_x = %f; a_sigma  = %f; ba_sigma_2  = %f; opt_ba_sigma_2  = %f; b_sigma = %f; b_sigma_2 = %f; D_val = %f;\n",t_ensemble.pi_x, t_ensemble.a_x, t_ensemble.b_x, t_ensemble.a_sigma , t_ensemble.ba_sigma_2 , t_ensemble.opt_ba_sigma_2 , t_ensemble.b_sigma, t_ensemble.b_sigma_2, t_ensemble.D_val); 	

	////  Checking 'grad' values --- print the matlab Syntax
	////mexPrintf("rows %d cols %d",grad.x1.rows, grad.x1.cols);
	//mexPrintf("grad_x1 = [");
	//for (int i = 0; i < grad.x1.cols ; i++)
	//	mexPrintf("%f ", grad.x1.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("grad_x2 = [");
	//double a = 1234;
	//for (int i = 0; i < grad.x2.cols ; i++) 
	//	mexPrintf("%f ", grad.x2.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("grad_pi_x_2 = [");
	//for (int i = 0; i < grad.pi_x_2.cols ; i++)		   
	//	mexPrintf("%f ", grad.pi_x_2.at<double>(0,i));		
	//mexPrintf("];\n");

	//mexPrintf("grad_ba_x_2 = [");
	//for (int i = 0; i < grad.ba_x_2.cols ; i++)
	//	mexPrintf("%f ", grad.ba_x_2.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("grad_b_x_2 = [");
	//for (int i = 0; i < grad.b_x_2.cols ; i++)
	//	mexPrintf("%f ", grad.b_x_2.at<double>(0,i));
	//mexPrintf("];\n");


	//vector<Mat> Hx_mx_mx2 = train_t_ensemble_rectified5_test_u(x1,x2,type);
	//toc(); 

}

 