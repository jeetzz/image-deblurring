function nvmex(cuFileName,debug)
% function nvmex(cuFileName,other_obj,debug)
%NVMEX Compiles and links a CUDA file for MATLAB usage
% NVMEX(FILENAME) will create a MEX-File (also with the name FILENAME) by
% invoking the CUDA compiler, nvcc, and then linking with the MEX
% function in MATLAB.

% Copyright 2009 The MathWorks, Inc.

% cl /c train_blind_deconv_test_mult_cpu.cpp /IC:/OpenCV2.4/build/include  /I"C:\Program Files\MATLAB\R2012a/extern/include" /IC:\Boost\1.55.0\VC\10.0\x64

%     
if ispc % Windows
    LIB_Dir = '''-LC:\cuda\lib\x64'',''-LD:\OpenCV2.4\build\gpu\x64\vc10\lib'',''-LD:\OpenCV2.4\build\x64\vc10\lib';
    Host_Compiler_Location = '-ccbin "C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\bin"';
    LIBs =  '''-lcudart'', ''-lcufft'', ''-lopencv_core240'', ''-lopencv_gpu240'',''-lopencv_highgui240'', ''-lopencv_imgproc240''';
    INCLUDE_Dir = [' -I"' matlabroot '/extern/include" -ID:/OpenCV2.4/build/include'];
    
    [~, filename] = fileparts(cuFileName);
    
    % compile code generation
    nvccCommandLine = ['nvcc --compile ' cuFileName ' '...
        Host_Compiler_Location ' -o '...
        filename '.o '...
        INCLUDE_Dir ];
    
    % mex command genearation
    if ~debug
        mexCommandLine = ['mex (''' filename '.o'',' LIB_Dir ''',' LIBs ')'];
    else
        mexCommandLine = ['mex (''-g'',''' filename '.o'',' LIB_Dir ''',' LIBs ')'];
    end

%     mexCommandLine = ['mex (''' filename '.o'',''' other_obj ''',' LIB_Dir ''',' LIBs ')'];
    
    disp(nvccCommandLine);
    disp(mexCommandLine);
    
    % Compile
    status = system(nvccCommandLine);
    if status < 0
        error 'Error invoking nvcc';
    end
    
    % Run the mex command - linking
    eval(mexCommandLine);
    
end
end