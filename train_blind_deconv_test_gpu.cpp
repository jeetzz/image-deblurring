void gpu_fft(gpu::GpuMat *,gpu::GpuMat , int ,int ,gpu::Stream *);

gpu::GpuMat zeros_gpu;

void train_blind_deconv_test_gpu(Mat* dx1, Mat* dx2, double* rerror, int* datapoints,Mat dimensions,Ensemble ensemble,Mat D,Mat Dp,int I,int J,int K,int L,int M,int N){

	//[dx1,dx2,rerror,data_points]=train_blind_deconv(dimensions,ensemble,P1,P2,P3,P4,P5,P6,P7,P8);

	zeros_gpu.upload(Mat::zeros(Size(J,I), CV_32F));   // zeero to be used for all dfts
	
	//nPlanes = dimensions(3,1);
	//%=========================================================================
	//	% Copy the expectations
	//	%=========================================================================
	Mat* mmu;
	train_ensemble_get_test(&mmu,0,dimensions,ensemble.mx); //mmu  = train_ensemble_get_test(1,dimensions,ensemble.mx);
	Mat* me_temp;
	train_ensemble_get_test(&me_temp,1,dimensions,ensemble.mx);//me   = reshape(train_ensemble_get_test(2,dimensions,ensemble.mx),K,L);
	Mat me(K,L,CV_64F);	
	//Mat me = me_temp->reshape(K,L,CV_64F);
	for (int i = 0; i < K; i++){
		for (int j = 0; j < L; j++){
			me.at<double>(i,j) = (*me_temp).at<double>(0,K*j + i);
			}

		}
	me_temp->release();
	Mat temptemp3 = ensemble.mx2;
	Mat* mx_temp;
	train_ensemble_get_test(&mx_temp,2,dimensions,ensemble.mx);//mx   = reshape(train_ensemble_get_test(3,dimensions,ensemble.mx),M,N);
	//Mat mx = mx_temp->reshape(M,N);
	Mat mx(M,N,CV_64F);		 
	for (int i = 0; i < M; i++){
		for (int j = 0; j < N; j++){
			mx.at<double>(i,j) = (*mx_temp).at<double>(0,M*j+i);
			}
		}
	mx_temp->release();


	Mat* mmu2;
	train_ensemble_get_test(&mmu2,0,dimensions,ensemble.mx2);//mmu2 = train_ensemble_get_test(1,dimensions,ensemble.mx2);
	Mat* me2_temp;
	train_ensemble_get_test(&me2_temp,1,dimensions,ensemble.mx2);//me2  = reshape(train_ensemble_get_test(2,dimensions,ensemble.mx2),K,L);
	//me2->reshape(K,L);
	Mat me2(K,L,CV_64F);	
	//Mat me = me_temp->reshape(K,L,CV_64F);
	for (int i = 0; i < K; i++){
		for (int j = 0; j < L; j++){
			me2.at<double>(i,j) = (*me2_temp).at<double>(0,K*j+i);
			}

		}
	me2_temp->release();
	Mat* mx2_temp;
	train_ensemble_get_test(&mx2_temp,2,dimensions,ensemble.mx2); //mx2  = reshape(train_ensemble_get_test(3,dimensions,ensemble.mx2),M,N);
	//mx2->reshape(M,N); // mx2->reshape(0,M);
	Mat mx2(M,N,CV_64F);		 
	for (int i = 0; i < M; i++){
		for (int j = 0; j < N; j++){
			mx2.at<double>(i,j) = (*mx2_temp).at<double>(0,M*j+i);
			}

		}
	mx2_temp->release();

	//%=========================================================================
	 
	//	%=========================================================================
	//	% Evaluate useful FFTs
	//	%=========================================================================
	double fft_time = (double)getTickCount();	 

	gpu::Stream stream;

	
	Mat temp_mx;	mx.convertTo(temp_mx,CV_32FC2);	 
	gpu::GpuMat mx_gpu; stream.enqueueUpload(temp_mx,mx_gpu);	 	gpu::GpuMat complex_mx_gpu;	 
	gpu_fft(&complex_mx_gpu,mx_gpu,I,J,&stream);   // Do the dance - dft
	Mat temp_complexI(complex_mx_gpu.size(),complex_mx_gpu.type()); stream.enqueueDownload(complex_mx_gpu,temp_complexI);
	Mat fft_mx_temp[2];	 
	split(temp_complexI, fft_mx_temp);  // 0 - real , 1 - complex
	Mat fft_mx[2];
	fft_mx_temp[1] = -fft_mx_temp[1];
	fft_mx_temp[0].convertTo(fft_mx[0],CV_64F);	(fft_mx_temp[1]).convertTo(fft_mx[1],CV_64F);
	

	// fft_mx2  - checked
	Mat temp_mx2; 	mx2.convertTo(temp_mx2,CV_32FC2);
	gpu::GpuMat mx2_gpu; stream.enqueueUpload(temp_mx2,mx2_gpu); 	gpu::GpuMat complexI2_gpu;
	gpu_fft(&complexI2_gpu,mx2_gpu,I,J,&stream);
	Mat temp_complexI2(complexI2_gpu.size(),complexI2_gpu.type());;	stream.enqueueDownload(complexI2_gpu,temp_complexI2); 
	Mat fft_mx2_temp[2];
	split(temp_complexI2, fft_mx2_temp);  // 0 - real , 1 - complex
	Mat fft_mx2[2];
	fft_mx2_temp[1] = -fft_mx2_temp[1];
	fft_mx2_temp[0].convertTo(fft_mx2[0],CV_64F);	fft_mx2_temp[1].convertTo(fft_mx2[1],CV_64F);


	


	// fft_mx3  - checked
 	gpu::GpuMat mx3_gpu; gpu::GpuMat complexI3_gpu;
	gpu::pow(mx_gpu,2,mx3_gpu,stream);
	gpu_fft(&complexI3_gpu,mx3_gpu,I,J,&stream);
	Mat temp_complexI3(complexI3_gpu.size(),complexI3_gpu.type());;	stream.enqueueDownload(complexI3_gpu,temp_complexI3);
	Mat fft_mx3_temp[2];
	split(temp_complexI3, fft_mx3_temp);  // 0 - real , 1 - complex
	Mat fft_mx3[2];
	fft_mx3_temp[1] = -fft_mx3_temp[1];
	fft_mx3_temp[0].convertTo(fft_mx3[0],CV_64F);	fft_mx3_temp[1].convertTo(fft_mx3[1],CV_64F);

	// fft_me  - checked
	Mat temp_me; 	me.convertTo(temp_me,CV_32FC2);
	gpu::GpuMat me_gpu; stream.enqueueUpload(temp_me,me_gpu); 	gpu::GpuMat complex_me_gpu;
	gpu_fft(&complex_me_gpu,me_gpu,I,J,&stream);
	Mat temp_complexIe(complex_me_gpu.size(),complex_me_gpu.type());;	stream.enqueueDownload(complex_me_gpu,temp_complexIe); 
	Mat fft_me_temp[2] ;
	split(temp_complexIe, fft_me_temp);  // 0 - real , 1 - complex
	Mat fft_me[2];
	fft_me_temp[1] = -fft_me_temp[1];
	fft_me_temp[0].convertTo(fft_me[0],CV_64F);	fft_me_temp[1].convertTo(fft_me[1],CV_64F);

	// fft_me2  - checked
	Mat temp_me2; 	me2.convertTo(temp_me2,CV_32FC2);
	gpu::GpuMat me2_gpu; stream.enqueueUpload(temp_me2,me2_gpu); 	gpu::GpuMat complexIe2_gpu;
	gpu_fft(&complexIe2_gpu,me2_gpu,I,J,&stream);
	Mat temp_complexIe2(complexIe2_gpu.size(),complexIe2_gpu.type());; stream.enqueueDownload(complexIe2_gpu,temp_complexIe2); 
	Mat fft_me2_temp[2];
	split(temp_complexIe2, fft_me2_temp);  // 0 - real , 1 - complex
	Mat fft_me2[2];
	fft_me2_temp[1]= - fft_me2_temp[1];
	fft_me2_temp[0].convertTo(fft_me2[0],CV_64F);	fft_me2_temp[1].convertTo(fft_me2[1],CV_64F);

	// fft_me3
	gpu::GpuMat me3_gpu; gpu::GpuMat complexIe3_gpu;
	gpu::pow(me_gpu,2,me3_gpu,stream);
	gpu_fft(&complexIe3_gpu,me3_gpu,I,J,&stream);
	Mat temp_complexIe3(complexIe3_gpu.size(),complexIe3_gpu.type());;	stream.enqueueDownload(complexIe3_gpu,temp_complexIe3);  
	Mat fft_me3_temp[2];
	split(temp_complexIe3, fft_me3_temp);  // 0 - real , 1 - complex
	Mat fft_me3[2];
	fft_me3_temp[1]= -fft_me3_temp[1];
	fft_me3_temp[0].convertTo(fft_me3[0],CV_64F);	fft_me3_temp[1].convertTo(fft_me3[1],CV_64F);

	// fft_Dp
	Mat temp_Dp; 	Dp.convertTo(temp_Dp,CV_32FC2);
	gpu::GpuMat Dp_gpu; stream.enqueueUpload(temp_Dp,Dp_gpu);	gpu::GpuMat complexIDp_gpu;
	gpu_fft(&complexIDp_gpu,Dp_gpu,I,J,&stream);
	Mat temp_complexIDp(complexIDp_gpu.size(),complexIDp_gpu.type());; stream.enqueueDownload(complexIDp_gpu,temp_complexIDp); 	
	Mat fft_Dp_temp[2];
	split(temp_complexIDp, fft_Dp_temp);  // 0 - real , 1 - complex
	Mat fft_Dp[2];
	fft_Dp_temp[1] = -fft_Dp_temp[1];
	fft_Dp_temp[0].convertTo(fft_Dp[0],CV_64F);	fft_Dp_temp[1].convertTo(fft_Dp[1],CV_64F);


	// mD=real(ifft2(fft_mx.*fft_me));   ------- checked
	 
// 	gpu::GpuMat mD_fft_gpu[2];
// 
// 	//mD_fft[0] 
// 	gpu::GpuMat splitted_gpu[2]; gpu::GpuMat mult_gpu;
// 	gpu::multiply(complex_me_gpu,complex_mx_gpu,mult_gpu);
// 	gpu::split(mult_gpu,splitted_gpu); 	
// 	gpu::subtract(splitted_gpu[0],splitted_gpu[1],mD_fft_gpu[0]);
// 	Mat temp_mult;	mD_fft_gpu[0].download(temp_mult);
// 	 	
// 	//mD_fft[1]
// 	gpu::GpuMat swap_gpu[2],complexI_swapped_gpu;
// 	gpu::split(complex_mx_gpu,swap_gpu);
// 	gpu::GpuMat swap_temp = swap_gpu[0];
// 	swap_gpu[0] = swap_gpu[1];
// 	swap_gpu[1] = swap_temp;
// 	gpu::merge(swap_gpu,2,complexI_swapped_gpu);
// 	gpu::multiply(complex_me_gpu,complexI_swapped_gpu,mult_gpu);
// 	gpu::split(mult_gpu,splitted_gpu); 	
// 	gpu::add(splitted_gpu[0],splitted_gpu[1],mD_fft_gpu[1]);
// 	Mat temp_mult2;	mD_fft_gpu[1].download(temp_mult2);
// 
// 	//merging mD_fft and doing the idft
// 	Mat mD_fft_complex,mD,mD_fft_complex_32;
// 	gpu::GpuMat mD_fft_complex_gpu;
// 	gpu::merge(mD_fft_gpu,2,mD_fft_complex_gpu);
// 	mD_fft_complex_gpu.download(mD_fft_complex_32);
// 	mD_fft_complex_32.convertTo(mD_fft_complex,CV_64FC2);	
// 	Mat mD_fft[2];
// 	idft(mD_fft_complex,mD_fft_complex,DFT_SCALE);
// 	split(mD_fft_complex, mD_fft);  
// 	mD = mD_fft[0]; 
	
	Mat mD_fft[2];	
	mD_fft[0] = fft_me[0].mul(fft_mx[0]) - fft_me[1].mul(fft_mx[1]);
	mD_fft[1] = fft_me[0].mul(fft_mx[1]) + fft_me[1].mul(fft_mx[0]);
	Mat mD_fft_complex,mD;
	merge(mD_fft, 2, mD_fft_complex); 
	idft(mD_fft_complex,mD_fft_complex,DFT_SCALE);
	split(mD_fft_complex, mD_fft);  
	mD = mD_fft[0];

	//fft_err=fft2(Dp.*(D-mD-mmu),I,J);
	Mat tempD = D-mD- Mat::ones(Size(J,I), CV_64F)*mmu->at<double>(0,0);
	Mat err = Dp.mul( D-mD- Mat::ones(Size(J,I), CV_64F)*mmu->at<double>(0,0)  );
	
	Mat temp_err; 	err.convertTo(temp_err,CV_32FC2);
	gpu::GpuMat err_gpu; stream.enqueueUpload(temp_err,err_gpu);	gpu::GpuMat complexIerr_gpu;
	gpu_fft(&complexIerr_gpu,err_gpu,I,J,&stream);
	Mat temp_complexIerr(complexIerr_gpu.size(),complexIerr_gpu.type());	stream.enqueueDownload(complexIerr_gpu,temp_complexIerr);  
	Mat fft_err_temp[2];
	split(temp_complexIerr, fft_err_temp);  // 0 - real , 1 - complexI
	Mat fft_err[2];
	fft_err_temp[1] = -fft_err_temp[1];
	fft_err_temp[0].convertTo(fft_err[0],CV_64F);	fft_err_temp[1].convertTo(fft_err[1],CV_64F);

	stream.waitForCompletion();
	//=========================================================================

	//=========================================================================
	// Evaluate the reconstruction error
	//=========================================================================
	int	data_points= (int) sum(Dp)[0];
	(*datapoints)  = data_points;
	//mexPrintf("%d ",data_points);
	Mat tempDmDmmu;
	pow(D-mD- Mat::ones(Size(J,I), CV_64F)*mmu->at<double>(0,0),2,tempDmDmmu);

	// START FROM CODING tempfft. fft_me2 are arrays of MAT !!!
	//Mat tempfft = fft_me2.mul(fft_mx2)-fft_me3.mul(fft_mx3);


	Mat term1[] = {Mat::zeros(Size(J,I), CV_64F), Mat::zeros(Size(J,I), CV_64F)};	
	Mat term2[] = {Mat::zeros(Size(J,I), CV_64F), Mat::zeros(Size(J,I), CV_64F)};	
	Mat term[] = {Mat::zeros(Size(J,I), CV_64F), Mat::zeros(Size(J,I), CV_64F)};	

	term1[0] = fft_me2[0].mul(fft_mx2[0]) - fft_me2[1].mul(fft_mx2[1]);
	term1[1] = fft_me2[0].mul(fft_mx2[1]) + fft_me2[1].mul(fft_mx2[0]);

	term2[0] = fft_me3[0].mul(fft_mx3[0]) - fft_me3[1].mul(fft_mx3[1]);
	term2[1] = fft_me3[0].mul(fft_mx3[1]) + fft_me3[1].mul(fft_mx3[0]);

	term[0] = term1[0] - term2[0];
	term[1] = term1[1] - term2[1];


	Mat term_complex,real_ifft_term;
	merge(term, 2, term_complex); 
	idft(term_complex,term_complex,DFT_SCALE);
	split(term_complex, term);  
	real_ifft_term = term[0];

	// error=sum(sum(Dp.*((D-mD-mmu).^2+real(ifft2(fft_me2.*fft_mx2-fft_me3.*fft_mx3)))))+data_points*(mmu2-mmu^2);
	double error_term = sum(Dp.mul(tempDmDmmu + real_ifft_term))[0] + data_points*(mmu2->at<double>(0,0) - (mmu->at<double>(0,0))*(mmu->at<double>(0,0))) ; 
	(*rerror) = error_term;
	mmu->release();
	mmu2->release();
	//=========================================================================

	//=========================================================================
	//Evaluate the optimal distributions for each parameter
	//=========================================================================
	double mu1 = sum(Dp.mul(D-mD))[0];
	int mu2 = data_points;

	//e1=real(ifft2(fft_err.*conj(fft_mx)));    - checked
	Mat e1_fft[] = {Mat::zeros(Size(J,I), CV_64F), Mat::zeros(Size(J,I), CV_64F)};	
	e1_fft[0] = fft_err[0].mul(fft_mx[0]) + fft_err[1].mul(fft_mx[1]);
	e1_fft[1] = -fft_err[0].mul(fft_mx[1]) + fft_err[1].mul(fft_mx[0]);	
	Mat e1_complex,e1;
	merge(e1_fft, 2, e1_complex); 
	idft(e1_complex,e1_complex,DFT_SCALE);
	split(e1_complex, e1_fft);  
	e1 = e1_fft[0]; 

	//corr=real(ifft2(fft_Dp.*conj(fft_mx3)));  - checked
	Mat corr_fft[] = {Mat::zeros(Size(J,I), CV_64F), Mat::zeros(Size(J,I), CV_64F)};	
	corr_fft[0] = fft_Dp[0].mul(fft_mx3[0]) + fft_Dp[1].mul(fft_mx3[1]);
	corr_fft[1] = -fft_Dp[0].mul(fft_mx3[1]) + fft_Dp[1].mul(fft_mx3[0]);
	Mat corr_complex,corr;
	merge(corr_fft, 2, corr_complex); 
	idft(corr_complex,corr_complex,DFT_SCALE);
	split(corr_complex, corr_fft);  
	corr = corr_fft[0];



	// e1(1:K,1:L)=e1(1:K,1:L)+me.*corr(1:K,1:L);
	for (int i = 0; i < K ; i++)
		for (int j  = 0 ; j < L ; j++)
			e1.at<double>(i,j) = e1.at<double>(i,j) + me.at<double>(i,j)*corr.at<double>(i,j);



	//e2=real(ifft2(fft_Dp.*conj(fft_mx2)));
	Mat e2_fft[] = {Mat::zeros(Size(J,I), CV_64F), Mat::zeros(Size(J,I), CV_64F)};	
	e2_fft[0] = fft_Dp[0].mul(fft_mx2[0]) + fft_Dp[1].mul(fft_mx2[1]);
	e2_fft[1] = -fft_Dp[0].mul(fft_mx2[1]) + fft_Dp[1].mul(fft_mx2[0]);	
	Mat e2_complex,e2;
	merge(e2_fft, 2, e2_complex); 
	idft(e2_complex,e2_complex,DFT_SCALE);
	split(e2_complex, e2_fft);  
	e2 = e2_fft[0];

	//x1=real(ifft2(fft_err.*conj(fft_me)));
	Mat x1_fft[] = {Mat::zeros(Size(J,I), CV_64F), Mat::zeros(Size(J,I), CV_64F)};	
	x1_fft[0] = fft_err[0].mul(fft_me[0]) + fft_err[1].mul(fft_me[1]);
	x1_fft[1] = -fft_err[0].mul(fft_me[1]) + fft_err[1].mul(fft_me[0]);	
	Mat x1_complex,x1;
	merge(x1_fft, 2, x1_complex); 
	idft(x1_complex,x1_complex,DFT_SCALE);
	split(x1_complex, x1_fft);  
	x1 = x1_fft[0];

	//corr=real(ifft2(fft_Dp.*conj(fft_me3)));
	Mat corr2_fft[] = {Mat::zeros(Size(J,I), CV_64F), Mat::zeros(Size(J,I), CV_64F)};	
	corr2_fft[0] = fft_Dp[0].mul(fft_me3[0]) + fft_Dp[1].mul(fft_me3[1]);
	corr2_fft[1] = -fft_Dp[0].mul(fft_me3[1]) + fft_Dp[1].mul(fft_me3[0]);
	Mat corr2_complex;
	merge(corr2_fft, 2, corr2_complex); 
	idft(corr2_complex,corr2_complex,DFT_SCALE);
	split(corr2_complex, corr2_fft);  
	corr = corr2_fft[0];


	//x1(1:M,1:N)=x1(1:M,1:N)+mx.*corr(1:M,1:N);
	for (int i = 0; i < M ; i++)
		for (int j  = 0 ; j < N ; j++)
			x1.at<double>(i,j) =x1.at<double>(i,j) + mx.at<double>(i,j)*corr.at<double>(i,j);

	//x2=real(ifft2(fft_Dp.*conj(fft_me2)));
	Mat x2_fft[] = {Mat::zeros(Size(J,I), CV_64F), Mat::zeros(Size(J,I), CV_64F)};	
	x2_fft[0] = fft_Dp[0].mul(fft_me2[0]) + fft_Dp[1].mul(fft_me2[1]);
	x2_fft[1] = -fft_Dp[0].mul(fft_me2[1]) + fft_Dp[1].mul(fft_me2[0]);	
	Mat x2_complex,x2;
	merge(x2_fft, 2, x2_complex); 
	idft(x2_complex,x2_complex,DFT_SCALE);
	split(x2_complex, x2_fft);  
	x2 = x2_fft[0];



	fft_time = (double)getTickCount() - fft_time;	
	fft_time = fft_time/cv::getTickFrequency(); 
	//printf("fft time %.4f\n ",fft_time );






	//=========================================================================

	//=========================================================================
	//Return the optimal distributions
	//=========================================================================
	//	x1=[mu1 reshape(e1(1:K,1:L),1,K*L) reshape(x1(1:M,1:N),1,M*N)];
	dx1->at<double>(0,0) = mu1;
	for (int i = 1;i < K*L+1 ; i++)
		dx1->at<double>(0,i)  =e1.at<double>( (i-1)%K, (int)floor((double)(i-1)/K ));   
	for (int i = K*L+1; i < M*N + K*L + 1 ; i++)
		dx1->at<double>(0,i)  =x1.at<double>( (i-1 -K*L)%M, (int)floor((double)(i-1- K*L)/M )); 		

	//x2=[mu2 reshape(e2(1:K,1:L),1,K*L) reshape(x2(1:M,1:N),1,M*N)];
	dx2->at<double>(0,0) = mu2;
	for (int i = 1;i < K*L+1 ; i++)
		dx2->at<double>(0,i)  =e2.at<double>( (i-1)%K, (int)floor((double)(i-1)/K ));
	for (int i = K*L+1; i < M*N + K*L + 1 ; i++)
		dx2->at<double>(0,i)  =x2.at<double>( (i-1 -K*L)%M, (int)floor((double)(i-1- K*L)/M ));

	//=========================================================================
	mexPrintf(".");
// 	mexPrintf("gpu\n");
// 	for (int k = 0; k < dx1->cols; k++)
// 		mexPrintf("%f , ",dx1->at<double>(0,k));
// 	mexPrintf("\n");
// 	for (int k = 0; k < dx2->cols; k++)
// 		mexPrintf("%f , ",dx2->at<double>(0,k));
	}




void gpu_fft(gpu::GpuMat *complexI_gpu_return,gpu::GpuMat mx_gpu, int I,int J,gpu::Stream *stream){

	//**** GPU start ******
// 	 
 	gpu::GpuMat fft_mx_padded_gpu;

	 
	gpu::copyMakeBorder(mx_gpu, fft_mx_padded_gpu, 0,I - mx_gpu.rows,0,J - mx_gpu.cols , BORDER_CONSTANT, Scalar::all(0),*stream);
	
	gpu::GpuMat fft_mx_gpu[] =  { fft_mx_padded_gpu, zeros_gpu}; 
	gpu::GpuMat complexI_gpu;
	gpu::merge(fft_mx_gpu,2,complexI_gpu,*stream);
    
	
	gpu::dft(complexI_gpu, complexI_gpu,complexI_gpu.size(),*stream);         // fft_mx =fft2(mx,I,J);         // Do the dft
	
	*complexI_gpu_return = complexI_gpu;
	
    // Need to make them all float !!!
	// **** GPU end ******

	}