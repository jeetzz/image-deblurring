

void train_ensemble_get_test(gpu::GpuMat**cx, int c,gpu::GpuMat dimensions, gpu::GpuMat x)
{


	int start;
	if(c>0) {

		gpu::GpuMat temp1(c,1,CV_64F); // Rect(x, y, width, height);
		gpu::GpuMat temp2(c,1,CV_64F); // Rect(x, y, width, height);
		for (int i=0;i<c;i++)
		{
			temp1.at<double>(i,0) = dimensions.at<int>(i,0);
			temp2.at<double>(i,0) = dimensions.at<int>(i,1);

		}
		start = (int)temp1.dot(temp2);
	}
	else{
		start = 0;
	}
	//mexPrintf("c=%d, len=%d\n",c,start+(int)dimensions.at<int>(c,0)*(int)dimensions.at<int>(c,1));
	
	//*cx = new Mat(0,0,CV_64F);
	*cx = new gpu::GpuMat(x,Rect(start,0,(int)dimensions.at<int>(c,0)*(int)dimensions.at<int>(c,1),1));
	//(*cx)->reshape(0,dimensions.at<int>(c,0));
}