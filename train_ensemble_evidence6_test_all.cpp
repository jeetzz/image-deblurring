#ifndef DEBLUR_INCLUDE_FILE_H
#define DEBLUR_INCLUDE_FILE_H

 #include <sdkddkver.h>   // majic header file

#include <ctime>
#include <stack>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <matrix.h>
#include <math.h>
#include <complex> 
#include <vector>
#include <float.h>
#include <opencv2/gpu/gpu.hpp>

#define PI       3.14159265358979323846

//#include <cstringt.h>
#include <mex.h>

using namespace std;
using namespace cv;


// CPU variables
typedef struct {
	Mat x1,x2,mx,mx2, log_lambda_x,pi_x_2,opt_pi_x_2,ba_x_2,opt_ba_x_2,b_x_2,opt_b_x_2,D_x;
	double pi_x, a_x, b_x, a_sigma , ba_sigma_2 , opt_ba_sigma_2 , b_sigma, b_sigma_2, D_val; 	  
	}Ensemble;

typedef struct{
	Mat x1,x2,pi_x_2,ba_x_2,b_x_2;
	} Direction;

typedef struct{
	Mat pi;
	Mat gamma;	
	}Priors;

// GPU variables
typedef struct {
	gpu::GpuMat x1,x2,mx,mx2, log_lambda_x,pi_x_2,opt_pi_x_2,ba_x_2,opt_ba_x_2,b_x_2,opt_b_x_2,D_x;
	double pi_x, a_x, b_x, a_sigma , ba_sigma_2 , opt_ba_sigma_2 , b_sigma, b_sigma_2, D_val; 
	}gpuEnsemble;

typedef struct{
	gpu::GpuMat x1,x2,pi_x_2,ba_x_2,b_x_2;
	} gpuDirection;

typedef struct{
	gpu::GpuMat pi;
	gpu::GpuMat gamma;	
	}gpuPriors;



void train_ensemble_main6_test(Mat dimensions,Mat initial_x1,Mat initial_x2,char* opt_func,char* text,double options[],Mat P1,Mat P2 ,int P3,int P4,int P5,int P6,int P7,int P8,Priors P9 ,int P10,Mat P11 ,Mat P12);
void train_ensemble_evidence6_test(Ensemble* t_ensemble,Ensemble ensemble,Direction* grad,double step_len,Mat dimensions,char* opt_func,Direction direction,int state,Mat P1,Mat P2,int P3,int P4,int P5,int P6,int P7,int P8,Priors P9,int P10,Mat P11,Mat P12);
void train_ensemble_get_test(Mat** cx, int c, Mat dimensions, Mat x);
void train_ensemble_rectified5_test_u(Mat** Hx, Mat** mx, Mat** mx2, Mat x1, Mat x2 , int type);
void train_ensemble_put_lambda_test(Mat*log_lambda_x, int c, Mat dimensions, Mat c_log_lambda_x);
void train_ensemble_put_test(Mat*x, int c, Mat dimensions, Mat cx);
void train_blind_deconv_test(Mat* dx1, Mat* dx2, double* rerror, int* datapoints,Mat dimensions,Ensemble ensemble,Mat D,Mat Dp,int I,int J,int K,int L,int M,int N);
void train_blind_deconv_test_mult_cpu(Mat* dx1, Mat* dx2, double* rerror, int* datapoints,Mat dimensions,Ensemble ensemble,Mat D,Mat Dp,int I,int J,int K,int L,int M,int N);
//void train_blind_deconv_test_gpu(Mat* dx1, Mat* dx2, double* rerror, int* datapoints,Mat dimensions,Ensemble ensemble,Mat D,Mat Dp,int I,int J,int K,int L,int M,int N);
//void train_blind_deconv_test_cuda_gpu(Mat* dx1, Mat* dx2, double* rerror, int* datapoints,Mat dimensions,Ensemble ensemble,Mat D,Mat Dp,int I,int J,int K,int L,int M,int N);

//void train_blind_deconv_test_gpu(gpu::GpuMat* dx1, gpu::GpuMat* dx2, double* rerror, int* datapoints,gpu::GpuMat dimensions,gpuEnsemble ensemble,gpu::GpuMat D,gpu::GpuMat Dp,int I,int J,int K,int L,int M,int N);

double lgamma_old(double x);

// Test functions for kernels
 
#include "lgamma.cpp" 
#include "timing.cpp"
#include "train_ensemble_evidence6_test.cpp" 
#include "train_blind_deconv_test.cpp" 
// #include "train_blind_deconv_test_gpu.cpp" 
// #include "train_blind_deconv_test_cuda_gpu.cu" 
#include "train_blind_deconv_test_mult_cpu.cpp" 
#include "train_ensemble_put_lambda_test.cpp" 
#include "train_ensemble_rectified5_test_u.cpp" 
#include "train_ensemble_get_test.cpp" 
#include "train_ensemble_put_test.cpp" 
#include "Faddeeva.cc" 


//#include "demo_kernel.cu"


#endif