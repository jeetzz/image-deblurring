#include "deblur_include_file.h"
void train_ensemble_main6_test(Mat dimensions,Mat initial_x1,Mat initial_x2,char* opt_func,char* text,double options[],Mat P1,Mat P2 ,int P3,int P4,int P5,int P6,int P7,int P8,Priors P9 ,int P10,Mat P11 ,Mat P12)		{



		int Niter= (int) options[5];

		Mat D_log= Mat::ones(2,Niter,CV_64F)*numeric_limits<double>::quiet_NaN( ); // NaN  
		Mat gamma_log= Mat::ones(1,Niter,CV_64F)*numeric_limits<double>::quiet_NaN( ); // NaN

		// Not necessary here. Checks the existance of parameters. It's method overloading. So if we encounter such case, we'll have to write a new method
		// if ~exist('P11')
		//      P11 = [];
		// end

		// if ~exist('P12')
		//      P12 = [];
		// end

		// Create the ensemble parameters

		Mat temp1(dimensions,Rect(0,0,1,3));  // Rect(x, y, width, height);
		Mat temp2(dimensions,Rect(1,0,1,3));
		Mat temp3(dimensions,Rect(2,0,1,3));

		Mat mult12 = temp1.t()*temp2;
		Mat mult13 = temp1.t()*temp3;
		Mat mult123 = temp1.mul(temp2).mul(temp3);

		int size12 = (int)mult12.at<double>(0,0);
		int size13 = (int)mult13.at<double>(0,0);
		int size123 = 0;
		for (int i = 0; i < mult123.rows; i++)
			size123 = size123 + (int)mult123.at<double>(i,0);
		int size1 = 0;
		for (int i = 0; i < temp1.rows; i++)
			size1 = size1 + (int)temp1.at<double>(i,0);

		Mat randn_(1,size12,CV_64F);
		Mat rand_(1,size12,CV_64F);

		randn(randn_, 0, 1);

		Ensemble ensemble;

		ensemble.x1 =  randn_*20000;               // 'x1' ,1e4*randn(1,dimensions(:,1)'*dimensions(:,2)).*ceil(rand(1,dimensions(:,1)'*dimensions(:,2))*2),...
		ensemble.x2 = Mat :: ones(1,size12,CV_64F)*10000; // 'x2',1e4*ones(1,dimensions(:,1)'*dimensions(:,2)),...
		ensemble.mx = Mat :: zeros(1,size12,CV_64F);      // 'mx' ,zeros(1,dimensions(:,1)'*dimensions(:,2)),...
		ensemble.mx2 =  Mat :: zeros(1,size12,CV_64F);    // 'mx2',zeros(1,dimensions(:,1)'*dimensions(:,2)),...
		ensemble.log_lambda_x = Mat :: zeros(1,size123,CV_64F);   // 'log_lambda_x',zeros(1,sum(dimensions(:,1).*dimensions(:,2).*dimensions(:,3))),..
		ensemble.pi_x_2 =  Mat :: ones(1,size13,CV_64F);  // 'pi_x_2',ones(1,dimensions(:,1)'*dimensions(:,3)),...
		ensemble.opt_pi_x_2 = Mat :: ones(1,size13,CV_64F); // 'opt_pi_x_2',ones(1,dimensions(:,1)'*dimensions(:,3)),...
		ensemble.ba_x_2 = Mat :: ones(1,size13,CV_64F);   // 'ba_x_2' ,ones(1,dimensions(:,1)'*dimensions(:,3)),...
		ensemble.opt_ba_x_2 = Mat :: ones(1,size13,CV_64F); // 'opt_ba_x_2' ,ones(1,dimensions(:,1)'*dimensions(:,3)),...
		ensemble.b_x_2 = Mat :: ones(1,size13,CV_64F);    // 'b_x_2' ,ones(1,dimensions(:,1)'*dimensions(:,3)),...
		ensemble.opt_b_x_2 = Mat :: ones(1,size13,CV_64F);// 'opt_b_x_2' ,ones(1,dimensions(:,1)'*dimensions(:,3)),...
		ensemble.D_x  = Mat :: zeros(size1+1,1,CV_64F);     // 'D_x',zeros(sum(dimensions(:,1))+1,1));

		ensemble.pi_x = 1;        // 'pi_x',1,...	 
		ensemble.a_x = 0.001;     // 'a_x',1e-3,...
		ensemble.b_x = 0.001;     // 'b_x',1e-3,...	
		ensemble.a_sigma = 0.001; // 'a_sigma',1e-3,...
		ensemble.ba_sigma_2 = 0;  // 'ba_sigma_2',0,...
		ensemble.opt_ba_sigma_2 = 0;// 'opt_ba_sigma_2',0,...
		ensemble.b_sigma  = 0.001;// 'b_sigma',1e-3,...
		ensemble.b_sigma_2 = 0;   // 'b_sigma_2',0,...
		ensemble.D_val = 0;       // 'D_val',0,...


		Direction direction;
		direction.x1 = Mat :: zeros(1,size12,CV_64F);     //('x1' ,zeros(1,dimensions(:,1)'*dimensions(:,2)),...
		direction.x2 = Mat :: zeros(1,size12,CV_64F);     // 'x2' ,zeros(1,dimensions(:,1)'*dimensions(:,2)),...
		direction.pi_x_2 = Mat :: zeros(1,size13,CV_64F); // 'pi_x_2',zeros(1,dimensions(:,1)'*dimensions(:,3)),...
		direction.b_x_2 = Mat :: zeros(1,size13,CV_64F); // 'ba_x_2' ,zeros(1,dimensions(:,1)'*dimensions(:,3)),...
		direction.ba_x_2 = Mat :: zeros(1,size13,CV_64F); // 'b_x_2' ,zeros(1,dimensions(:,1)'*dimensions(:,3)));


		// %Set the initial values of the distributions
		if (initial_x1.cols > 0)
			ensemble.x1    =initial_x1;

		if (initial_x2.cols > 0)
			ensemble.x2    =initial_x2;



		// Make sure no parameters are set to zero initially 

		for (int i = 0 ; i < ensemble.x1.cols ; i++) 
			if (ensemble.x1.at<double>(0,i) == 0)
				ensemble.x1.at<double>(0,i)= 1.0e-16;

		for (int i = 0 ; i < ensemble.x2.cols ; i++) 
			if (ensemble.x2.at<double>(0,i) == 0)
				ensemble.x2.at<double>(0,i)= 1.0e-16;

		int state=1;
		int last_change_iter=-1;
		double alpha=1.0;
		double beta=0.9;

		//Copy options
		double  converge_criteria= options[0];
		int plot_step= (int) options[1];
		ensemble.ba_sigma_2=  (int)pow((double)options[2],-2);
		int restart_priors= (int) options[3];
		int restart_switched_off= (int) options[4];

		double step_len;
		//Iterate algorithm to improve the approximating ensemble
		double oD_val=numeric_limits<double>::quiet_NaN();
		for (int iter=0; iter < Niter ; iter++)
			{


			// Re-evaluate after a state change
			if (iter==last_change_iter+1)
				{
				if (state<3)
					{
					//Evaluate the model before updating the priors
					step_len=0.0;
					Direction grad;
					 
					train_ensemble_evidence6_test(&ensemble,&grad,step_len,dimensions,opt_func,direction,state,P1,P2,P3,P4,P5,P6,P7,P8,P9,P10,P11,P12);
					  
					//direction=grad;
					direction.x1 = grad.x1.clone();
					direction.x2 = grad.x2.clone();
					direction.pi_x_2 = grad.pi_x_2.clone() ; // 'pi_x_2',zeros(1,dimensions(:,1)'*dimensions(:,3)),...
					direction.b_x_2  = grad.b_x_2.clone(); // 'ba_x_2' ,zeros(1,dimensions(:,1)'*dimensions(:,3)),...
					direction.ba_x_2 = grad.ba_x_2.clone(); // 'b_x_2' ,zeros(1,dimensions(:,1)'*dimensions(:,3)));


					//Set the priors to their optimal values
					ensemble.pi_x_2 = ensemble.opt_pi_x_2;
					ensemble.b_x_2 =ensemble.opt_b_x_2;
					ensemble.ba_x_2=ensemble.opt_ba_x_2;

					//Re-initialise priors that have coallesced
					int ptr=0;
					for (int c=0 ; c < dimensions.rows ; c++)
						{
						// Only need to check if there is a mixture
						if (dimensions.at<double>(c,2)>1)
							{
							
							//  c_pi_x_2=reshape(ensemble.pi_x_2(ptr+1:ptr+dimensions(c,1)*dimensions(c,3)),dimensions(c,1),dimensions(c,3));
							//  c_b_x_2=reshape(ensemble.b_x_2(ptr+1:ptr+dimensions(c,1)*dimensions(c,3)),dimensions(c,1),dimensions(c,3));
							//  c_ba_x_2=reshape(ensemble.ba_x_2(ptr+1:ptr+dimensions(c,1)*dimensions(c,3)),dimensions(c,1),dimensions(c,3));

							  Mat c_pi_temp(ensemble.pi_x_2 ,Rect(ptr,0,ptr+(int)dimensions.at<double>(c,0)*dimensions.at<double>(c,2),1) );						   
						      Mat c_pi_x_2= c_pi_temp.reshape( (int)dimensions.at<double>(c,0),(int)dimensions.at<double>(c,2));

							  Mat c_b_temp(ensemble.b_x_2 ,Rect(ptr,0,ptr+(int)dimensions.at<double>(c,0)*dimensions.at<double>(c,2),1) );
							  Mat  c_b_x_2= c_b_temp.reshape( (int)dimensions.at<double>(c,0),(int)dimensions.at<double>(c,2));

							  Mat c_ba_temp(ensemble.b_x_2 ,Rect(ptr,0,ptr+(int)dimensions.at<double>(c,0)*dimensions.at<double>(c,2),1) );
							  Mat  c_ba_x_2= c_ba_temp.reshape( (int)dimensions.at<double>(c,0),(int)dimensions.at<double>(c,2));

							 
							  for (int k = 0;k< (int)dimensions.at<double>(c,0);k++)  //k=1:dimensions(c,1)
								  {							
								  // %Check for each distribution type for coallescence
								  if (dimensions.at<double>(c,3) == 0 || dimensions.at<double>(c,3) == 1 || dimensions.at<double>(c,3) == 2)
									  {
									  //  Gaussian or Exponential
									  Mat  c_ba_x_2_temp_sorted(c_ba_x_2,Rect(0,k,c_ba_x_2.cols,1));
									  Mat  sorted_scales;  //  sorted_scales=sort(c_ba_x_2(k,:));
									  cv::sort(c_ba_x_2_temp_sorted,sorted_scales,CV_SORT_ASCENDING);

									  // 2nd Argument calculation  -  any(c_pi_x_2(k,:)<ensemble.pi_x+1/dimensions(c,3))
									  boolean arg2 = false;
									  Mat  c_pi_x_2_temp_2(c_pi_x_2,Rect(0,k,c_pi_x_2.cols,1));
									  
									  double threshold_arg2 = (ensemble.pi_x+1)/dimensions.at<double>(c,2);
									  for (int j = 0; j < c_pi_x_2_temp_2.cols ; j++ )
										  {
										  if (c_pi_x_2_temp_2.at<double>(0,j) < threshold_arg2)
											  {
											      arg2 = true;
											      break;
											  }
										  }
									 
									  // 3rd Argument caclculation - any(sorted_scales(2:end)<1.5*sorted_scales(1:end-1))
									  boolean arg3 = false;
									  for (int j = 1; j < sorted_scales.cols ; j++){
										  if (sorted_scales.at<double>(0,j) < 1.5*sorted_scales.at<double>(0,j-1))
											  {
											       arg3 = true;
												   break;
											  }
									  }

									  if (restart_priors || arg2 || arg3)
										  {
										  // If any component has approximately zero weight or if any two have approximately the same scale, they should be
										  // restarted

										  Mat temp_sum_1(c_b_x_2,Rect(0,k,c_b_x_2.cols,1));
										  Mat temp_sum_2(c_ba_x_2,Rect(0,k,c_ba_x_2.cols,1));

										  double sum_denominator = sum(temp_sum_1)[0];
										  double sum_numerator   = sum(temp_sum_1.mul(temp_sum_2))[0];
										  double mean_scale=sum_numerator/sum_denominator;

										  for (int j = 0; j < c_pi_x_2.cols ; j++)  //  c_pi_x_2(k,:)=ensemble.pi_x+dimensions(c,2)/dimensions(c,3);
											  {
											  c_pi_x_2.at<double>(k,j)=ensemble.pi_x+ dimensions.at<double>(c,1)/dimensions.at<double>(c,2);
											  }

										  for (int j = 0; j < c_pi_x_2.cols ; j++)  //  c_b_x_2(k,:) =ensemble.b_x +dimensions(c,2)/dimensions(c,3);
											  {
											  c_b_x_2.at<double>(k,j)=ensemble.b_x+ dimensions.at<double>(c,1)/dimensions.at<double>(c,2);
											  }

										 
										  //c_ba_x_2(k,:)=c_b_x_2(k,:)./(ensemble.a_x+0.5*(1:dimensions(c,3))*mean_scale*dimensions(c,2)/dimensions(c,3));
										  for (int j = 0; j < (int)dimensions.at<double>(c,2) ; j++)
											  {
											  c_ba_x_2.at<double>(0,j) = c_b_x_2.at<double>(0,j)/(ensemble.a_x +  0.5*(j+1)*mean_scale*dimensions.at<double>(c,1)/dimensions.at<double>(c,2));
											  }										  
										  }//end		
									  }
								  else if (dimensions.at<double>(c,3)==3 || dimensions.at<double>(c,3)==4)
									  {
									  // Discrete so no prior properties
									  }// end
								  } // end

							  // Store the new parameters
							  Mat temp_reshape_c_pi_x_2 = c_pi_x_2.reshape(1, (int)dimensions.at<double>(c,0)*(int)dimensions.at<double>(c,2));
							  Mat temp_reshape_c_b_x_2    =  c_b_x_2.reshape(1, (int)dimensions.at<double>(c,0)*(int)dimensions.at<double>(c,2));
							  Mat temp_reshape_c_ba_x_2   = c_ba_x_2.reshape(1, (int)dimensions.at<double>(c,0)*(int)dimensions.at<double>(c,2));


							  for (int j = ptr ; j < (int)(ptr+dimensions.at<double>(c,0)*dimensions.at<double>(c,2)) ; j ++)
								  {
								    ensemble.pi_x_2.at<double>(0,j) = temp_reshape_c_pi_x_2.at<double>(0,j); //ensemble.pi_x_2(ptr+1:ptr+dimensions(c,1)*dimensions(c,3))=reshape(c_pi_x_2,1,dimensions(c,1)*dimensions(c,3));
									ensemble.b_x_2.at<double>(0,j)  = temp_reshape_c_b_x_2.at<double>(0,j);    //ensemble.b_x_2(ptr+1:ptr+dimensions(c,1)*dimensions(c,3)) =reshape(c_b_x_2,1,dimensions(c,1)*dimensions(c,3));
									ensemble.ba_x_2.at<double>(0,j) = temp_reshape_c_ba_x_2.at<double>(0,j);   //ensemble.ba_x_2(ptr+1:ptr+dimensions(c,1)*dimensions(c,3))=reshape(c_ba_x_2,1,dimensions(c,1)*dimensions(c,3));
								  }
							}//end

						// Increment the pointer 
						//   ptr=ptr+dimensions.at<double>(c,0)*dimensions.at<double>(c,2);  **************************************
						}//end

					}// end
				// Re-evaluate the evidence and the search direction
				step_len=0.0;

				// **************************************
				//[ensemble,grad]=train_ensemble_evidence6(step_len,dimensions,opt_func,ensemble,direction,state,P1,P2,P3,P4,P5,P6,P7,P8,P9,P10,P11,P12);
								
				//direction.x1=alpha*grad.x1;
				//direction.x2=alpha*grad.x2;
				//direction.b_x_2=alpha*grad.b_x_2;
				//direction.ba_x_2=alpha*grad.ba_x_2;
				//direction.pi_x_2=alpha*grad.pi_x_2;
				// ***************************************
				step_len=1.0;
				//end
				}

				if (plot_step)
					{


					double  ostep_len=step_len;
					Mat x_vals(1,21,CV_64F);

					for (int j = 0; j < 21; j++)
						{
						x_vals.at<double>(0,j) = 0.1*j*step_len;  // x_vals=(0:0.1:2)*step_len;
						}
					
					Mat f_vals = Mat::zeros(1,21,CV_64F);         //f_vals=zeros(size(x_vals));
										
					for (int i=0; i < 21 ; i++)
						{
						 step_len=x_vals.at<double>(i);
						// ********************************
						//[pensemble,pgrad]=train_ensemble_evidence6(step_len,dimensions,opt_func,ensemble,direction,state,P1,P2,P3,P4,P5,P6,P7,P8,P9,P10,P11,P12);
						//f_vals(i)=pensemble.D_val;
						}//end

					}// end

			// drawnow;

			//[tensemble,tgrad]=train_ensemble_evidence6(step_len,dimensions,opt_func,ensemble,direction,state,P1,P2,P3,P4,P5,P6,P7,P8,P9,P10,P11,P12);
			//success=(tensemble.D_val<ensemble.D_val);

				//if (tensemble.D_val>ensemble.D_val)
				//	{
				//	//direction.x1=alpha*grad.x1;
				//	//direction.x2=alpha*grad.x2;
				//	//direction.b_x_2=alpha*grad.b_x_2;
				//	//direction.ba_x_2=alpha*grad.ba_x_2;
				//	//direction.pi_x_2=alpha*grad.pi_x_2;
				//	step_len=2*step_len;
				//	while (tensemble.D_val>ensemble.D_val+converge_criteria/1e4)
				//		{
				//		// drawnow;
				//		step_len=0.5*step_len;
				//		//[tensemble,tgrad]=train_ensemble_evidence6(step_len,dimensions,opt_func,ensemble,direction,state,P1,P2,P3,P4,P5,P6,P7,P8,P9,P10,P11,P12);
				//		}//end
				//	}// end

			// direction.x1=alpha*tgrad.x1+beta*direction.x1;
			//direction.x2=alpha*tgrad.x2+beta*direction.x2;
			//direction.b_x_2=alpha*tgrad.b_x_2+beta*direction.b_x_2;
			//direction.ba_x_2=alpha*tgrad.ba_x_2+beta*direction.ba_x_2;
			//direction.pi_x_2=alpha*tgrad.pi_x_2+beta*direction.pi_x_2;
			//step_len=min(1,1.1*step_len);

			//%Accept the interpolated point
			// dD_val=tensemble.D_val-oD_val;
			//ensemble=tensemble;
			//grad=tgrad;

			//%Store training logs
			// D_log(1,iter)=ensemble.D_val;
			//gamma_log(1,iter)=ensemble.ba_sigma_2;

			//%Store current value
			// oD_val=ensemble.D_val;
			//% **************************************** PRINTF IS HERE *********
			// fprintf(1, [text '  Iteration %4d  Noise=%11.6e\n'],iter,gamma_log(1,iter)^-0.5);



			//%Check if algorithm has converged
			// converged=0;
			//if (iter>3)
			// last_dD_log=D_log(1,iter-2:iter)-D_log(1,iter-3:iter-1);
			//if (all(last_dD_log<converge_criteria/1e4 & last_dD_log>-converge_criteria))
			// converged=1;
			//end
			// end
			// if (converged)
			//  if (state==3)
			//	  %Have finished so exit
			//	  break;
			//elseif (state==2 & ensemble.opt_ba_sigma_2<1.1*ensemble.ba_sigma_2)
			// %Have converged so update noise and continue
			// state=3;
			//  else
			//	  %Swap between 1 and 2
			//	  state=3-state;
			//end
			// last_change_iter=iter;

			//%Run through each class and switch back on any switched off components
			// if (state==1)
			//  %Set the noise to the optimum
			//  ensemble.ba_sigma_2=ensemble.opt_ba_sigma_2;
			//%Randomise values for any components that have been switched off
			// %(they can always switch back off again later)
			// for c=1:size(dimensions,1)
			//  cmx=train_ensemble_get(c,dimensions,ensemble.mx);
			//cmx2=train_ensemble_get(c,dimensions,ensemble.mx2);
			//if (restart_switched_off & dimensions(c,5) & dimensions(c,4)<3)
			// %Find variance scale for this class, this is the ratio of <x>^2/<x^2>
			// %For non-rectified distributions, this ratio tends to zero as the
			// %component is switched off and the posterior tends to the prior.
			// %For rectified distributions, this ratio tends to 0.6366 as the
			// %distributions tend to rectified gaussians.
			// %Therefore randomise the components if the ratio goes below 0.7
			// cx1=train_ensemble_get(c,dimensions,ensemble.x1);
			//cx2=train_ensemble_get(c,dimensions,ensemble.x2);
			//scales=mean(cmx.^2,2)./mean(cmx2,2);

			//for k=1:dimensions(c,1)
			// if (scales(k)<0.7)
			//  %Scale of this component is really low or C_KL is low, so
			//  %randomise
			//  disp(['  Reinitialising class=' int2str(c) ' k=' int2str(k)])

			//  if (dimensions(c,4)==0)
			//	  %Not rectified
			//	  cx1(k,:)=1e4*randn(1,dimensions(c,2)).*ceil(rand(1,dimensions(c,2))*2);
			//  else
			//	  %Rectified
			//	  cx1(k,:)=1e4*abs(randn(1,dimensions(c,2)));
			//end
			// cx2(k,:)=1e4;
			//end
			// end
			// ensemble.x1=train_ensemble_put(c,dimensions,ensemble.x1,cx1);
			//ensemble.x2=train_ensemble_put(c,dimensions,ensemble.x2,cx2);
			//end
			// end
			// end
			// end
			// end
			}
		// %Shrink the logs to use the smallest possible size
		// D_log=D_log(:,1:iter);
		//gamma_log=gamma_log(1,1:iter);





		} 