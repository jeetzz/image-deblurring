#include "deblur_include_file.h"

void train_ensemble_put_lambda_test(Mat*log_lambda_x, int c, Mat dimensions, Mat c_log_lambda_x)
{

	//if (c>1)
	//	start=sum(prod(dimensions(1:c-1,1:3),2),1);
	//else
	//	start=0;
	//end
	int start = 0;
	if(c>0) {
		int prod;
		for (int i=0; i<c;i++)
		{
			prod = 1;
			for (int j=0; j<3;j++)
			{
				prod = prod * dimensions.at<int>(i,j);
			}
			start = start + prod;
		}
	}

	//int idx = 0;
	//for (int i = 0; i<dimensions.at<int>(c,0); i++)
	//{
	//	for (int j =0; j < dimensions.at<int>(c,2); j++)
	//	{
	//		for (int k=0; k< dimensions.at<int>(c,1); k++)
	//		{
	//			(log_lambda_x)->at<double>(0,start + idx) = c_log_lambda_x.at<double>(i,k,j); 
	//			idx++;
	//		}
	//	}
	//}
	Mat roi = (*log_lambda_x)(Rect(start,0,dimensions.at<int>(c,1)*dimensions.at<int>(c,2),1));
	//cv::Range rngs[] = {cv::Range(0,dimensions.at<int>(c,2)), cv::Range(start,dimensions.at<int>(c,1))};
	//cv::Mat dst = (*log_lambda_x)(rngs);
	Mat log_lambda_row(1,dimensions.at<int>(c,1)*dimensions.at<int>(c,2),CV_64F,(c_log_lambda_x.data));
	log_lambda_row.copyTo(roi);

}