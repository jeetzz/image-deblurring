// -------------- the kernel runs on the GPU ---------------------------
__global__ void addKernel(int *c, const int *a, const int *b)
{
    int i = threadIdx.x;
    c[i] = a[i] + b[i];
}


 __global__ void swap_rb_kernel(const gpu::PtrStepSz<uchar3> src, gpu::PteStep<uchar3> dst)  	{  	//int x = threadIdx.x + blockIdx.x * blockDim.x;  	//int y = threadIdx.y + blockIdx.y * blockDim.y;   	//if (x < src.cols && y < src.rows)  	//	{  	//	uchar3 v = src(y, x); // Reads pixel in GPU memory. Valid! We are on GPU!  	//	dst(y, x) = make_uchar3(v.z, v.y, v.x);  	//	}  	}

// ----------------- the MEX driver runs on the CPU --------------------
void call_kernel() 
{
    Mat image_test = imread("D:/My Stuff/IMG_5462.jpg");
	resize(image_test,image_test,Size(),0.3,0.3);	
	imshow("uda",image_test);
	waitKey(0);

	gpu::GpuMat src,dst;
	src.upload(image_test);


	dim3 block(32, 8);  	dim3 grid((src.cols + block.x - 1)/block.x, (src.rows + block.y - 1)/ block.y); 

//     const int arraySize = 5;
//     const int a[arraySize] = { 1, 2, 3, 4, 5 };
//     const int b[arraySize] = { 10, 20, 30, 40, 50 };
//     int c[arraySize] = { 0 };
// 
//     // Add vectors in parallel.
//     cudaError_t cudaStatus = addWithCuda(c, a, b, arraySize);
//      
// 
//     printf("{1,2,3,4,5} + {10,20,30,40,50} = {%d,%d,%d,%d,%d}\n",
//         c[0], c[1], c[2], c[3], c[4]);
// 
//     // cudaDeviceReset must be called before exiting in order for profiling and
//     // tracing tools such as Nsight and Visual Profiler to show complete traces.
    cudaDeviceReset();

	 
}

// Helper function for using CUDA to add vectors in parallel.
cudaError_t addWithCuda(int *c, const int *a, const int *b, size_t size)
{
    int *dev_a = 0;
    int *dev_b = 0;
    int *dev_c = 0;
    cudaError_t cudaStatus;
 

    // Allocate GPU buffers for three vectors (two input, one output)    .
    cudaStatus = cudaMalloc((void**)&dev_c, size * sizeof(int));
    cudaStatus = cudaMalloc((void**)&dev_a, size * sizeof(int));
	cudaStatus = cudaMalloc((void**)&dev_b, size * sizeof(int));
     
   
    cudaStatus = cudaMemcpy(dev_a, a, size * sizeof(int), cudaMemcpyHostToDevice);
    cudaStatus = cudaMemcpy(dev_b, b, size * sizeof(int), cudaMemcpyHostToDevice);
    
    // Launch a kernel on the GPU with one thread for each element.
    addKernel<<<1, size>>>(dev_c, dev_a, dev_b);

    // cudaDeviceSynchronize waits for the kernel to finish, and returns
    // any errors encountered during the launch.
    cudaStatus = cudaDeviceSynchronize();
   
    // Copy output vector from GPU buffer to host memory.
    cudaStatus = cudaMemcpy(c, dev_c, size * sizeof(int), cudaMemcpyDeviceToHost);
    if (cudaStatus != cudaSuccess) {
        fprintf(stderr, "cudaMemcpy failed!");
        goto Error;
    }

Error:
    cudaFree(dev_c);
    cudaFree(dev_a);
    cudaFree(dev_b);
    
    return cudaStatus;
}
