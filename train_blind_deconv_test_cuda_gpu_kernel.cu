#ifndef TRAIN_BLIND_DECONV_TEST_CUDA_GPU_KERNEL_H
#define TRAIN_BLIND_DECONV_TEST_CUDA_GPU_KERNEL_H
// **********************************************
// FFTS
// **********************************************

__global__ void return_err(cufftReal *err, cufftReal * Dp, cufftReal * D, cufftReal * mD,float mmu,int I,int J){
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; // row index
  	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y; // col index

	if (i < I && j < J)
   		{
		err[i*J+j] = Dp[i*J+j]*( D[i*J+j] - mD[i*J+j] - mmu );
		}

	}

__global__ void error_mat_kernel(float *mu1,  float *error, float* data_points, cufftReal *error_mat, cufftReal *Dp_D_mD_mmu, cufftReal *D, cufftReal * mD, float mmu, float mmu2, cufftReal *me_mx2_me_mx3,cufftReal *Dp, int I,int J){
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; // row index
  	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y; // col index

	if (i < I && j < J)
   		{
			error_mat[i*J+j] =  Dp[i*J+j] * ((D[i*J+j] - mD[i*J+j] - mmu)* (D[i*J+j] - mD[i*J+j] - mmu) + me_mx2_me_mx3[i*J +j] );
			Dp_D_mD_mmu[i*J+j] =   Dp[i*J+j] *(D[i*J+j] - mD[i*J+j] - mmu);
		}
		__syncthreads();

	if(i==0 && j==0){
		mu1[0] = 0.0;
		error[0] = 0.0;
		data_points[0] = 0.0;
			// parallelize  kernel later//////////
		for (int r = 0; r < I; ++r)
		{ 
			for (int c = 0; c < J; ++c)
			{
				if(Dp[r*J + c] == 1.0){
					data_points[0] = data_points[0] + 1;
					// error[0] = error[0] + (mmu2 - (mmu*mmu)) ;
					error[0] = error[0] + error_mat[r*J + c] + (mmu2 - (mmu*mmu)) ;
					mu1[0] = mu1[0] + D[r*J + c] - mD[r*J + c];
				}

			}
		}	
	}

}

__global__ void error_mat_kernel_no_sum(cufftReal *error_mat, cufftReal *Dp_D_mD_mmu, cufftReal *D, cufftReal * mD, float mmu, float mmu2, cufftReal *me_mx2_me_mx3, cufftReal *Dp, int I,int J){
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; // row index
  	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y; // col index

	if (i < I && j < J)
   		{
			error_mat[i*J+j] =  Dp[i*J+j] * ((D[i*J+j] - mD[i*J+j] - mmu)* (D[i*J+j] - mD[i*J+j] - mmu) + me_mx2_me_mx3[i*J +j] );
			Dp_D_mD_mmu[i*J+j] =   Dp[i*J+j] *(D[i*J+j] - mD[i*J+j] - mmu);
		}

}

__global__ void error_mat_kernel_no_sum_error_full(cufftReal *error_mat, cufftReal *Dp_D_mD_mmu, cufftReal *D, cufftReal * mD, float mmu, float mmu2, cufftReal *me_mx2_me_mx3, cufftReal *Dp, int I,int J){
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; // row index
  	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y; // col index

	if (i < I && j < J)
   		{
			error_mat[i*J+j] =  Dp[i*J+j] * ((D[i*J+j] - mD[i*J+j] - mmu)* (D[i*J+j] - mD[i*J+j] - mmu) + me_mx2_me_mx3[i*J +j] + (mmu2 - (mmu*mmu) ));
			Dp_D_mD_mmu[i*J+j] =   Dp[i*J+j] *(D[i*J+j] - mD[i*J+j] - mmu);
		}

}

__global__ void error_mat_kernel_no_sum_new(cufftReal *error_mat, cufftReal *Dp_D_mD_mmu, cufftReal *D, cufftReal * mD, float mmu, float mmu2, cufftReal *me_mx2_me_mx3, cufftReal *Dp, int I,int J){
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; // row index
  	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y; // col index

	if (i < I && j < J)
   		{
			error_mat[i*J+j] =  Dp[i*J+j] * ((D[i*J+j] - mD[i*J+j] - mmu)* (D[i*J+j] - mD[i*J+j] - mmu) + me_mx2_me_mx3[i*J +j] + (mmu2 - (mmu*mmu) ));
			Dp_D_mD_mmu[i*J+j] =   Dp[i*J+j] *(D[i*J+j] - mD[i*J+j] - mmu);
			D[i*J+j]	= Dp[i*J+j] * (D[i*J+j] - mD[i*J+j]);
		}

}

__global__ void error_mat_kernel_sum_only(float *mu1,  float *error, float* data_points, cufftReal *error_mat, cufftReal *D, cufftReal * mD, float mmu, float mmu2,cufftReal *Dp, int I,int J){
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; // row index
  	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y; // col index


	if(i==0 && j==0){
		mu1[0] = 0.0;
		error[0] = 0.0;
		data_points[0] = 0.0;
			// parallelize  kernel later//////////
		for (int r = 0; r < I; ++r)
		{ 
			for (int c = 0; c < J; ++c)
			{
				if(Dp[r*J + c] == 1.0){
					data_points[0] = data_points[0] + 1;
					// error[0] = error[0] + (mmu2 - (mmu*mmu)) ;
					error[0] = error[0] + error_mat[r*J + c] + (mmu2 - (mmu*mmu)) ;
					mu1[0] = mu1[0] + (D[r*J + c] - mD[r*J + c]);
				}

			}
		}	
	}

}

__global__ void error_mat_kernel_sum_only_error_full(float *mu1,  float *error, float* data_points, cufftReal *error_mat, cufftReal *D, cufftReal * mD, float mmu, float mmu2,cufftReal *Dp, int I,int J){
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; // row index
  	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y; // col index


	if(i==0 && j==0){
		mu1[0] = 0.0;
		error[0] = 0.0;
		data_points[0] = 0.0;
			// parallelize  kernel later//////////
		for (int r = 0; r < I; ++r)
		{ 
			for (int c = 0; c < J; ++c)
			{
				if(Dp[r*J + c] == 1.0){
					data_points[0] = data_points[0] + 1;
					// error[0] = error[0] + (mmu2 - (mmu*mmu)) ;
					error[0] = error[0] + error_mat[r*J + c] ;
					mu1[0] = mu1[0] + (D[r*J + c] - mD[r*J + c]);
				}

			}
		}	
	}

}

__global__ void sum_kernel( float* g_data1, float* per_block_results1, float* g_data2, float* per_block_results2, float* g_data3, float* per_block_results3, const int n )
{
    // write data to global memory
    const unsigned int tid = threadIdx.x;
	const unsigned int myId = threadIdx.x + blockDim.x* blockIdx.x;
	unsigned int s_tmp = blockDim.x>>1;
	for(unsigned int s = (blockDim.x+1)>>1;s_tmp>0;s=(s+1)>>1){
		//printf("myIdx %d tid %d gdata %d step %d strt %d n=%d\n",myId, tid, g_data[myId],s , s_tmp,n);
		if(tid <s_tmp && (myId+s) < n){
				g_data1[myId] += g_data1[myId+s];
				g_data2[myId] += g_data2[myId+s];
				g_data3[myId] += g_data3[myId+s];
			}
		__syncthreads();
		s_tmp =s>>1;
	}

	if(threadIdx.x == 0)
	{
		per_block_results1[blockIdx.x] = g_data1[myId];
		per_block_results2[blockIdx.x] = g_data2[myId];
		per_block_results3[blockIdx.x] = g_data3[myId];
		//printf("per_block_results %d my Idx %d\n",myId,per_block_results[blockIdx.x]);
	}
}

__global__ void fft_me2_mx2_me3_mx3_kernel(cufftComplex * d_mul,  cufftComplex * fft_me, cufftComplex * fft_mx, cufftComplex *fft_ans, cufftComplex *fft_me2, cufftComplex *fft_mx2, cufftComplex *fft_me3, cufftComplex *fft_mx3, int I,int J){
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; // row index
  	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y; // col index

	if (i < I && j < J)
   		{
	   		fft_ans[i*J+j].x = (fft_me2[i*J+j].x * fft_mx2[i*J+j].x - fft_me2[i*J+j].y * fft_mx2[i*J+j].y) - (fft_me3[i*J+j].x * fft_mx3[i*J+j].x - fft_me3[i*J+j].y * fft_mx3[i*J+j].y);
	   		fft_ans[i*J+j].y = (fft_me2[i*J+j].x * fft_mx2[i*J+j].y + fft_me2[i*J+j].y * fft_mx2[i*J+j].x) - (fft_me3[i*J+j].x * fft_mx3[i*J+j].y + fft_me3[i*J+j].y * fft_mx3[i*J+j].x);
	   		d_mul[i*J+j].x = fft_me[i*J+j].x * fft_mx[i*J+j].x - fft_me[i*J+j].y * fft_mx[i*J+j].y;
   			d_mul[i*J+j].y = fft_me[i*J+j].x * fft_mx[i*J+j].y + fft_me[i*J+j].y * fft_mx[i*J+j].x;
   			fft_me[i*J+j].y  = -fft_me[i*J+j].y;
   			fft_me2[i*J+j].y = -fft_me2[i*J+j].y;
   			fft_me3[i*J+j].y = -fft_me3[i*J+j].y;
   			fft_mx[i*J+j].y  = -fft_mx[i*J+j].y;
   			fft_mx2[i*J+j].y = -fft_mx2[i*J+j].y;
   			fft_mx3[i*J+j].y = -fft_mx3[i*J+j].y;
		}

	}
__global__ void mult_by_element(cufftComplex * d_mul,  cufftComplex * d_A, cufftComplex * d_B, int I,int J){
 	
	// For optimization : http://www.embedded.com/design/embedded/4007256/Digital-Signal-Processing-Tricks--Fast-multiplication-of-complex-numbers]
  	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; // row index
  	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y; // col index
  
   	if (i < I && j < J)
   		{
   		d_mul[i*J+j].x = d_A[i*J+j].x * d_B[i*J+j].x - d_A[i*J+j].y * d_B[i*J+j].y;
   		d_mul[i*J+j].y = d_A[i*J+j].x * d_B[i*J+j].y + d_A[i*J+j].y * d_B[i*J+j].x;
   		}
	}


__global__ void complex_to_real(cufftReal * d_A, cufftComplex * d_B, int I,int J){
 	
	// For optimization : http://www.embedded.com/design/embedded/4007256/Digital-Signal-Processing-Tricks--Fast-multiplication-of-complex-numbers]
  	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; // row index
  	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y; // col index
  
   	if (i < I && j < J)
   		{
   			d_A[i*J+j] =  d_B[i*J+j].x;
   		}
	}
__global__ void complex_to_img(cufftReal * d_A, cufftComplex * d_B, int I,int J){
 	
	// For optimization : http://www.embedded.com/design/embedded/4007256/Digital-Signal-Processing-Tricks--Fast-multiplication-of-complex-numbers]
  	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; // row index
  	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y; // col index
  
   	if (i < I && j < J)
   		{
   			d_A[i*J+j] =  d_B[i*J+j].y;
   		}
	}


__global__ void fft_conj_mult(cufftComplex * fft_Dp,  cufftComplex * fft_err, cufftComplex * fft_me, cufftComplex * fft_mx, cufftComplex *fft_me2, cufftComplex *fft_mx2, cufftComplex *fft_me3, cufftComplex *fft_mx3, int I,int J){
 	
	// For optimization : http://www.embedded.com/design/embedded/4007256/Digital-Signal-Processing-Tricks--Fast-multiplication-of-complex-numbers]
  	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; // row index
  	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y; // col index
  	float temp_x;

   	if (i < I && j < J)
   		{
	   		temp_x =  (fft_me[i*J+j].x * fft_err[i*J+j].x) - (fft_me[i*J+j].y * fft_err[i*J+j].y);
	   		fft_me[i*J+j].y =  (fft_me[i*J+j].x * fft_err[i*J+j].y) + (fft_me[i*J+j].y * fft_err[i*J+j].x);
	   		fft_me[i*J+j].x = temp_x;

	   		temp_x =  (fft_mx[i*J+j].x * fft_err[i*J+j].x) - (fft_mx[i*J+j].y * fft_err[i*J+j].y);
	   		fft_mx[i*J+j].y =  (fft_mx[i*J+j].x * fft_err[i*J+j].y) + (fft_mx[i*J+j].y * fft_err[i*J+j].x);
	   		fft_mx[i*J+j].x = temp_x;

	   		temp_x = (fft_me2[i*J+j].x * fft_Dp[i*J+j].x) - (fft_me2[i*J+j].y * fft_Dp[i*J+j].y);
	   		fft_me2[i*J+j].y = (fft_me2[i*J+j].x * fft_Dp[i*J+j].y) + (fft_me2[i*J+j].y * fft_Dp[i*J+j].x);
	   		fft_me2[i*J+j].x = temp_x;

			temp_x = (fft_me3[i*J+j].x * fft_Dp[i*J+j].x) - (fft_me3[i*J+j].y * fft_Dp[i*J+j].y);
	   		fft_me3[i*J+j].y = (fft_me3[i*J+j].x * fft_Dp[i*J+j].y) + (fft_me3[i*J+j].y * fft_Dp[i*J+j].x);	 
	   		fft_me3[i*J+j].x = temp_x;

	   		temp_x = (fft_mx2[i*J+j].x * fft_Dp[i*J+j].x) - (fft_mx2[i*J+j].y * fft_Dp[i*J+j].y);
	   		fft_mx2[i*J+j].y = (fft_mx2[i*J+j].x * fft_Dp[i*J+j].y) + (fft_mx2[i*J+j].y * fft_Dp[i*J+j].x);
	   		fft_mx2[i*J+j].x = temp_x;

			temp_x = (fft_mx3[i*J+j].x * fft_Dp[i*J+j].x) - (fft_mx3[i*J+j].y * fft_Dp[i*J+j].y);
	   		fft_mx3[i*J+j].y = (fft_mx3[i*J+j].x * fft_Dp[i*J+j].y) + (fft_mx3[i*J+j].y * fft_Dp[i*J+j].x);	
	   		fft_mx3[i*J+j].x = temp_x;
   		}
	}

__global__ void mult_by_element_real(cufftReal * d_mul,  cufftReal * d_A, cufftReal * d_B, int I,int J){
 	
	// For optimization : http://www.embedded.com/design/embedded/4007256/Digital-Signal-Processing-Tricks--Fast-multiplication-of-complex-numbers]
  	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; // row index
  	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y; // col index
  
   	if (i < I && j < J)
   		{
			d_mul[i*J+j] = d_A[i*J+j] * d_B[i*J+j] ;
   		}
	}


__global__ void square(cufftReal * d_out, cufftReal * d_in,int M,int N){
	// - I,J after padding
	// - M,N before padding
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; // row index
	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y; // col index

	if (i < M && j < N)
				d_out[i*N + j] = d_in[i*N + j]*d_in[i*N + j];
	 
	}

__global__ void zero_pad_input(cufftReal * d_out, cufftReal * d_in,int M,int N,int I,int J){
	// - I,J after padding
	// - M,N before padding
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; // row index
	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y; // col index

	if (i < M && j < N)
				d_out[i*J + j] = d_in[i*N + j];
	else if (i < I && j < J)
				d_out[i*J + j]  = 0.0f;
	}

__global__ void zero_pad_input_sq_flag(cufftReal * d_out, cufftReal * d_in,int M,int N,int I,int J, int square_flag){
	// - I,J after padding
	// - M,N before padding
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; // row index
	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y; // col index

	if (i < M && j < N){
		if(square_flag){
			d_out[i*J + j] = d_in[i*N + j]*d_in[i*N + j];	
		}
		else{
			d_out[i*J + j] = d_in[i*N + j];
		}
	}
	else if (i < I && j < J)
				d_out[i*J + j]  = 0.0f;
	}

__global__ void expand_output(cufftComplex * d_out, cufftComplex * d_in,int I,int J){
//    	 	int j = threadIdx.x;
//    	    int i = threadIdx.y;

	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y;
	
	if ( j < J/2 + 1 && i < I)
		d_out[i*J + j] = d_in[i*(J/2+1) + j];
	else if ( i == 0 && j < J)
		{ 
		d_out[i*J + j].x = d_in[i*(J/2+1) + (J-j)].x;
		d_out[i*J + j].y = -d_in[i*(J/2+1) + (J-j)].y;
		}
	else if (i < I && j < J)
		{
		d_out[i*J + j].x = d_in[(I-i)*(J/2+1) + (J-j)].x;
		d_out[i*J + j].y = -d_in[(I-i)*(J/2+1) + (J-j)].y;
		}

	}
// **********************************************


// **********************************************
// FOR IFFTS
// **********************************************
__global__ void halve_input(cufftComplex * d_out, cufftComplex * d_in,int I,int J){
// 	int i = threadIdx.x;
// 	int j = threadIdx.y;
	unsigned int i= blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y;
	if ( i < I && j < J/2 + 1)
		d_out[i*(J/2+1) + j] = d_in[i*J + j];		
	}

__global__ void normalize_ifft(cufftReal * d_out, cufftReal * d_in,int I,int J, int M, int N){
	unsigned int i= blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y;
	if ( i < M && j < N) 
		d_out[i*N + j] = d_in[i*J + j]/(I*J);
	}

__global__ void normalize_ifft_plus_corr(cufftReal* corr, cufftReal * d_out, cufftReal * d_in,int I,int J, int M, int N){
	unsigned int i= blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y;
	if ( i < M && j < N) 
		d_out[i*N + j] = (d_in[i*J + j]/(I*J)) + (corr[i*N + j]*d_out[i*N + j]);
		// d_out[i*N + j] =  (corr[i*N + j]*d_out[i*N + j]);
	}
// **********************************************

#endif	// TRAIN_BLIND_DECONV_TEST_CUDA_GPU_KERNEL_H