stack<clock_t> tictoc_stack;
stack<clock_t> tictoc_sum_stack;

void tic() {
	tictoc_stack.push(clock());
}

void toc(string s,int ID) {

	//mexPrintf("%s time: %f ", s , ((double)(clock() - tictoc_stack.top())) / CLOCKS_PER_SEC);
	tictoc_stack.pop();
}

void tictic() {
	tictoc_sum_stack.push(clock());
}

void toctoc(string s,int ID) {

	double total_sum = 0;
	clock_t temp1,temp2;

	while(!tictoc_sum_stack.empty())
		{
		temp1 = tictoc_sum_stack.top(); tictoc_sum_stack.pop();
		temp2 = tictoc_sum_stack.top(); tictoc_sum_stack.pop();

		total_sum = total_sum + ((double)(temp1 - temp2))/CLOCKS_PER_SEC;
		}
	

	//mexPrintf("%s time: %f ", s , total_sum);
	 
}

void tictic_clear(){
	stack<clock_t> empty;
	swap( tictoc_sum_stack, empty );
}

