#include "deblur_include_file.h"

void train_ensemble_put_test(Mat*x, int c, Mat dimensions, Mat cx)
{
	//function x=train_ensemble_put(c,dimensions,x,cx)

	//if (c>1)
	//	start=sum(prod(dimensions(1:c-1,1:3),2),1);
	//else
	//	start=0;
	//end
	int start = 0;
	if(c>0) {
		int prod;
		for (int i=0; i<c;i++)
		{
			prod = 1;
			for (int j=0; j<2;j++)
			{
				prod = prod * dimensions.at<int>(i,j);
			}
			start = start + prod;
		}
	}
	//int idx = 0;
	
	/*for (int i = 0; i<dimensions.at<int>(c,0); i++)
	{
		for (int j =0; j < dimensions.at<int>(c,1); j++)
		{
				(x)->at<double>(0,start + idx) = cx.at<double>(i,j); 
				idx++;
		}
	}*/
	//mexPrintf("roi width %d roi height %d m.cols %d m.rows %d roi.x %d roi.y %d\n",dimensions.at<int>(c,1),dimensions.at<int>(c,0),x->cols, x->rows,start,0 );
	//Mat roi((*x), Rect(start,0,10,1));
	Mat roi = (*x)(Rect(start,0,dimensions.at<int>(c,1),dimensions.at<int>(c,0)));
	cx.copyTo(roi);
	
	
}