#include "deblur_include_file.h"


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
	{

	// 0 obs, 1 blur, 2 im, 3 true_blur, 4 true_im, 5 pres,
	// 6 prior, 7 prior_num, 8 mode_im, 9 mode_blur, 10 obs_im, 
	// 11 big_blur, 12 spatial_mask, 13 priors, 14 FFT_MODE, 15 COLOR, 16 nLayers


	// obs is an M x N matrix. It contains two grad_x and grad_y concatenated to a single matrix ( two birds )
	mxArray *mx_obs = mxDuplicateArray(prhs[0]);  
	double* obs = mxGetPr(mx_obs);
	int obs_M = mxGetM(mx_obs);
	int obs_N = mxGetN(mx_obs);

	// Blur kernel. M x N matrix. Begins from 3 x 3 and then 5 x 5 and like that size increases...
	mxArray *mx_blur = mxDuplicateArray(prhs[1]);  
	double* blur = mxGetPr(mx_blur);
	int blur_M = mxGetM(mx_blur);
	int blur_N = mxGetN(mx_blur);

	// im is also an M x N matrix that contains two gradient matrices. grad_x and grad_y concatenated to a single matrix ( two birds ) 
	// find the true definitions <-------------
	mxArray *mx_im = mxDuplicateArray(prhs[2]);  
	double* im = mxGetPr(mx_im);
	int im_M = mxGetM(mx_im);
	int im_N = mxGetN(mx_im);

	// For the moment it's an empty matrix
	mxArray *mx_true_blur = mxDuplicateArray(prhs[3]);  
	double* true_blur = mxGetPr(mx_true_blur);
	int true_blur_M = mxGetM(mx_true_blur);
	int true_blur_N = mxGetN(mx_true_blur);

	// For the moment it's an empty matrix
	mxArray *mx_true_im = mxDuplicateArray(prhs[4]);  
	double* true_im = mxGetPr(mx_true_im);
	int true_im_M = mxGetM(mx_true_im);
	int true_im_N = mxGetN(mx_true_im);

	//  
	mxArray *mx_pres = mxDuplicateArray(prhs[5]);  
	double* pres = mxGetPr(mx_pres);
	int pres_M = mxGetM(mx_pres);
	int pres_N = mxGetN(mx_pres);

	//   
	mxArray *mx_prior = mxDuplicateArray(prhs[6]);  
	double* prior = mxGetPr(mx_prior);
	int prior_M = mxGetM(mx_prior);
	int prior_N = mxGetN(mx_prior);

	//  
	mxArray *mx_prior_num = mxDuplicateArray(prhs[7]);  
	double* prior_num = mxGetPr(mx_prior_num);
	int prior_num_M = mxGetM(mx_prior_num);
	int prior_num_N = mxGetN(mx_prior_num);

	// mode_im is a string 
	mxArray *mx_mode_im = mxDuplicateArray(prhs[8]);  
	size_t buflen = 30; 
	char *mode_im =(char *)mxMalloc(buflen);	
	buflen = mxGetN(mx_mode_im)*sizeof(mxChar)+1;	 
	mxGetString(mx_mode_im, mode_im, (mwSize)buflen);    // mode_blur char* is assigned with the input string

	//  mode_blur is a string 
	mxArray *mx_mode_blur = mxDuplicateArray(prhs[9]);  
	char *mode_blur =(char *)mxMalloc(buflen);	
	buflen = mxGetN(mx_mode_blur)*sizeof(mxChar)+1;	 
	mxGetString(mx_mode_blur, mode_blur, (mwSize)buflen);    // mode_blur char* is assigned with the input string

	//  
	mxArray *mx_obs_im = mxDuplicateArray(prhs[10]);  
	double* obs_im = mxGetPr(mx_obs_im);
	int obs_im_M = mxGetM(mx_obs_im);
	int obs_im_N = mxGetN(mx_obs_im);

	//  
	mxArray *mx_big_blur = mxDuplicateArray(prhs[11]);  
	double* big_blur = mxGetPr(mx_big_blur);
	int big_blur_M = mxGetM(mx_big_blur);
	int big_blur_N = mxGetN(mx_big_blur);

	// 
	mxArray *mx_spatial_mask = mxDuplicateArray(prhs[12]);  
	double* spatial_mask = mxGetPr(mx_spatial_mask);
	int spatial_mask_M = mxGetM(mx_spatial_mask);
	int spatial_mask_N = mxGetN(mx_spatial_mask);

	// 


	Priors priors;
	mxArray *mx_priors = mxDuplicateArray(prhs[13]);  
	int priors_M = (int)mxGetM(mx_priors);
	int priors_N = (int)mxGetN(mx_priors);
	mxArray *pi = mxGetField(mx_priors, 0, "pi");
	priors_M = mxGetM(pi);
	priors_N = mxGetN(pi);
	double* priors_pi = mxGetPr(pi); 
	priors.pi = Mat(priors_M,priors_N,CV_64F,priors_pi);

	mxArray *gamma = mxGetField(mx_priors, 0, "gamma");
	priors_M = mxGetM(gamma);
	priors_N = mxGetN(gamma);
	double* priors_gm = mxGetPr(gamma); 
	priors.pi = Mat(priors_M,priors_N,CV_64F,priors_gm);

	//  
	mxArray *mx_FFT_MODE = mxDuplicateArray(prhs[14]);  
	double* FFT_MODE_ = mxGetPr(mx_FFT_MODE);
	int FFT_MODE  = (int)FFT_MODE_[0];

	// 
	mxArray *mx_COLOR = mxDuplicateArray(prhs[15]);  
	double* COLOR_ = mxGetPr(mx_COLOR);
	int COLOR = (int)(*COLOR_);


	// 
	mxArray *mx_nLayers = mxDuplicateArray(prhs[16]);  
	double* nLayers = mxGetPr(mx_nLayers);
	int nLayers_M = mxGetM(mx_nLayers);
	int nLayers_N = mxGetN(mx_nLayers);

	// Begins the task



	Mat me2;

	if (strcmp(mode_blur,"direct") == 0 )
		mexPrintf("1");
	//me2 = blur;
	else if ( strcmp(mode_blur,"true") == 0 )
		mexPrintf("2");
	//me2 = true_blur;
	else if (strcmp(mode_blur,"updown") == 0 )
		mexPrintf("3");
	//[K,L] = size(true_blur);
	//me2 = imresize(imresize(true_blur,0.5,'bilinear'),[K L],'bilinear');
	else if (strcmp(mode_blur,"delta") == 0)
		mexPrintf("4");
	//[K,L] = size(true_blur);
	//me2 = delta_kernel(K);
	else if (strcmp(mode_blur,"hbar") == 0)
	{

		int K = true_blur_M;
		int L = true_blur_N;

		int hK = floor(((double)K)/2);   
		int hL = floor(((double)L)/2); 

		me2 = Mat::zeros(K,L,CV_64F);  // K x L matrix

		me2.at<double>(hK,hL) = 1.0;
		me2.at<double>(hK,hL-1) = 1.0;
		me2.at<double>(hK,hL+1) = 1.0;
			

		// Print
		//for (int i = 0;i<K;i++)			 
		//	mexPrintf("%f %f %f \n",me2.at<double>(i,0),me2.at<double>(i,1),me2.at<double>(i,2));

	}
	else if (strcmp(mode_blur,"vbar") == 0)
	{
		int K = true_blur_M;
		int L = true_blur_N;

		int hK = floor(((double)K)/2);   
		int hL = floor(((double)L)/2);
        
		me2 = Mat::zeros(K,L,CV_64F);  // K x L matrix

		me2.at<double>(hK-1,hL) = 1.0;
		me2.at<double>(hK,hL) = 1.0;
		me2.at<double>(hK+1,hL) = 1.0;

	}
	else if (strcmp(mode_blur,"star") == 0)
		mexPrintf("7");
	//[K,L] = size(true_blur);
	//hK = floor(K/2)+1;
	//hL = floor(L/2)+1;
	//me2 = zeros(K,L);
	//me2(hK-1,hL+1) = 1;
	//me2(hK-1,hL-1) = 1;
	//me2(hK+1,hL-1) = 1;
	//me2(hK+1,hL+1) = 1;


	else if (strcmp(mode_blur,"random") == 0)
		mexPrintf("8");
	//[M,N] = size(true_blur);
	//me2 = rand(M,N);
	else if (strcmp(mode_blur,"variational") == 0)
		mexPrintf("9");
	//me2 = blur;

	else
		mexPrintf("error");

	int C = 0;
	if (COLOR == 1)
		C = 3;
	else
		C = 1;   // s = 1 , 2 - 9 <-----------------


	// spatial_mask = spatial_mask(:);   //  Do not need it in here because when passed to mex files, it automatically 
	//  get converted to the column vector with elements in a column being the adjacent ones. 
	//  anyway, once we have the full implmentation this MUST BE IMPLEMENTED *********************

	int mx2;
	Mat dimensions;
	Mat Dpf;
	Mat Df;
	int	I,J,K,L,M,N; 

	if (strcmp(mode_im,"direct") == 0)  // s = 2-9 <-----------------
		mexPrintf("----1");
	else if (strcmp(mode_im,"true")== 0)
		mexPrintf("2");
	//  mx2 = true_im;
	else if (strcmp(mode_im,"slight_blur_obs")== 0)
		mexPrintf("3");
	//	[M,N] = size(obs);
	//  f = [1 2 1; 2 4 2; 1 2 1]/16;

	//  for c=1:C
	//	  mx2(:,:,c) = real(ifft2(fft2(f,M,N).*fft2(obs(:,:,c),M,N)));
	//  end

	else if (strcmp(mode_im,"lucy")== 0)
		mexPrintf("4");
	//	obs2 = edgetaper(obs_im,big_blur);
	//  im_tmp = deconvlucy(obs2,big_blur);

	//  for c=1:C
	//	  im_x(:,:,c) = conv2(im_tmp(:,:,c),[1 -1],'valid');
	//  im_y(:,:,c) = conv2(im_tmp(:,:,c),[1 -1]','valid');
	//	  end

	//	[M,N] = size(obs);

	//  im_xs = imresize(im_x,[M N/2]+2,'bilinear');
	//  im_ys = imresize(im_y,[M N/2]+2,'bilinear');

	//  mx2 = zeros(M,N,C);
	//  mx2(1:M,1:N/2,:) = im_xs(2:end-1,2:end-1,:);
	//  mx2(1:M,N/2+1:N,:) = im_ys(2:end-1,2:end-1,:);

	else if (strcmp(mode_im,"reg")== 0)
		mexPrintf("5");
	//	obs2 = edgetaper(obs,blur);
	//  mx2 = deconvreg(obs2,blur);
	else if (strcmp(mode_im,"lucy_true")== 0)
		mexPrintf("6");
	//	obs2 = edgetaper(obs,true_blur);
	//  mx2 = deconvlucy(obs2,true_blur,20);
	else if (strcmp(mode_im,"reg_true")== 0)
		mexPrintf("7");
	//	obs2 = edgetaper(obs,true_blur);
	//  mx2 = deconvreg(obs2,true_blur);
	else if (strcmp(mode_im,"updown")== 0)
		mexPrintf("8");
	//	[M,N] = size(true_im);
	//  mx2 = imresize(imresize(true_im,0.5,'bilinear'),[M N],'bilinear');
	else if (strcmp(mode_im,"random")== 0)
		{
		mexPrintf("9");
		//	%%% generate image using laplacian distribution of 
		//	SCALE_PARAMETER = [7 6 4];
		//  [M,N] = size(im);

		//  tmp1 = rand(M,N,C);
		//  tmp2 = rand(M,N,C);

		//  mx2 = SCALE_PARAMETER(3) * (-log(tmp1) + log(tmp2));
		}
	else if (strcmp(mode_im,"variational")== 0)  // < s = 1----------------
		{		
		//	% ************ BEGIN OF VARIATIONAL ********************
		double	MAX_ITERATIONS = 5000;
		M = im_M; 		N = im_N;
		K = blur_M;     L = blur_N;  // Using K,L for me2 size seems to work. But need more inspection !!!

		double dimensions_array[3][6]  = {{1,1,1,0,0,1},{1,K*L,4,1,0,0},{C,M*N,4,0,1,1}};
		dimensions = Mat(3,6,CV_64F,dimensions_array);

		double sum_ = sum(me2)[0];
		
		Mat norm_blur(K,L,CV_64F);
		for (int i = 0 ; i<K ; i++){		 
			for (int j = 0; j<L ; j++){
				norm_blur.at<double>(i,j) = me2.at<double>(i,j)/sum_;
				//mexPrintf("%f ", norm_blur.at<double>(i,j));
			}
			//mexPrintf("\n");
		}


		//  n.b. for SIGGRAPH only 2 blur components were used.
		//	set dimensions(2,6)=1 to get back to SSG & IMA runs


		//Print the 'dimensions' matrix
		//	for (int i = 0; i < 3 ; i++){
		//		for (int j = 0 ; j < 6 ; j++){
		//			mexPrintf("%f ",dimensions.at<double>(i,j));
		//		}					
		//		mexPrintf("\n ");
		//	}

	 

		if (FFT_MODE == 1 )  // s = 1<---------------
			{
			I = M*2; J = N*2;  	
			
			Dpf = Mat(I,J,CV_64F);  
			for (int i = 0 ; i<I ; i++){
				for (int j = 0; j<J; j++){
					if ((i>= K-1 && i <= M-1 && j >= L-1 && j <= N/2 -1 ) || (i >= K-1 && i <= M-1 && j >= (L+N/2)-1 && j <= N-1 ))
						Dpf.at<double>(i,j) = 1;
					else 
						Dpf.at<double>(i,j) = 0;
					//mexPrintf("%.0f,", Dpf.at<double>(i,j));
				}
				//mexPrintf("\n ");
			}

			Df =  Mat::zeros(2*M,2*N,CV_64F);
			for (int i = 0; i < obs_M ; i++){
				for (int j = 0 ; j < obs_N ; j ++){
					Df.at<double>(i,j)= (double)obs[obs_M*j + i];   // columns in the matlab matrix will be stored adjacently in the double array in c++					
				}
			}


			//mexPrintf("M %d, N %d, obs_M %d, obs_N %d\n",M,N,obs_M,obs_N);
			//for (int i = 0; i < 2*M; i++){
			//	for (int j = 0 ; j < 2*N ; j ++){
			//		mexPrintf("%f,",Df.at<double>(i,j));				
			//		}
			//	mexPrintf(";\n");
			//	}


			}
		else
			{
			//	I = M; J = N;
			//  hK = floor(K/2); hL = floor(L/2);

			//  Dpf = zeros(I,J,C);
			//  Dpf(hK+1:M-hK,hL+1:N/2-hL,:) = 1;
			//  Dpf(hK+1:M-hK,N/2+hL+1:N-hL,:) = 1;

			//  shift_kernel = zeros(K,L);
			//  shift_kernel(1,1)=1;

			//  for c=1:C
			//	  Df(:,:,c) = conv2(obs(:,:,c),shift_kernel,'same');
			//  end
			}

		Mat pres_vector = Mat::ones(1, blur_M*blur_N + im_M*im_N + 1, CV_64F)*pres[0];
		
	 
		//  Two below lines not required for this case, becasue q is an empty vector for this configuration. Thus nothing happens to pres_vector
		//  q=find(spatial_mask(:));
		//  pres_vector(q+1+length(norm_blur(:))) = spatial_mask(q)';

		 
		Mat dummy_blur_mask = Mat::zeros(dimensions.at<double>(1,2), blur_M*blur_N , CV_64F);

		 
		Mat temp = Mat::zeros(1,blur_M*blur_N + im_M*im_N + 1, CV_64F);
		
		for (int i = 1 ; i < blur_M*blur_N + 1 ; i++)		    
			temp.at<double>(0,i) = (double)norm_blur.at<double>((i-1)%3,(int)floor((double)((i-1)/3)));		
		
		for (int i = blur_M*blur_N + 1 ; i < blur_M*blur_N + im_M*im_N + 1 ; i++)		   
		   temp.at<double>(0,i) =  im[i- (blur_M*blur_N + 1)];
		  
		 
		//  %%% Make vectors
		Mat xx1 = temp.mul(pres_vector);
		Mat  xx2 = pres_vector; 

		// Preparing variables for train_ensemble_main6(...)
		double options[] = {0.0001, 0, 1.0, 0.0, 0.0, MAX_ITERATIONS, 0.0};
		 
		Mat P12(1,spatial_mask_M*spatial_mask_N,CV_64F);
		for (int i = 0; i < spatial_mask_M*spatial_mask_N ; i++ ){
			if (spatial_mask[i] > 0)
				P12.at<double>(0,i) = 0;
			else
				P12.at<double>(0,i) = 1;
		}
		char* opt_fun = NULL;
		char* text = NULL;
		train_ensemble_main6_test(dimensions,xx1,xx2,opt_fun,text,options,Df,Dpf,I,J,K,L,M,N,priors,FFT_MODE,dummy_blur_mask,P12);    

		//  [ensemble,D_log,gamma_log]=train_ensemble_main6(dimensions,xx1,xx2,'','',[1e-4 0 1 0 0 MAX_ITERATIONS 0],Df,Dpf,I,J,K,L,M,N,priors,FFT_MODE,dummy_blur_mask,[1-(spatial_mask>0)]);    

		//  mx2 = reshape(train_ensemble_get(3,dimensions,ensemble.mx),M,N,C);

		//  % ************ END OF VARIATIONAL ********************
		}
	//	elseif strcmp(mode_im,'greenspan')  

	//	%%% Use default parameters
	//	s = create_greenspan_settings;
	//s.factor = 0;

	//[en,lo] = greenspan(obs,s);
	//mx2 = en;
	//%%% get size of estimated image at current scale
	//	%[M,N] = size(im);

	//%%% downsample obs_im (must work in intensity space)
	//	%obs_small = imresize(obs_im,([M N/2]/2)+1,'bilinear');

	//%%% use greenspan
	//	%[en,lo] = greenspan(obs_small,s);

	//%%% Now go back to gradient space
	//	%x_plane = conv2(en,[1 -1],'valid');
	//%y_plane = conv2(en,[1 -1]','valid');

	//	%mx2 = [x_plane(1:M,1:N/2), y_plane(1:M,1:N/2)];

	//else
	//	error foo
	//	end



	mexPrintf("\n");
	//mxArray* out = plhs[0] = mxCreateDoubleMatrix(s,s,mxREAL);
	//double *out_d = mxGetPr(out);

	}

		
		
