#include <sdkddkver.h>   // majic header file
#include <ctime>
#include <stack>
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <math.h>
#include <complex> 
#include <vector>
#include <float.h>
#include <opencv2/gpu/gpu.hpp>

#define PI       3.14159265358979323846

#include <cstringt.h>
#include <mex.h>

using namespace std;
using namespace cv;

  
// CPU variables
typedef struct {
	Mat x1,x2,mx,mx2, log_lambda_x,pi_x_2,opt_pi_x_2,ba_x_2,opt_ba_x_2,b_x_2,opt_b_x_2,D_x;
	double pi_x, a_x, b_x, a_sigma , ba_sigma_2 , opt_ba_sigma_2 , b_sigma, b_sigma_2, D_val; 	  
	}Ensemble;

typedef struct{
	Mat x1,x2,pi_x_2,ba_x_2,b_x_2;
	} Direction;

typedef struct{
	Mat pi;
	Mat gamma;	
	}Priors;

// GPU variables
typedef struct {
	gpu::GpuMat x1,x2,mx,mx2, log_lambda_x,pi_x_2,opt_pi_x_2,ba_x_2,opt_ba_x_2,b_x_2,opt_b_x_2,D_x;
	double pi_x, a_x, b_x, a_sigma , ba_sigma_2 , opt_ba_sigma_2 , b_sigma, b_sigma_2, D_val; 
	}gpuEnsemble;

typedef struct{
	gpu::GpuMat x1,x2,pi_x_2,ba_x_2,b_x_2;
	} gpuDirection;

typedef struct{
	gpu::GpuMat pi;
	gpu::GpuMat gamma;	
	}gpuPriors;
 
void train_ensemble_get_test(Mat** cx, int c, Mat dimensions, Mat x);
void tic();
void toc(string s,int ID);
void tictic();
void toctoc(string s,int ID);
void tictic_clear();

 
 