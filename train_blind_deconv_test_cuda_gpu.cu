﻿#define NX 4
#define NY 5
#define NRANK 2
#define BATCH 10



//#include <cufft.h>

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cufft.h>
#include <stdio.h>
#include "train_blind_deconv_test_cuda_gpu_kernel.cu"

#define BLOCKSIZE 512
#define MAX_BLOCK_SIZE 1024

/*void fft_R2C(float2 *h_out_data,float *h_in_data,int I,int J);*/
void fft_R2C(cufftComplex * d_out_data,float *h_in_data,int M,int N,int I,int J,int flag_square);
void fft_R2C_cuda_real(cufftComplex * d_out_data,cufftReal *d_in_data,int M,int N,int I,int J,int flag_square);
void fft_C2R(cufftReal * d_out_data,cufftComplex * d_in_data,int I,int J, int M, int N);
void fft_C2R_plus_corr(cufftReal *corr, cufftReal * d_out_data,cufftComplex * d_in_data,int I,int J, int M, int N);
 
  

void train_blind_deconv_test_cuda_gpu(Mat* dx1, Mat* dx2, double* rerror, int* datapoints,Mat dimensions,Ensemble ensemble,Mat D_mat,Mat Dp_mat,int I,int J,int K,int L,int M,int N){

	
	
	//nPlanes = dimensions(3,1);
	//%=========================================================================
	//	% Copy the expectations
	//	%=========================================================================
    int num_blocks = (I*J/BLOCKSIZE) + ((I*J%BLOCKSIZE) ? 1 : 0);
	

 	float  *h_out_data  = new float[I*J]; 


 	float  *h_out_val = new float[1];
	float *h_error = new float[1];

	Mat* mmu_mat;
	train_ensemble_get_test(&mmu_mat,0,dimensions,ensemble.mx); //mmu  = train_ensemble_get_test(1,dimensions,ensemble.mx);
	float mmu = (float)mmu_mat->at<double>(0,0);
	
	Mat* me_temp;
	train_ensemble_get_test(&me_temp,1,dimensions,ensemble.mx);//me   = reshape(train_ensemble_get_test(2,dimensions,ensemble.mx),K,L);
	float *h_me = new float[L*K];
	for (int r = 0; r < K; ++r)
	{		
		for (int c = 0; c < L; ++c)
		{
			h_me[r*L + c] = (float)(*me_temp).at<double>(0,K*c + r);
		}
	}	
	cufftReal *    me;  cudaMalloc((void**)&me , sizeof(cufftReal)*L*K); cudaMemcpy(me, h_me, sizeof(cufftReal)*L*K, cudaMemcpyHostToDevice);   
//debug----------------------
 	 
 	// mexPrintf("\n me in\n ");
  // 	for (int i = 0; i < K; ++i)
  // 		{
  // 		for (int j = 0; j < L; ++j)
  // 			{
  // 				// mexPrintf("%f\t ",(float)(*me_temp).at<double>(0,K*j+i));
		//  		mexPrintf("%f\t ",h_me[i*L + j]);
  // 			}
  // 		mexPrintf("\n ");
  // 		}

//-----------------------	
	

	me_temp->release();
	Mat temptemp3 = ensemble.mx2;
	Mat* mx_temp;
	train_ensemble_get_test(&mx_temp,2,dimensions,ensemble.mx);//mx   = reshape(train_ensemble_get_test(3,dimensions,ensemble.mx),M,N);
	float *h_mx = new float[M*N];
	for (int r = 0; r < M; ++r)
		{
		for (int c = 0; c < N; ++c)
			{
			h_mx[r*N + c] = (float)(*mx_temp).at<double>(0,M*c+r);
			}
		}

	mx_temp->release();
	cufftReal *    mx;  cudaMalloc((void**)&mx , sizeof(cufftReal)*M*N); cudaMemcpy(mx, h_mx, sizeof(cufftReal)*M*N, cudaMemcpyHostToDevice);   
	
	
	//debug----------------------
 	
	// mexPrintf("cmmu : %f ",mmu);
	// mexPrintf("\nme \n");
	// for (int r = 0;r < L*K; r++)
	// {
		// mexPrintf("%f ",(float)h_me[r]);
	// }
	// mexPrintf("\nmx\n",M,N);
	// for (int r = 0;r < M*N; r++)
	// {
		// mexPrintf("%f ",(float)h_mx[r]);
	// }
	// mexPrintf("\n\n\n");

	//-----------------------	


 	

	Mat* mmu2_mat;
	train_ensemble_get_test(&mmu2_mat,0,dimensions,ensemble.mx2);//mmu2 = train_ensemble_get_test(1,dimensions,ensemble.mx2);
	float mmu2 = (float)mmu2_mat->at<double>(0,0);
	
	Mat* me2_temp;
	train_ensemble_get_test(&me2_temp,1,dimensions,ensemble.mx2);//me2  = reshape(train_ensemble_get_test(2,dimensions,ensemble.mx2),K,L);
	//me2->reshape(K,L);
	float *h_me2 = new float[K*L];
	for (int r = 0; r < K; ++r)
		{
		for (int c = 0; c < L; ++c)
			{
			h_me2[r*L + c] = (float)(*me2_temp).at<double>(0,K*c+r);
			}
		}
//debug----------------------
 	 
 	// mexPrintf("\n me2 in\n ");
  // 	for (int i = 0; i < K; ++i)
  // 		{
  // 		for (int j = 0; j < L; ++j)
  // 			{
  // 				// mexPrintf("%f\t ",(float)(*me_temp).at<double>(0,K*j+i));
		//  		mexPrintf("%f\t ",h_me2[i*L + j]);
  // 			}
  // 		mexPrintf("\n ");
  // 		}

//-----------------------	
	me2_temp->release();
	cufftReal *    me2;  cudaMalloc((void**)&me2 , sizeof(cufftReal)*L*K); cudaMemcpy(me2, h_me2, sizeof(cufftReal)*L*K, cudaMemcpyHostToDevice);   

	Mat* mx2_temp;
	train_ensemble_get_test(&mx2_temp,2,dimensions,ensemble.mx2); //mx2  = reshape(train_ensemble_get_test(3,dimensions,ensemble.mx2),M,N);
	float *h_mx2 = new float[N*M];
	for (int r = 0; r < M; ++r)
		{ 
		for (int c = 0; c < N; ++c)
			{
			h_mx2[r*N + c] = (float)(*mx2_temp).at<double>(0,M*c+r);
			}
		}
//debug----------------------
 	 
 	// mexPrintf("\n mx2 in\n ");
  // 	for (int i = 0; i < M; ++i)
  // 		{
  // 		for (int j = 0; j < N; ++j)
  // 			{
  // 			// mexPrintf("%f\t ",(float)(*mx_temp).at<double>(0,M*j+i));
  // 			mexPrintf("%f\t ",h_mx2[i*N + j]);
  // 			}
  // 		mexPrintf("\n ");
  // 		}

//-----------------------	
	mx2_temp->release();
	cufftReal *    mx2;  cudaMalloc((void**)&mx2 , sizeof(cufftReal)*M*N); cudaMemcpy(mx2, h_mx2, sizeof(cufftReal)*M*N, cudaMemcpyHostToDevice); 


	// -----------------------------------------
	// debug
	// -----------------------------------------
	
	// mexPrintf("cmmu2 : %f ",mmu2);
	// mexPrintf("\nme \n");
	// for (int r = 0;r < L*K; r++)
	// {
		// mexPrintf("%f ",(float)h_me2[r]);
	// }
	// mexPrintf("\nmx\n",M,N);
	// for (int r = 0;r < M*N; r++)
	// {
		// mexPrintf("%f ",(float)h_mx2[r]);
	// }
	// mexPrintf("\n\n\n");
	// -----------------------------------------
	

	float *h_Dp = new float[I*J];
	for (int r = 0; r < I; ++r)
		{ 
		for (int c = 0; c < J; ++c)
			{
			h_Dp[r*J + c] = (float)Dp_mat.at<double>(0,J*r+c);
			}
		}
	cufftReal *Dp;       // Input array - device side
	cudaMalloc((void**)&Dp, sizeof(cufftReal)*I*J);
 	cudaMemcpy(Dp, h_Dp, sizeof(cufftReal)*I*J, cudaMemcpyHostToDevice); 
 

	float *h_D = new float[I*J];
	for (int r = 0; r < I; ++r)
		{ 
		for (int c = 0; c < J; ++c)
			{
			h_D[r*J + c] = (float)D_mat.at<double>(0,J*r+c);
			}
		}
	cufftReal *D;       // Input array - device side
	cudaMalloc((void**)&D, sizeof(cufftReal)*I*J);
 	cudaMemcpy(D, h_D, sizeof(cufftReal)*I*J, cudaMemcpyHostToDevice); 
	
	// -----------------------------------------
	// debug
	// -----------------------------------------
	  
	// mexPrintf("cpu\n");
	// for (int r = 0;r < I*J; r++)
	// {
		// mexPrintf("%f ",h_Dp[r]);
	// }
	// mexPrintf("\n");
	// for (int r = 0;r < I*J; r++)
	// {
		// mexPrintf("%f ",h_D[r]);
	// }
	// mexPrintf("\n\n\n");
	// -----------------------------------------
	
	//%============== ===========================================================
	 
	//	%=========================================================================
	//	% Evaluate  FFTs
	//	%=========================================================================
	double fft_time = (double)getTickCount();	
	dim3 blockDim(16, 16); 	dim3 gridDim((I + blockDim.x - 1) / blockDim.x , (J + blockDim.y - 1) / blockDim.y);

	// mx ffts
	cufftComplex * fft_mx;  cudaMalloc((void**)&fft_mx , sizeof(cufftComplex)*I*J);   	fft_R2C_cuda_real(fft_mx ,mx ,M,N,I,J,0); // by using this function, redundant cudamemcpy with 2 flag values inside the function could be removed 

//debug--
	// cufftReal *    corr_temp;  cudaMalloc((void**)&corr_temp , sizeof(cufftReal)*I*J);
	// complex_to_real<<<gridDim,blockDim>>>(corr_temp,fft_mx,I,J);
	//  // complex_to_img<<<gridDim,blockDim>>>(corr_temp,fft_mx3,I,J);
	// // fft_C2R(corr_temp,fft_mx3,I,J,I,J);
//enddebug

//debug----------------------
 	// cudaMemcpy(h_mx,mx, sizeof(cufftReal)*M*N, cudaMemcpyDeviceToHost); 
 	// mexPrintf("\n mx in\n ");
  // 	for (int i = 0; i < M; ++i)
  // 		{
  // 		for (int j = 0; j < N; ++j)
  // 			{
  // 			// mexPrintf("%f\t ",(float)(*mx_temp).at<double>(0,M*j+i));
  // 			mexPrintf("%f\t ",h_mx[i*N + j]);
  // 			}
  // 		mexPrintf("\n ");
  // 		}

//-----------------------	

//debug----------------------

 	// cudaMemcpy(h_out_data,corr_temp   , sizeof(float)*I*J, cudaMemcpyDeviceToHost);
 	// // cudaError  cudaStat13 = 	cudaMemcpy(h_out_val,mu1   , sizeof(float), cudaMemcpyDeviceToHost);
 	 
 	// mexPrintf("\n fft_mx real\n ");
  // 	for (int i = 0; i < I; i++)
  // 		{
  // 		for (int j = 0; j < J; j++)
  // 			{
  // 			mexPrintf("%5f\t ",h_out_data[i*J + j]);
  // 			}
  // 		mexPrintf("\n ");
  // 		}
 	// mexPrintf("\n mu1 %f GPU ",h_out_val[0]);
//-----------------------


	cufftComplex * fft_mx2; cudaMalloc((void**)&fft_mx2, sizeof(cufftComplex)*I*J); 	fft_R2C_cuda_real(fft_mx2,mx2,M,N,I,J,0);
	cufftComplex * fft_mx3; cudaMalloc((void**)&fft_mx3, sizeof(cufftComplex)*I*J); 	fft_R2C_cuda_real(fft_mx3,mx ,M,N,I,J,1); // last parameter , is a flag that'll square the input vector
	// me ffts
	cufftComplex * fft_me;  cudaMalloc((void**)&fft_me , sizeof(cufftComplex)*I*J);    	fft_R2C_cuda_real(fft_me ,me ,K,L,I,J,0);
	cufftComplex * fft_me2; cudaMalloc((void**)&fft_me2, sizeof(cufftComplex)*I*J);    	fft_R2C_cuda_real(fft_me2,me2,K,L,I,J,0);
	cufftComplex * fft_me3; cudaMalloc((void**)&fft_me3, sizeof(cufftComplex)*I*J);    	fft_R2C_cuda_real(fft_me3,me ,K,L,I,J,1);
	// Dp fft
	cufftComplex * fft_Dp;  cudaMalloc((void**)&fft_Dp , sizeof(cufftComplex)*I*J);    	fft_R2C(fft_Dp ,h_Dp ,I,J,I,J,0);

	cufftComplex * fft_mD;  				cudaMalloc((void**)&fft_mD , sizeof(cufftComplex)*I*J);
	cufftComplex * fft_me2_mx2_me3_mx3;  	cudaMalloc((void**)&fft_me2_mx2_me3_mx3 , sizeof(cufftComplex)*I*J);


	// mD ifft
	fft_me2_mx2_me3_mx3_kernel<<<gridDim,blockDim>>>(fft_mD,fft_me,fft_mx,fft_me2_mx2_me3_mx3,fft_me2,fft_mx2,fft_me3,fft_mx3,I,J);


	cufftReal *    me_mx2_me_mx3;  cudaMalloc((void**)&me_mx2_me_mx3 , sizeof(cufftReal)*I*J);fft_C2R(me_mx2_me_mx3,fft_me2_mx2_me3_mx3,I,J,I,J);
	cufftReal *    mD;      cudaMalloc((void**)&mD     , sizeof(cufftReal   )*I*J);     fft_C2R(mD,fft_mD,I,J,I,J);




	cufftReal *    Dp_D_mD_mmu;  cudaMalloc((void**)&Dp_D_mD_mmu , sizeof(cufftReal)*I*J);  

  	cufftReal *    error_mat;  cudaMalloc((void**)&error_mat , sizeof(cufftReal)*I*J);

	 
// sum reduction in same kernel with thread sync---------------------
	//error_mat_kernel<<<gridDim,blockDim>>>(mu1,error, mu2,error_mat, Dp_D_mD_mmu,D,mD,mmu,mmu2,me_mx2_me_mx3,Dp,I,J); 
//sum reduction in separate kernel---

	float* mu1;			cudaMalloc((void**) &mu1, sizeof(float));	 
	float* error;		cudaMalloc((void**) &error, sizeof(float));	 
	float* mu2;			cudaMalloc((void**) &mu2, sizeof(float));	 

	float *error_partial_sums; 	cudaMalloc((void**)&error_partial_sums, sizeof(float) * (num_blocks));
	float *mu1_partial_sums; 	cudaMalloc((void**)&mu1_partial_sums, sizeof(float) * (num_blocks));
	float *mu2_partial_sums; 	cudaMalloc((void**)&mu2_partial_sums, sizeof(float) * (num_blocks));

	if(I*J > BLOCKSIZE*MAX_BLOCK_SIZE){
		error_mat_kernel_no_sum_error_full<<<gridDim,blockDim>>>(error_mat, Dp_D_mD_mmu,D,mD,mmu,mmu2,me_mx2_me_mx3,Dp,I,J); 
		error_mat_kernel_sum_only_error_full<<<gridDim,blockDim>>>(mu1, error, mu2, error_mat, D,mD,mmu, mmu2,Dp, I, J);
	}
	else{
	//separate reduction kernels
		error_mat_kernel_no_sum_new<<<gridDim,blockDim>>>(error_mat, Dp_D_mD_mmu,D,mD,mmu,mmu2,me_mx2_me_mx3,Dp,I,J); 
		/// use D as array for mu1, Dp as array for data points
	    dim3 grid_unidim(num_blocks, 1, 1);
	    dim3 threads_unidim(BLOCKSIZE, 1, 1);
	    sum_kernel<<< grid_unidim, threads_unidim >>>(error_mat,error_partial_sums,Dp,mu2_partial_sums,D,mu1_partial_sums, I*J);
		sum_kernel<<<1,num_blocks>>>(error_partial_sums,error,mu2_partial_sums,mu2,mu1_partial_sums,mu1, num_blocks);
	}
	

	cudaFree(error_partial_sums);
	cudaFree(mu2_partial_sums);
	cudaFree(mu1_partial_sums);

//-------------------------------------------------------------
	

	cufftComplex * fft_err;  cudaMalloc((void**)&fft_err , sizeof(cufftComplex)*I*J);   fft_R2C_cuda_real(fft_err ,Dp_D_mD_mmu ,I,J,I,J,0);

	fft_conj_mult<<<gridDim,blockDim>>>(fft_Dp,fft_err,fft_me,fft_mx,fft_me2,fft_mx2,fft_me3,fft_mx3,I,J);
	cufftReal *    corr_e;  cudaMalloc((void**)&corr_e , sizeof(cufftReal)*L*K);
	cufftReal *    corr_x;  cudaMalloc((void**)&corr_x , sizeof(cufftReal)*M*N);

	


	fft_C2R(corr_e,fft_mx3,I,J,K,L);
	fft_C2R(corr_x,fft_me3,I,J,M,N);
	fft_C2R(me2,fft_mx2,I,J,K,L);
	fft_C2R(mx2,fft_me2,I,J,M,N);
	
	fft_C2R_plus_corr(corr_e, me,fft_mx,I,J,K,L);
	fft_C2R_plus_corr(corr_x, mx,fft_me,I,J,M,N);





 	// cudaError  cudaStat4 = 	cudaMemcpy(h_out_data,corr_temp   , sizeof(cufftReal)*I*J, cudaMemcpyDeviceToHost);
 	// cudaError  cudaStat4 = 	cudaMemcpy(h_out_data,me   , sizeof(cufftReal)*K*L, cudaMemcpyDeviceToHost);
 	//cudaError  cudaStat4 = 	cudaMemcpy(h_out_data,mx   , sizeof(cufftReal)*M*N, cudaMemcpyDeviceToHost);

 	

 	cudaError  cudaStat4 = 	cudaMemcpy(&mmu,mu1    , sizeof(float), cudaMemcpyDeviceToHost);
 	cudaError  cudaStat5 = 	cudaMemcpy(&mmu2,mu2   , sizeof(float), cudaMemcpyDeviceToHost);

 	cudaError  cudaStat6 = 	cudaMemcpy(h_me,me   , sizeof(cufftReal)*K*L, cudaMemcpyDeviceToHost);
 	cudaError  cudaStat7 = 	cudaMemcpy(h_me2,me2   , sizeof(cufftReal)*K*L, cudaMemcpyDeviceToHost);

 	cudaError  cudaStat8 = 	cudaMemcpy(h_mx,mx   , sizeof(cufftReal)*M*N, cudaMemcpyDeviceToHost);
 	cudaError  cudaStat9 = 	cudaMemcpy(h_mx2,mx2   , sizeof(cufftReal)*M*N, cudaMemcpyDeviceToHost);

 	cudaError  cudaStat10 = 	cudaMemcpy(h_error,error    , sizeof(float), cudaMemcpyDeviceToHost);


 	*rerror = (double) (*h_error);
 	*datapoints = (int) mmu2;


 
	cudaFree(fft_mx); cudaFree(fft_mx2); cudaFree(fft_mx3);
	cudaFree(fft_me); cudaFree(fft_me2); cudaFree(fft_me3);
	cudaFree(fft_Dp);  
	cudaFree(fft_err);  

	cudaFree(mu1			);
	cudaFree(error 			);
	cudaFree(mu2	);

	cudaFree(error_mat	);
	cudaFree(me_mx2_me_mx3	);

	cudaFree(Dp_D_mD_mmu);  
	cudaFree(D);  
	cudaFree(Dp);  

	cudaFree(fft_me2_mx2_me3_mx3);  

	cudaFree(fft_mD); cudaFree(mD);  

	cudaFree(me);  
	cudaFree(me2);  
	cudaFree(mx);  
	cudaFree(mx2);  


	cudaFree(corr_e	);
	cudaFree(corr_x	);

	//debug -----
	// cudaFree(corr_temp);
	//--------------

	dx1->at<double>(0,0) = (double)mmu;
	dx2->at<double>(0,0) = (double)mmu2;
	int idx=1;
	for (int j = 0;j < L ; j++){
		for(int i=0;i <K ; i++){
			dx1->at<double>(0,idx) = (double) h_me[i*L + j];
			dx2->at<double>(0,idx) = (double) h_me2[i*L + j];
			idx++;
		}
	}
	for (int j = 0;j < N ; j++){
		for(int i=0;i <M ; i++){
			dx1->at<double>(0,idx) = (double) h_mx[i*N + j];
			dx2->at<double>(0,idx) = (double) h_mx2[i*N + j];
			idx++;
		}
	}

	 


 	// mexPrintf("\n output\n ");
  	// for (int i = 0; i < (dimensions.at<int>(0,1) + dimensions.at<int>(1,1) + dimensions.at<int>(2,1)); i++)
	// {
		// {
		// mexPrintf("%f ",dx1->at<double>(0,i));
		// }
	// }
	// mexPrintf("\n output\n ");
	// for (int i = 0; i < (dimensions.at<int>(0,1) + dimensions.at<int>(1,1) + dimensions.at<int>(2,1)); i++)
	// {
		// {
		// mexPrintf("%f ",dx2->at<double>(0,i));
		// }
	// }
	// mexPrintf("\ndata_points %d error %f\n ",*datapoints,*rerror);

	delete h_out_data;
 	delete h_out_val;
 	delete h_error;
 	delete mmu_mat;
 	delete h_me;
 	delete h_mx;
	delete mmu2_mat;
	delete h_me2; 	
	delete h_Dp;
	delete h_D;
	 
}

void fft_R2C_cuda_real(cufftComplex * d_out_data,cufftReal *d_in_data,int M,int N,int I,int J,int flag_square){

	// Output array - host side
	// float2 *h_out_data_temp = new float2[I*(J/2+1)] ; 

	cufftHandle plan;
	cufftReal *d_in_data_padded;       // Input array - device side	
	     // Output array - device side - full array 
	cufftComplex * d_out_data_temp;  // Output array - device side - half array (output from the fft2)
	 
	//debug- ---------------------------------------------------------------------------------------------------------------
	// float  *print_data  = new float[I*J]; 
	// cufftReal *    corr_temp;  cudaMalloc((void**)&corr_temp , sizeof(cufftReal)*I*J);
	//-----------------------------------------------------------------------------------------------------------------------

	 
	cudaMalloc((void**)&d_in_data_padded, sizeof(cufftReal)*I*J);

	// if (flag_square)
	// 	{
	// 	dim3 blockDim0(16, 16);
	// 	dim3 gridDim0((M + blockDim0.x - 1) / blockDim0.x , (N + blockDim0.y - 1) / blockDim0.y);
	// 	square<<<gridDim0,blockDim0>>>(d_in_data,d_in_data,M,N);
	// 	}

	dim3 blockDim(16, 16);
	dim3 gridDim((I + blockDim.x - 1) / blockDim.x , (J + blockDim.y - 1) / blockDim.y);
	if(flag_square){
		zero_pad_input_sq_flag<<<gridDim,blockDim>>>(d_in_data_padded,d_in_data,M,N,I,J,1);
	}
	else {
		zero_pad_input_sq_flag<<<gridDim,blockDim>>>(d_in_data_padded,d_in_data,M,N,I,J,0);
	}
	 
	cudaMalloc((void**)&d_out_data_temp, sizeof(cufftComplex)*I*(J/2 + 1));
	


	//Performe the fft
	cufftPlan2d(&plan, I, J , CUFFT_R2C);
	cufftSetCompatibilityMode(plan, CUFFT_COMPATIBILITY_NATIVE);
	// debug --------------------------------------------------------------------------------------------------
 	// cudaMemcpy(print_data,d_in_data_padded, sizeof(cufftReal)*I*J, cudaMemcpyDeviceToHost); 
 	// mexPrintf("\n fft in\n ");
  // 	for (int i = 0; i < I; ++i)
  // 		{
  // 		for (int j = 0; j < J; ++j)
  // 			{
  // 			// mexPrintf("%f\t ",(float)(*mx_temp).at<double>(0,M*j+i));
  // 			mexPrintf("%f\t ",print_data[i*J + j]);
  // 			}
  // 		mexPrintf("\n ");
  // 		}	
  // 		mexPrintf("\n end of fft in\n ");
	//------------------------------------------------------------------------------------------------------

	cufftExecR2C(plan, d_in_data_padded, d_out_data_temp);
	cudaThreadSynchronize();
	 
	expand_output<<<gridDim,blockDim>>>(d_out_data,d_out_data_temp,I,J);  // d_in_data - halved matrix - input to ifft2 - C2R

	// debug --------------------------------------------------------------------------------------------------
	// complex_to_real<<<gridDim,blockDim>>>(corr_temp,d_out_data,I,J);
 // 	cudaMemcpy(print_data,corr_temp, sizeof(cufftReal)*I*J, cudaMemcpyDeviceToHost); 
 // 	mexPrintf("\n fft out real\n ");
 //  	for (int i = 0; i < I; ++i)
 //  		{
 //  		for (int j = 0; j < J; ++j)
 //  			{
 //  			// mexPrintf("%f\t ",(float)(*mx_temp).at<double>(0,M*j+i));
 //  			mexPrintf("%f\t ",print_data[i*J + j]);
 //  			}
 //  		mexPrintf("\n ");
 //  		}	
 //  		mexPrintf("\n end of fft out\n ");
	// cudaFree(corr_temp);
	//------------------------------------------------------------------------------------------------------
	
 
	cufftDestroy(plan);
	cudaFree(d_out_data_temp);
	cudaFree(d_in_data_padded);	
	

	// delete [] h_out_data_temp;


	}


void fft_R2C(cufftComplex * d_out_data,float *h_in_data,int M,int N,int I,int J,int flag_square){

	// Output array - host side
	float2 *h_out_data_temp = new float2[I*(J/2+1)] ; 

	cufftHandle plan;
	cufftReal *d_in_data;       // Input array - device side
	cufftReal *d_in_data_padded;       // Input array - device side	
	     // Output array - device side - full array 
	cufftComplex * d_out_data_temp;  // Output array - device side - half array (output from the fft2)
	 
	 
	cudaMalloc((void**)&d_in_data_padded, sizeof(cufftReal)*I*J);
	cudaMalloc((void**)&d_in_data, sizeof(cufftReal)*M*N);
	cudaMemcpy(d_in_data, h_in_data, sizeof(cufftReal)*M*N, cudaMemcpyHostToDevice);    

	if (flag_square)
		{
		dim3 blockDim0(16, 16);
		dim3 gridDim0((M + blockDim0.x - 1) / blockDim0.x , (N + blockDim0.y - 1) / blockDim0.y);
		square<<<gridDim0,blockDim0>>>(d_in_data,d_in_data,M,N);
		}

	dim3 blockDim(16, 16);
	dim3 gridDim((I + blockDim.x - 1) / blockDim.x , (J + blockDim.y - 1) / blockDim.y);
	zero_pad_input<<<gridDim,blockDim>>>(d_in_data_padded,d_in_data,M,N,I,J);

	 
	cudaMalloc((void**)&d_out_data_temp, sizeof(cufftComplex)*I*(J/2 + 1));
	


	//Performe the fft
	cufftPlan2d(&plan, I, J , CUFFT_R2C);
	cufftSetCompatibilityMode(plan, CUFFT_COMPATIBILITY_NATIVE);
	cufftExecR2C(plan, d_in_data_padded, d_out_data_temp);
	cudaThreadSynchronize();
	 
	expand_output<<<gridDim,blockDim>>>(d_out_data,d_out_data_temp,I,J);  // d_in_data - halved matrix - input to ifft2 - C2R
	
 
	cufftDestroy(plan);
	//cudaFree(d_out_data);
	cudaFree(d_out_data_temp);
	cudaFree(d_in_data); 
	cudaFree(d_in_data_padded);	

	delete [] h_out_data_temp;


	}

void fft_C2R(cufftReal * d_out_data, cufftComplex *d_in_data_temp, int I, int J, int M, int N){

	// ***********************
	// Make the half matrix on CPU 
	// Output array - host side
	// 	float2 *h_in_data_temp = new float2[I*(J/2+1)] ; 
	// 
	// 	for (int i = 0; i < I ; i++)
	// 		for (int j = 0; j < J ; j++)
	// 			if ( j < J/2 + 1)
	// 				h_in_data_temp[i*(J/2+1) + j] = h_in_data[i*J + j];
	// ***********************

	cufftHandle plan;
	cufftComplex *d_in_data;       // Input array half - device side
	cufftReal * d_out_data_IJ; cudaMalloc((void**)&d_out_data_IJ , sizeof(cufftReal)*I*J);


	  // Input array full- device side
	  // Output array - device side
	int n[NRANK] = {I, J};

	cudaMalloc((void**)&d_in_data, sizeof(cufftComplex)*I*(J/2 + 1));
	//cudaMemcpy(d_in_data, h_in_data_temp, sizeof(cufftComplex)*I*(J/2 + 1), cudaMemcpyHostToDevice);  // Uncomment if it is already haved in CPU
	
	// ******************
	// Halve the input in gpu
 	//cudaMalloc((void**)&d_in_data_temp, sizeof(cufftComplex)*I*J);                    
 	//cudaMemcpy(d_in_data_temp, h_in_data, sizeof(float2)*I*J, cudaMemcpyHostToDevice);

	// Halve the input array
	dim3 blockDim(16, 16);
	dim3 gridDim((I + blockDim.x - 1) / blockDim.x , (J/2 + 1 + blockDim.y - 1) / blockDim.y);
	halve_input<<<gridDim,blockDim>>>(d_in_data,d_in_data_temp,I,J);  // d_in_data - halved matrix - input to ifft2 - C2R
	// ******************

	

	//Performe the fft
	cufftPlan2d(&plan, I, J , CUFFT_C2R);
	cufftSetCompatibilityMode(plan, CUFFT_COMPATIBILITY_NATIVE);
	cufftExecC2R(plan, d_in_data, d_out_data_IJ);
	cudaThreadSynchronize();

	// Noramlize the output - on GPU
	dim3 blockDim2(16, 16); 	dim3 gridDim2((M + blockDim.x - 1) / blockDim.x , (N + blockDim.y - 1) / blockDim.y);
	normalize_ifft<<<gridDim2,blockDim2>>>(d_out_data,d_out_data_IJ,I,J,M,N);


	cufftDestroy(plan);
	//cudaFree(d_in_data_temp);
	cudaFree(d_in_data);
	cudaFree(d_out_data_IJ);
 


	}

void fft_C2R_plus_corr(cufftReal* corr, cufftReal * d_out_data, cufftComplex *d_in_data_temp, int I, int J, int M, int N){

	// ***********************
	// Make the half matrix on CPU 
	// Output array - host side
	// 	float2 *h_in_data_temp = new float2[I*(J/2+1)] ; 
	// 
	// 	for (int i = 0; i < I ; i++)
	// 		for (int j = 0; j < J ; j++)
	// 			if ( j < J/2 + 1)
	// 				h_in_data_temp[i*(J/2+1) + j] = h_in_data[i*J + j];
	// ***********************

	cufftHandle plan;
	cufftComplex *d_in_data;       // Input array half - device side
	cufftReal * d_out_data_IJ; cudaMalloc((void**)&d_out_data_IJ , sizeof(cufftReal)*I*J);


	  // Input array full- device side
	  // Output array - device side
	int n[NRANK] = {I, J};

	cudaMalloc((void**)&d_in_data, sizeof(cufftComplex)*I*(J/2 + 1));
	//cudaMemcpy(d_in_data, h_in_data_temp, sizeof(cufftComplex)*I*(J/2 + 1), cudaMemcpyHostToDevice);  // Uncomment if it is already haved in CPU
	
	// ******************
	// Halve the input in gpu
 	//cudaMalloc((void**)&d_in_data_temp, sizeof(cufftComplex)*I*J);                    
 	//cudaMemcpy(d_in_data_temp, h_in_data, sizeof(float2)*I*J, cudaMemcpyHostToDevice);

	// Halve the input array
	dim3 blockDim(16, 16);
	dim3 gridDim((I + blockDim.x - 1) / blockDim.x , (J/2 + 1 + blockDim.y - 1) / blockDim.y);
	halve_input<<<gridDim,blockDim>>>(d_in_data,d_in_data_temp,I,J);  // d_in_data - halved matrix - input to ifft2 - C2R
	// ******************

	

	//Performe the fft
	cufftPlan2d(&plan, I, J , CUFFT_C2R);
	cufftSetCompatibilityMode(plan, CUFFT_COMPATIBILITY_NATIVE);
	cufftExecC2R(plan, d_in_data, d_out_data_IJ);
	cudaThreadSynchronize();

	// Noramlize the output - on GPU
	dim3 blockDim2(16, 16); 	dim3 gridDim2((M + blockDim.x - 1) / blockDim.x , (N + blockDim.y - 1) / blockDim.y);
	normalize_ifft_plus_corr<<<gridDim2,blockDim2>>>(corr,d_out_data,d_out_data_IJ,I,J,M,N);


	cufftDestroy(plan);
	//cudaFree(d_in_data_temp);
	cudaFree(d_in_data);
	cudaFree(d_out_data_IJ);
 


	}


void fft_R2C_old(float2 *h_out_data,float **h_in_data,int M,int N,int I,int J){


	float *mx_padded = new float[I*J];  
	for (int r = 0; r < I; ++r)  // this can be also done on GPU
		{	 
		for (int c = 0; c < J; ++c)
			{
			if (r < M && c < N)
				mx_padded[r*J + c] = h_in_data[r][c];
			else
				mx_padded[r*J + c]  = 0;
			}
		}


	// Output array - host side
	float2 *h_out_data_temp = new float2[I*(J/2+1)] ; 

	cufftHandle plan;
	cufftReal *d_in_data;       // Input array - device side
	cufftComplex * d_out_data;  // Output array - device side
	int n[NRANK] = {I, J};


	cudaMalloc((void**)&d_in_data, sizeof(cufftReal)*I*J);
	cudaMemcpy(d_in_data, mx_padded, sizeof(cufftReal)*I*J, cudaMemcpyHostToDevice);    	  
	cudaMalloc((void**)&d_out_data, sizeof(cufftComplex)*I*(J/2 + 1));


	//Performe the fft
	cufftPlan2d(&plan, I, J , CUFFT_R2C);
	cufftSetCompatibilityMode(plan, CUFFT_COMPATIBILITY_NATIVE);
	cufftExecR2C(plan, d_in_data, d_out_data);
	cudaThreadSynchronize();
	cudaError  cudaStat2 = 	cudaMemcpy(h_out_data_temp,d_out_data,  sizeof(cufftComplex)*I*(J/2+1), cudaMemcpyDeviceToHost);

	for (int i = 0; i < I ; i++){
		for (int j = 0; j < J ; j++)
			{
			if ( j < J/2 + 1)
				h_out_data[i*J + j] = h_out_data_temp[i*(J/2+1) + j];
			else if ( i == 0)
				{ 
				h_out_data[i*J + j].x = h_out_data_temp[i*(J/2+1) + (J-j)].x;
				h_out_data[i*J + j].y = -h_out_data_temp[i*(J/2+1) + (J-j)].y;
				}
			else 
				{
				h_out_data[i*J + j].x = h_out_data_temp[(I-i)*(J/2+1) + (J-j)].x;
				h_out_data[i*J + j].y = -h_out_data_temp[(I-i)*(J/2+1) + (J-j)].y;
				}
			}
		}


	cufftDestroy(plan);
	cudaFree(d_out_data);


	}