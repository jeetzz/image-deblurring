#ifndef TRAIN_ENSEMBLE_EVIDENCE6_TEST_KERNELS_H
#define TRAIN_ENSEMBLE_EVIDENCE6_TEST_KERNELS_H

#define _a_ 2.751938393884109
//a = PI/(PI-2)
#define _a2_ 7.573164923733449
// a^2

#define  MIN_FLOAT -3.4e+38F
__global__ void copy_device_floats_to_double(float * d_c_log_lambda_x_2,double * d_c_log_lambda_x_2_dbl, float* d_cmx, double* d_cmx_dbl, float* d_cmx2, double* d_cmx2_dbl,
float* d_opt_c_b_x_2, double* d_opt_c_b_x_2_dbl,float* d_opt_c_pi_x_2, double* d_opt_c_pi_x_2_dbl,float* d_opt_c_ba_x_2, double* d_opt_c_ba_x_2_dbl,
float* d_opt_cx1, double* d_opt_cx1_dbl, float* d_opt_cx2, double* d_opt_cx2_dbl, 
double* grad_d_pi_x_2_dbl, double* grad_d_b_x_2_dbl, double* grad_d_ba_x_2_dbl, int total_length, int comp_length, int total_comp_length, int state )
{
	unsigned int i =  blockIdx.x * blockDim.x + threadIdx.x;
	if (i < total_comp_length)
	{
		d_c_log_lambda_x_2_dbl[i] = (double) d_c_log_lambda_x_2[i];
	}
	if (i < total_length)
	{
		d_cmx_dbl[i] = (double) d_cmx[i];
		d_cmx2_dbl[i] = (double) d_cmx2[i];
		d_opt_cx1_dbl[i] = (double) d_opt_cx1[i];
		d_opt_cx2_dbl[i] = (double) d_opt_cx2[i];
	}
	if(i < comp_length)
	{
		if(state >=2){
			grad_d_pi_x_2_dbl[i] = (double) d_opt_c_pi_x_2[i];
			grad_d_b_x_2_dbl[i] = (double) d_opt_c_b_x_2[i];
			grad_d_ba_x_2_dbl[i] = (double) d_opt_c_ba_x_2[i];
		}
		else {
			grad_d_pi_x_2_dbl[i] = 0.0;
			grad_d_b_x_2_dbl[i]  = 0.0;
			grad_d_ba_x_2_dbl[i] = 0.0;		
		}
		d_opt_c_b_x_2_dbl[i] = (double) d_opt_c_b_x_2[i];
		d_opt_c_pi_x_2_dbl[i] = (double) d_opt_c_pi_x_2[i];
		d_opt_c_ba_x_2_dbl[i] = (double) d_opt_c_ba_x_2[i];
	}
}
__global__ void update_grad_x1_x2_new(float * x1,float * x2,float * t_dx1,float * t_dx2,double ba_sigma_2, float * d_dx1,float *d_dx2,int c1,int c2,int c3,int f1,int f2, int f3)
{
	unsigned int i =  blockIdx.x * blockDim.x + threadIdx.x;
	if ( i < c1 )	{
		if (f1  == 0)
		{
			x1[i] = 0.0;
			x2[i]  = 0.0;
		}
		else {
			x1[i] = x1[i] + d_dx1[i]*(double)ba_sigma_2;
			x1[i] = x1[i] - t_dx1[i];
			x2[i] = x2[i] + d_dx2[i]*(double)ba_sigma_2;
			x2[i] = x2[i] - t_dx2[i];
		}
	}	
	else if ( i < c1 + c2 ) {
		if (f2  == 0)
		{
			x1[i] = 0.0;
			x2[i]  = 0.0;
		}
		else{
			x1[i] = x1[i] + d_dx1[i]*(double)ba_sigma_2;
			x1[i] = x1[i] - t_dx1[i];
			x2[i] = x2[i] + d_dx2[i]*(double)ba_sigma_2;
			x2[i] = x2[i] - t_dx2[i];		
		}
	}		
	else if ( i < c1 + c2 + c3 )	{
		if (f3  == 0)
		{
			x1[i] = 0.0;
			x2[i]  = 0.0;
		}
		else{
			x1[i] = x1[i] + d_dx1[i]*(double)ba_sigma_2;
			x1[i] = x1[i] - t_dx1[i];
			x2[i] = x2[i] + d_dx2[i]*(double)ba_sigma_2;
			x2[i] = x2[i] - t_dx2[i];		
		}
	}	

}

// __global__ void reset_grad_x1_x2_by_key(float * x1,float * x2,int c1,int c2,int c3,int f1,int f2, int f3)
// {
	// unsigned int i =  blockIdx.x * blockDim.x + threadIdx.x;
 
	// if ( i < c1 )	{
		// if (f1  == 0)
		// {
			// x1[i] = 0;
			// x2[i]  = 0;
		// }
	// }	
	// else if ( i < c1 + c2 ) {
		// if (f2  == 0)
		// {
			// x1[i] = 0;
			// x2[i]  = 0;
		// }
	// }		
	// else if ( i < c1 + c2 + c3 )	{
		// if (f3  == 0)
		// {
			// x1[i] = 0;
			// x2[i]  = 0;
		// }
	// }	
	
	   
// }


// __global__ void update_grad_all(float * x1,float * x2,float * x3,float * t_dx1,float * t_dx2,float * t_dx3,int total_length,bool set3rd)
// {
	// unsigned int i =  blockIdx.x * blockDim.x + threadIdx.x;
	// if (i < total_length)
	// {
		// x1[i] = x1[i] - t_dx1[i];
		// x2[i] = x2[i] - t_dx2[i];
		// if (set3rd)
			// x3[i] = x3[i] - t_dx3[i];
	// }
// }

// __global__ void update_grad_x1_x2(float * x1,float * x2,float * d_dx1,float * d_dx2, double ba_sigma_2,int total_length)
// {
	// unsigned int i =  blockIdx.x * blockDim.x + threadIdx.x;
	// if (i < total_length)
	// {
		// x1[i] = x1[i] + d_dx1[i]*(double)ba_sigma_2;
		// x2[i] = x2[i] + d_dx2[i]*(double)ba_sigma_2;
	// }

// }
__global__ void swap_d_c_log_lambda_x(float *d_c_log_lambda_x_2,float * d_c_log_lambda_x,int b1,int b2,int b3,int width){
	
	unsigned int i =  blockIdx.x * blockDim.x + threadIdx.x;
	int dimensions[3][2] = { 	{1 ,b1  }, 
								{1 ,b2  },
								{1 ,b3  }, 	};
								
	int total_length = b1+b2+b3;
	int k = 0;
	d_c_log_lambda_x_2[i] = -1;
	if (i < total_length*width)
	{
		if (i < 4*b1)
		{
			
			if (i <  b1)   
			k = i   + 0*total_length;
			else if (i <  2*b1)        
			k = i - b1  + 1*total_length;
			else if (i < 3*b1)
			k = i  - 2*b1  + 2*total_length;
			else            
			k = i - 3*b1  + 3*total_length;
			
			
		}
		else if (i < 4*(b1+b2))
		{
			if (i < 4*b1 + b2)          
			k = i - 4*b1  + 0*total_length + b1;
			else if (i < 4*b1 + 2*b2   )       
			k = i - 4*b1 - b2  + 1*total_length+ b1;
			else if (i < 4*b1 + 3*b2    )     
			k = i - 4*b1 - 2*b2  + 2*total_length+ b1;
			else            
			k = i - 4*b1 - 3*b2  + 3*total_length+ b1;
			
			i = i-3;
			 
		}
		else if (i < 4*(b1+b2+b3))
		{
			
			if (i < 4*(b1+b2) + b3  )          
			k = i - 4*(b1+b2)  + 0*total_length + b1 + b2;
			else if (i < 4*(b1+b2) + 2*b3  )       
			k = i - 4*(b1+b2) - b3  + 1*total_length + b1 + b2;
			else if (i < 4*(b1+b2) + 3*b3 )       
			k = i - 4*(b1+b2) - 2*b3  + 2*total_length + b1 + b2;
			else            
			k = i - 4*(b1+b2) - 3*b3  + 3*total_length + b1 + b2;
			
			i = i-3; 
			
		}

		 
		 d_c_log_lambda_x_2[i] = d_c_log_lambda_x[k];			
	}
	
}
__global__ void calc_sum5(float * d_sum5,float * d_c_log_lambda_x,int b1,int b2,int b3,int c1,int c2, int c3)
{
	unsigned int i =  blockIdx.x * blockDim.x + threadIdx.x;
	
	int dimensions[3][3] = { 	{1 ,b1 ,c1 }, 
								{1 ,b2 ,c2 },
								{1 ,b3 ,c3 }, 	};
	int c = -1;
	if (i < b1+b2+b3)
	{
		if ( i < b1 )	c = 0;			
		else if ( i < b1 + b2 ) c = 1;			
		else if ( i < b1 + b2 + b3 )	c = 2;		
		
		d_sum5[i] = 0;
		for (int k = 0; k < dimensions[c][2];k++)
			d_sum5[i] += d_c_log_lambda_x[i + k*(b1+b2+b3)]*std::exp(d_c_log_lambda_x[i + k*(b1+b2+b3)]);
		
	}

}

__global__ void calc_sum4(float * d_sum4,float * d_c_pi_x_2,float * d_opt_c_pi_x_2,float * d_temp2,float * d_temp5,float pi_x,int c1,int c2,int c3)
{
	unsigned int i =  blockIdx.x * blockDim.x + threadIdx.x;
	int c[] = {c1,c2,c3};
 
	if (i < 3)
		d_sum4[i] =  - lgamma(c[i]*pi_x) + lgamma(d_temp2[i]) + (0.5/d_temp2[i] - std::log(d_temp2[i]) )*d_temp5[i];
		 
}

__global__ void calc_sum3 (float * sum3,float * d_c_pi_x_2,float * d_opt_c_pi_x_2,float pi_x,int comp_length)
{		
		unsigned int i =  blockIdx.x * blockDim.x + threadIdx.x;
		
		if ( i < comp_length)
			sum3[i] = lgamma(pi_x) - lgamma(d_c_pi_x_2[i]) + (d_c_pi_x_2[i] - d_opt_c_pi_x_2[i])*(std::log(d_c_pi_x_2[i]) - 0.5/d_c_pi_x_2[i]);
		
				
}
__global__ void calc_sum2(float * sum2,float * d_c_b_x_2,float *  d_c_ba_x_2,float *  d_opt_c_b_x_2,float * d_opt_c_ba_x_2,float a_x, float b_x,int comp_length,int * comp_keys,int c1,int c2,int c3)
	{
	
	unsigned int i =  blockIdx.x * blockDim.x + threadIdx.x;
	
	if (i < comp_length)
	{
		sum2[i] = lgamma(b_x) - b_x*std::log(a_x)- lgamma(d_c_b_x_2[i]) + std::log(d_c_b_x_2[i]/d_c_ba_x_2[i])*d_c_b_x_2[i] + (std::log(d_c_ba_x_2[i]) - 0.5/d_c_b_x_2[i] )*(d_c_b_x_2[i] - d_opt_c_b_x_2[i]) + (d_opt_c_b_x_2[i]/d_opt_c_ba_x_2[i] - d_c_b_x_2[i]/d_c_ba_x_2[i])*d_c_ba_x_2[i];
		
		// sum21[i] = (std::log(d_c_ba_x_2[i]) - 0.5/d_c_b_x_2[i] );
		// sum22[i] = (d_c_b_x_2[i] - d_opt_c_b_x_2[i]);
		// sum23[i] = (d_opt_c_b_x_2[i]/d_opt_c_ba_x_2[i] - d_c_b_x_2[i]/d_c_ba_x_2[i]);
		// sum24[i] = d_c_ba_x_2[i];
 	if ( i < c1 )	comp_keys[i]  = 1;			
	else if ( i < c1 + c2 ) comp_keys[i]  = 2;			
	else if ( i < c1 + c2 + c3 )	comp_keys[i]  = 3;			
		
		
	}

	
	}
	
// __global__ void get_comp_keys(int * comp_keys,int c1,int c2,int c3)
// {
	// unsigned int i =  blockIdx.x * blockDim.x + threadIdx.x;
 
	// if ( i < c1 )	comp_keys[i]  = 1;			
	// else if ( i < c1 + c2 ) comp_keys[i]  = 2;			
	// else if ( i < c1 + c2 + c3 )	comp_keys[i]  = 3;		
	
	   
// }
		
	
__global__ void get_opt_cx(float * d_opt_cx1, float * d_opt_cx2, float * d_c_ba_x_2, float * d_c_log_lambda_x,int total_length,int b1,int b2,int b3,int c1,int c2,int c3,int d1,int d2,int d3)
{
	int dimensions[3][4] = { 	{1 ,b1 ,c1, d1 }, 
								{1 ,b2 ,c2, d2 },
								{1 ,b3 ,c3, d3 }, 								 
							};  // [3][6]
	unsigned int i =  blockIdx.x * blockDim.x + threadIdx.x;
	int c = -1;
	
		
	if ( i < total_length)
	{
		if ( i < b1 )	c = 0;			
		else if ( i < b1 + b2 ) c = 1;			
		else if ( i < b1 + b2 + b3 )	c = 2;			 
		 
		int offset = 0;
		if (c == 0)
			offset = 0;
		else if (c == 1)
			offset = dimensions[0][2];
		else if (c == 2)
			offset = dimensions[0][2]  + dimensions[1][2]; 
		 
		d_opt_cx1[i] = 0; 		d_opt_cx2[i] = 0;
		
		if (dimensions[c][3] == 0 || dimensions[c][3] == 2){
			for (int k = 0; k < dimensions[c][2]; k++)
				d_opt_cx2[i] = d_opt_cx2[i] + d_c_ba_x_2[offset + k]* exp(d_c_log_lambda_x[k*total_length + i]);
		}
		else if (dimensions[c][3] == 1 || dimensions[c][3] == 4){
			for (int k = 0; k < dimensions[c][2]; k++)
				d_opt_cx1[i] = d_opt_cx1[i] - d_c_ba_x_2[offset + k]* exp(d_c_log_lambda_x[k*total_length + i]);
		}
	
	}

}


// __global__ void get_keys(float * output, int n, int b1,int b2,int b3, int c1, int c2, int c3)
// {
	// unsigned int i =  blockIdx.x * blockDim.x + threadIdx.x;
	// int total_length = b1 + b2 + b3;
	// int c = -1;
	// if ( i < n)
	// {
		// if ( i%total_length < b1 )
		// {
			// c = 0;			 
		 
		// }
		// else if ( i%total_length < b1 + b2 )
		// {
			// c = 1;			 
	 
		// }
		// else if ( i%total_length < b1 + b2 + b3 )
		// {
			// c = 2;			 
		 
		// } 
		
		// output[i] = c;
	// }
// }

// __global__ void get_exp(float *output, float *input,int n)
// {
	// unsigned int i =  blockIdx.x * blockDim.x + threadIdx.x;
	// if ( i < n)
		// output[i] = exp(input[i]);
// }

__global__ void get_exp_mul_cmx_(float *input1,float * d_cmx,float * d_cmx2, int total_length,int width, int b1,int b2,int b3 )
{
	unsigned int i =  blockIdx.x * blockDim.x + threadIdx.x;
	if ( i < total_length*width)
	{
		
		if ( i%total_length < b1 )
		{
			 
			input1[i] = input1[i]*d_cmx2[i%(total_length)];
		}
		else if ( i%total_length < b1 + b2 )
		{
			 
			input1[i] = input1[i]*d_cmx[i%(total_length)];
		}
		else if ( i%total_length < b1 + b2 + b3 )
		{
		
			input1[i] = input1[i]*d_cmx2[i%(total_length)];
		} 
	}
}
 
 
//void train_ensemble_rectified5_test_cuda_gpu(int i,float* Hx, float* mx, float* mx2, float* x1, float* x2 , int type);  
 // __device__ void train_ensemble_rectified5_test_cuda_gpu(int i, float * Hx_r,float * Hx_c, float* mx, float* mx2, float* x1, float* x2 , int type,double * check){
 __device__ void train_ensemble_rectified5_test_cuda_gpu(int i, float * Hx_r,float * Hx_c, float* mx, float* mx2, float* x1, float* x2 , int type){

	if (type == 0)
	{
		mx[i] = x1[i]/x2[i]; //  mx=x1./x2;
 
		mx2[i] = pow (x1[i], 2) * pow (x2[i], -2) + 1/x2[i];
		
		Hx_r[i] = -0.5 + 0.5*log(x2[i]); 
		Hx_c[i] = 0;
		//Hx=-0.5+0.5*log(x2);
		
		//check[i] = 1;
	}
	else if (type == 1)
	{
		 
		
		//	Laplacian
		double t = -((double)x1[i])/sqrt(2*((double)x2[i]));	// t is correct

		  	 
		double erf_table = erfcx((double)t); //erf_table=erfcx(t);
	
		
		// mx term 1		 
		float term1_mx;
		if (erf_table == erf_table)
			term1_mx =  x1[i]/x2[i]  + ((sqrt((2/PI)/x2[i]))/erf_table);	 
		else
			term1_mx = x1[i]/x2[i];
			
		// mx term 2		 
		float term2_mx =   -1/x1[i] + 2*x2[i]*pow(x1[i],-3) - 10*pow(x2[i],2)*pow(x1[i],-5) ;

		// mx2 term 1
		float term1_mx2;
		if (erf_table == erf_table)
			term1_mx2 = pow(x1[i],2)*pow(x2[i],-2) + 1/x2[i] + 2*x1[i]/x2[i]/sqrt(2*PI*x2[i])/erf_table;	 
		else
			term1_mx2 = pow(x1[i],2)*pow(x2[i],-2) + 1/x2[i] + 0;	 
		
		// check[i]    =   (double) erf_table;
		// check[i+49] =    (double) (t);
		// check[i+49*2] =   (double) log( (erfc((double)t)) );
		// check[i+49*3] =   (double) (log(_a_/((_a_-1) *sqrt(PI*pow(t,2)) + sqrt( (PI*pow(t,2)+_a2_)))) - pow(t,2)) ;
		
		
		// check[i+9] =    pow(x1[i],2)*pow(x2[i],-2) + 1/x2[i] + 2*x1[i]/x2[i]/sqrt(2*PI*x2[i])*((float)1/erf_table);
		// check[i+18] =   pow(x1[i],2)*pow(x2[i],-2) + 1/x2[i] + 2*x1[i]/x2[i]/sqrt(2*PI*x2[i])*(1/(float)erf_table);
		// check[i+27] = pow(t,2);
		// if (erf_table == erf_table)
			// check[i+36] = 13;
		// else
			// check[i+36] = 23;
		// mx2 term2
		double term2_mx2 =  2*pow((double)(-x1[i]),-2) - 10*((double)x2[i])*pow((double)x1[i],-4) + 74*pow((double)x2[i],2)*pow((double)x1[i],-6);


		
		// Hx term1
		double term1_Hx;
		// if (erf_table == erf_table)
			if (t  <= 8.5){
			// t should be less than 25 for this branch to be active
				term1_Hx = -log( (erfc((double)t)) )  + 0.5*log((double)(2*x2[i]/PI))  - 0.5  + ((double)(x1[i]))/sqrt((double)(2*PI*x2[i]))/erf_table;	
			}
			else{
				term1_Hx =  -(log(_a_/((_a_-1) *sqrt(PI*pow(t,2)) + sqrt( (PI*pow(t,2)+_a2_)))) - pow(t,2)) + 0.5*log((double)(2*x2[i]/PI))  - 0.5  + ((double)(x1[i]))/sqrt((double)(2*PI*x2[i]))/erf_table;	
			}
		// else
			// if (t  <= 8.5){
				// term1_Hx = -log( (erfc((double)t)) )  + 0.5*log((double)(2*x2[i]/PI))   - 0.5  +  0;	
			// }
			// else{
				// term1_Hx = -(log(_a_/((_a_-1) *sqrt(PI*pow(t,2)) + sqrt( (PI*pow(t,2)+_a2_)))) - pow(t,2))  + 0.5*log((double)(2*x2[i]/PI))   - 0.5  +  0;	
			// }
		
		
		// Hx term2
		double term2_Hx_real = log((double)(-x1[i]))  - 1 + 2*((double)x2[i])*pow((double)x1[i],-2) - 15*	pow((double)x2[i],2)*(pow((double)x1[i],-4))/2 + 148*pow((double)x2[i],3)*pow((double)x1[i],-6)/3;
		
		// check[i+49*4] =   (double) term2_Hx_real;
		// check[i+49*5] =   (double) term1_Hx;
		 // (log(-x1)       -      1      +     2*x2.*x1.^-2       -       15*x2.^2.*x1.^-4/2    +    148*x2.^3.*x1.^-6/3)
		if (t <= 25)
		{
			mx[i] = term1_mx;  // (x1./x2+sqrt(2/pi./x2)./erf_table)
			mx2[i] = term1_mx2;  // (x1.^2.*x2.^-2+1./x2+2*x1./x2./sqrt(2*pi*x2)./erf_table)
			
		}
		else 
		{
			mx[i] =  term2_mx; // (-1./x1 + 2*x2.*x1.^-3  - 10*x2.^2.*x1.^-5)
			mx2[i] =  term2_mx2; // (2.*x1.^-2-10*x2.*x1.^-4+74*x2.^2.*x1.^-6)
			 
		}

		if (t  < 25)
		{
				//Hx_real.at<double>(0,i) = term1_Hx.at<double>(0,i);  // (-log(erfc(min(t,25)))+0.5*log(2*x2/pi)-0.5+x1./sqrt(2*pi*x2)./erf_table)
				//Hx_complex.at<double>(0,i) =  0;
			Hx_r[i] = term1_Hx;
			Hx_c[i] = 0;
		 


		}
		else 
		{
				//Hx_real.at<double>(0,i) =  term1_Hx.at<double>(0,i); // (log(-x1)-1+2*x2.*x1.^-2-15*x2.^2.*x1.^-4/2+148*x2.^3.*x1.^-6/3)
				//Hx_complex.at<double>(0,i) =  PI; //  
			Hx_r[i] = term2_Hx_real;
			Hx_c[i] = -PI;

		}	

	}

}
 

// __global__ void find_evidence(bool*neg_found, float* d_c_log_lambda_keys, double* d_cmx_dbl, double* d_cmx2_dbl, float * d_cHx_r,float * d_cHx_c, float  * d_cmx, float  * d_cmx2, float  *d_cx1, float  *d_cx2, float* d_c_pi_x_2, float* d_c_b_x_2, float* d_c_ba_x_2, float * d_c_log_lambda_x, float* d_c_log_lambda_x_exp, int  b1,int b2,int b3, int c1, int c2, int c3, int d1, int d2, int d3,double * check)
__global__ void find_evidence(bool*neg_found, float* d_c_log_lambda_keys, double* d_cmx_dbl, double* d_cmx2_dbl, float * d_cHx_r,float * d_cHx_c, float  * d_cmx, float  * d_cmx2, float  *d_cx1, float  *d_cx2, float* d_c_pi_x_2, float* d_c_b_x_2, float* d_c_ba_x_2, float * d_c_log_lambda_x, float* d_c_log_lambda_x_exp, int  b1,int b2,int b3, int c1, int c2, int c3, int d1, int d2, int d3)
{
	unsigned int i =  blockIdx.x * blockDim.x + threadIdx.x;
	int c,ptr = 0;
	float d_log_sum_lambda_x = 0;
	float d_max_c_log_lambda_x = -1000000.0f;
	
	int dimensions[3][4] = { 	{1 ,b1 ,c1, d1 }, 
								{1 ,b2 ,c2, d2 },
								{1 ,b3 ,c3, d3 }, 								 
							};  // [3][6]
							
	int width  = b1 + b2 + b3;
	
	if( i < b1 + b2 + b3) {
		// Initializing values
		d_c_log_lambda_x[i] = 0.0f;
		// check[i]  = 42.0;
 		// Start the magic !!!
		if ( i < b1 )
		{
			c = 0;			 
			ptr = 0;
			for (int j =0 ; j< dimensions[2][2];j++)
			d_c_log_lambda_keys[i+width*j] = 0;
		}
		else if ( i < b1 + b2 )
		{
			c = 1;			 
			ptr =  c1;		
			for (int j =0 ; j< dimensions[2][2];j++)
            d_c_log_lambda_keys[i+width*j] = 1;			
		}
		else if ( i < b1 + b2 + b3 )
		{
			c = 2;			 
			ptr =  c1 + c2;	
			for (int j =0 ; j< dimensions[2][2];j++)
			d_c_log_lambda_keys[i+width*j] = 2;
		} 
		d_cx1[i] = (float) d_cmx_dbl[i];
		d_cx2[i] = (float) d_cmx2_dbl[i];
		if(d_cx2<0){
			neg_found[0] = true;
		}
		// copying to d_c_pi_x_2, float* d_c_b_x_2, float* d_c_ba_x_2 is already done before passing to kernel
		// train_ensemble_rectified5_test_cuda_gpu(i, d_cHx_r,d_cHx_c, d_cmx, d_cmx2, d_cx1, d_cx2 ,  dimensions[c][3],check);	
		train_ensemble_rectified5_test_cuda_gpu(i, d_cHx_r,d_cHx_c, d_cmx, d_cmx2, d_cx1, d_cx2 ,  dimensions[c][3]);	
		
		 
		
		 
		
		int offset = 0;
		if (c == 0)
			offset = 0;
		else if (c == 1)
			offset = dimensions[0][2];
		else if (c == 2)
			offset = dimensions[0][2]  + dimensions[1][2];
		
		if(dimensions[c][2]>=1){

			if (dimensions[c][3] == 0 || dimensions[c][3] == 2)
			{					
				for (int alpha = 0; alpha < dimensions[c][2];alpha++)
				{  
					d_c_log_lambda_x[alpha*width + i] = log(d_c_pi_x_2[offset + alpha]) - 0.5/d_c_pi_x_2[offset + alpha] + 0.5*log(d_c_ba_x_2[offset + alpha])-0.25/d_c_b_x_2[offset + alpha]-0.5*d_cmx2[i]*d_c_ba_x_2[offset + alpha];		 
				 
				}
				
			}
			//	elseif (dimensions(c,4)==1)
			//	%Exponential prior
			else if (dimensions[c][3] == 1)
			{
				for (int alpha = 0; alpha < dimensions[c][2];alpha++)
				{
					if ( false )//P11.empty())
					{
						
					}
					else{					 
						d_c_log_lambda_x[alpha*width + i] = log(d_c_pi_x_2[offset + alpha])-0.5/d_c_pi_x_2[offset + alpha] + log(d_c_ba_x_2[offset + alpha])-0.5/d_c_b_x_2[offset + alpha] -d_cmx[i]*d_c_ba_x_2[offset + alpha];
						// check[i +alpha*9] = d_c_log_lambda_x[alpha*width + i];
					}
					// check[i]    =  log(d_c_pi_x_2[offset + 0]);
					// check[i+9] =  0.5/d_c_pi_x_2[offset + 0];
					// check[i+18] = log(d_c_ba_x_2[offset + 0]);
					// check[i+27] = 0.5/d_c_b_x_2[offset + 0];
					// check[i+36] = d_cmx[i]*d_c_ba_x_2[offset + 0];
				}				
		 

			}
			//	elseif (dimensions(c,4)==3)
			//	%Discrete prior  
			else if (dimensions[c][3] == 3)
			{
			 
			}

			//	elseif  (dimensions(c,4)==4)
			//	%Exponential prior
			else if (dimensions[c][3] == 4)
			{
				 
				for (int alpha = 0; alpha < dimensions[c][2];alpha++)
				{ 
					d_c_log_lambda_x[alpha*width + i] = log(d_c_pi_x_2[offset + alpha])-0.5/d_c_pi_x_2[offset + alpha] + 0.5*log(d_c_ba_x_2[offset + alpha])-0.25/d_c_b_x_2[offset + alpha]-0.5*d_cmx[i]*d_c_ba_x_2[offset + alpha];
					
				}
				
			}		
			
			//	%Now normalise c_log_lambda_x
			//	max_c_log_lambda_x=max(c_log_lambda_x,[],3);
			 
			 


			 
			 
			for (int j =0 ; j< dimensions[c][2];j++)
			{
				if (d_c_log_lambda_x[j*width + i] > d_max_c_log_lambda_x)
				{
					d_max_c_log_lambda_x = d_c_log_lambda_x[j*width + i];
					 
				} 
			}
			
		
			 
			for (int j =0 ; j< dimensions[c][2];j++)
			{
				 d_c_log_lambda_x[j*width + i] =  d_c_log_lambda_x[j*width + i] - d_max_c_log_lambda_x ;
			}
				
			
			
				
			for (int j =0 ; j< dimensions[c][2];j++)
			{
				d_log_sum_lambda_x += exp(d_c_log_lambda_x[j*width + i]);
			}
			 
			
			d_log_sum_lambda_x = log(d_log_sum_lambda_x);
			 
			
			
			for (int j =0 ; j< dimensions[c][2];j++)
			{
				d_c_log_lambda_x[j*width + i] = d_c_log_lambda_x[j*width + i] - d_log_sum_lambda_x;
				d_c_log_lambda_x_exp[j*width + i] = exp(d_c_log_lambda_x[j*width + i]);
			}
				

			
			// Testing
			// d_cmx[i]  =	d_c_log_lambda_x[i];
			// d_cmx2[i]  = d_c_log_lambda_x[1*width +i];
			// d_cHx[i].x = d_c_log_lambda_x[2*width +i];
			// d_cHx[i].y = d_c_log_lambda_x[3*width +i]; 
			
		}

		
		
		


		
		
		
			
	}
}
#endif	// TRAIN_ENSEMBLE_EVIDENCE6_TEST_KERNELS_H