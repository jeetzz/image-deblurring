#include "mex.h"
#include <matrix.h>
#include <math.h>

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	if(nrhs !=3){
		mexErrMsgTxt("No of input arguments wrong.");
	}
	double *A = mxGetPr(prhs[0]);
	int m = (int) (*mxGetPr(prhs[1]));
	int n = (int)(*mxGetPr(prhs[2]));
	mxArray* B_m = plhs[0] = mxCreateDoubleMatrix(m,n,mxREAL);//dout_y can be optmized to 1 given dimensions is fixed
	double *B = mxGetPr(B_m);
	int count = 0;
	for(int i=0;i<n;i++) // parallelizable for loops
	{
		for(int j=0;j<m;j++)
		{
			B[i*m+j] = A[count++];// since A matrix is taken in column wise order for reshaping simple increment of index would suffice
		}
	}
}