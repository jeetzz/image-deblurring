load('elephant_old.mat');
deblurred_im_old = deblurred_im;
clearvars -except deblurred_im_old
load('elephant.mat');
clearvars -except deblurred_im_old deblurred_im
if sum(sum(deblurred_im-deblurred_im_old))~=0
    fprintf('output not matched\n');
else
    fprintf('output matched!\n');
end