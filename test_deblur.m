function test_deblur(CONFIG_FNAME)
%% parse CONFIG_FNAME. If it has an image extension, remove. This assumes
%a .mat file with the same name is present
q = findstr(CONFIG_FNAME,'.');

if isempty(q)
  out_fname = CONFIG_FNAME;
else
  out_fname = CONFIG_FNAME;
  CONFIG_FNAME = CONFIG_FNAME(1:q-1);
end

%%% Loadup all settings
load(char(CONFIG_FNAME));
 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Set defaults
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


DEFAULT_GAMMA = 2.2;

if (~exist('MANUAL_KERNEL'))
  MANUAL_KERNEL = 0;
end
%%%%% Color not working yet, so turn it off
COLOR = 0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Preprocess observed image 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get size
[obs_imy,obs_imx,obs_imz] = size(obs_im);

%%% Save original image
obs_im_orig = obs_im;

%%% ROB NOTE: shouldn't rgb2gray be AFTER gamma correction?
% Convert to grayscale
if (~COLOR & obs_imz>1)
	%  obs_im = obs_im(:,:,3);

	if SPECIFIC_COLOR_CHANNEL
		obs_im = obs_im(:,:,SPECIFIC_COLOR_CHANNEL);
    else
        tic
		obs_im_orig = rgb2gray_rob(obs_im);
        toc
        tic
        obs_im_test = rgb2gray_rob_test(double(obs_im));
        toc
        if isa(obs_im_orig, 'uint8')
            obs_im_test = uint8(obs_im_test);
        end
        if sum(sum(obs_im_orig - obs_im_test)) ~=0
            fprintf('mex rgb difference\n');
        end
	end
	obs_imz = 1;
end
