
#include "Faddeeva.hh"

Mat recipro(Mat m);
void train_ensemble_rectified5_test_u(Mat** Hx, Mat** mx, Mat** mx2, Mat x1, Mat x2 , int type){


	if (type==0)
	{
		//	Gaussian
		*mx = new Mat(x1.mul(1/x2)); //  mx=x1./x2;

		Mat x1_2,x2_2;

		pow(x1,2.0,x1_2);
		pow(x2,-2.0,x2_2);
		*mx2 = new Mat(x1_2.mul(x2_2)  + Mat::ones(1,x2.cols,CV_64F).mul(recipro(x2)));  //mx2=x1.^2.*x2.^-2+ 1./x2;

		*Hx = new Mat(1,x2.cols,CV_64F);
		for (int i = 0; i < x2.cols ; i++)
		{
			(*Hx)->at<double>(0,i) = -0.5 + 0.5*log(x2.at<double>(0,i)); //Hx=-0.5+0.5*log(x2);
		}

		// Check variables here !!!
		//mexPrintf("type 0");
		//for (int i = 0; i < mx.cols ; i++){
		//	mexPrintf("%.21f\n ",mx.at<double>(0,i) );
		//	}

		//return_vector[0] = mx;
		//return_vector[1]  = mx2;
		//return_vector[2]  = Hx;
		//return_vector[3]  = Mat::zeros(1,Hx.cols,CV_64F); // Complex part of Hx is zero in this case

	}
	else if (type==1)
	{


		//	Laplacian
		Mat x2_sqrt;
		pow(2*x2,0.5,x2_sqrt);
		Mat t = -x1.mul(recipro(x2_sqrt));//	t=-x1./sqrt(2*x2);

		Mat erf_table(1,t.cols,CV_64F);
		for (int i = 0; i < t.cols ; i++){
			erf_table.at<double>(0,i)= Faddeeva::erfcx(t.at<double>(0,i));//erf_table=erfcx(t);
		}

		*mx = new Mat(1,t.cols,CV_64F);
		*mx2 = new Mat(1,t.cols,CV_64F);
		*Hx = new Mat(1,t.cols,CV_64FC2,cv::Scalar(0,0));


		// mx term 1
		Mat temp_sqx2;
		Mat x2_reciprocal = recipro(x2);
		pow((2/PI)*(x2_reciprocal),0.5,temp_sqx2);
		Mat erf_table_reciprocal = recipro(erf_table);
		Mat term1_mx = x1.mul(x2_reciprocal) + temp_sqx2.mul(erf_table_reciprocal);

		// mx term 2
		Mat temp_cux1,temp_5x1,temp_sqx22;
		pow(x1,-3,temp_cux1);
		pow(x2,2,temp_sqx22);
		pow(x1,-5,temp_5x1);
		Mat term2_mx = -recipro(x1) + 2*x2.mul(temp_cux1) - 10*temp_sqx22.mul(temp_5x1);

		// mx2 term 1
		Mat temp_sqx1,temp_sqx24;
		pow(x1,2, temp_sqx1);
		pow(x2,-2, temp_sqx24);
		Mat temp_sqx23;
		pow(2*PI*x2,0.5,temp_sqx23);
		Mat term1_mx2 = temp_sqx1.mul(temp_sqx24) + x2_reciprocal + 2*x1.mul(x2_reciprocal).mul(recipro(temp_sqx23)).mul(erf_table_reciprocal);

		// mx2 term2 
		Mat temp_sqx12,temp_4x1,temp_6x1;
		pow(x1,-2,temp_sqx12);
		pow(x1,-4,temp_4x1);
		pow(x1,-6,temp_6x1);
		Mat term2_mx2 = 2*temp_sqx12  - 10*x2.mul(temp_4x1)  + 74*temp_sqx22.mul(temp_6x1);

		// Hx term1 - since erfc doesn't support element wise operations on a Mat, we have to use a for loop. So did rest of the 
		// operations as well inside the loop.
		Mat term1_Hx(1,t.cols,CV_64F);
		double min_t;
		for (int j = 0; j < t.cols; j++)
		{    
			if (t.at<double>(0,j) <= 25)
				min_t = t.at<double>(0,j);
			else
				min_t = 25;

			term1_Hx.at<double>(0,j) = -log(Faddeeva::erfc(min_t))  + 0.5*log(2*x2.at<double>(0,j)/PI)   - 0.5  + x1.at<double>(0,j)/sqrt(2*PI*x2.at<double>(0,j))/erf_table.at<double>(0,j);
			//mexPrintf("%f \n", term1_Hx.at<double>(0,j));
		}

		// Hx term2
		Mat term2_Hx_real(1,t.cols,CV_64F);
		Mat log_x1_real,temp_3x2;
		cv::log(x1,log_x1_real);

		pow(x2,3,temp_3x2);
		term2_Hx_real = log_x1_real  - 1 + 2*x2.mul(temp_sqx12) - 15*temp_sqx22.mul(temp_4x1)/2 + 148*temp_3x2.mul(temp_6x1)/3;

		for (int i = 0; i < t.cols; i++)
		{

			// (log(-x1)       -      1      +     2*x2.*x1.^-2       -       15*x2.^2.*x1.^-4/2    +    148*x2.^3.*x1.^-6/3)
			if (t.at<double>(0,i) <= 25)
			{
				(*mx)->at<double>(0,i) = term1_mx.at<double>(0,i);  // (x1./x2+sqrt(2/pi./x2)./erf_table)
				(*mx2)->at<double>(0,i) = term1_mx2.at<double>(0,i);  // (x1.^2.*x2.^-2+1./x2+2*x1./x2./sqrt(2*pi*x2)./erf_table)

			}
			else 
			{
				(*mx)->at<double>(0,i) =  term2_mx.at<double>(0,i); // (-1./x1 + 2*x2.*x1.^-3  - 10*x2.^2.*x1.^-5)
				(*mx2)->at<double>(0,i) =  term2_mx2.at<double>(0,i); // (2.*x1.^-2-10*x2.*x1.^-4+74*x2.^2.*x1.^-6)

			}

			if (t.at<double>(0,i) < 25)
			{
				//Hx_real.at<double>(0,i) = term1_Hx.at<double>(0,i);  // (-log(erfc(min(t,25)))+0.5*log(2*x2/pi)-0.5+x1./sqrt(2*pi*x2)./erf_table)
				//Hx_complex.at<double>(0,i) =  0;
				(*Hx)->at<Vec2d>(0,i)[0] = term1_Hx.at<double>(0,i);
				(*Hx)->at<Vec2d>(0,i)[1] = 0;


			}
			else 
			{
				//Hx_real.at<double>(0,i) =  term1_Hx.at<double>(0,i); // (log(-x1)-1+2*x2.*x1.^-2-15*x2.^2.*x1.^-4/2+148*x2.^3.*x1.^-6/3)
				//Hx_complex.at<double>(0,i) =  PI; //  
				(*Hx)->at<Vec2d>(0,i)[0] = term2_Hx_real.at<double>(0,i);
				(*Hx)->at<Vec2d>(0,i)[1] = -PI;

			}		 
		}

		// Check variables here !!!
		/*for (int i = 0; i < (*mx2)->cols ; i++){
			mexPrintf("%.20f \n ",(*Hx)->at<Vec2d>(0,i)[0] );
			}*/
		//return_vector[0] = mx;
		//return_vector[1]  = mx2;
		//return_vector[2]  = Hx_real;
		//return_vector[3]  = Hx_complex;


	}
	else if (type==2)
	{


		//	%Rectified Gaussian
		//	t=-x1./sqrt(2*x2);
		//erf_table=erfcx(t);

		//mx=(x1./x2+sqrt(2/pi./x2)./erf_table).*(t<=25)+(-1./x1+2*x2.*x1.^-3-10*x2.^2.*x1.^-5).*(t>25);
		//mx2=(x1.^2.*x2.^-2+1./x2+2*x1./x2./sqrt(2*pi*x2)./erf_table).*(t<=25)+(2.*x1.^-2-10*x2.*x1.^-4+74*x2.^2.*x1.^-6).*(t>25);
		//Hx=(-log(erfc(min(t,25)))+0.5*log(2*x2/pi)-0.5+x1./sqrt(2*pi*x2)./erf_table).*(t<25)+(log(-x1)-1+2*x2.*x1.^-2-15*x2.^2.*x1.^-4/2+148*x2.^3.*x1.^-6/3).*(t>=25)+0.5*log(pi/2);
	}
	else if (type==3){
		//	%Discrete (1,-1)
		//	mx=tanh(x1);
		//mx2=ones(size(x1));
		//Hx=x1.*mx-abs(x1)-log(1+exp(-2*abs(x1)))+log(2);
		//elseif (type==4) %%% Laplacian prior - two recified Gaussians
		//	%Rectified Gaussian
		//	% Buggy and non-functional...
		//	t=-x1./sqrt(2*x2);
		//erf_table=erfcx(t);

		//mx=(x1./x2+sqrt(2/pi./x2)./erf_table).*(t<=25)+(-1./x1+2*x2.*x1.^-3-10*x2.^2.*x1.^-5).*(t>25);
		//mx2=(x1.^2.*x2.^-2+1./x2+2*x1./x2./sqrt(2*pi*x2)./erf_table).*(t<=25)+(2.*x1.^-2-10*x2.*x1.^-4+74*x2.^2.*x1.^-6).*(t>25);
		//Hx=(-log(erfc(min(t,25)))+0.5*log(2*x2/pi)-0.5+x1./sqrt(2*pi*x2)./erf_table).*(t<25)+(log(-x1)-1+2*x2.*x1.^-2-15*x2.^2.*x1.^-4/2+148*x2.^3.*x1.^-6/3).*(t>=25)+0.5*log(pi/2);


	}



	//	return return_vector;
}


Mat recipro(Mat m){
	Mat reci(m.rows,m.cols,CV_64F,Scalar(0));
	for (int i = 0; i < m.rows; i++)
		for (int j = 0; j < m.cols; j++)
			reci.at<double>(i,j) = 1/m.at<double>(i,j);
	return reci;
			 
}

//
//void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
//	{
//
//	// x1,x2,type
//
//	// x1
//	mxArray *mx_x1 = mxDuplicateArray(prhs[0]);  
//	double* x1_array = mxGetPr(mx_x1);
//	int x1_M = mxGetM(mx_x1);
//	int x1_N = mxGetN(mx_x1);
//	Mat x1 = Mat(x1_M,x1_N,CV_64F,x1_array);
//
//	// x2
//	mxArray *mx_x2 = mxDuplicateArray(prhs[1]);  
//	double* x2_array = mxGetPr(mx_x2);
//	int x2_M = mxGetM(mx_x2);
//	int x2_N = mxGetN(mx_x2);
//	Mat x2 = Mat(x2_M,x2_N,CV_64F,x2_array);
//
//	// type
//	mxArray *mx_type = mxDuplicateArray(prhs[2]);  
//	double* type_array = mxGetPr(mx_type);
//	int type_M = mxGetM(mx_type);
//	int type_N = mxGetN(mx_type);
//	int type = type_array[0];
//
//	Mat* cHx;//(0,0,CV_64F);
//	Mat* cmx;//(0,0,CV_64F);
//	Mat* cmx2;//(0,0,CV_64F);
//	train_ensemble_rectified5_test_u(&cHx,&cmx,&cmx2,x1,x2,type);
//	//vector<Mat> Hx_mx_mx2 = train_ensemble_rectified5_test_u(x1,x2,type);
//
//	}
//
