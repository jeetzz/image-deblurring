﻿#define NX 4
#define NY 5
#define NRANK 2
#define BATCH 10

// #define DEBUG_LEVEL 4

//#include <cufft.h>

 #include "cuda_runtime.h"
 #include "device_launch_parameters.h"
 #include <cufft.h>
 #include <stdio.h>
 #include <float.h>
 #include "train_blind_deconv_test_cuda_gpu_kernel.cu"

#define BLOCKSIZE 512
#define MAX_BLOCK_SIZE 1024

#define  MAX_FLOAT 3.4e+38F
#define  MIN_FLOAT -3.4e+38F

/*void fft_R2C(float2 *h_out_data,float *h_in_data,int I,int J);*/
void fft_R2C(cufftComplex * d_out_data,float *h_in_data,int M,int N,int I,int J,int flag_square);
void fft_R2C_cuda_real(cufftComplex * d_out_data,cufftReal *d_in_data,int M,int N,int I,int J,int flag_square);
void fft_C2R(cufftReal * d_out_data,cufftComplex * d_in_data,int I,int J, int M, int N);
void fft_C2R_plus_corr(cufftReal *corr, cufftReal * d_out_data,cufftComplex * d_in_data,int I,int J, int M, int N);
 
__global__ void get_mu_me_mx(float * input ,float *me, int L,int K,int offset)
{	
	unsigned int r =  blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int c =  blockIdx.y * blockDim.y + threadIdx.y;
	 
  
	if (r < L && c < K){
		if (input[offset + L*c + r] >= (MAX_FLOAT))
			me[r*K + c] = MAX_FLOAT;
		else if(input[offset + L*c + r] <= (MIN_FLOAT))
			me[r*K + c] = MIN_FLOAT;
		else 
			me[r*K + c] = input[offset + L*c + r];
	}
	
}

// __global__ void get_e(float * input , float *mx, int M, int N)
// {
// }
__global__ void get_dx(float * dx1,float * dx2,float * me,float * me2, int L,int K,int offset)
{
	unsigned int i =  blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int j =  blockIdx.y * blockDim.y + threadIdx.y;
	 
	 
	if ( j < L && i < K)
	{
		dx1[j*K + i + offset] 		=	me[i*L + j];
		dx2[j*K + i + offset]      = 	me2[i*L + j];
	}
	 

}
void train_blind_deconv_test_cuda_gpu_2(float * dx1, float * dx2, double* rerror, int* datapoints,Mat dimensions,float* d_mx,  float* d_mx2 ,Mat D_mat,Mat Dp_mat,int I,int J,int K,int L,int M,int N){

	
	int b1,b2,b3;//,d1,d2,d3,c1,c2,c3;
	b1  = dimensions.at<int>(0,1); 		b2  =  dimensions.at<int>(1,1);		b3  = dimensions.at<int>(2,1);
	//c1 = dimensions.at<int>(0,2); 		c2 =  dimensions.at<int>(1,2);		c3 = dimensions.at<int>(2,2);
	//d1 = dimensions.at<int>(0,3);	d2 = dimensions.at<int>(1,3); 	d3 = dimensions.at<int>(2,3); 
	
	int total_length =  b1 + b2 + b3;
	//int comp_length =  c1 + c2 + c3;
	
	
	//nPlanes = dimensions(3,1);
	//%=========================================================================
	//	% Copy the expectations
	//	%=========================================================================
    int num_blocks = (I*J/BLOCKSIZE) + ((I*J%BLOCKSIZE) ? 1 : 0);
	

 	float  *h_out_data  = new float[I*J]; 
 	float  *h_out_val = new float[1];
	float *h_error = new float[1];

	 
	float *d_mmu; cufftReal * me,* mx;  
	cudaMalloc((void**)&d_mmu , sizeof(float));
	cudaMalloc((void**)&me , sizeof(cufftReal)*L*K);
	cudaMalloc((void**)&mx , sizeof(cufftReal)*M*N);  
	
	dim3 blockDim2(16, 16); 	
	dim3 gridDimKL((L + blockDim2.x - 1) / blockDim2.x , (K + blockDim2.y - 1) / blockDim2.y);
	dim3 gridDimMN((M + blockDim2.x - 1) / blockDim2.x , (N + blockDim2.y - 1) / blockDim2.y);
	
	
	//====================== better if these can be put to one kernel
	get_mu_me_mx<<<gridDimKL,blockDim2>>>(d_mx, d_mmu, 1, 1, 0);	
	get_mu_me_mx<<<gridDimKL,blockDim2>>>(d_mx, me, L, K, 1);
	get_mu_me_mx<<<gridDimMN,blockDim2>>>(d_mx, mx, M, N, 1 + L*K);
	
	float  * h_mmu  = new float[1]; 
	cudaMemcpy(h_mmu,  d_mmu, sizeof(float), cudaMemcpyDeviceToHost); 
	float mmu = h_mmu[0];
	
	// -----------------------------------------
	// debug - checked - correct
	// -----------------------------------------
	// thrust::device_ptr<float> t_me(me);
	// thrust::device_ptr<float> t_mx(mx);
	
	// mexPrintf("mmu : %f ",mmu);
	// mexPrintf("\nme \n");
	// for (int r = 0;r < L*K; r++)
	// {
		// mexPrintf("%f ",(float)t_me[r]);
	// }
	// mexPrintf("\nmx\n",M,N);
	// for (int r = 0;r < M*N; r++)
	// {
		// mexPrintf("%f ",(float)t_mx[r]);
	// }
	// mexPrintf("\n\n\n");
	// -----------------------------------------
	
	
 


 	float *d_mmu2; cufftReal * me2,* mx2;  
	cudaMalloc((void**)&d_mmu2 , sizeof(float));
	cudaMalloc((void**)&me2 , sizeof(cufftReal)*L*K);
	cudaMalloc((void**)&mx2 , sizeof(cufftReal)*M*N); 
	
	get_mu_me_mx<<<gridDimKL,blockDim2>>>(d_mx2, d_mmu2, 1, 1, 0);	
	get_mu_me_mx<<<gridDimKL,blockDim2>>>(d_mx2, me2, L, K, 1);
	get_mu_me_mx<<<gridDimMN,blockDim2>>>(d_mx2, mx2, M, N, 1 + L*K);
	
	float  * h_mmu2  = new float[1]; 
	cudaMemcpy(h_mmu2,  d_mmu2, sizeof(float), cudaMemcpyDeviceToHost); 
	float mmu2 = h_mmu2[0];

	// -----------------------------------------
	// debug - checked - correct
	// -----------------------------------------
	
	// thrust::device_ptr<float> t_me2(me2);
	// thrust::device_ptr<float> t_mx2(mx2);
	
	// mexPrintf("mmu2 : %f ",mmu2);
	// mexPrintf("\n2------------------me \n");
	// for (int r = 0;r < L*K; r++)
	// {
		// mexPrintf("%f ",(float)t_me2[r]);
	// }
	// mexPrintf("\n-------------%d %d---\n",M,N);
	// for (int r = 0;r < M*N; r++)
	// {
		// mexPrintf("%f ",(float)t_mx2[r]);
	// }
	// mexPrintf("\n\n\n");
	// -----------------------------------------
	
	

	float *h_Dp = new float[I*J];
	for (int r = 0; r < I; ++r)
		{ 
		for (int c = 0; c < J; ++c)
			{
			h_Dp[r*J + c] = (float)Dp_mat.at<double>(0,J*r+c);
			}
		}
	cufftReal *Dp;       // Input array - device side
	cudaMalloc((void**)&Dp, sizeof(cufftReal)*I*J);
 	cudaMemcpy(Dp, h_Dp, sizeof(cufftReal)*I*J, cudaMemcpyHostToDevice); 
 

	float *h_D = new float[I*J];
	for (int r = 0; r < I; ++r)
		{ 
		for (int c = 0; c < J; ++c)
			{
			h_D[r*J + c] = (float)D_mat.at<double>(0,J*r+c);
			}
		}
	cufftReal *D;       // Input array - device side
	cudaMalloc((void**)&D, sizeof(cufftReal)*I*J);
 	cudaMemcpy(D, h_D, sizeof(cufftReal)*I*J, cudaMemcpyHostToDevice);

	

	// -----------------------------------------
	// debug -checked - correct
	// -----------------------------------------
	  
	// mexPrintf("\n");
	// for (int r = 0;r < I*J; r++)
	// {
		// mexPrintf("%f ",h_Dp[r]);
	// }
	// mexPrintf("\n");
	// for (int r = 0;r < I*J; r++)
	// {
		// mexPrintf("%f ",h_D[r]);
	// }
	// mexPrintf("\n\n\n");
	// -----------------------------------------
	
	//%============== ===========================================================
	 
	//	%=========================================================================
	//	% Evaluate  FFTs
	//	%=========================================================================
	double fft_time = (double)getTickCount();	
	dim3 gridDim((I + blockDim2.x - 1) / blockDim2.x , (J + blockDim2.y - 1) / blockDim2.y);

	// mx ffts
	cufftComplex * fft_mx;  cudaMalloc((void**)&fft_mx , sizeof(cufftComplex)*I*J);   	fft_R2C_cuda_real(fft_mx ,mx ,M,N,I,J,0); // by using this function, redundant cudamemcpy with 2 flag values inside the function could be removed 

//debug--
	// cufftReal *    corr_temp;  cudaMalloc((void**)&corr_temp , sizeof(cufftReal)*I*J);
	// complex_to_real<<<gridDim,blockDim2>>>(corr_temp,fft_mx,I,J);
	//  // complex_to_img<<<gridDim,blockDim2>>>(corr_temp,fft_mx3,I,J);
	// // fft_C2R(corr_temp,fft_mx3,I,J,I,J);
//enddebug

//debug----------------------
 	// cudaMemcpy(h_mx,mx, sizeof(cufftReal)*M*N, cudaMemcpyDeviceToHost); 
 	// mexPrintf("\n mx in\n ");
  // 	for (int i = 0; i < M; ++i)
  // 		{
  // 		for (int j = 0; j < N; ++j)
  // 			{
  // 			// mexPrintf("%f\t ",(float)(*mx_temp).at<double>(0,M*j+i));
  // 			mexPrintf("%f\t ",h_mx[i*N + j]);
  // 			}
  // 		mexPrintf("\n ");
  // 		}

//-----------------------	

//debug----------------------

 	// cudaMemcpy(h_out_data,corr_temp   , sizeof(float)*I*J, cudaMemcpyDeviceToHost);
 	// // cudaError  cudaStat13 = 	cudaMemcpy(h_out_val,mu1   , sizeof(float), cudaMemcpyDeviceToHost);
 	 
 	// mexPrintf("\n fft_mx real\n ");
  // 	for (int i = 0; i < I; i++)
  // 		{
  // 		for (int j = 0; j < J; j++)
  // 			{
  // 			mexPrintf("%5f\t ",h_out_data[i*J + j]);
  // 			}
  // 		mexPrintf("\n ");
  // 		}
 	// mexPrintf("\n mu1 %f GPU ",h_out_val[0]);
//-----------------------
#if DEBUG_LEVEL > 4	
	thrust::device_ptr<float> t_test5(me);
	mexPrintf("\n me \n");
	for (int r = 0;r < L*K; r++)
	{
		mexPrintf("%f ",(float)(t_test5[r]));
	}
	mexPrintf("\n");
	
	thrust::device_ptr<float> t_test3(me2);
	mexPrintf("\n me2 \n");
	for (int r = 0;r < L*K; r++)
	{
		mexPrintf("%f ",(float)t_test3[r]);
	}
	mexPrintf("\n");
	
	thrust::device_ptr<float> t_test4(me);
	mexPrintf("\n me3 \n");
	for (int r = 0;r < L*K; r++)
	{
		mexPrintf("%f ",(float)(t_test4[r]*t_test4[r]));
	}
	mexPrintf("\n");
	
#endif	


	cufftComplex * fft_mx2; cudaMalloc((void**)&fft_mx2, sizeof(cufftComplex)*I*J); 	fft_R2C_cuda_real(fft_mx2,mx2,M,N,I,J,0);
	cufftComplex * fft_mx3; cudaMalloc((void**)&fft_mx3, sizeof(cufftComplex)*I*J); 	fft_R2C_cuda_real(fft_mx3,mx ,M,N,I,J,1); // last parameter , is a flag that'll square the input vector
	// me ffts
	cufftComplex * fft_me;  cudaMalloc((void**)&fft_me , sizeof(cufftComplex)*I*J);    	fft_R2C_cuda_real(fft_me ,me ,K,L,I,J,0);
	cufftComplex * fft_me2; cudaMalloc((void**)&fft_me2, sizeof(cufftComplex)*I*J);    	fft_R2C_cuda_real(fft_me2,me2,K,L,I,J,0);
	cufftComplex * fft_me3; cudaMalloc((void**)&fft_me3, sizeof(cufftComplex)*I*J);    	fft_R2C_cuda_real(fft_me3,me ,K,L,I,J,1);
	// Dp fft
	cufftComplex * fft_Dp;  cudaMalloc((void**)&fft_Dp , sizeof(cufftComplex)*I*J);    	fft_R2C(fft_Dp ,h_Dp ,I,J,I,J,0);

	cufftComplex * fft_mD;  				cudaMalloc((void**)&fft_mD , sizeof(cufftComplex)*I*J);
	cufftComplex * fft_me2_mx2_me3_mx3;  	cudaMalloc((void**)&fft_me2_mx2_me3_mx3 , sizeof(cufftComplex)*I*J);

#if DEBUG_LEVEL > 4	

	cufftReal *    d_test1;      cudaMalloc((void**)&d_test1     , sizeof(cufftReal   )*I*J);
	complex_to_real<<<gridDim,blockDim2>>>(d_test1,fft_me3,I,J);
	thrust::device_ptr<float> t_test1(d_test1);
	mexPrintf("\n real fft_me3 \n");
	for (int r = 0;r < I*J; r++)
	{
		mexPrintf("%f ",(float)t_test1[r]);
	}
	mexPrintf("\n");
	
	cufftReal *    d_test2;      cudaMalloc((void**)&d_test2     , sizeof(cufftReal   )*I*J);
	complex_to_real<<<gridDim,blockDim2>>>(d_test2,fft_mx3,I,J);
	thrust::device_ptr<float> t_test2(d_test2);
	mexPrintf("\n real fft_mx3 \n");
	for (int r = 0;r < I*J; r++)
	{
		mexPrintf("%f ",(float)t_test2[r]);
	}
	mexPrintf("\n");
#endif	

	// mD ifft
	fft_me2_mx2_me3_mx3_kernel<<<gridDim,blockDim2>>>(fft_mD,fft_me,fft_mx,fft_me2_mx2_me3_mx3,fft_me2,fft_mx2,fft_me3,fft_mx3,I,J);


	cufftReal *    me_mx2_me_mx3;  cudaMalloc((void**)&me_mx2_me_mx3 , sizeof(cufftReal)*I*J);fft_C2R(me_mx2_me_mx3,fft_me2_mx2_me3_mx3,I,J,I,J);
	cufftReal *    mD;      cudaMalloc((void**)&mD     , sizeof(cufftReal   )*I*J);     fft_C2R(mD,fft_mD,I,J,I,J);

	



	cufftReal *    Dp_D_mD_mmu;  cudaMalloc((void**)&Dp_D_mD_mmu , sizeof(cufftReal)*I*J);  

  	cufftReal *    error_mat;  cudaMalloc((void**)&error_mat , sizeof(cufftReal)*I*J);

	
	
	
// sum reduction in same kernel with thread sync---------------------
	//error_mat_kernel<<<gridDim,blockDim2>>>(mu1,error, mu2,error_mat, Dp_D_mD_mmu,D,mD,mmu,mmu2,me_mx2_me_mx3,Dp,I,J); 
//sum reduction in separate kernel---

	float* mu1;			cudaMalloc((void**) &mu1, sizeof(float));	 
	float* error;		cudaMalloc((void**) &error, sizeof(float));	 
	float* mu2;			cudaMalloc((void**) &mu2, sizeof(float));	 

	float *error_partial_sums; 	cudaMalloc((void**)&error_partial_sums, sizeof(float) * (num_blocks));
	float *mu1_partial_sums; 	cudaMalloc((void**)&mu1_partial_sums, sizeof(float) * (num_blocks));
	float *mu2_partial_sums; 	cudaMalloc((void**)&mu2_partial_sums, sizeof(float) * (num_blocks));

	if(I*J > BLOCKSIZE*MAX_BLOCK_SIZE){
		error_mat_kernel_no_sum_error_full<<<gridDim,blockDim2>>>(error_mat, Dp_D_mD_mmu,D,mD,mmu,mmu2,me_mx2_me_mx3,Dp,I,J); 
		error_mat_kernel_sum_only_error_full<<<gridDim,blockDim2>>>(mu1, error, mu2, error_mat, D,mD,mmu, mmu2,Dp, I, J);
		 
	}
	else{
	//separate reduction kernels
		error_mat_kernel_no_sum_new<<<gridDim,blockDim2>>>(error_mat, Dp_D_mD_mmu,D,mD,mmu,mmu2,me_mx2_me_mx3,Dp,I,J); 

#if DEBUG_LEVEL > 4		
		thrust::device_ptr<float> t_a1(Dp_D_mD_mmu); // Error  - it seems the source of error must be common to all for 4 these variables
		for (int i = 0; i < I*J; i ++)
			 mexPrintf("%f ",(float)t_a1[i]);
		mexPrintf("\n");
		
		thrust::device_ptr<float> t_a2(D); // Error
		for (int i = 0; i < I*J; i ++)
			 mexPrintf("%f ",(float)t_a2[i]);
		mexPrintf("\n");
		
		thrust::device_ptr<float> t_a3(mD); // Error
		for (int i = 0; i < I*J; i ++)
			 mexPrintf("%f ",(float)t_a3[i]);
		mexPrintf("\n");
		
		 
		mexPrintf("%f %f \n",mmu,mmu2);
		
		 
		thrust::device_ptr<float> t_a6(me_mx2_me_mx3); // Error
		for (int i = 0; i < I*J; i ++)
			 mexPrintf("%f ",(float)t_a6[i]);
		mexPrintf("\n");		 
		
		thrust::device_ptr<float> t_a7(Dp);
		for (int i = 0; i < I*J; i ++)
			 mexPrintf("%f ",(float)t_a7[i]);
		mexPrintf("\n");
#endif
		
		/// use D as array for mu1, Dp as array for data points
	    dim3 grid_unidim(num_blocks, 1, 1);
	    dim3 threads_unidim(BLOCKSIZE, 1, 1);
	    sum_kernel<<< grid_unidim, threads_unidim >>>(error_mat,error_partial_sums,Dp,mu2_partial_sums,D,mu1_partial_sums, I*J);
		sum_kernel<<<1,num_blocks>>>(error_partial_sums,error,mu2_partial_sums,mu2,mu1_partial_sums,mu1, num_blocks);
		
		
	}
	
	
		

	cudaFree(error_partial_sums);
	cudaFree(mu2_partial_sums);
	cudaFree(mu1_partial_sums);

//-------------------------------------------------------------
	

	cufftComplex * fft_err;  cudaMalloc((void**)&fft_err , sizeof(cufftComplex)*I*J);   fft_R2C_cuda_real(fft_err ,Dp_D_mD_mmu ,I,J,I,J,0);

	fft_conj_mult<<<gridDim,blockDim2>>>(fft_Dp,fft_err,fft_me,fft_mx,fft_me2,fft_mx2,fft_me3,fft_mx3,I,J);
	cufftReal *    corr_e;  cudaMalloc((void**)&corr_e , sizeof(cufftReal)*L*K);
	cufftReal *    corr_x;  cudaMalloc((void**)&corr_x , sizeof(cufftReal)*M*N);

	


	fft_C2R(corr_e,fft_mx3,I,J,K,L);
	fft_C2R(corr_x,fft_me3,I,J,M,N);
	fft_C2R(me2,fft_mx2,I,J,K,L);
	fft_C2R(mx2,fft_me2,I,J,M,N);
	
	fft_C2R_plus_corr(corr_e, me,fft_mx,I,J,K,L);
	fft_C2R_plus_corr(corr_x, mx,fft_me,I,J,M,N);





 	// cudaError  cudaStat4 = 	cudaMemcpy(h_out_data,corr_temp   , sizeof(cufftReal)*I*J, cudaMemcpyDeviceToHost);
 	// cudaError  cudaStat4 = 	cudaMemcpy(h_out_data,me   , sizeof(cufftReal)*K*L, cudaMemcpyDeviceToHost);
 	//cudaError  cudaStat4 = 	cudaMemcpy(h_out_data,mx   , sizeof(cufftReal)*M*N, cudaMemcpyDeviceToHost);

	dim3 blockDimdx(16,16); 	
	dim3 gridDimKL_((K + blockDim2.x - 1) / blockDim2.x , (L + blockDim2.y - 1) / blockDim2.y);
	dim3 gridDimMN_((M + blockDim2.x - 1) / blockDim2.x , (N + blockDim2.y - 1) / blockDim2.y);
 	get_dx<<<gridDimKL_,blockDim2>>>(dx1,dx2,mu1,mu2,1,1,0);
	get_dx<<<gridDimKL_,blockDim2>>>(dx1,dx2,me,me2,L,K,1);
	get_dx<<<gridDimMN_,blockDim2>>>(dx1,dx2,mx,mx2,N,M,L*K+1);

 	
	// -----------------------------------
	// debug
	// -----------------------------------
	// thrust::device_ptr<float> t_me2(dx1);
	// thrust::device_ptr<float> t_mx2(dx2);
	
	// mexPrintf("\ndx1 \n");
	// for (int r = 0;r < total_length; r++)
	// {
		// mexPrintf("%f ",(float)t_me2[r]);
	// }
	// mexPrintf("\ndx2\n");
	// for (int r = 0;r < total_length; r++)
	// {
		// mexPrintf("%f ",(float)t_mx2[r]);
	// }
	// -----------------------------------
	
 	cudaError  cudaStat5 = 	cudaMemcpy(&mmu2,mu2   , sizeof(float), cudaMemcpyDeviceToHost);

 	cudaError  cudaStat10 = 	cudaMemcpy(h_error,error    , sizeof(float), cudaMemcpyDeviceToHost);

	
 	*rerror = (double) (*h_error);
 	*datapoints = (int) mmu2;

	// mexPrintf("\nValues %f \n",  *h_error  );

 
	cudaFree(fft_mx); cudaFree(fft_mx2); cudaFree(fft_mx3);
	cudaFree(fft_me); cudaFree(fft_me2); cudaFree(fft_me3);
	cudaFree(fft_Dp);  
	cudaFree(fft_err);  

	cudaFree(mu1			);
	cudaFree(error 			);
	cudaFree(mu2	);

	cudaFree(error_mat	);
	cudaFree(me_mx2_me_mx3	);

	cudaFree(Dp_D_mD_mmu);  
	cudaFree(D);  
	cudaFree(Dp);  

	cudaFree(fft_me2_mx2_me3_mx3);  

	cudaFree(fft_mD); cudaFree(mD);  

	cudaFree(me);  
	cudaFree(me2);  
	cudaFree(mx);  
	cudaFree(mx2);  
	cudaFree(d_mmu);
	cudaFree(d_mmu2);
 

	cudaFree(corr_e	);
	cudaFree(corr_x	);

	//debug -----
	// cudaFree(corr_temp);
	//--------------


	// float  * h_dx1  = new float[total_length]; 
	// cudaMemcpy(h_dx1, dx1, sizeof(float)*total_length, cudaMemcpyDeviceToHost); 
	
 	// mexPrintf("cuda\n ");
  	// for (int i = 0; i < total_length; i++)
	// {
		// {
		// mexPrintf("%f ",h_dx1[i]);
		// }
	// }
	// mexPrintf("\ndata_points %d error %f\n ",*datapoints,*rerror);

	delete h_out_data;
 	delete h_out_val;
 	delete h_error;
  
 
	delete h_Dp;
	delete h_D;
	 
}

void fft_R2C_cuda_real(cufftComplex * d_out_data,cufftReal *d_in_data,int M,int N,int I,int J,int flag_square){

	// Output array - host side
	// float2 *h_out_data_temp = new float2[I*(J/2+1)] ; 

	cufftHandle plan;
	cufftReal *d_in_data_padded;       // Input array - device side	
	     // Output array - device side - full array 
	cufftComplex * d_out_data_temp;  // Output array - device side - half array (output from the fft2)
	 
	//debug- ---------------------------------------------------------------------------------------------------------------
	// float  *print_data  = new float[I*J]; 
	// cufftReal *    corr_temp;  cudaMalloc((void**)&corr_temp , sizeof(cufftReal)*I*J);
	//-----------------------------------------------------------------------------------------------------------------------

	 
	cudaMalloc((void**)&d_in_data_padded, sizeof(cufftReal)*I*J);

	// if (flag_square)
	// 	{
	// 	dim3 blockDim0(16, 16);
	// 	dim3 gridDim0((M + blockDim0.x - 1) / blockDim0.x , (N + blockDim0.y - 1) / blockDim0.y);
	// 	square<<<gridDim0,blockDim0>>>(d_in_data,d_in_data,M,N);
	// 	}

	dim3 blockDim2(16, 16);
	dim3 gridDim((I + blockDim2.x - 1) / blockDim2.x , (J + blockDim2.y - 1) / blockDim2.y);
	if(flag_square){
		zero_pad_input_sq_flag<<<gridDim,blockDim2>>>(d_in_data_padded,d_in_data,M,N,I,J,1);
	}
	else {
		zero_pad_input_sq_flag<<<gridDim,blockDim2>>>(d_in_data_padded,d_in_data,M,N,I,J,0);
	}
	 
	cudaMalloc((void**)&d_out_data_temp, sizeof(cufftComplex)*I*(J/2 + 1));
	


	//Performe the fft
	cufftPlan2d(&plan, I, J , CUFFT_R2C);
	cufftSetCompatibilityMode(plan, CUFFT_COMPATIBILITY_NATIVE);
	// debug --------------------------------------------------------------------------------------------------
 	// cudaMemcpy(print_data,d_in_data_padded, sizeof(cufftReal)*I*J, cudaMemcpyDeviceToHost); 
 	// mexPrintf("\n fft in\n ");
  // 	for (int i = 0; i < I; ++i)
  // 		{
  // 		for (int j = 0; j < J; ++j)
  // 			{
  // 			// mexPrintf("%f\t ",(float)(*mx_temp).at<double>(0,M*j+i));
  // 			mexPrintf("%f\t ",print_data[i*J + j]);
  // 			}
  // 		mexPrintf("\n ");
  // 		}	
  // 		mexPrintf("\n end of fft in\n ");
	//------------------------------------------------------------------------------------------------------

	cufftExecR2C(plan, d_in_data_padded, d_out_data_temp);
	cudaThreadSynchronize();
	 
	expand_output<<<gridDim,blockDim2>>>(d_out_data,d_out_data_temp,I,J);  // d_in_data - halved matrix - input to ifft2 - C2R

	// debug --------------------------------------------------------------------------------------------------
	// complex_to_real<<<gridDim,blockDim2>>>(corr_temp,d_out_data,I,J);
 // 	cudaMemcpy(print_data,corr_temp, sizeof(cufftReal)*I*J, cudaMemcpyDeviceToHost); 
 // 	mexPrintf("\n fft out real\n ");
 //  	for (int i = 0; i < I; ++i)
 //  		{
 //  		for (int j = 0; j < J; ++j)
 //  			{
 //  			// mexPrintf("%f\t ",(float)(*mx_temp).at<double>(0,M*j+i));
 //  			mexPrintf("%f\t ",print_data[i*J + j]);
 //  			}
 //  		mexPrintf("\n ");
 //  		}	
 //  		mexPrintf("\n end of fft out\n ");
	// cudaFree(corr_temp);
	//------------------------------------------------------------------------------------------------------
	
 
	cufftDestroy(plan);
	cudaFree(d_out_data_temp);
	cudaFree(d_in_data_padded);	
	

	// delete [] h_out_data_temp;


	}


void fft_R2C(cufftComplex * d_out_data,float *h_in_data,int M,int N,int I,int J,int flag_square){

	// Output array - host side
	float2 *h_out_data_temp = new float2[I*(J/2+1)] ; 

	cufftHandle plan;
	cufftReal *d_in_data;       // Input array - device side
	cufftReal *d_in_data_padded;       // Input array - device side	
	     // Output array - device side - full array 
	cufftComplex * d_out_data_temp;  // Output array - device side - half array (output from the fft2)
	 
	 
	cudaMalloc((void**)&d_in_data_padded, sizeof(cufftReal)*I*J);
	cudaMalloc((void**)&d_in_data, sizeof(cufftReal)*M*N);
	cudaMemcpy(d_in_data, h_in_data, sizeof(cufftReal)*M*N, cudaMemcpyHostToDevice);    

	if (flag_square)
		{
		dim3 blockDim0(16, 16);
		dim3 gridDim0((M + blockDim0.x - 1) / blockDim0.x , (N + blockDim0.y - 1) / blockDim0.y);
		square<<<gridDim0,blockDim0>>>(d_in_data,d_in_data,M,N);
		}

	dim3 blockDim2(16, 16);
	dim3 gridDim((I + blockDim2.x - 1) / blockDim2.x , (J + blockDim2.y - 1) / blockDim2.y);
	zero_pad_input<<<gridDim,blockDim2>>>(d_in_data_padded,d_in_data,M,N,I,J);

	 
	cudaMalloc((void**)&d_out_data_temp, sizeof(cufftComplex)*I*(J/2 + 1));
	


	//Performe the fft
	cufftPlan2d(&plan, I, J , CUFFT_R2C);
	cufftSetCompatibilityMode(plan, CUFFT_COMPATIBILITY_NATIVE);
	cufftExecR2C(plan, d_in_data_padded, d_out_data_temp);
	cudaThreadSynchronize();
	 
	expand_output<<<gridDim,blockDim2>>>(d_out_data,d_out_data_temp,I,J);  // d_in_data - halved matrix - input to ifft2 - C2R
	
 
	cufftDestroy(plan);
	//cudaFree(d_out_data);
	cudaFree(d_out_data_temp);
	cudaFree(d_in_data); 
	cudaFree(d_in_data_padded);	

	delete [] h_out_data_temp;


	}

void fft_C2R(cufftReal * d_out_data, cufftComplex *d_in_data_temp, int I, int J, int M, int N){

	// ***********************
	// Make the half matrix on CPU 
	// Output array - host side
	// 	float2 *h_in_data_temp = new float2[I*(J/2+1)] ; 
	// 
	// 	for (int i = 0; i < I ; i++)
	// 		for (int j = 0; j < J ; j++)
	// 			if ( j < J/2 + 1)
	// 				h_in_data_temp[i*(J/2+1) + j] = h_in_data[i*J + j];
	// ***********************

	cufftHandle plan;
	cufftComplex *d_in_data;       // Input array half - device side
	cufftReal * d_out_data_IJ; cudaMalloc((void**)&d_out_data_IJ , sizeof(cufftReal)*I*J);


	  // Input array full- device side
	  // Output array - device side
	//int n[NRANK] = {I, J};

	cudaMalloc((void**)&d_in_data, sizeof(cufftComplex)*I*(J/2 + 1));
	//cudaMemcpy(d_in_data, h_in_data_temp, sizeof(cufftComplex)*I*(J/2 + 1), cudaMemcpyHostToDevice);  // Uncomment if it is already haved in CPU
	
	// ******************
	// Halve the input in gpu
 	//cudaMalloc((void**)&d_in_data_temp, sizeof(cufftComplex)*I*J);                    
 	//cudaMemcpy(d_in_data_temp, h_in_data, sizeof(float2)*I*J, cudaMemcpyHostToDevice);

	// Halve the input array
	dim3 blockDim2(16, 16);
	dim3 gridDim((I + blockDim2.x - 1) / blockDim2.x , (J/2 + 1 + blockDim2.y - 1) / blockDim2.y);
	halve_input<<<gridDim,blockDim2>>>(d_in_data,d_in_data_temp,I,J);  // d_in_data - halved matrix - input to ifft2 - C2R
	// ******************

	

	//Performe the fft
	cufftPlan2d(&plan, I, J , CUFFT_C2R);
	cufftSetCompatibilityMode(plan, CUFFT_COMPATIBILITY_NATIVE);
	cufftExecC2R(plan, d_in_data, d_out_data_IJ);
	cudaThreadSynchronize();

	// Noramlize the output - on GPU
	  	dim3 gridDim2((M + blockDim2.x - 1) / blockDim2.x , (N + blockDim2.y - 1) / blockDim2.y);
	normalize_ifft<<<gridDim2,blockDim2>>>(d_out_data,d_out_data_IJ,I,J,M,N);


	cufftDestroy(plan);
	//cudaFree(d_in_data_temp);
	cudaFree(d_in_data);
	cudaFree(d_out_data_IJ);
 


	}

void fft_C2R_plus_corr(cufftReal* corr, cufftReal * d_out_data, cufftComplex *d_in_data_temp, int I, int J, int M, int N){

	// ***********************
	// Make the half matrix on CPU 
	// Output array - host side
	// 	float2 *h_in_data_temp = new float2[I*(J/2+1)] ; 
	// 
	// 	for (int i = 0; i < I ; i++)
	// 		for (int j = 0; j < J ; j++)
	// 			if ( j < J/2 + 1)
	// 				h_in_data_temp[i*(J/2+1) + j] = h_in_data[i*J + j];
	// ***********************

	cufftHandle plan;
	cufftComplex *d_in_data;       // Input array half - device side
	cufftReal * d_out_data_IJ; cudaMalloc((void**)&d_out_data_IJ , sizeof(cufftReal)*I*J);


	  // Input array full- device side
	  // Output array - device side
	//int n[NRANK] = {I, J};

	cudaMalloc((void**)&d_in_data, sizeof(cufftComplex)*I*(J/2 + 1));
	//cudaMemcpy(d_in_data, h_in_data_temp, sizeof(cufftComplex)*I*(J/2 + 1), cudaMemcpyHostToDevice);  // Uncomment if it is already haved in CPU
	
	// ******************
	// Halve the input in gpu
 	//cudaMalloc((void**)&d_in_data_temp, sizeof(cufftComplex)*I*J);                    
 	//cudaMemcpy(d_in_data_temp, h_in_data, sizeof(float2)*I*J, cudaMemcpyHostToDevice);

	// Halve the input array
	dim3 blockDim2(16, 16);
	dim3 gridDim((I + blockDim2.x - 1) / blockDim2.x , (J/2 + 1 + blockDim2.y - 1) / blockDim2.y);
	halve_input<<<gridDim,blockDim2>>>(d_in_data,d_in_data_temp,I,J);  // d_in_data - halved matrix - input to ifft2 - C2R
	// ******************

	

	//Performe the fft
	cufftPlan2d(&plan, I, J , CUFFT_C2R);
	cufftSetCompatibilityMode(plan, CUFFT_COMPATIBILITY_NATIVE);
	cufftExecC2R(plan, d_in_data, d_out_data_IJ);
	cudaThreadSynchronize();

	// Noramlize the output - on GPU
	  	dim3 gridDim2((M + blockDim2.x - 1) / blockDim2.x , (N + blockDim2.y - 1) / blockDim2.y);
	normalize_ifft_plus_corr<<<gridDim2,blockDim2>>>(corr,d_out_data,d_out_data_IJ,I,J,M,N);


	cufftDestroy(plan);
	//cudaFree(d_in_data_temp);
	cudaFree(d_in_data);
	cudaFree(d_out_data_IJ);
 


	}


void fft_R2C_old(float2 *h_out_data,float **h_in_data,int M,int N,int I,int J){


	float *mx_padded = new float[I*J];  
	for (int r = 0; r < I; ++r)  // this can be also done on GPU
		{	 
		for (int c = 0; c < J; ++c)
			{
			if (r < M && c < N)
				mx_padded[r*J + c] = h_in_data[r][c];
			else
				mx_padded[r*J + c]  = 0;
			}
		}


	// Output array - host side
	float2 *h_out_data_temp = new float2[I*(J/2+1)] ; 

	cufftHandle plan;
	cufftReal *d_in_data;       // Input array - device side
	cufftComplex * d_out_data;  // Output array - device side
	int n[NRANK] = {I, J};


	cudaMalloc((void**)&d_in_data, sizeof(cufftReal)*I*J);
	cudaMemcpy(d_in_data, mx_padded, sizeof(cufftReal)*I*J, cudaMemcpyHostToDevice);    	  
	cudaMalloc((void**)&d_out_data, sizeof(cufftComplex)*I*(J/2 + 1));


	//Performe the fft
	cufftPlan2d(&plan, I, J , CUFFT_R2C);
	cufftSetCompatibilityMode(plan, CUFFT_COMPATIBILITY_NATIVE);
	cufftExecR2C(plan, d_in_data, d_out_data);
	cudaThreadSynchronize();
	cudaError  cudaStat2 = 	cudaMemcpy(h_out_data_temp,d_out_data,  sizeof(cufftComplex)*I*(J/2+1), cudaMemcpyDeviceToHost);

	for (int i = 0; i < I ; i++){
		for (int j = 0; j < J ; j++)
			{
			if ( j < J/2 + 1)
				h_out_data[i*J + j] = h_out_data_temp[i*(J/2+1) + j];
			else if ( i == 0)
				{ 
				h_out_data[i*J + j].x = h_out_data_temp[i*(J/2+1) + (J-j)].x;
				h_out_data[i*J + j].y = -h_out_data_temp[i*(J/2+1) + (J-j)].y;
				}
			else 
				{
				h_out_data[i*J + j].x = h_out_data_temp[(I-i)*(J/2+1) + (J-j)].x;
				h_out_data[i*J + j].y = -h_out_data_temp[(I-i)*(J/2+1) + (J-j)].y;
				}
			}
		}


	cufftDestroy(plan);
	cudaFree(d_out_data);


	}