#define DEBUG_LEVEL 4
void train_ensemble_evidence6_test(Ensemble* t_ensemble, Ensemble ensemble,Direction* grad,double step_len,Mat dimensions,char* opt_func,Direction direction,int state,Mat P1,Mat P2,int P3,int P4,int P5,int P6,int P7,int P8,Priors P9,int P10,Mat P11,Mat P12)
{
 
	//tic();
/************************************************************************/
/* GPU KERNEL CALL DEMO                                                 */
/************************************************************************/   
// Here we call 'call_kernel' in demo_kernel.cu file - it will run on gpu
//  call_kernel();
/************************************************************************/
	t_ensemble->a_sigma         = ensemble.a_sigma;
	t_ensemble->a_x             = ensemble.a_x;
	t_ensemble->b_sigma         = ensemble.b_sigma;
	t_ensemble->b_sigma_2       = ensemble.b_sigma_2;
	t_ensemble->b_x             = ensemble.b_x;
	t_ensemble->b_x_2           = ensemble.b_x_2.clone();
	t_ensemble->ba_sigma_2      = ensemble.ba_sigma_2;
	t_ensemble->ba_x_2          = ensemble.ba_x_2.clone();
	t_ensemble->D_val           = ensemble.D_val;
	t_ensemble->D_x             = ensemble.D_x.clone();
	t_ensemble->log_lambda_x    = ensemble.log_lambda_x.clone();
	t_ensemble->mx              = ensemble.mx.clone();
	t_ensemble->mx2             = ensemble.mx2.clone();
	t_ensemble->opt_b_x_2       = ensemble.opt_b_x_2.clone();
	t_ensemble->opt_ba_sigma_2  = ensemble.opt_ba_sigma_2;
	t_ensemble->opt_ba_x_2      = ensemble.opt_ba_x_2.clone();
	t_ensemble->opt_pi_x_2      = ensemble.opt_pi_x_2.clone();
	t_ensemble->pi_x            = ensemble.pi_x;
	t_ensemble->pi_x_2          = ensemble.pi_x_2.clone();
	t_ensemble->x1              = ensemble.x1.clone();
	t_ensemble->x2              = ensemble.x2.clone();


	//%Step to the test point
	t_ensemble->x1 = t_ensemble->x1   + step_len*direction.x1;
	t_ensemble->x2  =abs(t_ensemble->x2    +step_len*direction.x2);
	t_ensemble->b_x_2 =abs(t_ensemble->b_x_2 +step_len*direction.b_x_2);
	t_ensemble->ba_x_2=abs(t_ensemble->ba_x_2 +step_len*direction.ba_x_2);
	t_ensemble->pi_x_2=abs(t_ensemble->pi_x_2 +step_len*direction.pi_x_2);

	//%Make a copy of the directions
	grad->b_x_2= direction.b_x_2;
	grad->ba_x_2= direction.ba_x_2;
	grad->pi_x_2= direction.pi_x_2;
	grad->x1= direction.x1;
	grad->x2 = direction.x2;

	//%Check for valid t_ensemble
	//Geethan :parallelise the following
	
	bool neg_found = false;
	for (int i=0;i<t_ensemble->x2.rows;i++)
	{
		for (int j=0;j<t_ensemble->x2.cols;j++)
		{
			if (t_ensemble->x2.at<double>(i,j)<0)
			{
				neg_found = true;
				break;
			}
		}
	}
	if (neg_found == false)
	{
		for (int i=0;i<t_ensemble->b_x_2.rows;i++)
		{
			for (int j=0;j<t_ensemble->b_x_2.cols;j++)
			{
				if (t_ensemble->b_x_2.at<double>(i,j)<0)
				{
					neg_found = true;
					break;
				}
			}
		}
	}
	if (neg_found == false)
	{
		for (int i=0;i<t_ensemble->ba_x_2.rows;i++)
		{
			for (int j=0;j<t_ensemble->ba_x_2.cols;j++)
			{
				if (t_ensemble->ba_x_2.at<double>(i,j)<0)
				{
					neg_found = true;
					break;
				}
			}
		}
	}
	if (neg_found == false)
	{
		for (int i=0;i<t_ensemble->pi_x_2.rows;i++)
		{
			for (int j=0;j<t_ensemble->pi_x_2.cols;j++)
			{
				if (t_ensemble->pi_x_2.at<double>(i,j)<0)
				{
					neg_found = true;
					break;
				}
			}
		}
	}
	if(neg_found){
		t_ensemble->D_val = numeric_limits<double>::infinity();	
		grad->x1		= Mat::ones(grad->x1.rows,grad->x1.cols,CV_64F)*numeric_limits<double>::quiet_NaN() ;	
		grad->x2		= Mat::ones(grad->x2.rows,grad->x2.cols,CV_64F)*numeric_limits<double>::quiet_NaN() ;
		grad->b_x_2		= Mat::ones(grad->b_x_2.rows,grad->b_x_2.cols,CV_64F)*numeric_limits<double>::quiet_NaN() ;
		grad->ba_x_2 = Mat::ones(grad->ba_x_2.rows,grad->ba_x_2.cols,CV_64F)*numeric_limits<double>::quiet_NaN() ;
		grad->pi_x_2 = Mat::ones(grad->pi_x_2.rows,grad->pi_x_2.cols,CV_64F)*numeric_limits<double>::quiet_NaN() ;
		return;
	}


	//	%Find evidence by summing over the components
	int ptr = 0;
	for(int c=0; c< dimensions.rows;c++){

		//	%Update the expectations based on the updated Q(x)
		Mat* cx1;//(0,0,CV_64F);
		Mat* cx2;//(0,0,CV_64F);
		train_ensemble_get_test(&cx1,c,dimensions,t_ensemble->x1);
		train_ensemble_get_test(&cx2,c,dimensions,t_ensemble->x2);
		Mat* cHx;//(0,0,CV_64F);
		Mat* cmx;//(0,0,CV_64F);
		Mat* cmx2;//(0,0,CV_64F);
 
	
		train_ensemble_rectified5_test_u(&cHx,&cmx,&cmx2,*cx1,*cx2,dimensions.at<int>(c,3));
		cx1->release();
		cx2->release();
		//train_ensemble_rectified5(cx1,cx2,dimensions(c,4)); 

		//%Find optimum weights
		Mat c_pi_x_2(t_ensemble->pi_x_2,Rect(ptr,0,(int)dimensions.at<int>(c,0)*(int)dimensions.at<int>(c,2),1));// Rect(x, y, width, height);
		c_pi_x_2.reshape(0,dimensions.at<int>(c,0));


		Mat c_b_x_2(t_ensemble->b_x_2,Rect(ptr,0,(int)dimensions.at<int>(c,0)*(int)dimensions.at<int>(c,2),1));// Rect(x, y, width, height);
		c_b_x_2.reshape(0,dimensions.at<int>(c,0));

		Mat c_ba_x_2(t_ensemble->ba_x_2,Rect(ptr,0,(int)dimensions.at<int>(c,0)*(int)dimensions.at<int>(c,2),1));// Rect(x, y, width, height);
		c_ba_x_2.reshape(0,dimensions.at<int>(c,0));




		//int* dims = new int[3];
		const int dims[3] = {(int) dimensions.at<int>(c,0),(int) dimensions.at<int>(c,1),(int) dimensions.at<int>(c,2)};
		//dims[0] = dimensions.at<int>(c,0);
		//dims[1] = dimensions.at<int>(c,1);
		//dims[2] = dimensions.at<int>(c,2);

		//Mat c_log_lambda_x= Mat::zeros(3,dims,CV_64F);

		Mat c_log_lambda_x(3,dims,CV_64F,Scalar(0));
		//Geethan initialize to zero is not required!!

		//for (int i =0; i<dimensions.at<int>(c,0) ; i++)
		//{
		//	for (int j =0; j<dimensions.at<int>(c,1);j++)
		//	{
		//		for (int k = 0; dimensions.at<int>(c,2); k++)
		//		{
		//			c_log_lambda_x.at<double>(i,j,k) = 0;
		//		}
		//	}
		//}

		if(dimensions.at<int>(c,2)>1){

			if (dimensions.at<int>(c,3) == 0 || dimensions.at<int>(c,3) == 2)
			{
				for (int alpha = 0; alpha < dimensions.at<int>(c,2);alpha++)
				{
					for (int k = 0; k< dimensions.at<int>(c,0);k++)
					{
						for (int j = 0; j<dimensions.at<int>(c,1);j++)
						{
							c_log_lambda_x.at<double>(k,j,alpha) = log(c_pi_x_2.at<double>(k,alpha))-0.5/c_pi_x_2.at<double>(k,alpha) + 0.5*log(c_ba_x_2.at<double>(k,alpha))-0.25/c_b_x_2.at<double>(k,alpha)-0.5*cmx2->at<double>(k,j)*c_ba_x_2.at<double>(k,alpha);
						}
						//c_log_lambda_x
						/*if (dimensions(c,4)==0 | dimensions(c,4)==2)
							%Gaussian or Rectified Gaussian prior
							for alpha=1:dimensions(c,3)
								for k=1:dimensions(c,1)
									c_log_lambda_x(k,:,alpha) = (log(c_pi_x_2(k,alpha))-0.5/ c_pi_x_2(k,alpha)+0.5*log(c_ba_x_2(k,alpha))-0.25/c_b_x_2(k,alpha)-0.5*cmx2(k,:)*c_ba_x_2(k,alpha));
						end*/
					}
				}
			}
			//	elseif (dimensions(c,4)==1)
			//	%Exponential prior
			else if (dimensions.at<int>(c,3) == 1)
			{
				for (int alpha = 0; alpha < dimensions.at<int>(c,2);alpha++)
				{
					if (P11.empty())
					{
						//		if isempty('P11')
						//			keyboard
						//			for k=1:dimensions(c,1)
						//				c_log_lambda_x(k,:,alpha) = (log(c_pi_x_2(k,alpha))-0.5/c_pi_x_2(k,alpha)+log(c_ba_x_2(k,alpha))-0.5/c_b_x_2(k,alpha)-cmx(k,:)*c_ba_x_2(k,alpha)) + P11(alpha,:);
						//end
					}
					else{
						for (int k = 0; k< dimensions.at<int>(c,0);k++)
						{
							for (int j = 0; j<dimensions.at<int>(c,1);j++)
							{
								c_log_lambda_x.at<double>(k,j,alpha) = log(c_pi_x_2.at<double>(k,alpha))-0.5/c_pi_x_2.at<double>(k,alpha) + log(c_ba_x_2.at<double>(k,alpha))-0.5/c_b_x_2.at<double>(k,alpha)-cmx->at<double>(k,j)*c_ba_x_2.at<double>(k,alpha);
							}
							//		else
							//		for k=1:dimensions(c,1)
							//			c_log_lambda_x(k,:,alpha) = (log(c_pi_x_2(k,alpha))-0.5/c_pi_x_2(k,alpha)+log(c_ba_x_2(k,alpha))-0.5/c_b_x_2(k,alpha)-cmx(k,:)*c_ba_x_2(k,alpha));
							//end
							//	end
						}

					}
				}

			}
			//	elseif (dimensions(c,4)==3)
			//	%Discrete prior  
			else if (dimensions.at<int>(c,3) == 3)
			{

			}

			//	elseif  (dimensions(c,4)==4)
			//	%Exponential prior
			else if (dimensions.at<int>(c,3) == 4)
			{
				for (int alpha = 0; alpha < dimensions.at<int>(c,2);alpha++)
				{
					for (int k = 0; k< dimensions.at<int>(c,0);k++)
					{
						for (int j = 0; j<dimensions.at<int>(c,1);j++)
						{
							c_log_lambda_x.at<double>(k,j,alpha) = log(c_pi_x_2.at<double>(k,alpha))-0.5/c_pi_x_2.at<double>(k,alpha) + 0.5*log(c_ba_x_2.at<double>(k,alpha))-0.25/c_b_x_2.at<double>(k,alpha)-0.5*cmx->at<double>(k,j)*c_ba_x_2.at<double>(k,alpha);
						}
					}
				}
				//	for alpha=1:dimensions(c,3)
				//		for k=1:dimensions(c,1)
				//			c_log_lambda_x(k,:,alpha)=log(c_pi_x_2(k,alpha))-0.5/c_pi_x_2(k,alpha)+0.5*log(c_ba_x_2(k,alpha))-0.25/c_b_x_2(k,alpha)-0.5*abs(cmx(k,:))*c_ba_x_2(k,alpha);
				//end
				//	end
				//	end
			}		

			//	%Now normalise c_log_lambda_x
			//	max_c_log_lambda_x=max(c_log_lambda_x,[],3);
			Mat max_c_log_lambda_x(dimensions.at<int>(c,0),dimensions.at<int>(c,1),CV_64F,Scalar(-DBL_MAX));
			Mat log_sum_lambda_x(dimensions.at<int>(c,0),dimensions.at<int>(c,1),CV_64F,Scalar(0));


			for (int i = 0; i<dimensions.at<int>(c,1);i++)
			{
				for (int k = 0; k< dimensions.at<int>(c,0);k++)
				{
					for (int j =0 ; j< dimensions.at<int>(c,2);j++)
					{
						if (c_log_lambda_x.at<double>(k,i,j)>max_c_log_lambda_x.at<double>(k,i))
						{
							max_c_log_lambda_x.at<double>(k,i) = c_log_lambda_x.at<double>(k,i,j);
						}
					}
				}
			}
			//for alpha=1:dimensions(c,3)
			//	c_log_lambda_x(:,:,alpha)=c_log_lambda_x(:,:,alpha)-max_c_log_lambda_x;
			//end
			for (int i = 0; i<dimensions.at<int>(c,1);i++)
			{
				for (int k = 0; k< dimensions.at<int>(c,0);k++)
				{
					for (int j =0 ; j< dimensions.at<int>(c,2);j++)
					{

						c_log_lambda_x.at<double>(k,i,j) = c_log_lambda_x.at<double>(k,i,j)- max_c_log_lambda_x.at<double>(k,i) ;
					}
				}
			}


			//	log_sum_lambda_x=log(sum(exp(c_log_lambda_x),3));
			//for alpha=1:dimensions(c,3)
			//	c_log_lambda_x(:,:,alpha)=c_log_lambda_x(:,:,alpha)-log_sum_lambda_x;
			//end 
			//	end

			for (int i = 0; i<dimensions.at<int>(c,1);i++)
			{
				for (int k = 0; k< dimensions.at<int>(c,0);k++)
				{
					for (int j =0 ; j< dimensions.at<int>(c,2);j++)
					{
						log_sum_lambda_x.at<double>(k,i) +=exp(c_log_lambda_x.at<double>(k,i,j));

					}
				}
			}

			log(log_sum_lambda_x,log_sum_lambda_x);

			for (int i = 0; i<dimensions.at<int>(c,1);i++)
			{
				for (int k = 0; k< dimensions.at<int>(c,0);k++)
				{
					for (int j =0 ; j< dimensions.at<int>(c,2);j++)
					{

							c_log_lambda_x.at<double>(k,i,j) = c_log_lambda_x.at<double>(k,i,j) - log_sum_lambda_x.at<double>(k,i);
					}
				}
			}


		}


		//	%Find the optimum parameters for the prior parameters
		Mat opt_c_b_x_2(dimensions.at<int>(c,0),dimensions.at<int>(c,2),CV_64F,Scalar(0));
		Mat opt_c_pi_x_2(opt_c_b_x_2);
		Mat opt_c_ba_x_2(dimensions.at<int>(c,0),dimensions.at<int>(c,2),CV_64F);

		if (dimensions.at<int>(c,3) == 0 || dimensions.at<int>(c,3) == 2){
			//		%(Rectified) Gaussian prior for P(x)

			
			for (int i = 0; i<dimensions.at<int>(c,2);i++)
			{
				for (int k = 0; k< dimensions.at<int>(c,0);k++)
				{
					for (int j =0 ; j< dimensions.at<int>(c,1);j++)
					{
						opt_c_b_x_2.at<double>(k,i) +=exp(c_log_lambda_x.at<double>(k,j,i));   // @Geethan ---- > we have to use sum reduction here

					}
				}
			}
			//		opt_c_b_x_2=t_ensemble.b_x+reshape(sum(exp(c_log_lambda_x),2),dimensions(c,1),dimensions(c,3))/2;
			//opt_c_pi_x_2=t_ensemble.pi_x+reshape(sum(exp(c_log_lambda_x),2),dimensions(c,1),dimensions(c,3));
			opt_c_pi_x_2 = opt_c_b_x_2.clone();
			opt_c_b_x_2 = opt_c_b_x_2/2;
			add(opt_c_b_x_2,t_ensemble->b_x,opt_c_b_x_2);
			add(opt_c_pi_x_2,t_ensemble->pi_x,opt_c_pi_x_2);
				
			
			//for alpha=1:dimensions(c,3)
			//	opt_c_ba_x_2(:,alpha)=opt_c_b_x_2(:,alpha)./(t_ensemble.a_x+sum(exp(c_log_lambda_x(:,:,alpha)).*cmx2,2)/2);
			//end
			Mat temp2(dimensions.at<int>(c,0),1,CV_64F,Scalar(0));

			for (int alpha = 0; alpha < dimensions.at<int>(c,2);alpha++)
			{

				for (int k = 0; k< dimensions.at<int>(c,0);k++)
				{
					for (int i = 0; i<dimensions.at<int>(c,1);i++)
					{
						temp2.at<double>(k,0) += exp(c_log_lambda_x.at<double>(k,i,alpha))*cmx2->at<double>(k,i);
					}
					temp2.at<double>(k,0) = t_ensemble->a_x + temp2.at<double>(k,0)/2;		// Geethan shouldn't t_ensemble.a_x have size kx1?---> K is Assumed to be 1 , geethan
					opt_c_ba_x_2.at<double>(k,alpha) = opt_c_b_x_2.at<double>(k,alpha)/temp2.at<double>(k,0);
				}
			}

		}
		//	elseif (dimensions(c,4)==1)
		//	%Exponential prior for P(x)
		else if (dimensions.at<int>(c,3) == 1)
		{
			
			//	opt_c_b_x_2=t_ensemble.b_x+reshape(sum(exp(c_log_lambda_x),2),dimensions(c,1),dimensions(c,3));
			//opt_c_pi_x_2=t_ensemble.pi_x+reshape(sum(exp(c_log_lambda_x),2),dimensions(c,1),dimensions(c,3));
			for (int i = 0; i<dimensions.at<int>(c,2);i++)
			{
				for (int k = 0; k< dimensions.at<int>(c,0);k++)
				{   
					//opt_c_b_x_2.at<double>(k,i) = 0;       
					for (int j =0 ; j< dimensions.at<int>(c,1);j++)
					{
						opt_c_b_x_2.at<double>(k,i) +=exp(c_log_lambda_x.at<double>(k,j,i));   // j,i twist ----- udaranga

					}
				}
			}
			opt_c_pi_x_2 = opt_c_b_x_2.clone();
			add(opt_c_b_x_2,t_ensemble->b_x,opt_c_b_x_2);
			add(opt_c_pi_x_2,t_ensemble->pi_x,opt_c_pi_x_2);
			//opt_c_ba_x_2=zeros(dimensions(c,1),dimensions(c,3));
			//for alpha=1:dimensions(c,3)
			//	opt_c_ba_x_2(:,alpha)=opt_c_b_x_2(:,alpha)./(t_ensemble.a_x+sum(exp(c_log_lambda_x(:,:,alpha)).*cmx,2));
			//end
			Mat temp2(dimensions.at<int>(c,0),1,CV_64F,Scalar(0));


			for (int alpha = 0; alpha < dimensions.at<int>(c,2);alpha++)
			{
				
				for (int k = 0; k< dimensions.at<int>(c,0);k++)
				{
					temp2.at<double>(k,0) = 0;
					for (int i = 0; i<dimensions.at<int>(c,1);i++)
					{
						temp2.at<double>(k,0) += exp(c_log_lambda_x.at<double>(k,i,alpha))*cmx->at<double>(k,i);
					}
					temp2.at<double>(k,0) += t_ensemble->a_x;
					opt_c_ba_x_2.at<double>(k,alpha) = opt_c_b_x_2.at<double>(k,alpha)/temp2.at<double>(k,0);
				}
			}
		}
			//	elseif ((dimensions(c,4)==3) | (dimensions(c,4)==4))
			//	%Discrete prior (not learning any parameters of prior)
		else if (dimensions.at<int>(c,3) == 3 || dimensions.at<int>(c,3) == 3)
		{
			//	opt_c_b_x_2=t_ensemble.b_x*ones(dimensions(c,1),dimensions(c,3));
			//opt_c_ba_x_2=t_ensemble.b_x/t_ensemble.a_x*ones(dimensions(c,1),dimensions(c,3));
			//opt_c_pi_x_2=t_ensemble.pi_x*ones(dimensions(c,1),dimensions(c,3));
			
		}
		else if (dimensions.at<int>(c,3) == 5)
		{
			//elseif (dimensions(c,4)==5)
			//	%Laplacian prior for P(x)
			//	opt_c_b_x_2=t_ensemble.b_x+reshape(sum(exp(c_log_lambda_x),2),dimensions(c,1),dimensions(c,3));
			//opt_c_pi_x_2=t_ensemble.pi_x+reshape(sum(exp(c_log_lambda_x),2),dimensions(c,1),dimensions(c,3));
			//opt_c_ba_x_2=zeros(dimensions(c,1),dimensions(c,3));
			//for alpha=1:dimensions(c,3)
			//	opt_c_ba_x_2(:,alpha)=opt_c_b_x_2(:,alpha)./(t_ensemble.a_x+sum(exp(c_log_lambda_x(:,:,alpha)).*cmx,2));
			//end
			//	end
		}


		//	%%%% Manual over-ride for priors
		//	if (dimensions(c,5)>0)
		if (dimensions.at<int>(c,4) >0)
		{
			//		if (c==3) %% Image prior
			if (c == 2)
			{
				//			opt_c_pi_x_2 = P9.pi(1:dimensions(c,1),:) * 1e3;

				opt_c_pi_x_2 = Mat(P9.pi,Rect(0,0,dimensions.at<int>(c,2),dimensions.at<int>(c,0)))*1000;

				//opt_c_ba_x_2 = P9.gamma(1:dimensions(c,1),:);
				opt_c_ba_x_2 = Mat(P9.gamma,Rect(0,0,dimensions.at<int>(c,2),dimensions.at<int>(c,0)));

				//opt_c_b_x_2 = ones(dimensions(c,1),dimensions(c,3)) * 1e-3; 
				opt_c_b_x_2 = Mat::ones(dimensions.at<int>(c,2),dimensions.at<int>(c,0),CV_64F)/1000;
			}

			//elseif (c==2) %% Blur prior
			else if (c == 1)
			{
				//	opt_c_ba_x_2 = [ 5.1143e3 5.0064e+03 173.8885 50.6538 ];
				opt_c_ba_x_2.at<double>(0,0) = 5114.3;
				opt_c_ba_x_2.at<double>(0,1) = 5006.4;
				opt_c_ba_x_2.at<double>(0,2) = 173.8885;
				opt_c_ba_x_2.at<double>(0,3) = 50.6538;
				//opt_c_b_x_2 = [787.8988 201.7349 236.1948 143.1756];
				opt_c_b_x_2.at<double>(0,0) = 787.8988;
				opt_c_b_x_2.at<double>(0,1) = 201.7349;
				opt_c_b_x_2.at<double>(0,2) = 236.1948;
				opt_c_b_x_2.at<double>(0,3) = 143.1756;
			}

			//		else
			//			error foo
			//			end
			else {
				mexPrintf("error\n");
			}
		}

		//	else
		//	% Leave alone
		//	end   


		//	%Optimum Q(x)
		//	opt_cx1=zeros(size(cx1));
		//opt_cx2=zeros(size(cx2));
		Mat opt_cx1(dimensions.at<int>(c,0),dimensions.at<int>(c,1),CV_64F,Scalar(0));
		Mat opt_cx2(dimensions.at<int>(c,0),dimensions.at<int>(c,1),CV_64F,Scalar(0));

		if (dimensions.at<int>(c,3) == 0 || dimensions.at<int>(c,3) == 2){

		//if (dimensions(c,4)==0 | dimensions(c,4)==2)
			for(int alpha = 0; alpha < dimensions.at<int>(c,2);alpha++){
				for(int k=0;k<dimensions.at<int>(c,0);k++){
					for (int i=0; i<dimensions.at<int>(c,1);i++)
					{
						opt_cx2.at<double>(k,i) = opt_cx2.at<double>(k,i) + c_ba_x_2.at<double>(k,alpha)*exp(c_log_lambda_x.at<double>(k,i,alpha));
					}
				}
			}
		//	for alpha=1:dimensions(c,3)
		//		for k=1:dimensions(c,1)
		//			opt_cx2(k,:)=opt_cx2(k,:)+c_ba_x_2(k,alpha)*exp(c_log_lambda_x(k,:,alpha));
		//end
		//	end
		}
		if (dimensions.at<int>(c,3) == 1 || dimensions.at<int>(c,3) == 4){
			//	elseif ((dimensions(c,4)==1)| dimensions(c,4)==4)
			//	for alpha=1:dimensions(c,3)
			//		for k=1:dimensions(c,1)
			//			opt_cx1(k,:)=opt_cx1(k,:)-c_ba_x_2(k,alpha)*exp(c_log_lambda_x(k,:,alpha));
			//end
			//	end
			//	end
			for(int alpha = 0; alpha < dimensions.at<int>(c,2);alpha++){
				for(int k=0;k<dimensions.at<int>(c,0);k++){
					for (int i=0; i<dimensions.at<int>(c,1);i++)
					{
						opt_cx1.at<double>(k,i) = opt_cx1.at<double>(k,i) - c_ba_x_2.at<double>(k,alpha)*exp(c_log_lambda_x.at<double>(k,i,alpha));
					}
				}
			}			

		}


		//	%Evaluate the KL divergence for the distributions trained in this function
		//	t_ensemble.D_x(sum(dimensions(1:c-1,1))+1:sum(dimensions(1:c,1)))=sum(cHx,2)+...
		//	sum(gammaln(t_ensemble.b_x)-t_ensemble.b_x*log(t_ensemble.a_x)-gammaln(c_b_x_2)+c_b_x_2.*log(c_b_x_2./c_ba_x_2)+(c_b_x_2-opt_c_b_x_2).*(log(c_ba_x_2)-0.5./c_b_x_2)+(opt_c_b_x_2./opt_c_ba_x_2-c_b_x_2./c_ba_x_2).*c_ba_x_2,2)+...
		//	sum(gammaln(t_ensemble.pi_x)-gammaln(c_pi_x_2)+(c_pi_x_2-opt_c_pi_x_2).*(log(c_pi_x_2)-0.5./c_pi_x_2),2)+...
		//	(-gammaln(dimensions(c,3)*t_ensemble.pi_x)+gammaln(sum(c_pi_x_2,2))+sum(c_pi_x_2-opt_c_pi_x_2,2).*(-log(sum(c_pi_x_2,2))+0.5./sum(c_pi_x_2,2)))+...
		//	sum(sum(c_log_lambda_x.*exp(c_log_lambda_x),2),3);

		Mat sum1;
		reduce(*cHx,sum1,1,CV_REDUCE_SUM);
		
#if DEBUG_LEVEL > 3	
	if(c==1){
	mexPrintf("\n cHx real cpp \n");
	for (int i=0; i<dimensions.at<int>(c,1);i++)
	{
		mexPrintf("%f ",cHx->at<Vec2d>(0,i)[0]);
	}
	mexPrintf("\n");
	}
	if(c==1){
	mexPrintf("\n cHx complex cpp \n");
	for (int i=0; i<dimensions.at<int>(c,1);i++)
	{
		mexPrintf("%f ",cHx->at<Vec2d>(0,i)[1]);
	}
	mexPrintf("\n");
	}
#endif	
		cHx->release();

		Mat temp3(c_b_x_2.size().height,c_b_x_2.size().width,CV_64F);
		for (int i=0 ; i< temp3.rows;i++)
		{
			for (int j=0; j<temp3.cols;j++)
			{
				temp3.at<double>(i,j) = lgamma(c_b_x_2.at<double>(i,j));
			}
		}

		Mat temp4;
		divide(c_b_x_2,c_ba_x_2,temp4);
		log(temp4,temp4);
		multiply(c_b_x_2,temp4,temp4);

		Mat temp5;
		divide(0.5,c_b_x_2,temp5);
		Mat temp6;
		log(c_ba_x_2,temp6);
		multiply(c_b_x_2-opt_c_b_x_2,temp6-temp5,temp5);

		Mat temp7;
		divide(c_b_x_2,c_ba_x_2,temp7);
		divide(opt_c_b_x_2,opt_c_ba_x_2,temp6);
		multiply(c_ba_x_2,temp6-temp7,temp6);

		Mat sum2;
		sum2 = lgamma(t_ensemble->b_x) - t_ensemble->b_x*std::log(t_ensemble->a_x) - temp3 + temp4 + temp5 + temp6;  // udaranga - bug fix : changed to -temp3 from +temp3
		reduce(sum2,sum2,1,CV_REDUCE_SUM);


		Mat temp2(c_pi_x_2.size().height,c_pi_x_2.size().width,CV_64F);
		for (int i=0 ; i< temp2.rows;i++)
		{
			for (int j=0; j<temp2.cols;j++)
			{
				temp2.at<double>(i,j) = lgamma(c_pi_x_2.at<double>(i,j));
			}
		}

		divide(0.5,c_pi_x_2,temp3);
		log(c_pi_x_2,temp4);
		multiply(c_pi_x_2-opt_c_pi_x_2,temp4 - temp3,temp3);
		Mat sum3;
		sum3 = lgamma(t_ensemble->pi_x) -temp2 + temp3;
		reduce(sum3,sum3,1,CV_REDUCE_SUM);

		reduce(c_pi_x_2,temp2,1,CV_REDUCE_SUM);
		divide(0.5,temp2,temp3);
		log(temp2,temp4);
		reduce(c_pi_x_2-opt_c_pi_x_2,temp5,1,CV_REDUCE_SUM);
		multiply(temp5,-temp4+temp3,temp3);
		for (int i=0 ; i< temp2.rows;i++)
		{
			for (int j=0; j<temp2.cols;j++)
			{
				temp2.at<double>(i,j) = lgamma(temp2.at<double>(i,j));
			}
		}
		Mat sum4;
		sum4 = -lgamma(dimensions.at<int>(c,2)*t_ensemble->pi_x) + temp2 + temp3;
		
		Mat temp1;
		exp(c_log_lambda_x,temp1);
		multiply(c_log_lambda_x,temp1,temp1);
		Mat sum5(dimensions.at<int>(c,0),1,CV_64F,Scalar(0));
		for (int i = 0; i<dimensions.at<int>(c,0); i++)
		{
			for (int j =0; j < dimensions.at<int>(c,1); j++)
			{
				for (int k=0; k< dimensions.at<int>(c,2); k++)
				{
					sum5.at<double>(i,0) += temp1.at<double>(i,j,k); 
				}
			}
		}
		int start_idx =0;
		int end_idx = 0;
		for (int i =0; i<=c;i++)
		{
			if (i<=c-1)
			{
				start_idx += dimensions.at<int>(i,0);
			}
			end_idx += dimensions.at<int>(i,0);
		}
		Mat totsum = sum1 + sum2 +sum3 + sum4 + sum5;
#if DEBUG_LEVEL > 3	
	if(c==1){
	mexPrintf("\nsums cpp \n");
	mexPrintf("%f ",(float)sum1.at<double>(0,0));
	mexPrintf("%f ",(float)sum2.at<double>(0,0));
	mexPrintf("%f ",(float)sum3.at<double>(0,0));
	mexPrintf("%f ",(float)sum4.at<double>(0,0));
	mexPrintf("%f ",(float)sum5.at<double>(0,0));
	mexPrintf("\n");
	}
#endif			 
		for (int i = 0; start_idx+i < end_idx; i++)
		{
			t_ensemble->D_x.at<double>(start_idx+i,0) = totsum.at<double>(i,0);
			 
		
		}	
			
		

		//%Store updated parameters and optimum distributions
		train_ensemble_put_lambda_test(&(t_ensemble->log_lambda_x),c,dimensions,c_log_lambda_x);
		//	t_ensemble.log_lambda_x=train_ensemble_put_lambda(c,dimensions,t_ensemble.log_lambda_x,c_log_lambda_x);
		
		train_ensemble_put_test(&(t_ensemble->mx),c,dimensions,*cmx);
		train_ensemble_put_test(&(t_ensemble->mx2),c,dimensions,*cmx2);		 

		cmx->release();
		cmx2->release();
		//t_ensemble.mx=train_ensemble_put(c,dimensions,t_ensemble.mx,cmx);
		//t_ensemble.mx2=train_ensemble_put(c,dimensions,t_ensemble.mx2,cmx2);

		int idx = 0;
		for (int i = 0; i<dimensions.at<int>(c,0); i++)
		{
			for (int j =0; j < dimensions.at<int>(c,2); j++)
			{
				t_ensemble->opt_ba_x_2.at<double>(0,ptr + idx) = opt_c_ba_x_2.at<double>(i,j); 
				t_ensemble->opt_b_x_2.at<double>(0,ptr + idx) = opt_c_b_x_2.at<double>(i,j); 
				t_ensemble->opt_pi_x_2.at<double>(0,ptr + idx) = opt_c_pi_x_2.at<double>(i,j); 
				idx++;
			}
		}
		//t_ensemble.opt_ba_x_2(ptr+1:ptr+dimensions(c,1)*dimensions(c,3)) = reshape(opt_c_ba_x_2,1,dimensions(c,1)*dimensions(c,3));
		//t_ensemble.opt_b_x_2(ptr+1:ptr+dimensions(c,1)*dimensions(c,3))  = reshape(opt_c_b_x_2 ,1,dimensions(c,1)*dimensions(c,3));
		//t_ensemble.opt_pi_x_2(ptr+1:ptr+dimensions(c,1)*dimensions(c,3)) = reshape(opt_c_pi_x_2,1,dimensions(c,1)*dimensions(c,3));  
		
		
		train_ensemble_put_test(&(grad->x1),c,dimensions,opt_cx1);
		train_ensemble_put_test(&(grad->x2),c,dimensions,opt_cx2);
		//grad.x1=train_ensemble_put(c,dimensions,grad.x1,opt_cx1);
		//grad.x2=train_ensemble_put(c,dimensions,grad.x2,opt_cx2);

		//%Increment the pointer
		ptr=ptr+dimensions.at<int>(c,0)*dimensions.at<int>(c,2);
		//ptr=ptr+dimensions(c,1)*dimensions(c,3);

		//end
	}

	//	%Only set the gradient for the priors if in state 2 or 3
	if(state >= 2){
		//	if (state>=2)
		//		grad.pi_x_2=t_ensemble.opt_pi_x_2;
		//grad.b_x_2 =t_ensemble.opt_b_x_2;
		//grad.ba_x_2=t_ensemble.opt_ba_x_2;
		grad->pi_x_2 = (t_ensemble->opt_pi_x_2).clone();
		grad->b_x_2 = (t_ensemble->opt_b_x_2).clone();
		grad->ba_x_2 = (t_ensemble->opt_ba_x_2).clone();
	}
	else{
		//	else
		//		grad.pi_x_2=t_ensemble.pi_x_2;
		//grad.b_x_2 =t_ensemble.b_x_2;
		//grad.ba_x_2=t_ensemble.ba_x_2;
		//end
		grad->pi_x_2 = (t_ensemble->pi_x_2).clone();
		grad->b_x_2 = (t_ensemble->b_x_2).clone();
		grad->ba_x_2 = (t_ensemble->ba_x_2).clone();
	}


	int data_points;
	double rerror;
	Mat dx1(t_ensemble->x1.rows,t_ensemble->x1.cols,CV_64F);
	Mat dx2(t_ensemble->x1.rows,t_ensemble->x1.cols,CV_64F);

	//	%Q(gamma)
	if (P10 == 1)
	{
		tic();

		// train_blind_deconv_test(&dx1,&dx2,&rerror,&data_points,dimensions,*t_ensemble,P1,P2,P3,P4,P5,P6,P7,P8);
		train_blind_deconv_test_cuda_gpu(&dx1,&dx2,&rerror,&data_points,dimensions,*t_ensemble,P1,P2,P3,P4,P5,P6,P7,P8);
 
		toc("\ndeconv",1);

	}
	else{
		//	else
		//		[dx1,dx2,rerror,data_points]=train_blind_deconv2(dimensions,t_ensemble,P1,P2,P3,P4,P5,P6,P7,P8);
		//end
	}

	t_ensemble->b_sigma_2 = t_ensemble->b_sigma + data_points/2;
	//	t_ensemble.b_sigma_2=t_ensemble.b_sigma+data_points/2;
	t_ensemble->opt_ba_sigma_2=t_ensemble->b_sigma_2/(t_ensemble->a_sigma+rerror/2);
	//t_ensemble.opt_ba_sigma_2=t_ensemble.b_sigma_2/(t_ensemble.a_sigma+rerror/2);


	//%Set to the optimum if in the final state
	if (state == 3)
	{
		//	if (state==3)
		//		t_ensemble.ba_sigma_2=t_ensemble.opt_ba_sigma_2;
		//end
		t_ensemble->ba_sigma_2 = t_ensemble->opt_ba_sigma_2;
	}


	//	%Q(x)
	//	grad.x1=grad.x1+t_ensemble.ba_sigma_2*dx1;
	grad->x1 = grad->x1 + t_ensemble->ba_sigma_2*dx1;
	//grad.x2=grad.x2+t_ensemble.ba_sigma_2*dx2;
	grad->x2 = grad->x2 + t_ensemble->ba_sigma_2 * dx2;

	int sum_row0 = 0;
	for (int i=0; i< dimensions.rows;i++)
	{
		sum_row0 += (int) dimensions.at<int>(i,0);
	}
	//%Evaluate the KL divergence
	//	t_ensemble.D_x(sum(dimensions(:,1))+1)=gammaln(t_ensemble.b_sigma)-gammaln(t_ensemble.b_sigma_2)-t_ensemble.b_sigma*log(t_ensemble.a_sigma)+t_ensemble.b_sigma_2*log(t_ensemble.b_sigma_2/t_ensemble.ba_sigma_2)+(t_ensemble.b_sigma_2/t_ensemble.opt_ba_sigma_2-t_ensemble.b_sigma_2/t_ensemble.ba_sigma_2)*t_ensemble.ba_sigma_2+data_points*log(2*pi)/2;
	float temptemp1 = lgamma(t_ensemble->b_sigma); 
	float temptemp2 =	- lgamma(t_ensemble->b_sigma_2); 
	float temptemp3 =	- t_ensemble->b_sigma*log(t_ensemble->a_sigma);
	float temptemp4 =	+ t_ensemble->b_sigma_2*log(t_ensemble->b_sigma_2/t_ensemble->ba_sigma_2); 
	float temptemp5 =	+ (t_ensemble->b_sigma_2/t_ensemble->opt_ba_sigma_2 - t_ensemble->b_sigma_2/t_ensemble->ba_sigma_2)*t_ensemble->ba_sigma_2;
	float temptemp6 =	+ data_points*log(2*PI)/2;

	 
	
	t_ensemble->D_x.at<double>(sum_row0,0) = lgamma(t_ensemble->b_sigma) - lgamma(t_ensemble->b_sigma_2) - t_ensemble->b_sigma*log(t_ensemble->a_sigma)+ t_ensemble->b_sigma_2*log(t_ensemble->b_sigma_2/t_ensemble->ba_sigma_2) + (t_ensemble->b_sigma_2/t_ensemble->opt_ba_sigma_2 - t_ensemble->b_sigma_2/t_ensemble->ba_sigma_2)*t_ensemble->ba_sigma_2 + data_points*log(2*PI)/2;

	 
	//%Normalise from log_e to bits per data point
	//	t_ensemble.D_x=t_ensemble.D_x/data_points*log2(exp(1));
	t_ensemble->D_x = (t_ensemble->D_x/data_points)*(log(exp(1.0))/log(2.0));	// Geethan: log base2 written as log(x)/log(2)
	
	 
	
	if((t_ensemble->D_val != t_ensemble->D_val)){		// Geethan: could not find isnan equivalent used f!=f
		//if (isnan(t_ensemble.D_val))
		//	t_ensemble.D_val=inf;
		//end
		t_ensemble->D_val = numeric_limits<double>::infinity();
	}


	//	%Evaluate the KL divergence
	//	t_ensemble.D_val=sum(t_ensemble.D_x);
	t_ensemble->D_val = sum(t_ensemble->D_x)[0]; 

	//%Find the direction
	//	grad.x1=grad.x1-t_ensemble.x1;
	//grad.x2=grad.x2-t_ensemble.x2;
	//grad.b_x_2=grad.b_x_2-t_ensemble.b_x_2;
	//grad.ba_x_2=grad.ba_x_2-t_ensemble.ba_x_2;
	//grad.pi_x_2=grad.pi_x_2-t_ensemble.pi_x_2;
	
	 
		
	grad->x1 = grad->x1 - (t_ensemble->x1).clone();
	grad->x2 = grad->x2 - (t_ensemble->x2).clone();
	grad->b_x_2 = grad->b_x_2 - (t_ensemble->b_x_2).clone();
	grad->ba_x_2 = grad->ba_x_2 - (t_ensemble->ba_x_2).clone();
	grad->pi_x_2 = grad->pi_x_2 -(t_ensemble->pi_x_2).clone();

 
	  

	//%Don't train classes if options spectify clamping
	//	ptr=0;
	//for c=1:size(dimensions,1)
	//	if (~dimensions(c,6))
	//		%%% Comment out line below & set dimensions(c,6) to 0 for MAP
	//		%%% estimate 
	//		grad.x1(ptr+1:ptr+dimensions(c,1)*dimensions(c,2))=0;
	//grad.x2(ptr+1:ptr+dimensions(c,1)*dimensions(c,2))=0;
	//end
	//	ptr=ptr+dimensions(c,1)*dimensions(c,2);
	//end
		ptr = 0;
		int idx;
		for (int c=0; c < dimensions.rows;c++)
		{
			idx = 0;
			if (dimensions.at<int>(c,5)==0)
			{
				for (int i =0; i<dimensions.at<int>(c,0); i++)
				{
					for (int j=0; j<dimensions.at<int>(c,1);j++)
					{
						grad->x1.at<double>(0,idx+ptr) = 0;
						grad->x2.at<double>(0,idx+ptr) = 0;
						idx++;
					}
				}
			}
			ptr = ptr + dimensions.at<int>(c,0)*dimensions.at<int>(c,1);
		}

	for (int i = 0; i < 542; i++)
		mexPrintf("%f ", grad->x1.at<double>(0,i));

	//mexPrintf("b_sigma_2 : %f\nopt_b_sigma_2:  %f\nD_val %f \n",t_ensemble->b_sigma_2,t_ensemble->opt_ba_sigma_2,t_ensemble->D_val);

	//for (int i = 0; i <4 ;i++){
	//	mexPrintf(" %f", t_ensemble->D_x.at<double>(i,0));
	//	}

	//	%% Image masking to correct for saturation
	//	ptr=0;
	//for c=1:size(dimensions,1)
	//	if (c==3) %%% image only
	//		%grad.x1(ptr+1:ptr+dimensions(c,1)*dimensions(c,2))=grad.x1(ptr+1:ptr+dimensions(c,1)*dimensions(c,2)) .* P12';

	//		%    grad.x2(ptr+1:ptr+dimensions(c,1)*dimensions(c,2))=grad.x2(ptr+1:ptr+dimensions(c,1)*dimensions(c,2)) .* P12';
	//		end   
	//		ptr=ptr+dimensions(c,1)*dimensions(c,2);
	//end
		//toc();
}


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	//tic();

 
	const char *field_names[] = {"x1"               ,
								 "x2"               ,
								 "mx"               ,
								 "mx2"              ,
								 "log_lambda_x"     ,
								 "pi_x"             ,
								 "pi_x_2"           ,
								 "opt_pi_x_2"       ,
								 "a_x"              ,
								 "ba_x_2"           ,
								 "opt_ba_x_2"       ,
								 "b_x"              ,
								 "b_x_2"            ,
								 "opt_b_x_2"        ,
								 "a_sigma"          ,
								 "ba_sigma_2"       ,
								 "opt_ba_sigma_2"   ,
								 "b_sigma"          ,
								 "b_sigma_2"        ,
								 "D_val"            ,
								 "D_x"              
	};
	
	const char *grad_field_name[] = {
								 "x1" ,
								 "x2" ,
								 "pi_x_2",
								 "ba_x_2" ,
								 "b_x_2" 
	};
	
	
	
	mwSize dims[2] = {1, 1 };
	plhs[0] = mxCreateStructArray(2, dims, 21, field_names);
	

	plhs[1] = mxCreateStructArray(2, dims, 5, grad_field_name);
	
	int grad_num_x1               = mxGetFieldNumber(plhs[1],"x1"               );
	int grad_num_x2               = mxGetFieldNumber(plhs[1],"x2"               );
	int grad_num_pi_x_2           = mxGetFieldNumber(plhs[1],"pi_x_2"           );
	int grad_num_ba_x_2           = mxGetFieldNumber(plhs[1],"ba_x_2"           );
	int grad_num_b_x_2            = mxGetFieldNumber(plhs[1],"b_x_2"            );
	
	int num_x1               = mxGetFieldNumber(plhs[0],"x1"               );
	int num_x2               = mxGetFieldNumber(plhs[0],"x2"               );
	int num_mx               = mxGetFieldNumber(plhs[0],"mx"               );
	int num_mx2              = mxGetFieldNumber(plhs[0],"mx2"              );
	int num_log_lambda_x     = mxGetFieldNumber(plhs[0],"log_lambda_x"     );
	int num_pi_x             = mxGetFieldNumber(plhs[0],"pi_x"             );
	int num_pi_x_2           = mxGetFieldNumber(plhs[0],"pi_x_2"           );
	int num_opt_pi_x_2       = mxGetFieldNumber(plhs[0],"opt_pi_x_2"       );
	int num_a_x              = mxGetFieldNumber(plhs[0],"a_x"              );
	int num_ba_x_2           = mxGetFieldNumber(plhs[0],"ba_x_2"           );
	int num_opt_ba_x_2       = mxGetFieldNumber(plhs[0],"opt_ba_x_2"       );
	int num_b_x              = mxGetFieldNumber(plhs[0],"b_x"              );
	int num_b_x_2            = mxGetFieldNumber(plhs[0],"b_x_2"            );
	int num_opt_b_x_2        = mxGetFieldNumber(plhs[0],"opt_b_x_2"        );
	int num_a_sigma          = mxGetFieldNumber(plhs[0],"a_sigma"          );
	int num_ba_sigma_2       = mxGetFieldNumber(plhs[0],"ba_sigma_2"       );
	int num_opt_ba_sigma_2   = mxGetFieldNumber(plhs[0],"opt_ba_sigma_2"   );
	int num_b_sigma          = mxGetFieldNumber(plhs[0],"b_sigma"          );
	int num_b_sigma_2        = mxGetFieldNumber(plhs[0],"b_sigma_2"        );
	int num_D_val            = mxGetFieldNumber(plhs[0],"D_val"            );
	int num_D_x              = mxGetFieldNumber(plhs[0],"D_x"              );
	
	
	// step_len
	mxArray *mx_step_len = mxDuplicateArray(prhs[0]);  
	double step_len = *mxGetPr(mx_step_len);

	//dimensions
	mxArray *mx_dimensions = mxDuplicateArray(prhs[1]);  
	double* dim_array = mxGetPr(mx_dimensions);
	int dim_M = mxGetM(mx_dimensions);
	int dim_N = mxGetN(mx_dimensions);
	int *r = new int[dim_M*dim_N];
	for (int i=0;i<dim_N*dim_M;i++)
	{
		*(r+i) = (int) *(dim_array+i);
	}

	Mat dimensions = Mat(dim_N,dim_M,CV_32S,r);
	dimensions = dimensions.t();



	mxArray *mx_opt_func = mxDuplicateArray(prhs[2]);  
	size_t buflen = 30;
	char *opt_func =(char *)mxMalloc(buflen);	
	buflen = mxGetN(mx_opt_func)*sizeof(mxChar)+1;	 
	mxGetString(mx_opt_func, opt_func, (mwSize)buflen);


	Ensemble ensemble;
	mxArray *mx_ensemble = mxDuplicateArray(prhs[3]);  
	mxArray *mx_en_x1 = mxGetFieldByNumber(mx_ensemble, 0, num_x1);
	double* en_x1 = mxGetPr(mx_en_x1); 
	dim_M = mxGetM(mx_en_x1);
	dim_N = mxGetN(mx_en_x1);
	ensemble.x1 = Mat(dim_M,dim_N,CV_64F,en_x1);

	mxArray *mx_en_x2 = mxGetFieldByNumber(mx_ensemble, 0, num_x2);
	double* en_x2 = mxGetPr(mx_en_x2); 
	dim_M = mxGetM(mx_en_x2);
	dim_N = mxGetN(mx_en_x2);
	ensemble.x2 = Mat(dim_M,dim_N,CV_64F,en_x2);

	
	
	mxArray *mx_en_mx = mxGetFieldByNumber(mx_ensemble, 0, num_mx); 
	double* en_mx= mxGetPr(mx_en_mx); 
	dim_M = mxGetM(mx_en_mx);
	dim_N = mxGetN(mx_en_mx);
	ensemble.mx = Mat(dim_M,dim_N,CV_64F,en_mx);

	mxArray *mx_en_mx2 = mxGetFieldByNumber(mx_ensemble, 0, num_mx2);
	double* en_mx2 = mxGetPr(mx_en_mx2); 
	dim_M = mxGetM(mx_en_mx2);
	dim_N = mxGetN(mx_en_mx2);
	ensemble.mx2 = Mat(dim_M,dim_N,CV_64F,en_mx2);

	mxArray *mx_en_log_lambda_x = mxGetFieldByNumber(mx_ensemble, 0, num_log_lambda_x);
	double* en_log_lambda = mxGetPr(mx_en_log_lambda_x); 
	dim_M = mxGetM(mx_en_log_lambda_x);
	dim_N = mxGetN(mx_en_log_lambda_x);
	ensemble.log_lambda_x = Mat(dim_M,dim_N,CV_64F,en_log_lambda);

	mxArray *mx_en_pi_x_2 = mxGetFieldByNumber(mx_ensemble, 0, num_pi_x_2);
	double* en_pi_x_2 = mxGetPr(mx_en_pi_x_2); 
	dim_M = mxGetM(mx_en_pi_x_2);
	dim_N = mxGetN(mx_en_pi_x_2);
	ensemble.pi_x_2 = Mat(dim_M,dim_N,CV_64F,en_pi_x_2);

	mxArray *mx_en_opt_pi_x_2 = mxGetFieldByNumber(mx_ensemble, 0, num_opt_pi_x_2);
	double* en_opt_pi_x_2 = mxGetPr(mx_en_opt_pi_x_2); 
	dim_M = mxGetM(mx_en_opt_pi_x_2);
	dim_N = mxGetN(mx_en_opt_pi_x_2);
	ensemble.opt_pi_x_2 = Mat(dim_M,dim_N,CV_64F,en_opt_pi_x_2);

	mxArray *mx_en_ba_x_2 = mxGetFieldByNumber(mx_ensemble, 0, num_ba_x_2);
	double* en_ba_x_2 = mxGetPr(mx_en_ba_x_2); 
	dim_M = mxGetM(mx_en_ba_x_2);
	dim_N = mxGetN(mx_en_ba_x_2);
	ensemble.ba_x_2 = Mat(dim_M,dim_N,CV_64F,en_ba_x_2);

	mxArray *mx_en_opt_ba_x_2 = mxGetFieldByNumber(mx_ensemble, 0, num_opt_ba_x_2);
	double* en_opt_ba_x_2 = mxGetPr(mx_en_opt_ba_x_2); 
	dim_M = mxGetM(mx_en_opt_ba_x_2);
	dim_N = mxGetN(mx_en_opt_ba_x_2);
	ensemble.opt_ba_x_2 = Mat(dim_M,dim_N,CV_64F,en_opt_ba_x_2);

	mxArray *mx_en_b_x_2 = mxGetFieldByNumber(mx_ensemble, 0, num_b_x_2);
	double* en_b_x_2 = mxGetPr(mx_en_b_x_2); 
	dim_M = mxGetM(mx_en_b_x_2);
	dim_N = mxGetN(mx_en_b_x_2);
	ensemble.b_x_2 = Mat(dim_M,dim_N,CV_64F,en_b_x_2);

	mxArray *mx_en_opt_b_x_2 = mxGetFieldByNumber(mx_ensemble, 0, num_opt_b_x_2);
	double* en_opt_b_x_2 = mxGetPr(mx_en_opt_b_x_2); 
	dim_M = mxGetM(mx_en_opt_b_x_2);
	dim_N = mxGetN(mx_en_opt_b_x_2);
	ensemble.opt_b_x_2 = Mat(dim_M,dim_N,CV_64F,en_opt_b_x_2);

	mxArray *mx_en_D_x = mxGetFieldByNumber(mx_ensemble, 0, num_D_x);
	double* en_D_x = mxGetPr(mx_en_D_x); 
	dim_M = mxGetM(mx_en_D_x);
	dim_N = mxGetN(mx_en_D_x);
	ensemble.D_x = Mat(dim_M,dim_N,CV_64F,en_D_x);

	mxArray *mx_en_pi_x = mxGetFieldByNumber(mx_ensemble, 0, num_pi_x);
	double* en_pi_x = mxGetPr(mx_en_pi_x); 
	ensemble.pi_x = *en_pi_x;   

	mxArray *mx_en_a_x = mxGetFieldByNumber(mx_ensemble, 0, num_a_x);
	double* en_a_x = mxGetPr(mx_en_a_x); 
	ensemble.a_x = *en_a_x;   

	mxArray *mx_en_b_x = mxGetFieldByNumber(mx_ensemble, 0, num_b_x);
	double* en_b_x = mxGetPr(mx_en_b_x); 
	ensemble.b_x = *en_b_x;   

	mxArray *mx_en_a_sigma = mxGetFieldByNumber(mx_ensemble, 0, num_a_sigma);
	double* en_a_sigma = mxGetPr(mx_en_a_sigma); 
	ensemble.a_sigma = *en_a_sigma;   

	mxArray *mx_en_ba_sigma_2 = mxGetFieldByNumber(mx_ensemble, 0, num_ba_sigma_2);
	double* en_ba_sigma_2 = mxGetPr(mx_en_ba_sigma_2); 
	ensemble.ba_sigma_2 = *en_ba_sigma_2;   

	mxArray *mx_en_opt_ba_sigma_2 = mxGetFieldByNumber(mx_ensemble, 0, num_opt_ba_sigma_2);
	double* en_opt_ba_sigma_2 = mxGetPr(mx_en_opt_ba_sigma_2); 
	ensemble.opt_ba_sigma_2 = *en_opt_ba_sigma_2;   

	mxArray *mx_en_b_sigma = mxGetFieldByNumber(mx_ensemble, 0, num_b_sigma);
	double* en_b_sigma = mxGetPr(mx_en_b_sigma); 
	ensemble.b_sigma = *en_b_sigma;   

	mxArray *mx_en_b_sigma_2 = mxGetFieldByNumber(mx_ensemble, 0, num_b_sigma_2);
	double* en_b_sigma_2 = mxGetPr(mx_en_b_sigma_2); 
	ensemble.b_sigma_2 = *en_b_sigma_2;   

	mxArray *mx_en_D_val = mxGetFieldByNumber(mx_ensemble, 0, num_D_val);
	double* en_D_val = mxGetPr(mx_en_D_val); 
	ensemble.D_val = *en_D_val;   

	Direction direction;
	mxArray *mx_direction = mxDuplicateArray(prhs[4]);

	mxArray *mx_di_x1 = mxGetField(mx_direction, 0, "x1");
	double* di_x1 = mxGetPr(mx_di_x1); 
	dim_M = mxGetM(mx_di_x1);
	dim_N = mxGetN(mx_di_x1);
	direction.x1 = Mat(dim_M,dim_N,CV_64F,di_x1);

	mxArray *mx_di_x2 = mxGetField(mx_direction, 0, "x2");
	double* di_x2 = mxGetPr(mx_di_x2); 
	dim_M = mxGetM(mx_di_x2);
	dim_N = mxGetN(mx_di_x2);
	direction.x2 = Mat(dim_M,dim_N,CV_64F,di_x2);

	mxArray *mx_di_pi_x_2 = mxGetField(mx_direction, 0, "pi_x_2");
	double* di_pi_x_2 = mxGetPr(mx_di_pi_x_2); 
	dim_M = mxGetM(mx_di_pi_x_2);
	dim_N = mxGetN(mx_di_pi_x_2);
	direction.pi_x_2 = Mat(dim_M,dim_N,CV_64F,di_pi_x_2);

	mxArray *mx_di_ba_x_2 = mxGetField(mx_direction, 0, "ba_x_2");
	double* di_ba_x_2 = mxGetPr(mx_di_ba_x_2); 
	dim_M = mxGetM(mx_di_ba_x_2);
	dim_N = mxGetN(mx_di_ba_x_2);
	direction.ba_x_2 = Mat(dim_M,dim_N,CV_64F,di_ba_x_2);

	mxArray *mx_di_b_x_2 = mxGetField(mx_direction, 0, "b_x_2");
	double* di_b_x_2 = mxGetPr(mx_di_b_x_2); 
	dim_M = mxGetM(mx_di_b_x_2);
	dim_N = mxGetN(mx_di_b_x_2);
	direction.b_x_2 = Mat(dim_M,dim_N,CV_64F,di_b_x_2);



	// state
	mxArray *mx_state = mxDuplicateArray(prhs[5]);  
	int state = (int)(*mxGetPr(mx_state));

	mxArray *mx_P1 = mxDuplicateArray(prhs[6]);  
	double* P1_array = mxGetPr(mx_P1);
	int P1_M = (int)mxGetM(mx_P1);
	int P1_N = (int)mxGetN(mx_P1);
	Mat P1 = Mat(P1_N,P1_M,CV_64F,P1_array).t();

	mxArray *mx_P2 = mxDuplicateArray(prhs[7]);  
	double* P2_array = mxGetPr(mx_P2);
	int P2_M = (int)mxGetM(mx_P2);
	int P2_N = (int)mxGetN(mx_P2);
	Mat P2 = Mat(P2_N,P2_M,CV_64F,P2_array).t();

	mxArray *mx_P11 = mxDuplicateArray(prhs[16]);  
	double* P11_array = mxGetPr(mx_P11);
	int P11_M = (int)mxGetM(mx_P11);
	int P11_N = (int)mxGetN(mx_P11);
	Mat P11 = Mat(P11_N,P11_M,CV_64F,P11_array).t();

	mxArray *mx_P12 = mxDuplicateArray(prhs[17]);  
	double* P12_array = mxGetPr(mx_P12);
	int P12_M = (int)mxGetM(mx_P12);
	int P12_N = (int)mxGetN(mx_P12);
	Mat P12 = Mat(P12_N,P12_M,CV_64F,P12_array).t();

	mxArray *mx_P3 = mxDuplicateArray(prhs[8]);  
	int P3 = (int)(*mxGetPr(mx_P3));

	mxArray *mx_P4 = mxDuplicateArray(prhs[9]);  
	int P4 = (int)(*mxGetPr(mx_P4));

	mxArray *mx_P5 = mxDuplicateArray(prhs[10]);  
	int P5 = (int)(*mxGetPr(mx_P5));

	mxArray *mx_P6 = mxDuplicateArray(prhs[11]);  
	int P6 = (int)(*mxGetPr(mx_P6));

	mxArray *mx_P7 = mxDuplicateArray(prhs[12]);  
	int P7 = (int)(*mxGetPr(mx_P7));

	mxArray *mx_P8 = mxDuplicateArray(prhs[13]);  
	int P8 = (int)(*mxGetPr(mx_P8));

	mxArray *mx_P10 = mxDuplicateArray(prhs[15]);  
	int P10 = (int)(*mxGetPr(mx_P10));

	Priors P9;
	mxArray *mx_priors = mxDuplicateArray(prhs[14]);

	mxArray *mx_p9_pi = mxGetField(mx_priors, 0, "pi");
	double* p9_pi = mxGetPr(mx_p9_pi); 
	dim_M = mxGetM(mx_p9_pi);
	dim_N = mxGetN(mx_p9_pi);
	P9.pi = Mat(dim_M,dim_N,CV_64F,p9_pi);

	mxArray *mx_p9_gam = mxGetField(mx_priors, 0, "gamma");
	double* p9_gam = mxGetPr(mx_p9_gam); 
	dim_M = mxGetM(mx_p9_gam);
	dim_N = mxGetN(mx_p9_gam);
	P9.gamma = Mat(dim_M,dim_N,CV_64F,p9_gam);

	Direction grad;
	 
// Checking 'ensemble' Mat variables ---- print the matlab syntax
// Mat x1,x2,mx,mx2, log_lambda_x,pi_x_2,opt_pi_x_2,ba_x_2,opt_ba_x_2,b_x_2,opt_b_x_2,D_x;
//	mexPrintf("ensemble_x1 = [");
//	for (int i = 0; i < ensemble.x1.cols ; i++)
//		mexPrintf("%f ", ensemble.x1.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("ensemble_x2 = [");
//	for (int i = 0; i < ensemble.x2.cols ; i++)
//		mexPrintf("%f ", ensemble.x2.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("ensemble_mx = [");
//	for (int i = 0; i < ensemble.mx.cols ; i++)
//		mexPrintf("%f ", ensemble.mx.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("ensemble_mx2 = [");
//	for (int i = 0; i < ensemble.mx2.cols ; i++)
//		mexPrintf("%f ", ensemble.mx2.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("ensemble_log_lambda_x = [");
//	for (int i = 0; i < ensemble.log_lambda_x.cols ; i++)
//		mexPrintf("%f ", ensemble.log_lambda_x.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("ensemble_pi_x_2 = [");
//	for (int i = 0; i < ensemble.pi_x_2.cols ; i++)
//		mexPrintf("%f ", ensemble.pi_x_2.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("ensemble_opt_pi_x_2 = [");
//	for (int i = 0; i < ensemble.opt_pi_x_2.cols ; i++)
//		mexPrintf("%f ", ensemble.opt_pi_x_2.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("ensemble_ba_x_2 = [");
//	for (int i = 0; i < ensemble.ba_x_2.cols ; i++)
//		mexPrintf("%f ", ensemble.ba_x_2.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("ensemble_opt_ba_x_2 = [");
//	for (int i = 0; i < ensemble.opt_ba_x_2.cols ; i++)
//		mexPrintf("%f ", ensemble.opt_ba_x_2.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("ensemble_b_x_2 = [");
//	for (int i = 0; i < ensemble.b_x_2.cols ; i++)
//		mexPrintf("%f ", ensemble.b_x_2.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("ensemble_opt_b_x_2 = [");
//	for (int i = 0; i < ensemble.opt_b_x_2.cols ; i++)
//		mexPrintf("%f ", ensemble.opt_b_x_2.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("ensemble_D_x = [");
//	for (int i = 0; i < ensemble.D_x.rows ; i++)
//		mexPrintf("%f ", ensemble.D_x.at<double>(0,i));
//	mexPrintf("];\n");
//
//// Checking 'ensemble' double values ---- print the matlab syntax
//	
//	
//	mexPrintf("pi_x = %f; a_x = %f; b_x = %f; a_sigma  = %f; ba_sigma_2  = %f; opt_ba_sigma_2  = %f; b_sigma = %f; b_sigma_2 = %f; D_val = %f;\n",ensemble.pi_x, ensemble.a_x, ensemble.b_x, ensemble.a_sigma , ensemble.ba_sigma_2 , ensemble.opt_ba_sigma_2 , ensemble.b_sigma, ensemble.b_sigma_2, ensemble.D_val); 	
//	
//	
////  Checking 'grad' values --- print the matlab Syntax
//	//mexPrintf("rows %d cols %d",grad.x1.rows, grad.x1.cols);
//	mexPrintf("grad_x1 = [");
//	for (int i = 0; i < grad.x1.cols ; i++)
//		mexPrintf("%f ", grad.x1.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("grad_x2 = [");
//	double a = 1234;
//	for (int i = 0; i < grad.x2.cols ; i++) 
//		mexPrintf("%f ", grad.x2.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("grad_pi_x_2 = [");
//	for (int i = 0; i < grad.pi_x_2.cols ; i++)		   
//		mexPrintf("%f ", grad.pi_x_2.at<double>(0,i));		
//	mexPrintf("];\n");
//
//	mexPrintf("grad_ba_x_2 = [");
//	for (int i = 0; i < grad.ba_x_2.cols ; i++)
//		mexPrintf("%f ", grad.ba_x_2.at<double>(0,i));
//	mexPrintf("];\n");
//
//	mexPrintf("grad_b_x_2 = [");
//	for (int i = 0; i < grad.b_x_2.cols ; i++)
//		mexPrintf("%f ", grad.b_x_2.at<double>(0,i));
//	mexPrintf("];\n");


	//vector<Mat> Hx_mx_mx2 = train_ensemble_rectified5_test_u(x1,x2,type);

	Ensemble t_ensemble;

	train_ensemble_evidence6_test(&t_ensemble,ensemble,&grad,step_len,dimensions,opt_func,direction,state,P1,P2,P3,P4,P5,P6,P7,P8,P9,P10,P11,P12);

 
	delete [] r;
	mxArray* mx_t_a_sigma       ;
	mxArray* mx_t_a_x           ;
	mxArray* mx_t_b_sigma       ;
	mxArray* mx_t_b_sigma_2     ;
	mxArray* mx_t_b_x           ;
	mxArray* mx_t_b_x_2         ;
	mxArray* mx_t_ba_sigma_2    ;
	mxArray* mx_t_ba_x_2        ;
	mxArray* mx_t_D_val         ;
	mxArray* mx_t_D_x           ;
	mxArray* mx_t_log_lambda_x  ;
	mxArray* mx_t_mx            ;
	mxArray* mx_t_mx2           ;
	mxArray* mx_t_opt_b_x_2     ;
	mxArray* mx_t_opt_ba_sigma_2;
	mxArray* mx_t_opt_ba_x_2    ;
	mxArray* mx_t_opt_pi_x_2    ;
	mxArray* mx_t_pi_x          ;
	mxArray* mx_t_pi_x_2        ;
	mxArray* mx_t_x1            ;
	mxArray* mx_t_x2            ;
	
	
	mxArray* mx_grad_x1            ;
	mxArray* mx_grad_x2            ;
	mxArray* mx_grad_b_x_2         ;
	mxArray* mx_grad_pi_x_2        ;
	mxArray* mx_grad_ba_x_2        ;

	mx_grad_x1       = mxCreateDoubleMatrix(grad.x1.size().height       ,grad.x1.size().width       ,mxREAL);
	mx_grad_x2       = mxCreateDoubleMatrix(grad.x2.size().height       ,grad.x2.size().width       ,mxREAL);
	mx_grad_b_x_2    = mxCreateDoubleMatrix(grad.b_x_2.size().height    ,grad.b_x_2.size().width    ,mxREAL);
	mx_grad_pi_x_2   = mxCreateDoubleMatrix(grad.pi_x_2.size().height   ,grad.pi_x_2.size().width   ,mxREAL);
	mx_grad_ba_x_2   = mxCreateDoubleMatrix(grad.ba_x_2.size().height   ,grad.ba_x_2.size().width   ,mxREAL);
	
	
	mx_t_a_sigma       = mxCreateDoubleMatrix(1,1,mxREAL);
	mx_t_a_x           = mxCreateDoubleMatrix(1,1,mxREAL);
	mx_t_b_sigma       = mxCreateDoubleMatrix(1,1,mxREAL);
	mx_t_b_sigma_2     = mxCreateDoubleMatrix(1,1,mxREAL);
	mx_t_b_x           = mxCreateDoubleMatrix(1,1,mxREAL);
	mx_t_b_x_2         = mxCreateDoubleMatrix(ensemble.b_x_2.rows,ensemble.b_x_2.cols,mxREAL);
	mx_t_ba_sigma_2    = mxCreateDoubleMatrix(1,1,mxREAL);
	mx_t_ba_x_2        = mxCreateDoubleMatrix(ensemble.ba_x_2.rows,ensemble.ba_x_2.cols,mxREAL);
	mx_t_D_val         = mxCreateDoubleMatrix(1,1,mxREAL);
	mx_t_D_x           = mxCreateDoubleMatrix(ensemble.D_x.rows,ensemble.D_x.cols,mxREAL);
	mx_t_log_lambda_x  = mxCreateDoubleMatrix(ensemble.log_lambda_x.rows,ensemble.log_lambda_x.cols,mxREAL);
	mx_t_mx            = mxCreateDoubleMatrix(ensemble.mx.rows,ensemble.mx.cols,mxREAL);
	mx_t_mx2           = mxCreateDoubleMatrix(ensemble.mx2.rows,ensemble.mx2.cols,mxREAL);
	mx_t_opt_b_x_2     = mxCreateDoubleMatrix(ensemble.opt_b_x_2.rows,ensemble.opt_b_x_2.cols,mxREAL);
	mx_t_opt_ba_sigma_2= mxCreateDoubleMatrix(1,1,mxREAL);
	mx_t_opt_ba_x_2    = mxCreateDoubleMatrix(ensemble.opt_ba_x_2.rows,ensemble.opt_ba_x_2.cols,mxREAL);
	mx_t_opt_pi_x_2    = mxCreateDoubleMatrix(ensemble.opt_pi_x_2.rows,ensemble.opt_pi_x_2.cols,mxREAL);
	mx_t_pi_x          = mxCreateDoubleMatrix(1,1,mxREAL);
	mx_t_pi_x_2        = mxCreateDoubleMatrix(ensemble.pi_x_2.rows,ensemble.pi_x_2.cols,mxREAL);
	mx_t_x1            = mxCreateDoubleMatrix(ensemble.x1.rows,ensemble.x1.cols,mxREAL);
	mx_t_x2            = mxCreateDoubleMatrix(ensemble.x2.rows,ensemble.x2.cols,mxREAL);

	mxSetFieldByNumber(plhs[0],0,num_a_sigma        ,mx_t_a_sigma        );
	mxSetFieldByNumber(plhs[0],0,num_a_x            ,mx_t_a_x            );
	mxSetFieldByNumber(plhs[0],0,num_b_sigma        ,mx_t_b_sigma        );
	mxSetFieldByNumber(plhs[0],0,num_b_sigma_2      ,mx_t_b_sigma_2      );
	mxSetFieldByNumber(plhs[0],0,num_b_x            ,mx_t_b_x            );
	mxSetFieldByNumber(plhs[0],0,num_b_x_2          ,mx_t_b_x_2          );
	mxSetFieldByNumber(plhs[0],0,num_ba_sigma_2     ,mx_t_ba_sigma_2     );
	mxSetFieldByNumber(plhs[0],0,num_ba_x_2         ,mx_t_ba_x_2         );
	mxSetFieldByNumber(plhs[0],0,num_D_val          ,mx_t_D_val          );
	mxSetFieldByNumber(plhs[0],0,num_D_x            ,mx_t_D_x            );
	mxSetFieldByNumber(plhs[0],0,num_log_lambda_x   ,mx_t_log_lambda_x   );
	mxSetFieldByNumber(plhs[0],0,num_mx             ,mx_t_mx             );
	mxSetFieldByNumber(plhs[0],0,num_mx2            ,mx_t_mx2            );
	mxSetFieldByNumber(plhs[0],0,num_opt_b_x_2      ,mx_t_opt_b_x_2      );
	mxSetFieldByNumber(plhs[0],0,num_opt_ba_sigma_2 ,mx_t_opt_ba_sigma_2 );
	mxSetFieldByNumber(plhs[0],0,num_opt_ba_x_2     ,mx_t_opt_ba_x_2     );
	mxSetFieldByNumber(plhs[0],0,num_opt_pi_x_2     ,mx_t_opt_pi_x_2     );
	mxSetFieldByNumber(plhs[0],0,num_pi_x           ,mx_t_pi_x           );
	mxSetFieldByNumber(plhs[0],0,num_pi_x_2         ,mx_t_pi_x_2         );
	mxSetFieldByNumber(plhs[0],0,num_x1             ,mx_t_x1             );
	mxSetFieldByNumber(plhs[0],0,num_x2             ,mx_t_x2             );
	

	mxSetFieldByNumber(plhs[1],0,grad_num_x1          ,mx_grad_x1          );
	mxSetFieldByNumber(plhs[1],0,grad_num_x2          ,mx_grad_x2          );
	mxSetFieldByNumber(plhs[1],0,grad_num_b_x_2       ,mx_grad_b_x_2       );
	mxSetFieldByNumber(plhs[1],0,grad_num_pi_x_2      ,mx_grad_pi_x_2      );
	mxSetFieldByNumber(plhs[1],0,grad_num_ba_x_2      ,mx_grad_ba_x_2      );
	
	*mxGetPr(mx_t_a_sigma       )= t_ensemble.a_sigma       ;
	*mxGetPr(mx_t_a_x           )= t_ensemble.a_x           ;
	*mxGetPr(mx_t_b_sigma       )= t_ensemble.b_sigma       ;
	*mxGetPr(mx_t_b_sigma_2     )= t_ensemble.b_sigma_2     ;
	*mxGetPr(mx_t_b_x           )= t_ensemble.b_x           ;

	memcpy(mxGetPr(mx_t_b_x_2         ), (double*)(t_ensemble.b_x_2.data), sizeof(double) * t_ensemble.b_x_2.size().height*t_ensemble.b_x_2.size().width);
	memcpy(mxGetPr(mx_t_ba_x_2        ),(double*)t_ensemble.ba_x_2.data,  sizeof(double) * t_ensemble.ba_x_2.size().height*t_ensemble.ba_x_2.size().width);
	memcpy(mxGetPr(mx_t_D_x           ),(double*)t_ensemble.D_x.data ,  sizeof(double) * t_ensemble.D_x.size().height*t_ensemble.D_x.size().width);
	memcpy(mxGetPr(mx_t_log_lambda_x  ),(double*)t_ensemble.log_lambda_x.data,sizeof(double)* t_ensemble.log_lambda_x.size().height*t_ensemble.log_lambda_x.size().width);
	memcpy(mxGetPr(mx_t_mx            ),(double*)t_ensemble.mx.data          ,sizeof(double)* t_ensemble.mx.size().height*t_ensemble.mx.size().width);
	memcpy(mxGetPr(mx_t_mx2           ),(double*)t_ensemble.mx2.data         ,sizeof(double)* t_ensemble.mx2.size().height*t_ensemble.mx2.size().width);
	memcpy(mxGetPr(mx_t_opt_b_x_2     ),(double*)t_ensemble.opt_b_x_2.data   ,sizeof(double)* t_ensemble.opt_b_x_2.size().height*t_ensemble.opt_b_x_2.size().width);
	memcpy(mxGetPr(mx_t_opt_ba_x_2    ),(double*)t_ensemble.opt_ba_x_2.data  ,sizeof(double)* t_ensemble.opt_ba_x_2.size().height*t_ensemble.opt_ba_x_2.size().width);
	memcpy(mxGetPr(mx_t_opt_pi_x_2    ),(double*)t_ensemble.opt_pi_x_2.data  ,sizeof(double)* t_ensemble.opt_pi_x_2.size().height*t_ensemble.opt_pi_x_2.size().width);
	memcpy(mxGetPr(mx_t_pi_x_2        ),(double*)t_ensemble.pi_x_2.data      ,sizeof(double)* t_ensemble.pi_x_2.size().height*t_ensemble.pi_x_2.size().width);
	memcpy(mxGetPr(mx_t_x1            ),(double*)t_ensemble.x1.data          ,sizeof(double)* t_ensemble.x1.size().height*t_ensemble.x1.size().width);
	memcpy(mxGetPr(mx_t_x2            ),(double*)t_ensemble.x2.data          ,sizeof(double)* t_ensemble.x2.size().height*t_ensemble.x2.size().width);


	*mxGetPr(mx_t_ba_sigma_2    )= t_ensemble.ba_sigma_2    ;
	*mxGetPr(mx_t_D_val         )= t_ensemble.D_val         ;
	*mxGetPr(mx_t_opt_ba_sigma_2)= t_ensemble.opt_ba_sigma_2;
	*mxGetPr(mx_t_pi_x          )= t_ensemble.pi_x          ;
	
	

	memcpy(mxGetPr(mx_grad_x1              ),(double*)grad.x1.data          ,sizeof(double)* grad.x1.size().height*grad.x1.size().width);
	memcpy(mxGetPr(mx_grad_x2              ),(double*)grad.x2.data          ,sizeof(double)* grad.x2.size().height*grad.x2.size().width);
	memcpy(mxGetPr(mx_grad_b_x_2           ),(double*)grad.b_x_2.data          ,sizeof(double)* grad.b_x_2.size().height*grad.b_x_2.size().width);
	memcpy(mxGetPr(mx_grad_pi_x_2          ),(double*)grad.pi_x_2.data          ,sizeof(double)* grad.pi_x_2.size().height*grad.pi_x_2.size().width);
	memcpy(mxGetPr(mx_grad_ba_x_2          ),(double*)grad.ba_x_2.data          ,sizeof(double)* grad.ba_x_2.size().height*grad.ba_x_2.size().width);
	
	
	
	
	
	
	
	//
	//
	//
	//// Checking 't_ensemble' Mat variables ---- print the matlab syntax
	//// Mat x1,x2,mx,mx2, log_lambda_x,pi_x_2,opt_pi_x_2,ba_x_2,opt_ba_x_2,b_x_2,opt_b_x_2,D_x;
	//mexPrintf("ensemble_x1 = [");
	//for (int i = 0; i < t_ensemble.x1.cols ; i++)
	//	mexPrintf("%f ", t_ensemble.x1.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("ensemble_x2 = [");
	//for (int i = 0; i < t_ensemble.x2.cols ; i++)
	//	mexPrintf("%f ", t_ensemble.x2.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("ensemble_mx = [");
	//for (int i = 0; i < t_ensemble.mx.cols ; i++)
	//	mexPrintf("%f ", t_ensemble.mx.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("ensemble_mx2 = [");
	//for (int i = 0; i < t_ensemble.mx2.cols ; i++)
	//	mexPrintf("%f ", t_ensemble.mx2.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("ensemble_log_lambda_x = [");
	//for (int i = 0; i < t_ensemble.log_lambda_x.cols ; i++)
	//	mexPrintf("%f ", t_ensemble.log_lambda_x.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("ensemble_pi_x_2 = [");
	//for (int i = 0; i < t_ensemble.pi_x_2.cols ; i++)
	//	mexPrintf("%f ", t_ensemble.pi_x_2.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("ensemble_opt_pi_x_2 = [");
	//for (int i = 0; i < t_ensemble.opt_pi_x_2.cols ; i++)
	//	mexPrintf("%f ", t_ensemble.opt_pi_x_2.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("ensemble_ba_x_2 = [");
	//for (int i = 0; i < t_ensemble.ba_x_2.cols ; i++)
	//	mexPrintf("%f ", t_ensemble.ba_x_2.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("ensemble_opt_ba_x_2 = [");
	//for (int i = 0; i < t_ensemble.opt_ba_x_2.cols ; i++)
	//	mexPrintf("%f ", t_ensemble.opt_ba_x_2.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("ensemble_b_x_2 = [");
	//for (int i = 0; i < t_ensemble.b_x_2.cols ; i++)
	//	mexPrintf("%f ", t_ensemble.b_x_2.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("ensemble_opt_b_x_2 = [");
	//for (int i = 0; i < t_ensemble.opt_b_x_2.cols ; i++)
	//	mexPrintf("%f ", t_ensemble.opt_b_x_2.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("ensemble_D_x = [");
	//for (int i = 0; i < t_ensemble.D_x.rows ; i++)
	//	mexPrintf("%f ", t_ensemble.D_x.at<double>(0,i));
	//mexPrintf("];\n");

	//// Checking 't_ensemble' double values ---- print the matlab syntax
	//mexPrintf("pi_x = %f; a_x = %f; b_x = %f; a_sigma  = %f; ba_sigma_2  = %f; opt_ba_sigma_2  = %f; b_sigma = %f; b_sigma_2 = %f; D_val = %f;\n",t_ensemble.pi_x, t_ensemble.a_x, t_ensemble.b_x, t_ensemble.a_sigma , t_ensemble.ba_sigma_2 , t_ensemble.opt_ba_sigma_2 , t_ensemble.b_sigma, t_ensemble.b_sigma_2, t_ensemble.D_val); 	

	////  Checking 'grad' values --- print the matlab Syntax
	////mexPrintf("rows %d cols %d",grad.x1.rows, grad.x1.cols);
	//mexPrintf("grad_x1 = [");
	//for (int i = 0; i < grad.x1.cols ; i++)
	//	mexPrintf("%f ", grad.x1.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("grad_x2 = [");
	//double a = 1234;
	//for (int i = 0; i < grad.x2.cols ; i++) 
	//	mexPrintf("%f ", grad.x2.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("grad_pi_x_2 = [");
	//for (int i = 0; i < grad.pi_x_2.cols ; i++)		   
	//	mexPrintf("%f ", grad.pi_x_2.at<double>(0,i));		
	//mexPrintf("];\n");

	//mexPrintf("grad_ba_x_2 = [");
	//for (int i = 0; i < grad.ba_x_2.cols ; i++)
	//	mexPrintf("%f ", grad.ba_x_2.at<double>(0,i));
	//mexPrintf("];\n");

	//mexPrintf("grad_b_x_2 = [");
	//for (int i = 0; i < grad.b_x_2.cols ; i++)
	//	mexPrintf("%f ", grad.b_x_2.at<double>(0,i));
	//mexPrintf("];\n");


	//vector<Mat> Hx_mx_mx2 = train_t_ensemble_rectified5_test_u(x1,x2,type);
	//toc(); 

}
