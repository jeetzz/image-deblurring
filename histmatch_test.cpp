#include <mex.h> 
#include "MxArray.hpp"
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;
using namespace cv;


void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    
	MxArray in_array(prhs[0]);
	MxArray reference_array(prhs[1]);
	Mat in,reference,gray_in,gray_reference,j;

	in = in_array.toMat(CV_8U,true);
	reference = reference_array.toMat(CV_8U,true);

	cvtColor(in, gray_in, CV_RGB2GRAY);										
	cvtColor(reference, gray_reference, CV_RGB2GRAY);	

	// When doing equalization on color images, we have to convert it to ycrcb and do it in intensity plain only.
	// otherwise things go wrong pretty badly...
	Mat ycrcb;

	cvtColor(in,ycrcb,CV_BGR2YCrCb);

	vector<Mat> channels;
	split(ycrcb,channels);

	equalizeHist(channels[0], channels[0]);  // equalize...

	
	merge(channels,ycrcb); // merge them back to the mat file

	Mat result;
	cvtColor(ycrcb,result,CV_YCrCb2BGR);  // convert to RGB
 	
	plhs[0] = MxArray(result);	
	//plhs[1] = MxArray(g);	
	//plhs[2] = MxArray(b);	
	

	//// let's quantize the hue to 30 levels
	//// and the saturation to 32 levels
	//int hbins = 30, sbins = 32;
	//int histSize[] = {hbins, sbins};
	//// hue varies from 0 to 179, see cvtColor
	//float hranges[] = { 0, 180 };
	//// saturation varies from 0 (black-gray-white) to
	//// 255 (pure spectrum color)
	//float sranges[] = { 0, 256 };
	//const float* ranges[] = { hranges, sranges };
	//MatND hist;
	//// we compute the histogram from the 0-th and 1-st channels
	//int channels[] = {0, 1};

	//calcHist( &gray_reference, 1, channels, Mat(), hist, 2, histSize, ranges,true, false );

	// 
	//double maxVal=0;
	//minMaxLoc(hist, 0, &maxVal, 0, 0);

	//int scale = 10;
	//Mat histImg = Mat::zeros(sbins*scale, hbins*10, CV_8UC3);

	//for( int h = 0; h < hbins; h++ )
	//	for( int s = 0; s < sbins; s++ )
	//	{
	//		float binVal = hist.at<float>(h, s);
	//		int intensity = cvRound(binVal*255/maxVal);
	//		cvRectangle( &histImg, Point(h*scale, s*scale), Point( (h+1)*scale - 1, (s+1)*scale - 1), Scalar::all(intensity), CV_FILLED );
	//	}

	//namedWindow( "Source", 1 );
	//imshow( "Source", gray_reference );
	//waitKey(1);

	//namedWindow( "H-S Histogram", 1 );
	//imshow( "H-S Histogram", histImg );
	//waitKey(1);

}