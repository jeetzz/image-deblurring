

void train_blind_deconv_test(Mat* dx1, Mat* dx2, double* rerror, int* datapoints,Mat dimensions,Ensemble ensemble,Mat D,Mat Dp,int I,int J,int K,int L,int M,int N){
		
	//[dx1,dx2,rerror,data_points]=train_blind_deconv(dimensions,ensemble,P1,P2,P3,P4,P5,P6,P7,P8);
	//mexprintf("aawa\n");

	//nPlanes = dimensions(3,1);
	//%=========================================================================
	//	% Copy the expectations
	//	%=========================================================================
	Mat* mmu;
	train_ensemble_get_test(&mmu,0,dimensions,ensemble.mx); //mmu  = train_ensemble_get_test(1,dimensions,ensemble.mx);
	Mat* me_temp;
	train_ensemble_get_test(&me_temp,1,dimensions,ensemble.mx);//me   = reshape(train_ensemble_get_test(2,dimensions,ensemble.mx),K,L);
	Mat me(K,L,CV_64F);	
	//Mat me = me_temp->reshape(K,L,CV_64F);
	for (int i = 0; i < K; i++){
		for (int j = 0; j < L; j++){
			me.at<double>(i,j) = (*me_temp).at<double>(0,K*j + i);
		}
		 
	}
	me_temp->release();
	Mat temptemp3 = ensemble.mx2;
	Mat* mx_temp;
	train_ensemble_get_test(&mx_temp,2,dimensions,ensemble.mx);//mx   = reshape(train_ensemble_get_test(3,dimensions,ensemble.mx),M,N);
	//Mat mx = mx_temp->reshape(M,N);
	Mat mx(M,N,CV_64F);		 
	for (int i = 0; i < M; i++){
		for (int j = 0; j < N; j++){
			mx.at<double>(i,j) = (*mx_temp).at<double>(0,M*j+i);
		}
	}
	mx_temp->release();
	

	Mat* mmu2;
	train_ensemble_get_test(&mmu2,0,dimensions,ensemble.mx2);//mmu2 = train_ensemble_get_test(1,dimensions,ensemble.mx2);
	Mat* me2_temp;
	train_ensemble_get_test(&me2_temp,1,dimensions,ensemble.mx2);//me2  = reshape(train_ensemble_get_test(2,dimensions,ensemble.mx2),K,L);
	//me2->reshape(K,L);
	Mat me2(K,L,CV_64F);	
	//Mat me = me_temp->reshape(K,L,CV_64F);
	for (int i = 0; i < K; i++){
		for (int j = 0; j < L; j++){
			me2.at<double>(i,j) = (*me2_temp).at<double>(0,K*j+i);
		}

	}
	me2_temp->release();
	Mat* mx2_temp;
	train_ensemble_get_test(&mx2_temp,2,dimensions,ensemble.mx2); //mx2  = reshape(train_ensemble_get_test(3,dimensions,ensemble.mx2),M,N);
	//mx2->reshape(M,N); // mx2->reshape(0,M);
	Mat mx2(M,N,CV_64F);		 
	for (int i = 0; i < M; i++){
		for (int j = 0; j < N; j++){
			mx2.at<double>(i,j) = (*mx2_temp).at<double>(0,M*j+i);
		}

	}
	mx2_temp->release();
 

	 
	//%=========================================================================

	//	%=========================================================================
	//	% Evaluate useful FFTs
	//	%=========================================================================
	 
	tic();
	Mat fft_mx_padded;  
	copyMakeBorder(mx, fft_mx_padded, 0,I - mx.rows,0,J - mx.cols , BORDER_CONSTANT, Scalar::all(0));	
	Mat fft_mx[] = {Mat_<double>(fft_mx_padded), Mat::zeros(fft_mx_padded.size(), CV_64F)}; Mat complexI;
	merge(fft_mx, 2, complexI); 
	tictic_clear();	
	tictic();dft(complexI, complexI);    tictic();     // fft_mx =fft2(mx,I,J);         -- checked		
	split(complexI, fft_mx);  
		 	 	
 	Mat fft_mx2_padded;         // checked
	copyMakeBorder(mx2, fft_mx2_padded, 0,I - mx2.rows,0,J - mx2.cols , BORDER_CONSTANT, Scalar::all(0));
	Mat fft_mx2[] = {Mat_<double>(fft_mx2_padded), Mat::zeros(fft_mx2_padded.size(), CV_64F)}; Mat complexI2;
	merge(fft_mx2, 2, complexI2); 
	tictic();dft(complexI2, complexI2);tictic();         // fft_mx2=fft2(mx2,I,J);
	split(complexI2, fft_mx2);  
	//Mat fft_mx_real2 = fft_mx2[0]; 	Mat fft_mx_cmplx2 = fft_mx2[1];


	Mat fft_mx3_padded,mx3;     // checked 
	pow(mx,2,mx3);
	copyMakeBorder(mx3, fft_mx3_padded, 0,I - mx3.rows,0,J - mx3.cols , BORDER_CONSTANT, Scalar::all(0));
	Mat fft_mx3[] = {Mat_<double>(fft_mx3_padded), Mat::zeros(fft_mx3_padded.size(), CV_64F)}; Mat complexI3;
	merge(fft_mx3, 2, complexI3);         
	tictic();dft(complexI3, complexI3); tictic();        // fft_mx3=fft2(mx.^2,I,J);
	split(complexI3, fft_mx3);  
	//Mat fft_mx_real3 = fft_mx3[0]; 	Mat fft_mx_cmplx3 = fft_mx3[1];

	// fft_me
	Mat fft_me_padded;  
	copyMakeBorder(me, fft_me_padded, 0,I - me.rows,0,J - me.cols , BORDER_CONSTANT, Scalar::all(0));
	Mat fft_me[] = {Mat_<double>(fft_me_padded), Mat::zeros(fft_me_padded.size(), CV_64F)}; Mat complexIe1;
	merge(fft_me, 2, complexIe1);         
	tictic();dft(complexIe1, complexIe1);tictic();         // fft_me =fft2(me,I,J);
	split(complexIe1, fft_me);  
	
	// fft_me2  - checked
	Mat fft_me2_padded;  
	copyMakeBorder(me2, fft_me2_padded, 0,I - me2.rows,0,J - me2.cols , BORDER_CONSTANT, Scalar::all(0));
	Mat fft_me2[] = {Mat_<double>(fft_me2_padded), Mat::zeros(fft_me2_padded.size(), CV_64F)}; Mat complexIe2;
	merge(fft_me2, 2, complexIe2);         
	tictic();dft(complexIe2, complexIe2);tictic();         // fft_me2=fft2(me2,I,J);
	split(complexIe2, fft_me2);  
	 
	// fft_me3
	Mat fft_me3_padded,me3; 
	pow(me,2,me3);
	copyMakeBorder(me3, fft_me3_padded, 0,I - me3.rows,0,J - me3.cols , BORDER_CONSTANT, Scalar::all(0));
	Mat fft_me3[] = {Mat_<double>(fft_me3_padded), Mat::zeros(fft_me3_padded.size(), CV_64F)}; Mat complexIe3;
	merge(fft_me3, 2, complexIe3);         
	tictic();dft(complexIe3, complexIe3); tictic();        // fft_me3=fft2(me.^2,I,J);
	split(complexIe3, fft_me3);  

	// fft_Dp
	Mat fft_Dp_padded;  
	copyMakeBorder(Dp, fft_Dp_padded, 0,I - Dp.rows,0,J - Dp.cols , BORDER_CONSTANT, Scalar::all(0));
	Mat fft_Dp[] = {Mat_<double>(fft_Dp_padded), Mat::zeros(fft_Dp_padded.size(), CV_64F)}; Mat complexIDp;
	merge(fft_Dp, 2, complexIDp);         
	tictic();dft(complexIDp, complexIDp);tictic();         // fft_Dp =fft2(Dp,I,J);
	split(complexIDp, fft_Dp);  


	toc("\ndft",2);
	toctoc("\ndft_only",4);	tictic_clear();
	tic();
	// mD=real(ifft2(fft_mx.*fft_me));   ------- checked
	Mat mD_fft[] = {Mat::zeros(fft_me_padded.size(), CV_64F), Mat::zeros(fft_me_padded.size(), CV_64F)};	
	mD_fft[0] = fft_me[0].mul(fft_mx[0]) - fft_me[1].mul(fft_mx[1]);
	mD_fft[1] = fft_me[0].mul(fft_mx[1]) + fft_me[1].mul(fft_mx[0]);
	Mat mD_fft_complex,mD;
	merge(mD_fft, 2, mD_fft_complex); 
	tictic();idft(mD_fft_complex,mD_fft_complex,DFT_SCALE);tictic();
	split(mD_fft_complex, mD_fft);  
	mD = mD_fft[0];






	//fft_err=fft2(Dp.*(D-mD-mmu),I,J);
	Mat tempD = D-mD- Mat::ones(fft_me_padded.size(), CV_64F)*mmu->at<double>(0,0);
	Mat err = Dp.mul( D-mD- Mat::ones(fft_me_padded.size(), CV_64F)*mmu->at<double>(0,0)  );

	// mexPrintf("\n Dp\n ");
	// for (int i = 0; i < I; i++)
		// {
		// for (int j = 0; j < J; j++)
			// {
			// mexPrintf("%f ",Dp.at<double>(i,j) );
			// }
		// mexPrintf("\n ");
		// }
	// mexPrintf("\nCPU");

	// mexPrintf("\n D\n ");
	// for (int i = 0; i < I; i++)
		// {
		// for (int j = 0; j < J; j++)
			// {
			// mexPrintf("%f ",D.at<double>(i,j) );
			// }
		// mexPrintf("\n ");
		// }
	// mexPrintf("\nCPU");

	// mexPrintf("\n mD\n ");
	// for (int i = 0; i < I; i++)
		// {
		// for (int j = 0; j < J; j++)
			// {
			// mexPrintf("%f ",mD.at<double>(i,j) );
			// }
		// mexPrintf("\n ");
		// }
	// mexPrintf("\nCPU");



	Mat err_padded;  
	copyMakeBorder(err, err_padded, 0,I - err.rows,0,J - err.cols , BORDER_CONSTANT, Scalar::all(0));
	Mat fft_err[] = {Mat_<double>(err_padded), Mat::zeros(err_padded.size(), CV_64F)}; Mat complexIerr;
	merge(fft_err, 2, complexIerr);
	tic();
	dft(complexIerr, complexIerr);         // fft_Dp =fft2(Dp,I,J);
	toc("\ndft_1",5);
	split(complexIerr, fft_err);  

	// mexPrintf("\n err\n ");
	// for (int i = 0; i < I; i++)
	// 	{
	// 	for (int j = 0; j < J; j++)
	// 		{
	// 		mexPrintf("%f ",fft_err[0].at<double>(i,j) );
	// 		}
	// 	mexPrintf("\n ");
	// 	}
	// mexPrintf("\nCPU");

	// mexPrintf("\n %f\n ",mmu->at<double>(0,0)); 
	
	//=========================================================================

	//=========================================================================
	// Evaluate the reconstruction error
	//=========================================================================
	int	data_points= (int) sum(Dp)[0];
	(*datapoints)  = data_points;
	//mexPrintf("%d ",data_points);
	Mat tempDmDmmu;
	pow(D-mD- Mat::ones(fft_me_padded.size(), CV_64F)*mmu->at<double>(0,0),2,tempDmDmmu);

	// START FROM CODING tempfft. fft_me2 are arrays of MAT !!!
	//Mat tempfft = fft_me2.mul(fft_mx2)-fft_me3.mul(fft_mx3);
	 

	Mat term1[] = {Mat::zeros(fft_me_padded.size(), CV_64F), Mat::zeros(fft_me_padded.size(), CV_64F)};	
	Mat term2[] = {Mat::zeros(fft_me_padded.size(), CV_64F), Mat::zeros(fft_me_padded.size(), CV_64F)};	
	Mat term[] = {Mat::zeros(fft_me_padded.size(), CV_64F), Mat::zeros(fft_me_padded.size(), CV_64F)};	

	term1[0] = fft_me2[0].mul(fft_mx2[0]) - fft_me2[1].mul(fft_mx2[1]);
	term1[1] = fft_me2[0].mul(fft_mx2[1]) + fft_me2[1].mul(fft_mx2[0]);

	term2[0] = fft_me3[0].mul(fft_mx3[0]) - fft_me3[1].mul(fft_mx3[1]);
	term2[1] = fft_me3[0].mul(fft_mx3[1]) + fft_me3[1].mul(fft_mx3[0]);

	term[0] = term1[0] - term2[0];
	term[1] = term1[1] - term2[1];

	
	Mat term_complex,real_ifft_term;
	merge(term, 2, term_complex); 
	tictic();idft(term_complex,term_complex,DFT_SCALE);tictic();
	split(term_complex, term);  
	real_ifft_term = term[0];

	// error=sum(sum(Dp.*((D-mD-mmu).^2+real(ifft2(fft_me2.*fft_mx2-fft_me3.*fft_mx3)))))+data_points*(mmu2-mmu^2);
	float error_term = sum(Dp.mul(tempDmDmmu + real_ifft_term))[0] + data_points*(mmu2->at<double>(0,0) - (mmu->at<double>(0,0))*(mmu->at<double>(0,0))) ; 
	(*rerror) = error_term;
	mmu->release();
	mmu2->release();
	//=========================================================================

	//=========================================================================
	//Evaluate the optimal distributions for each parameter
	//=========================================================================
	float mu1 = sum(Dp.mul(D-mD))[0];
	int mu2 = data_points;

	
	//e1=real(ifft2(fft_err.*conj(fft_mx)));    - checked
	Mat e1_fft[] = {Mat::zeros(fft_me_padded.size(), CV_64F), Mat::zeros(fft_me_padded.size(), CV_64F)};	
	e1_fft[0] = fft_err[0].mul(fft_mx[0]) + fft_err[1].mul(fft_mx[1]);
	e1_fft[1] = -fft_err[0].mul(fft_mx[1]) + fft_err[1].mul(fft_mx[0]);	
	Mat e1_complex,e1;
	merge(e1_fft, 2, e1_complex); 
	tictic();idft(e1_complex,e1_complex,DFT_SCALE);tictic();
	split(e1_complex, e1_fft);  
	e1 = e1_fft[0]; 

	//corr=real(ifft2(fft_Dp.*conj(fft_mx3)));  - checked
	Mat corr_fft[] = {Mat::zeros(fft_me_padded.size(), CV_64F), Mat::zeros(fft_me_padded.size(), CV_64F)};	
	corr_fft[0] = fft_Dp[0].mul(fft_mx3[0]) + fft_Dp[1].mul(fft_mx3[1]);
	corr_fft[1] = -fft_Dp[0].mul(fft_mx3[1]) + fft_Dp[1].mul(fft_mx3[0]);
	Mat corr_complex,corr;
	merge(corr_fft, 2, corr_complex); 
	tictic();idft(corr_complex,corr_complex,DFT_SCALE);tictic();
	split(corr_complex, corr_fft);  
	corr = corr_fft[0];



	// e1(1:K,1:L)=e1(1:K,1:L)+me.*corr(1:K,1:L);
	for (int i = 0; i < K ; i++)
		for (int j  = 0 ; j < L ; j++)
			e1.at<double>(i,j) = e1.at<double>(i,j) + me.at<double>(i,j)*corr.at<double>(i,j);

	

	//e2=real(ifft2(fft_Dp.*conj(fft_mx2)));
	Mat e2_fft[] = {Mat::zeros(fft_me_padded.size(), CV_64F), Mat::zeros(fft_me_padded.size(), CV_64F)};	
	e2_fft[0] = fft_Dp[0].mul(fft_mx2[0]) + fft_Dp[1].mul(fft_mx2[1]);
	e2_fft[1] = -fft_Dp[0].mul(fft_mx2[1]) + fft_Dp[1].mul(fft_mx2[0]);	
	Mat e2_complex,e2;
	merge(e2_fft, 2, e2_complex); 
	tictic();idft(e2_complex,e2_complex,DFT_SCALE);tictic();
	split(e2_complex, e2_fft);  
	e2 = e2_fft[0];

	//x1=real(ifft2(fft_err.*conj(fft_me)));
	Mat x1_fft[] = {Mat::zeros(fft_me_padded.size(), CV_64F), Mat::zeros(fft_me_padded.size(), CV_64F)};	
	x1_fft[0] = fft_err[0].mul(fft_me[0]) + fft_err[1].mul(fft_me[1]);
	x1_fft[1] = -fft_err[0].mul(fft_me[1]) + fft_err[1].mul(fft_me[0]);	
	Mat x1_complex,x1;
	merge(x1_fft, 2, x1_complex); 
	tictic();idft(x1_complex,x1_complex,DFT_SCALE);tictic();
	split(x1_complex, x1_fft);  
	x1 = x1_fft[0];

	//corr=real(ifft2(fft_Dp.*conj(fft_me3)));
	Mat corr2_fft[] = {Mat::zeros(fft_me_padded.size(), CV_64F), Mat::zeros(fft_me_padded.size(), CV_64F)};	
	corr2_fft[0] = fft_Dp[0].mul(fft_me3[0]) + fft_Dp[1].mul(fft_me3[1]);
	corr2_fft[1] = -fft_Dp[0].mul(fft_me3[1]) + fft_Dp[1].mul(fft_me3[0]);
	Mat corr2_complex;
	merge(corr2_fft, 2, corr2_complex); 
	tictic();idft(corr2_complex,corr2_complex,DFT_SCALE);tictic();
	split(corr2_complex, corr2_fft);  
	corr = corr2_fft[0];


	//x1(1:M,1:N)=x1(1:M,1:N)+mx.*corr(1:M,1:N);
	for (int i = 0; i < M ; i++)
		for (int j  = 0 ; j < N ; j++)
			x1.at<double>(i,j) =x1.at<double>(i,j) + mx.at<double>(i,j)*corr.at<double>(i,j);
	
	//x2=real(ifft2(fft_Dp.*conj(fft_me2)));
	Mat x2_fft[] = {Mat::zeros(fft_me_padded.size(), CV_64F), Mat::zeros(fft_me_padded.size(), CV_64F)};	
	x2_fft[0] = fft_Dp[0].mul(fft_me2[0]) + fft_Dp[1].mul(fft_me2[1]);
	x2_fft[1] = -fft_Dp[0].mul(fft_me2[1]) + fft_Dp[1].mul(fft_me2[0]);	
	Mat x2_complex,x2;
	merge(x2_fft, 2, x2_complex); 
	tictic();idft(x2_complex,x2_complex,DFT_SCALE);tictic();
	split(x2_complex, x2_fft);  
	x2 = x2_fft[0];

	toc("\nidft",3);
	toctoc("\nidft_only",4);	
// 	fft_time = (double)getTickCount() - fft_time;	
// 	printf(" sss -------------ffT_cpu Time : %.4f\n ", fft_time/cv::getTickFrequency());



	 

	//=========================================================================

	//=========================================================================
	//Return the optimal distributions
	//=========================================================================
	//	x1=[mu1 reshape(e1(1:K,1:L),1,K*L) reshape(x1(1:M,1:N),1,M*N)];
	dx1->at<double>(0,0) = mu1;
	for (int i = 1;i < K*L+1 ; i++)
		dx1->at<double>(0,i)  =e1.at<double>( (i-1)%K, (int)floor((float)(i-1)/K ));   
	for (int i = K*L+1; i < M*N + K*L + 1 ; i++)
		dx1->at<double>(0,i)  =x1.at<double>( (i-1 -K*L)%M, (int)floor((float)(i-1- K*L)/M )); 		

	//x2=[mu2 reshape(e2(1:K,1:L),1,K*L) reshape(x2(1:M,1:N),1,M*N)];
	dx2->at<double>(0,0) = mu2;
	for (int i = 1;i < K*L+1 ; i++)
		dx2->at<double>(0,i)  =e2.at<double>( (i-1)%K, (int)floor((float)(i-1)/K ));
	for (int i = K*L+1; i < M*N + K*L + 1 ; i++)
		dx2->at<double>(0,i)  =x2.at<double>( (i-1 -K*L)%M, (int)floor((float)(i-1- K*L)/M ));
	
	//=========================================================================
	
	// for (int k = 0; k < dx1->cols; k++){
	// 	mexPrintf("%d. %f %f\n ",k,dx1->at<double>(0,k),dx2->at<double>(0,k));
	// }
	// mexPrintf("\n");
	// mexPrintf("data_points %d error %f\n ",*datapoints,*rerror);
	// mexPrintf("cpu\n");
}




